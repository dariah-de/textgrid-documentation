# TextGridLab Troubleshooting (old)

last modified on Dez 14, 2023

## Windows

### Anmeldung schlägt fehl, die Meldung "java.lang.StackOverflowError" erscheint (Windows 10, TextGridLab \< 3.1)

Windows 10 Nutzer müssen in der Datei textgridlab.ini, zu finden in dem entpackten Ordner, in dem auch die textgridlab.exe liegt, die Zeile

    -Dorg.eclipse.swt.browser.IEVersion=11

ändern in

    #-Dorg.eclipse.swt.browser.IEVersion=11

Starten sie nun das TextGridLab.

### TextGridLab started nicht: es ist ein Fehler aufgetren

Bitte kopieren Sie den TextGridLab Ordner nicht nach c:\Programme. TextGridLab muss in seinen Ordner schreiben können und wenn dieser Ordner nach Programme verschoben wird, verbietet Windows den Zugriff. Bitte den Ordner an eine Stelle des Benutzerverzeichnises kopieren und TextGridLab von dort starten.

### Probleme beim Entpacken des TextGridLab (nur Windows XP)

Wenn man unter Windows XP mit dem eingebauten Entpacker das TextGridLab-ZIP extrahieren möchte und als Zielverzeichnis einen relativ langen Pfad wählt (`C:\Dokumente und Einstellungen\Maximilian Mustermann\Downloads\TextGridLab-2.0.4-win32`), kann es zu merkwürdigen Fehlern kommen: Der Extrahierassistent fragt
nach einem Passwort, und beim Entpacken per Drag&Drop fehlen einige Dateien (z.B. textgridlab.exe). Es gibt zwei alternative Lösungen:

- einen besseren Entpacker benutzen, z.B. [7-Zip](http://www.7-zip.de/)
- in einen kürzeren Pfad entpacken, z.B. `C:\TEMP`

### Erweiterte Unicode-Zeichen werden nicht korrekt angezeigt (Windows XP)

Unicode-Zeichen jenseits der BMP (d.h., Codepoints über 0xFFFF wie etwa Notensymbole oder gotische Schriftzeichen) werden mit sogenannten *Surrogatpaaren* codiert, d.h. durch zwei aufeinanderfolgende Sonderzeichen aus speziellen Unicode-Blöcken. Windows XP unterstützt die Anzeige dieser Zeichen, dies ist jedoch im Auslieferungszustand nicht eingeschaltet. (In neueren Windowsversionen sowie unter Mac OS X und Linux ist die Unterstützung vorhanden und aktiviert).

Wenn unter Windows XP statt bestimmter Unicodezeichen zwei Ersatzzeichen angezeigt werden, schalten Sie Surrogate-Unterstützung wie folgt ein:

1. Laden Sie die Datei [Unicode-Surrogate-Support.reg](attachments/18809102/18940134.reg) herunter.
2. Doppelklicken Sie die Datei und bestätigen Sie das Dialogfenster
3. Starten Sie Ihr Windows neu

(vgl. auch [den entsprechenden Wikipedia-Eintrag](http://de.wikipedia.org/wiki/Hilfe:UTF-8-Probleme#Warum_werden_bei_Unicode-Charakteren_.C3.BCber_U.2B10000_trotz_installierter_Schriften_zwei_Rechtecke_angezeigt.3F))
\[[TG-1940](https://develop.sub.uni-goettingen.de/jira/browse/TG-1940)\]

## Linux

### Der Login-Dialog behauptet, Sie seien offline (Ubuntu)

Infolge eines [Bugs in Ubuntu](https://bugs.launchpad.net/ubuntu/+source/ca-certificates-java/+bug/983302) ist das TextGridLab möglicherweise nicht in der Lage, eine sichere Verbindung zu unseren Servern aufzubauen. Die Folge ist, dass der Logindialog sich beschwert, [Sie seien offline](https://projects.gwdg.de/issues/15645).

Wenn dies der Fall ist, ist der Java-Zertifikatsspeicher auf Ihrem System defekt (die Datei `/etc/ssl/certs/java/cacerts` auf Ihrem System ist nur 32 bytes lang). Reparieren können Sie das, indem Sie ein Terminalfenster öffnen und die folgenden beiden Kommandos eingeben (Sie werden nach Ihrem Passwort für den Rechner gefragt):

    ``` syntaxhighlighter-pre
    sudo dpkg --purge --force-depends ca-certificates-java
    sudo apt-get install ca-certificates-java
    ```

Wenn Sie das TextGridLab danach starten, sollte es funktionieren.

## Mac OS

### Java-Versionschwierigkeiten

Das TextGridLab benötigt eine neuere Java-Version als Java 6. Leider scheint Oracle's „normales“ Java-Installationsprogramm für Mac OS Java so zu installieren, dass es *nur* von Browsern gefunden wird. Um Anwendungen von Drittanbietern wie das TextGridLab Java finden zu lassen, müssen Sie das [JDK installieren, die erweiterte Entwickler-Version von Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.md).

Weiterhin ist es nicht möglich, das TextGridLab vom MaxOS X Download-Ordner aus zu starten! Bitte verschieben Sie die TextGridLab.app in Ihren Programme-Ordner und starten Sie das TextGriLab erneut.

### Die GUI ist komplett eingefroren auf MacOS X

Dieses Problem betrifft nur TextGridLab älter als Version 3.3. Bitte in diesem Fall auf die neuste Version von der TextGrid Webseite aktualisieren, um das Problem zu beheben.

## Deploy a portable TextGridLab including Java

Sometimes it may be desireable to deploy the TextGridLab in a setting where there is no recent Java installed on the client computers – e.g., in a workshop situation. In this case it is possible to create a TextGridLab installation that includes its own JRE, e.g., on USB drives. To do so, put the JRE in a subdirectory named `jre` of the TextGridLab. The TextGridLab will automatically use this Java installation.
