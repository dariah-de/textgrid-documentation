# Interaction of the Publish Tool SADE with Other Components

last modified on Mai 03, 2016

Nach der Installation kann SADE mit "Publish to SADE" aus dem
Kontextmenü
des [Navigators](https://wiki.de.dariah.eu/pages/viewpage.action?pageId=40220346),
dem [XML-Editor](https://wiki.de.dariah.eu/display/TextGrid/XML-Editor) und
– sofern installiert – dem [XML-Editor oXygen](https://wiki.de.dariah.eu/pages/viewpage.action?pageId=40220597).
geöffnet werden.
