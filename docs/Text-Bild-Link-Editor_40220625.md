# Text-Bild-Link-Editor

last modified on Aug 03, 2015

Der Text-Bild-Link-Editor kann verwendet werden, um innerhalb des
TextGridLab Textsegmente mit Bildausschnitten zu verknüpfen. Eine
typische Anwendung ist, ein Faksimile und Transkriptionen zu verknüpfen,
aber diese Texte können auch während des Verknüpfungsprozesses erstellt
werden, was die Verwendung weiterer Werkzeuge wie Bildannotationen
erlaubt.

XML-Texte und Bilder können in den entsprechenden Sichten geöffnet
werden. Die entsprechenden Komponenten werden dann paarweise markiert
und der Verknüpfungsvorgang wird bestätigt. Die Ergebnisse können als
ein neues Objekt, das mit dem
Symbol ![](attachments/40220625/40436818.png "069-zeige-BildLinkEditorPerspektive.png")
versehene Text-Bild-Link-Objekt, gespeichert werden, das die
Verknüpfungsinformationen (die Text- und Bildkoordinaten, Pfad der
verwendeten XML- und Bild-Objekte) enthält. Sobald ein
Text-Bild-Link-Objekt gespeichert ist, können Sie durch einen
Doppelklick XML-Texte, Bilder und Verknüpfungen neu laden und mit der
Bearbeitung fortfahren.

- [Text-Bild-Link-Editor-Perspektive](Text-Bild-Link-Editor-Perspektive_40220630.md)
- [Bild-Sicht](Bild-Sicht_40220638.md)
- [XML-Editor-Sicht](XML-Editor-Sicht_40220653.md)
- [Bildvorschau-Sicht](Bildvorschau-Sicht_40220655.md)
- [Werkzeugkasten](Werkzeugkasten_40220657.md)
- [Interaktion des Text-Bild-Link-Editors mit anderen Komponenten](Interaktion-des-Text-Bild-Link-Editors-mit-anderen-Komponenten_40220673.md)
- [Text-Bild-Link-Editor verwenden](Text-Bild-Link-Editor-verwenden_40220659.md)

## Attachments

- [069-zeige-BildLinkEditorPerspektive.png](attachments/40220625/40436818.png)
(image/png)  
