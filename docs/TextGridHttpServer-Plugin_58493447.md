# TextGridHttpServer Plugin

last modified on Jul 18, 2017

Der TextGridHttpServer ermöglicht es, die persönlichen TextGrid Objekte
per HTTP abzurufen. Dies ist vor allem nützlich, wenn in XSLT zu HTML
Transformationen Quellen wie Bilder und JavaScript eingebunden werden,
die ebenfalls im TextGrid Repository liegen. Ohne das Plugin sind diese
Quellen nicht nutzbar, da der Webrowser die textgrid URIs nicht auflösen
kann.

## Installation

TextGrid Lab starten und auf Marketplace klicken. Nun zum Eintrag von
"TextGridHttpServer" scrollen und auf "Install" klicken, nach
akzeptieren der Lizenzbedingungen und Neustarten der TextGrid Lab
Umgebung steht das Plugin zur Verfügung.

### Verifizieren der Installation

Einen beliebigen Webbrowser öffnen und die URL
[http://localhost:9090](http://localhost:9090) eingeben. Folgende Seite
öffnet sich:

![](https://raw.githubusercontent.com/Hannah-Arendt-Project/TextGridHttpServerPlugin/master/gh-imgs/indexpage.png)

Hiermit ist die Installation abgeschlossen. Sollte die Seite nicht
erscheinen, bitte TextGrid Lab ein weiteres mal neu starten. Sollte die
Windows Firewall nachfragen, so bitte die Verbindung zulassen. Der HTTP
Server lauscht nur auf Anfragen von der lokalen Maschine und stellt
daher kein Sicherheitsproblem dar.

## Verwendung

Im Projekt müssen die Bilder und Javascript Dateien in das TG-Repository
eingespielt sein. Jetzt können die XSLT/HTML Dateien des Projekts so
bearbeitet werden, dass sie auf den TextGridHttpServer zugreifen. Dazu
muss man sich die TextGrid URI des gewünschten Objekts in die
Zwischenablage kopieren: Mit der rechten Maustaste auf das Objekt gehen
und "URI kopieren" auswählen. In den XSLT/HTML Dateien gibt man nun
folgende Quellen an:

    <script type="text/javascript" src="http://localhost:9090/textgrid:323ab.0"></script>

Die Adresse <http://localhost:9090> bezieht sich auf den lokalen HTTP
Server und muss immer vorangestellt werden. Hinter dem / wird die
TextGrid Objekt URI angegeben. Diese URI hat man sich im Schritt vorher
in die Zwischenablage kopiert.

Das gleiche Prinzip gilt auch für Bilder. Anstatt z.B.

    <img src="logo.jpg"/> 

schreibt man

    <img src="http://localhost:9090/textgrid:323ac.0"/> 

in die Datei. textgrid:323ac.0 ist die URI der Datei logo.jpg, die im
TG-Repository gespeichert ist.

Jetzt kann man die XSLT Transformation über Oxygen mit XML → Apply
Transformation scenarios ausführen bzw. eine HTML Datei in internen
Webbrowser öffnen.

## Weitere Verwendung

Das Plugin kann auch für die SADE Umgebung nützlich sein. Solange
TextGrid Lab gestartet ist, kann auf beliebige (unveröffentlichte)
Objekte zugegriffen werden. Die Logininformationen müssen dabei nicht
fest in SADE eingegeben werden.

## Weitere informationen und Quellcode

Kann auf der Github Seite gefunden werden:
[https://github.com/Hannah-Arendt-Project/TextGridHttpServerPlugin](https://github.com/Hannah-Arendt-Project/TextGridHttpServerPlugin)
