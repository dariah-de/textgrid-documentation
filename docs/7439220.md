# Project & User Management Perspective

Die Perspektive ist in zwei Sichten unterteilt.
Der [Navigator](40220331.md) befindet sich auf der linken und
die [Benutzerverwaltung-Sicht](Benutzerverwaltung-Sicht_40220383.md) auf
der rechten Seite.

![](attachments/40220376/40436714.png)

|                                     |
|-------------------------------------|
| **Projekt- und Benutzerverwaltung** |

## Attachments

- [pum-navigatorlang.png](attachments/7439220/9240670.png) (image/png)
- [pum-navigatorlang.png](attachments/7439220/7766088.png) (image/png)  
