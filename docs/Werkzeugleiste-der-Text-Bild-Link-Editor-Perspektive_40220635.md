# Werkzeugleiste der Text-Bild-Link-Editor-Perspektive

last modified on Aug 20, 2015

Wenn die [Bild-Sicht](Bild-Sicht_40220638.md) aktiviert ist, stehen
zusätzliche Elemente in der Werkzeugleiste zur Verfügung:

- Klicken Sie ![](attachments/40220635/40436828.png "delete.png") , um
    aktive Markierungen zu löschen
- Klicken
    Sie ![](attachments/40220635/40436827.png "014-schliesse-Datei.png") ,
    um den Text-Bild-Link-Editor zurückzusetzen

Wenn auch eine Datei im [XML-Editor](XML-Editor_40220521.md) geöffnet
ist, werden weitere Schaltflächen in der Werkzeugleiste aktiviert:

- Klicken Sie ![](attachments/40220635/40436826.png "link.png") , um
    ein gewähltes Textsegment mit einer gewählten Markierung zu
    verknüpfen.
- Klicken Sie ![](attachments/40220635/40436825.png "unlink_obj.png")
    , um (eine) gewählte Verknüpfung(en) aufzuheben.
- Klicken Sie ![](attachments/40220635/40436824.png "save.png") (neben
    dem Verknüpfungssymbol), um das Text-Bild-Link-Objekt zu speichern.
- Klicken Sie ![](attachments/40220635/40436823.png "save-as.png") ,
    um das Text-Bild-Link-Objekt als... zu speichern.
- Klicken
    Sie ![](attachments/40220635/40436822.gif "152-Revisionen.gif") , um
    das Text-Bild-Link-Objekt als neue
    [Revision](https://wiki.de.dariah.eu/display/tgarchiv2/Revisionen)
    zu speichern.

## Attachments

- [152-Revisionen.gif](attachments/40220635/40436822.gif) (image/gif)  
- [save-as.png](attachments/40220635/40436823.png) (image/png)  
- [save.png](attachments/40220635/40436824.png) (image/png)  
- [unlink_obj.png](attachments/40220635/40436825.png) (image/png)  
- [link.png](attachments/40220635/40436826.png) (image/png)  
- [014-schliesse-Datei.png](attachments/40220635/40436827.png)
(image/png)  
- [delete.png](attachments/40220635/40436828.png) (image/png)
