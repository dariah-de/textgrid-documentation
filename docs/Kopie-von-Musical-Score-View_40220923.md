# Kopie von Musical Score View

Created on Jul 24, 2015

At least one perspective must be open to contain the Musical Score View.
As soon as an MEI Object has been created or opened, the view will
appear. In this area the musical scores that you can open from the
TextGrid Repository, or create and modify with the Palette and the
Outline View, will be displayed.

The encoded note text is displayed in a WYSIWYM View to display the
encoding properly. All changes in the document can be made in this
WYSIWYG-style. The Score View is primarily meant for the two-dimensional
display of the rendering, as drag & drop operations are disabled.
Elements in the note sheet should be modified with the assistance of the
Editor’s [Palette](Palette_7439859.md)  and the
[Outline](Outline-View-of-the-MEI-Score-Editor_7439872.md) and
[Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md) . Single
elements in the Score View as notes or rests can be selected and appear
in blue by left-clicking on them.  

More complex interventions in the source code of an MEI document will
require the [XML Editor](XML-Editor_7439318.md). To open the source
code of an MEI Object, please use its context menu in the
[Navigator](Navigator_7439188.md) and choose “Open with \> XML
Editor”.

![](attachments/40220923/40436982.png)

|                          |
|--------------------------|
| MEISE Musical Score View |

The context menu is enabled on the Score View, but direct user
manipulation via drag & drop is disabled. It offers the following
options:

- Undo ![](attachments/40220923/40436988.png) and
    Redo ![](attachments/40220923/40436987.png) changes .
- If single elements are selected in the Score View, they can be
    deleted with ![](attachments/40220923/40436985.gif).
- The user can duplicate preselected elements with "Clone"
    ![](attachments/40220923/40436983.png).
- "Run as \> Run Configurations…" opens a new window. There the user
    will be able to create new configurations for the launch and define
    the settings.
- You can validate your MEI document by clicking “Validate”.
- Use some Eclipse-specific options by selecting “Team”, “Compare
    With”, or “Replace With”.
- To associate an adaptor to your document, please select “Associate
    an Adaptor...”. An associated adaptor can translate the given
    documents to the TextGrid Baseline Encoding. In the new dialog box
    the selected file is presented with information about title,
    project, contributor, and creation date. You can mark the box below
    to make the assignment persistent when clicking “OK”. In the lower
    list you can select the adaptor by clicking on it. Here you can also
    receive additional information. To reverse your selection, you can
    mark the button under this list.
- To save your changes, click ![](attachments/40220923/40436984.gif)
    on the bottom of the context menu.

## Attachments

- [mei-musicalscoreview.png](attachments/40220923/40436982.png)
(image/png)  
- [action-clone.png](attachments/40220923/40436983.png) (image/png)  
- [016-speichere-Datei.gif](attachments/40220923/40436984.gif)
(image/gif)  
- [042-loesche-Auswahl.gif](attachments/40220923/40436985.gif)
(image/gif)  
- [eclipse-icons.zip](attachments/40220923/40436986.zip)
(application/zip)  
- [redo_edit.png](attachments/40220923/40436987.png) (image/png)  
- [undo_edit.png](attachments/40220923/40436988.png) (image/png)  
- [renderview.PNG](attachments/40220923/40436989.png) (image/png)  
