# Metadata

last modified on Jun 22, 2023

This page currently collects information on TextGrid's metadata scheme and concept.

## Overview

- The chapter [The Logical View](The-Logical-View_9013285.md) of the architecture overview document contains a high-level view on the metadata concept.
- [Metadata Cheatsheet illustrating which fields are where](attachments/metadata/cheatsheet.pdf)
- The User's Manual illustrates how to work with metadata from the TextGridLab, cf.:
    - The chapter on the [Metadata Editor](Metadata-Editor_7439270.md)
    - Description of the logical model: [TextGrid Objects](TextGrid-Objects.md)

## Mappings and specifications

- Here is a large [Excel sheet](attachments/metadata/uebersicht-ebenen-v2.xls) that contains all metadata fields including mappings to the metadata standards TEI header, Dublin Core and MODS.
- The canonical form for TextGrid's metadata is XML as specified by the schema at [https://textgrid.info/namespaces/metadata/core/2010](https://textgrid.info/namespaces/metadata/core/2010).
This schema, together with its components, is maintained in a git repository at [https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-metadata](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-metadata).
- TextGrid Metadata to TEI header:
    - The metadata editor contains an [XSLT stylesheet](http://dev.digital-humanities.de/source/xref/textgrid-trunk/lab/core/info.textgrid.lab.core.metadataeditor/resources/md2tei.xsl) that generates a TEI header for an object from a set of TextGrid metadata records. The input of the stylesheet is the concatenation of the metadata records that make up the path to the object (ask TG-search for that), i.e. the aggregations/editions/works/collections the object is contained in.

## TODO

- Generated schema doc – is this useful?
- What of the very detailed Metadatenprofil documentation needs to be ported (and in which form?)
    - Most of this information is in [uebersicht-ebenen_v2.xls](attachments/metadata/uebersicht-ebenen-v2.xls)

## Dateien

### Metadata Cheat Sheet

![Metadata-Cheatsheet](attachments/metadata/cheatsheet.png)

### Übersiche Ebenen v1

![Übersicht Ebenen](attachments/metadata/uebersicht-ebenen.xls)

### Übersicht Ebenen v2

![Übersiche Ebenen v2](attachments/metadata/uebersicht-ebenen-v2.xls)

## Attachments

- [Metadata-Cheatsheet.pdf](attachments/metadata/cheatsheet.pdf) (application/pdf)  
- [Uebersicht_Ebenen.xls](attachments/metadata/uebersicht-ebenen.xls) (application/vnd.ms-excel)  
- [Uebersicht_Ebenen_v2.xls](attachments/metadata/uebersicht-ebenen-v2.xls) (application/vnd.ms-excel)  
