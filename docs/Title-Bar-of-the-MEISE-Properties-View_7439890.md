# Title Bar of the MEISE Properties View

last modified on Mai 04, 2016

The Title Bar of the MEISE Properties View offers you several icons to
perform a range of operations:

- Click ![](attachments/7439890/8192047.png "Bildschirmfoto 2012-01-30 um 11.56.17.png") to
    pin this property view to the current selection
- Click **![](attachments/7439890/8192046.png "Bildschirmfoto 2012-01-30 um 11.56.41.png") ** to
    show all properties of the preselected element in the view
    (including the properties in the “Basic” category)
- Click** ![](attachments/7439890/8192044.png "Bildschirmfoto 2012-01-30 um 11.56.52.png")** to
    show advanced properties
- Click **![](attachments/7439890/8192045.png "Bildschirmfoto 2012-01-30 um 11.57.07.png")** to
    restore the default value of a selected property
- Click the white triangle to open the view menu. The view menu allows
    you to open a new Properties
    View ![](attachments/7439890/8192323.png "085-oeffne-InNeuenFenster.png"), 
    pin a property to a selection and  show categories and advanced
    properties to define the width of the two columns “Property” and
    “Value” after clicking “Configure Columns…”
- Click the remaining icons  to minimize and maximize the view

There is also a context menu in the title bar that is identical to all
view title bar context menus in Eclipse.

## Attachments

- [tree_mode.png](attachments/7439890/8192035.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.56.52.png](attachments/7439890/8192044.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.57.07.png](attachments/7439890/8192045.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.56.41.png](attachments/7439890/8192046.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.56.17.png](attachments/7439890/8192047.png) (image/png)  
- [086-oeffne-ImNeuenEditor.gif](attachments/7439890/8192322.gif)
(image/gif)  
- [085-oeffne-InNeuenFenster.png](attachments/7439890/8192323.png)
(image/png)  
