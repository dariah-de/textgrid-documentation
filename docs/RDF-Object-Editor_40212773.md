# RDF Object Editor

last modified on Mai 04, 2016

|                                    |                                                                                                                                            |
|------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | RDF Object Editor                                                                                                                          |
| Beschreibung                       | TextGridLab plugin for the RDF Object Editor                                                                                               |
| Logo                               | ![](attachments/40212773/44827117.gif)                                                                                                     |
| Lizenz                             | [http://www.gnu.org/licenses/lgpl-3.0.txt](http://www.gnu.org/licenses/lgpl-3.0.txt)                                                       |
| Systemanforderungen                |                                                                                                                                            |
| Lizenz                             | LGPL 3                                                                                                                                     |
| Sourcecode                         | [https://projects.gwdg.de/projects/blumenbachiana-textgridlab-plugin](https://projects.gwdg.de/projects/blumenbachiana-textgridlab-plugin) |
| Projekte, die das Plugin verwenden |                                                                                                                                            |
| Beispieldateien                    |                                                                                                                                            |
| Installationsanleitung             |                                                                                                                                            |
| Dokumentation                      |                                                                                                                                            |
| Screenshots                        |                                                                                                                                            |
| Tutorials                          |                                                                                                                                            |
| Entwickler                         |                                                                                                                                            |
| Mitentwickler                      |                                                                                                                                            |
| Homepage/Kontakt                   |                                                                                                                                            |
| Bugtracker                         |                                                                                                                                            |

## Attachments

- [meise_64.png](attachments/40212773/40436059.png) (image/png)  
- [digilib-logo-big.png](attachments/40212773/40436060.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/40212773/40436061.png)
(image/png)  
- [screenshotSADE.png](attachments/40212773/40436062.png) (image/png)  
- [rdf_flyer.64.gif](attachments/40212773/44827117.gif) (image/gif)  
