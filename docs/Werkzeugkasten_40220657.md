# Werkzeugkasten

last modified on Aug 20, 2015

Der Werkzeugkasten durch Klicken
von ![](attachments/40220657/40437235.png) in der Titelleiste der
Bild-Sicht geöffnet und geschlossen werden. Alle Funktionen des
Werkzeugkastens betreffen die Bild-Sicht.

- Lupenfunktion ![](attachments/40220657/40436845.png "Lense16.png") :
    Klicken und ziehen Sie, um den gewählten Bildschirmbereich
    vorübergehend zu vergrößern (die Größe kann mit der Strg-Taste
    angepasst werden)
- Heranzoomen ![](attachments/40220657/40436844.png "ZoomIn16.png") :
    Klicken des Desktops zoomt in und zentriert auf den Arbeitsbereich
    an der gewählten Position
- Herauszoomen ![](attachments/40220657/40436843.png "ZoomOut16.png")
    zoomt heraus
- Die
    Verschiebefunktion ![](attachments/40220657/40436842.png "fourway.png")
    erlaubt Ihnen, den Arbeitsbereichs zu verschieben
- Rechteck-Modus ![](attachments/40220657/40436851.png "Select16.png") :
    Klicken und ziehen Sie, um einen rechteckigen Bereich zu markieren
- Polygon-Modus ![](attachments/40220657/40436850.png "PolySelect16_6.png") :
    Klicken und ziehen Sie, um einen Bereich in Polygonform zu
    markieren. Die Markierung kann durch Doppelklicken vervollständigt
    werden
- Das Klicken
    von ![](attachments/40220657/40436849.png "Pencil16.png") wechselt
    zum [Andocklinien-Modus](Funktionen-der-Bild-Sicht_40220641.md)
- "Farbe wählen" verändert die Vordergrund- (= gewählte, aktive
    Markierung) oder Hintergrund- (= inaktive Markierung) Farbe der
    Markierungen. Beide können durch Klicken der großen farbigen
    Quadrate verändert werden
- "Farben tauschen"
    ![](attachments/40220657/40436848.png "switch.png") tauscht die
    Vordergrund- und Hintergrund-Farbe aus
- "Farbe zurücksetzen": Durch Klicken des kleinen Quadrate-Paars
    werden die Vordergrund- und Hintergrund-Farbe auf die Standardfarben
    zurückgesetzt (schwarz und weiß)
- Das Klicken des
    Rasters ![](attachments/40220657/40436847.png "Grid16.png")
    aktiviert das Linienraster in der Bild-Sicht, um die Ausrichtung der
    Markierungen zu erleichtern

![](attachments/40220657/40436846.png "tble-toolkit-window.png")

|                    |
|--------------------|
| **Werkzeugkasten** |

## Attachments

- [fourway.png](attachments/40220657/40436842.png) (image/png)  
- [ZoomOut16.png](attachments/40220657/40436843.png) (image/png)  
- [ZoomIn16.png](attachments/40220657/40436844.png) (image/png)  
- [Lense16.png](attachments/40220657/40436845.png) (image/png)  
- [tble-toolkit-window.png](attachments/40220657/40436846.png)
(image/png)  
- [Grid16.png](attachments/40220657/40436847.png) (image/png)  
- [switch.png](attachments/40220657/40436848.png) (image/png)  
- [Pencil16.png](attachments/40220657/40436849.png) (image/png)
- [PolySelect16_6.png](attachments/40220657/40436850.png) (image/png)  
- [Select16.png](attachments/40220657/40436851.png) (image/png)  
- [image2015-8-3 9:40:27.png](attachments/40220657/40437235.png) (image/png)  
