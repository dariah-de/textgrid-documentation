# Metadaten-Editor-Sicht

last modified on Aug 19, 2015

Einige Felder im Metadaten-Editor sind generisch, während andere
spezifisch für bestimmte Objekte sind: [Elemente, Editionen, Werke und Kollektionen](TextGrid-Objekte.md). Abhängig vom gewählten
Objekt variieren die dargestellten Felder. Ein paar Felder werden
zugeklappt angezeigt. Klicken Sie das schwarze Dreieck vor dem
Elementnamen, um das Feld zu öffnen. Einige Felder sind wiederholbar.
Sie können durch Klicken der Pfeile neben der Zuweisung der
verschiedenen Gruppen geöffnet werden. Um ein weiteres Feld
hinzuzufügen, klicken Sie die entsprechende Schaltfläche “Hinzufügen”.
Einträge, die nicht benötigt werden, können durch Klicken der
entsprechenden Schaltfläche “Entfernen” entfernt werden.

Jedes Eingabefeld des Metadaten-Editors gehört zu einer der folgenden
Gruppen: Identifikatoren, Schlüsselwörter, Daten, Personen oder Agenten.
Die Gruppen gehören nicht zu irgendeinem individuellen angezeigten Feld,
sondern die Felder der Elemente, die zu einer dieser Gruppen gehören,
haben dieselbe Struktur.

Identifikatoren sind von einem speziellen Typ und haben einen
charakteristischen Wert. In ihre Eingabefelder können Werte ohne
Einschränkungen eingefügt werden. Der Typ kann beispielsweise eine ISBN,
ISSN, URL oder Kalliope (siehe RNA: "Regeln zur Erschließung von
Nachlässen und Autographen") sein. Der Wert ist ein spezifischer
Zifferncode oder Adresse des Objekts.

Schlüsselwörter haben ein Attribut “id” (= identifizierend) und einen
Wert. Das Attribut “id” ist eine Identifikation in einem kontrollierten
Vokabular, beispielsweise *pnd:13414032*. Der Wert ist die Information,
die angezeigt wird.

Datumsangaben haben das Attribut “date” oder die Attribute “notBefore”
und “notAfter”. Das Attribut “date” wird für die Information genutzt,
wann ein Ereignis eingetreten ist. Zusätzlich kann in einem zweiten Feld
die Datumsangabe in ihrer ursprünglichen Form im Dokument angegeben
werden. Klicken Sie “Zu Zeitspanne wechseln”, um die Attribute
“notBefore” und “notAfter” für den Anfang und das Ende der angegebenen
Ära nutzen zu können. Klicken Sie die Schaltfläche “Zu ungefährem Datum
wechseln”, um zurückzukehren. Das Pflichtfeld “Ungefähres
Gregorianisches Datum” muss zumindest eine vierstellige Jahresangabe
beinhalten. Mögliche Werte für das Feld “Ungefähres Gregorianisches
Datum” sind:

- Jahr (vierstellig)-Monat (zweistellig)-Tag (zweistellig),
    z. B. 2005-06-17
- Jahr (vierstellig)-Monat (zweistellig), z. B. 2005-06
- Jahr (vierstellig), z. B. 2005

Felder vom Typ person haben die Attribute “id” und “corporate body”.
Verwenden Sie “id” für einen identifizierenden URI, beispielsweise PND
oder FOAF. Das Attribut “Is corporate body?” kann wahlweise verwendet
werden. Der Wert, der in das Feld eingefügt werden muss, ist der Name
der Person. Hierfür steht Ihnen eine Autovervollständigen-Funktion zur
Verfügung. Wenn Sie den Namen einer Person eintragen, wird TextGrid
Ihnen passende PND-Einträge vorschlagen. Wählen Sie einen davon und die
PND-Nummer wird automatisch hinzugefügt.

Agenten werden durch ihren Namen als Wert und die Attribute “role” und
“id” spezifiziert. “role” ist dabei ein Dublin Core Relator Term, den
Sie im Aufklappmenü wählen können. Das Attribut “id” wird für einen
identifizierenden URI wie PND oder FOAF verwendet.

![](attachments/40220463/40436731.png)

- [Generische Metadaten](Generische-Metadaten_40220466.md)
- [Element-spezifische Metadaten](Element-spezifische-Metadaten_40220468.md)
- [Werk-spezifische Metadaten](Werk-spezifische-Metadaten_40220471.md)
- [Edition-spezifische Metadaten](Edition-spezifische-Metadaten_40220473.md)
- [Kollektion-spezifische Metadaten](Kollektion-spezifische-Metadaten_40220475.md)

## Attachments

- [Metadata-Cheatsheet.png](attachments/40220463/40436731.png)
(image/png)  
- [Metadata-Cheatsheet.pdf](attachments/40220463/40436732.pdf)
(application/pdf)  
