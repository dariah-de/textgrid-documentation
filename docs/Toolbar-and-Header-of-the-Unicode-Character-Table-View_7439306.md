# Toolbar and Header of the Unicode Character Table View

last modified on Mai 03, 2016

Die Werkzeugleiste in der Kopfzeile bietet eine breite Auswahl aller
Unicode-Zeichen an, sortiert nach Zuordnung zu Blöcken oder Schriften.
Standardmäßig fasst Unicode Gruppen von Zeichen in Blöcke zusammen. Eine
Schriftart in Unicode ist eine Sammlung von geschriebenen Zeichen, die
verwendet werden, um Textinformationen in einem oder mehreren
Schriftsystem wiederzugeben. Für weitere Informationen, siehe

[*http://unicode.org/*](http://unicode.org/)

Um die Blockweite zu wählen, klicken
Sie ![](https://dev2.dariah.eu/wiki/download/attachments/7439306/font-x-generic.png?version=1&modificationDate=1331051472835&api=v2) für
den “Blockmodus umschalten” in der Werkzeugleiste und wählen Sie den
gewünschten Eintrag im Aufklappmenü rechts davon in der Werkzeugleiste.
Um einen Schriftbereich zu wählen, klicken
Sie ![](https://dev2.dariah.eu/wiki/download/attachments/7439306/format-justify-fill.png?version=1&modificationDate=1331051518880&api=v2) “Schriftmodus
umschalten”. Wenn Windows XP als Betriebssystem verwenden, können
Probleme bei der Darstellung der Zeichen auftreten.

Um zu einem Zeichen an einem bekannten Unicode Codepoint zu springen,
geben Sie den Codepoint im Eingabefeld ein. Drücken Sie dann die
Eingabetaste. Als zusätzliche Besonderheit können Sie ein
Unicode-Zeichen im Textfeld eingeben, die Sicht springt dann zu dessen
Codepoint, was hilfreich für die Identifikation eines Symbols oder den
Zugriff auf weiterführende Informationen darüber ist.

Verwenden
Sie ![](https://dev2.dariah.eu/wiki/download/attachments/7439306/character-sum.png?version=1&modificationDate=1331051565177&api=v2) für
Mathematische Zeichen
und ![](https://dev2.dariah.eu/wiki/download/attachments/7439306/character-note.png?version=1&modificationDate=1331051590547&api=v2) für
Musikologische Zeichen. Nachdem Sie das Sichtmenü der
Unicode-Zeichen-Tabelle-Sicht geöffnet haben (indem Sie das weiße nach
unten gerichtete Dreieck auf der rechten Seite klicken), können Sie die
mathematischen und musikologischen Zeichen über ein Aufklappmenü wählen.
Von hier aus können Sie auch den
“[Zeichensatz-Anpassung-Editor](Zeichensatz-Anpassung-Editor_40220687.md)”
zum Erstellen und Verändern Nutzer-definierter Zeichensätze
initialisieren.

## Attachments

- [font-x-generic.png](attachments/7439306/7766110.png) (image/png)  
- [format-justify-fill.png](attachments/7439306/7766111.png) (image/png)  
- [character-sum.png](attachments/7439306/7766112.png) (image/png)  
- [character-note.png](attachments/7439306/7766113.png) (image/png)  
- [uct-charseteditor-wizard.png](attachments/7439306/7766114.png)
(image/png)  
