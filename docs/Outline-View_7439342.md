# Outline View

last modified on Mai 04, 2016

Die Gliederung-Sicht erleichtert die Navigation in großen
XML-Dokumenten, indem sie einen Überblick über die Baumstruktur des
Dokuments gibt.

![](attachments/40220561/40436795.png "xml-outline-view.png")

|                      |
|----------------------|
| **Gliederung-Sicht** |

- [Title Bar of the Outline View](Title-Bar-of-the-Outline-View_7439345.md)
- [Context Menu of the Outline View](Context-Menu-of-the-Outline-View_7439347.md)

## Attachments

- [xml-outline-view.png](attachments/7439342/7766140.png) (image/png)  
