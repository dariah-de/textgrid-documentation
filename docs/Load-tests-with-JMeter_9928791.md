# Load tests with JMeter

last modified on Aug 19, 2014

Apache [JMeter](http://jmeter.apache.org/) is an Apache project that can
be used as a load testing tool for analyzing and measuring the
performance of a variety of services.

This tool is used to test the backend services of Textgrid.

To have real parallelism, the tool can cluster the tests on multiple
nodes.

Test scenarios are available for:

- TG-crud
- TG-auth
- TG-aggregation
- TG-search
- eXist

on [subversion](https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/tests/loadtests/).
