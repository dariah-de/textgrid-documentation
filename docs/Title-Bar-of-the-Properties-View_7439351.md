# Title Bar of the Properties View

last modified on Mai 04, 2016

Die Titelleiste der Eigenschaften-Sicht bietet Ihnen verschiedene
Symbole, um eine Reihe von Operationen durchzuführen:

- Klicken Sie ![](attachments/40220569/40436811.png "pin.png") diese
    Eigenschaften-Sicht an die aktuelle Auswahl anzuheften.
- Klicken
    Sie ![](attachments/40220569/40436810.png "tree_mode.png") um
    Kategorien wie “Attribute” in der Sicht anzuzeigen.
- Klicken Sie ![](attachments/40220569/40436809.png "filter.png") um
    Fortgeschrittene Eigenschaften anzuzeigen.
- Klicken Sie ![](attachments/40220569/40436808.png "restore.png") um
    den Standardwert wiederherzustellen.
- Klicken Sie ![](attachments/40220569/40436807.png "delete.png") um
    die gewählte Eigenschaft zu entfernen.
- Klicken Sie das kleine weiße Dreieck, um das Sichtmenü zu öffnen.
    Mit dem Sichtmenü können Sie
  - eine neue Eigenschaften-Sicht öffnen
  - eine Eigenschaft an eine Auswahl anheften
  - Kategorien und Erweiterte Eigenschaften anzeigen
  - eine ausgewählte Eigenschaft entfernen
  - durch Auswahl von “Spalteneinstellungen...” die Breite der
        beiden Spalten “Eigenschaft” und “Wert” festlegen
- Klicken Sie die beiden weiteren Symbole zum Minimieren und
    Maximieren der Sicht.

## Attachments

- [pin.png](attachments/7439351/7766143.png) (image/png)  
- [tree_mode.png](attachments/7439351/7766144.png) (image/png)  
- [filter.png](attachments/7439351/7766145.png) (image/png)  
- [restore.png](attachments/7439351/7766146.png) (image/png)  
- [delete.png](attachments/7439351/7766147.png) (image/png)  
