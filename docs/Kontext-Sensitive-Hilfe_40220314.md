# Kontext-Sensitive Hilfe

last modified on Mai 03, 2016

Es ist möglich, eine Art "Dynamische Hilfe" über die Menüleiste zu
initiieren. Eine Sicht, die über die Schaltflächen “Weiter” und “Zurück”
zur Navigation verfügt, öffnet sich mit einer kurzen Beschreibung der
Sicht oder des Editors, der oder die gerade geöffnet ist. Wenn Sie in
eine andere Sicht wechseln, ändert sich der Inhalt der Hilfe-Sicht
simultan.
