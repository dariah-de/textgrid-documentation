# Titelleiste der Gliederung-Sicht

last modified on Jul 31, 2015

Durch Klicken des
Symbols ![](attachments/40220563/40436796.png "collapse-all-nodes.png")
können Sie die Knoten in der Gliederung-Sicht zuklappen. Durch Klicken
des weißen nach unten gerichteten Dreiecks können Sie das Sichtmenü
öffnen. Es stellt die drei Funktionen “Filters”, “Mit Editor verlinken”
und “Attribute anzeigen” zur Verfügung. Die beiden weiteren Symbole
können Sie zum Minimieren und Maximieren der Sicht verwenden.

## Attachments

- [collapse-all-nodes.png](attachments/40220563/40436796.png)
(image/png)  
