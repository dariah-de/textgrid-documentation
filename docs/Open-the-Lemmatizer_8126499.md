# Open the Lemmatizer

last modified on Mai 04, 2016

Sie können den Lemmatisierer auf verschiedene Weisen öffnen:

1. Klicken Sie “Werkzeuge \> Sicht anzeigen” in der Menüleiste und
    wählen Sie “Wortform lemmatisieren” oder “Datei lemmatisieren”.
2. “Wortform lemmatisieren” kann auch durch Klicken
    von ![](attachments/40220722/40436896.gif "082-zeige-WortformLemmatisiererAnsicht.gif") in
    der Werkzeugleiste geöffnet werden.
3. Wenn ein Dokument in
    der [Quelle-Sicht](Quelle-Ansicht_40220547.md) oder
    der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) des [XML-Editors](XML-Editor_40220521.md) geöffnet
    ist, kann der Lemmatisierer im Kontextmenü ausgewählt werden.

## Attachments

- [082-zeige-WortformLemmatisiererAnsicht.gif](attachments/8126499/8192309.gif)
(image/gif)  
