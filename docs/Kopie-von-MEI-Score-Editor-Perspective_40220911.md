# Kopie von MEI Score Editor Perspective

Created on Jul 24, 2015

The MEI Score Editor Perspective consists of four views:

1. The [Musical Score View](Musical-Score-View_7439823.md) on the
    left.
2. A [Palette](Palette_7439859.md) which appears simultaneously on
    the right side of the Score View when a document is opened. The
    Palette can be closed and opened separately via the white triangle
    at the top.
3. The [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) in the
    upper right area.
4. The [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md) on the
    lower right. The [Navigator View](Navigator-View_7439192.md) from
    which the Note Editor has been started after an MEI Object has been
    opened appears as a hidden tab behind the Properties View.

The Outline and Properties Views are only opened when an MEI document is
created or opened or if the [XML Editor](XML-Editor_7439318.md) is
open in the background. Both views make it possible to edit an MEI
Object.

- [Kopie von Menu Bar of the MEI Score Editor Perspective](Kopie-von-Menu-Bar-of-the-MEI-Score-Editor-Perspective_40220913.md)
- [Kopie von Shortcuts in the MEI Score Editor Perspective](Kopie-von-Shortcuts-in-the-MEI-Score-Editor-Perspective_40220921.md)
- [Kopie von Toolbar of the MEI Score Editor Perspective](Kopie-von-Toolbar-of-the-MEI-Score-Editor-Perspective_40220919.md)

## Attachments

- [meise-f1_perspectives.png](attachments/40220911/40436961.png)
(image/png)  
