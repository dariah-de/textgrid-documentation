# Getting Started

last modifid on Feb 28, 2018

Bevor Sie mit der Arbeit mit TextGrid beginnen können, müssen Sie die
Software herunterladen, sich für eine TextGrid Nutzerkennung
registrieren und sich einloggen. 

- [The Mission of TextGrid](The-Mission-of-TextGrid_9012977.md)
- [Why use TextGrid?](7440079.md)
- [Download](Download_7439066.md)
- [Marketplace](Marketplace_8130167.md)
- [Installing and Removing Software](Installing-and-Removing-Software_11472512.md)
- [Updating](Updating_11472516.md)
- [Account and Registration](Account-and-Registration_7439077.md)
- [Welcome Screen and Login](Welcome-Screen-and-Login_7439080.md)
- [Language Selection](Language-Selection_18808888.md)
- [Troubleshooting](Troubleshooting.md)
