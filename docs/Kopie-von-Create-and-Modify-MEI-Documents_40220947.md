# Kopie von Create and Modify MEI Documents

last modified on Jul 24, 2015

A newly-created MEI Item will already display a music sheet in four-four
time with only one measure and no notes within it. The [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) and the
[Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md)
will appear, showing the structure of the MEI document. For more
information about how to open a new MEI document, please see "[Open the MEI Score Editor](Open-the-MEI-Score-Editor_7439812.md)". Now you can
create new elements for your score.

**Staff**

"ScoreDef" is a default element and not deletable. Under the “ScoreDef”
element you can create the score declarations “StaffGroup” and
“StaffDef”.

Start with a "StaffGroup", which is a group of bracketed or braced
staves. A “StaffGroup” contains lines with clef and time signature.
Left-click on a “StaffGroup” element in the
[Palette](Palette_7439859.md) in the „Create Staves“ section and drag
it into the “ScoreDef” element in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) while pressing
the left mouse button. After performing this action, you will see no
visible results like scores or staves in the [Musical Score View](Musical-Score-View_7439823.md) since you have not yet entered a
musical event such as a note or rest.

![](attachments/40220947/40437059.png "meise-f5_staffgroup_pfeil.png")

Now you can drag a "StaffDef" element, which is a container for staff
meta-information, from the Palette on the “StaffGroup” element in the
[Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md). In
the figure below three, “StaffDef” elements have been added and named
Staffdef 1, Staffdef 2 and Staffdef 3. This figure also demonstrates how
to name a staff and other possible options to change or initialize
properties of the single “StaffDef” in the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md).

![](attachments/40220947/40437057.png "meise-f6_staffdefs1.png")

**Container Creation**

The "Section" element is a container for actual music data. “Section” is
not a subelement from “ScoreDef”, so you do not need to drag it to the
“ScoreDef” element. Drag it instead into the white space underneath the
“ScoreDef/StaffGroup/StaffDef” section. Since it is a container and not
an event, no visible results will be shown in the [Musical Score View](Musical-Score-View_7439823.md).

Dragging the "Measure" element into the “Section” element in the
[Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) will
have the same (invisible) effect because it is just a container of the
content for a measure.

After dragging a "Staff" element from the Palette on the “Measure”
element in the Outline View, you see the measure with its staff on the
left. In the screen shot below, there are the three “Staffdefs” and four
measures, called Measure 1-4. To illustrate the fact that the staves
belong to their measure, the different measures have different numbers
of staves. Before you start creating notes and rests, a container for
those kind of events is required.

![](attachments/40220947/40437055.png "meise-f8_staff1.png")

For this reason, you will need to create a "Layer" element on the staff
in the [Outlin View](Outline-View-of-the-MEI-Score-Editor_7439872.md), as shown in
the next figure. You can only add events to a “Layer”, since adding an
event directly within a staff is not allowed.

![](attachments/40220947/40437054.png "meise-f9_layerinstaff.png")

**Event: Note, Rest, Beam and Chord**

As described in the paragraph above, it is necessary to create a “Layer”
before you will be able to add an event such as a quarter note.

To create a "Note", you must create a “Note” element on a “Layer” in the
[Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md). The
default properties (e.g. Duration and Octave 4, Pitchname c) of this
note are shown in the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md). In the
figure below the table some properties of the note in the second
measure, which you can edit in the Properties View, are demonstrated.
All editing possibilities are listed in the following table:

|               |                                                                                                                                                                                                           |
|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Property**  | **Explanation**                                                                                                                                                                                           |
| N             | Label for a note, only visible in the source code                                                                                                                                                         |
| Accidental    | Accidental of the note, sharps (s), flats (f), neutrals (n), double sharps (x), double flats (ff) and other double accidentals; triple flats (tb), triple sharps (xs) and other less common combinations) |
| Articulation  | Articulations such as acc or stacc                                                                                                                                                                        |
| Dots          | Number of dots                                                                                                                                                                                            |
| Duration      | 1 (whole note), 2, 4, 8, 16, 32, 64, 128 (duration of notes)                                                                                                                                              |
| Octave        | Octave of the note                                                                                                                                                                                        |
| Pitchname     | A, B, C, D, E, F, G as pitchnames                                                                                                                                                                         |
| StemDirection | Up or down                                                                                                                                                                                                |
| StemLength    | Length of the stem (the default is 19)                                                                                                                                                                    |
| Syllable      | Lyrics under the note, visible in the musical score                                                                                                                                                       |
| Timestamp     | a time stamp is defined in the form X.000                                                                                                                                                                 |

![](attachments/40220947/40437053.png "meise-f10_note_property.png")

In the figure on the left you can see the result of the use of the
property “Syllable” (text under a note), which you can modify for every
note. It is the beginning of a German Christmas song. The value of the
property “Syllable” is displayed under the note, to which you added this
element. The figure on the right shows an excerpt of its
corresponding source code in MEI, where you can see the usage of a
“Syllable” element.

![](attachments/40220947/40437052.png "meise-f11_syllable.png")   
![](attachments/40220947/40437051.png "meise-f12_syllable_source.png")

|                                   |
|-----------------------------------|
| figure A (left), figure B (right) |

A "Rest" is created in the same way as a note. You will need to drag it
onto the “Layer” of the “Staff” where you would like to place the rest.
In the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md), you can
change the duration of a rest.

A "Space" is created in the same way as a note or a rest. After adding a
“Space” element, no visible result will be seen. This element is visible
only in the context of use with other events because it acts as a
placeholder between two events like two notes. The user can also define
its duration.

The MEI Score Editor is not only able to show single notes, but also
connected ones. For this purpose, you will need to drag a "Beam" or a
"Chord" from the Palette onto a “Layer”. Afterwards you can add notes on
the “Beam” or “Chord” element in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md). An example for
the creation of a beam and a chord is given in the figure below . The
editable properties of a chord are “Articulation”, “Dots”, “Duration”,
“Note count”, “Stem direction”, and “Stem length”, and also "Timestamp".

![](attachments/40220947/40437050.png "meise-f13_beam_chord1.png")

Alternatively, you can use the context menu of the Outline View for the
creation of new elements as children of an existing element. Please use
the items "Insert Container/Variant/Event" and "Add Additions..." in the
context menu depending where they appear. Since the MEI Schema has
strict rules defining the allowed occurrences of elements and their use,
the modification of the content must follow certain policies. To guide
the user's workflow and reduce the learning curve for MEI, the context
menu offers a convenient subset of the most frequently used elements
that are valid for creation at the given location, as shown in the
figure below. In Windows, adding child elements via context menu can
only be done when the MEI Object is open in the [XML Editor](XML-Editor_7439318.md).

![](attachments/40220947/40437056.png "meise-f7_context menu add staffdef.png")

When you open an existing MEI document that was not created with the MEI
Score Editor, it is possible that there will be "Unknown Elements"
displayed in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md). Not every
possible element and attribute from the Music Encoding Initiative data
format has been implemented (yet) into the MEI Score Editor. These
elements and attributes are not being erased or ignored by the editor,
but instead they are represented as an object with a question mark
titled “Unknown Element”. Furthermore the name of this unknown element
is shown as a property in the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md). For an
example, please see the last figure below. In the source code of the MEI
document, the four staves of the example have a fermata in measure 4,
which can not (yet) be displayed by the MEI Score Editor.

![](attachments/40220947/40437049.png "meise-f14_unknownelements.png")  

## Attachments

- [mei-insertmenu.png](attachments/40220947/40437048.png) (image/png)  
- [meise-f14_unknownelements.png](attachments/40220947/40437049.png)
(image/png)  
- [meise-f13_beam_chord1.png](attachments/40220947/40437050.png)
(image/png)
- [meise-f12_syllable_source.png](attachments/40220947/40437051.png)
(image/png)  
- [meise-f11_syllable.png](attachments/40220947/40437052.png)
(image/png)  
- [meise-f10_note_property.png](attachments/40220947/40437053.png)
(image/png)  
- [meise-f9_layerinstaff.png](attachments/40220947/40437054.png)
(image/png)  
- [meise-f8_staff1.png](attachments/40220947/40437055.png) (image/png)  
- [meise-f7_context menu add staffdef.png](attachments/40220947/40437056.png) (image/png)  
- [meise-f6_staffdefs1.png](attachments/40220947/40437057.png)
(image/png)  
- [meise-f6_staffdefs.png](attachments/40220947/40437058.png)
(image/png)  
- [meise-f5_staffgroup_pfeil.png](attachments/40220947/40437059.png)
(image/png)  
