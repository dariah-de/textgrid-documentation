# Kopie von Musical Variants

Created on Jul 24, 2015

The MEI Score Editor also provides a digital representation of musical
variants. This is necessary because of the limits of the presentation of
variants in printed media. While the MEI Score Editor offers the
opportunity to switch between different variants, the encoding of
variants should be done with the “Apparatus” and “Reading” elements.

**Apparatus and Reading**

In the figure on the left  the use of the *app* (Apparatus) and the
*rdg* (Reading) elements are shown in the MEI source code and on the
right the corresponding [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md), where you can
see how to insert the “Apparatus” and “Reading” elements into the
hierarchical structure.

![](attachments/40220949/40437064.png "meise-f19_variants_source code.png")    
![](attachments/40220949/40437063.png "meise-f20_variants_outlineview.png")

**Declare Sources**

To use the possibilities for presenting different variants, you will
have to declare sources for the readings. The definition which “Reading”
belongs to which source is already visible in the source code of *figure
A* in the attribute of each *rdg* element. Before you will be able to
define the sources, you must declare them in the following way: Click
the "Manage Sources" button on the toolbar of the MEISE Editor, which
was already mentioned in the
“[Toolbar](Toolbar-of-the-MEI-Score-Editor-Perspective_7439819.md)”
paragraph . A new dialog box will open, as shown in the figure below.
Here you can create sources by clicking the "Create New" button. Now you
can give your source a name or modify the “Label” and “N(otes)”
attributes. After clicking the „Close“ button, your sources will be
saved.

![](attachments/40220949/40437062.png "meise-f21_manageSources.png")

**Define Belongings**

The next step is to define which source belongs to which reading. For
this purpose, click on a “Reading” element in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) and then open
the value of its property “Rdg Sources” in the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md). As soon as
you click the properties editor in the value column, a new dialog will
open. Select a source from the available sources you have declared and
click the „Add Source“ button. You can also add more than one source to
a "Reading" element and you can also use the “Remove” or “Remove All”
buttons. After clicking the „Close“ button, your modifications will be
saved. For an example, see the figure below : here, every “Reading”
element has one corresponding source.

![](attachments/40220949/40437061.png "meise-f22_selectsource.png")

**Show different variants**

After you have added the sources to the readings, you can switch between
the different variants. Click on the “Apparatus” element in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) and select a
source you want to see in the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md). In the last
figure, the “SourceForVariant_2” is selected and displayed in the Score
View. The selected source is also highlighted in green in the Outline
View.

![](attachments/40220949/40437060.png "meise-f23_app_source2selected.png")

## Attachments

- [meise-f23_app_source2selected.png](attachments/40220949/40437060.png)
(image/png)  
- [meise-f22_selectsource.png](attachments/40220949/40437061.png)
(image/png)  
- [meise-f21_manageSources.png](attachments/40220949/40437062.png)
(image/png)  
- [meise-f20_variants_outlineview.png](attachments/40220949/40437063.png)
(image/png)  
- [meise-f19_variants_source code.png](attachments/40220949/40437064.png) (image/png)  
