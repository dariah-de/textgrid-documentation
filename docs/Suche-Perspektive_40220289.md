# Suche-Perspektive

last modified on Aug 18, 2015

Die Suche-Perspektive besteht aus zwei Sichten: der Suche-Sicht und der
Suchergebnisse-Sicht.

![](attachments/40220289/40437417.png)

|                       |
|-----------------------|
| **Suche-Perspektive** |

## Attachments

- [sea-search.png](attachments/40220289/40436662.png) (image/png)  
- [image2015-8-18 14:39:44.png](attachments/40220289/40437417.png) (image/png)  
