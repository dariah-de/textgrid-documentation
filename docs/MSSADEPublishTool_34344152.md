# MSSADEPublishTool

last modified on Dez 07, 2017

| **Titel**                              | **SADE Publish Tool**                                                                                                        |
|----------------------------------------|------------------------------------------------------------------------------------------------------------------------------|
| **Beschreibung**                       | Mit SADE (Skalierbare Architektur für Digitale Editionen) können TextGrid-Projekte auf einer SADE-Instanz publiziert werden. |
| **Logo**                               |                                                                                                                              |
| **Lizenz**                             | <http://www.gnu.org/licenses/lgpl-3.0.txt>                                                                                     |
| **Systemanforderungen**                |                                                                                                                              |
| **Lizenz**                             | LGPL 3                                                                                                                       |
| **Sourcecode**                         | <https://gitlab.gwdg.de/SADE>                                                                                                  |
| **Projekte, die das Plugin verwenden** | Fontane-Notizbücher, Bibliothek der Neologie                                                                                 |
| **Beispieldateien**                    |                                                                                                                              |
| **Installationsanleitung**             | Install the Publish Tool SADE                                                                                                |
| **Dokumentation**                      | SADE Publish Tool                                                                                                            |
| **Screenshots**                        |                                                                                                                              |
| **Tutorials**                          |                                                                                                                              |
| **Entwickler**                         | Ubbo Veentjer; Mathias Göbel; Markus Matoni; Johannes Biermann                                                               |
| **Mitentwickler**                      |
| **Homepage/Kontakt**                   | <https://sade.textgrid.de>                                                                                                     |
| **Bugtracker**                         |

## Attachments

- [screenshotSADE.png](attachments/34344152/34472167.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/34344152/34472168.png)
(image/png)  
- [digilib-logo-big.png](attachments/34344152/34472169.png) (image/png)  
- [meise_64.png](attachments/34344152/34472170.png) (image/png)  
- [Screenshot from 2017-12-07 10-15-10.png](attachments/34344152/58505063.png) (image/png)  
