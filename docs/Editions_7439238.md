# Editions

last modified on Mai 03, 2016

Eine Edition ist die Manifestation eines Werks. Eine Edition ist eine
spezielle Form von [Aggregation](Aggregationen_40220430.md) und
ihre [Metadaten](Edition-spezifische-Metadaten_40220473.md) enthalten
Felder, um beispielsweise die Personen und Organisationen zu
beschreiben, die in die Erstellung der Edition oder der Quelle
(außerhalb von TextGrid, beispielsweise ein Buch), die die elektronische
Edition repräsentiert, involviert sind. Eine Edition wird oft mit
einem [Werk](Werke_40220432.md) in ihrer Vorveröffentlichungsform
assoziiert und muss für die [Veröffentlichung](40220493.md) mit einem
Werk assoziiert werden. Der tatsächliche Inhalt einer Edition wird in
den Element-Objekten gespeichert, die die Edition aggregiert. Editionen
können veröffentlicht werden, wenn Sie korrekte und vollständige
Metadaten haben, mit einem Werk assoziiert sind und mindestens ein
Element enthalten. Das Edition-Objekt stellt die präzise Struktur für
die Veröffentlichung von Elementen zur Verfügung.
