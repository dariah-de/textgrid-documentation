# Collections

last modified on Mai 03, 2016

Die Kollektion ist eine Anhäufung von TextGrid-Objekten auf Basis der
Mitgliedschaft in einer bestimmten Organisation und/oder einem Thema.
Kollektionen sind für zwei Anwendungsfälle vorgesehen: Im
Organisationskontext kann eine Edition Teil einer Kollektion sein
(beispielsweise die Kombination von Editionen des Blumenbach-Projekts
unter Kennzeichnung “Blumenbach” oder die Kombination von
digitalisierten Drucken des achtzehnten Jahrhunderts unter der
Kennzeichnung “ VD 18”). Zusätzlich können Kollektionen verwendet
werden, um Elemente, für die das Edition/Werk-Konzept nicht geeignet
ist, zusammenzufassen, beispielsweise Digitalisate von Museumsstücken.
Kollektionen sind valide Startpunkte für
eine [Veröffentlichung](40220493.md).
