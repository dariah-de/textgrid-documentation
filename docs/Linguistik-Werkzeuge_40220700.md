# Linguistik-Werkzeuge

last modified on Feb 26, 2019

Dieses Kapitel bietet eine Zusammenfassung der Werkzeuge für
Textanalyse. Mit diesen Werkzeugen und zukünftigen Ergänzungen können
Sie das TextGridLab verwenden, um Forschung über linguistischen und
grammatikalischen Phänomenen zu betreiben, wie beispielsweise
Inhaltsanalyse, Kontextevaluation (der Verwendungskontext von Vokabeln,
einzelnen Redewendungen, etc.), Indexierung, Annotation, Segmentierung,
Klassifikation von Texten sowie morphologische Studien.

- [Wörterbuchsuche](40220702.md)

Die linguistischen Werkzeuge Lemmatisierer, LEXUS, Cosmas II und ANNEX
stehen im Marketplace nicht mehr zum Download zur Verfügung.
