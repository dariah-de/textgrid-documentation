# Search

last modified on Feb 28, 2018

Das Suche-Werkzeug erlaubt Ihnen, gleichzeitig die Inhalte (beispielsweise TEI-kodierte Dokumente) und die Objekt-Metadaten im TextGrid Repository zu durchsuchen.

- [Open the Search Perspective](Open-the-Search-Perspective_7439474.md)
- [Search Perspective](Search-Perspective_7439476.md)
- [Search View](Search-View_7439478.md)
- [Search Results View](Search-Results-View_7439485.md)
