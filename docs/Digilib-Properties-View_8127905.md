# Digilib Properties View

last modified on Mai 04, 2016

Die Digilib-Eigenschaften-Sicht bietet eine Reihe von fortgeschrittenen
Möglichkeiten zur Bearbeitung von Bildern.

- Die Eigenschaft "Bildinformation" zeigt Datenelemente wie die
    Originalhöhe und -breite des Bildes auf dem Server an. Diese Werte
    können nicht verändert werden.
- Die Eigenschaft "DPI" stellt die Auflösungseinstellungen für den
    Bildschirm des Klienten zu Verfügung. Diese Eigenschaften werden nur
    für die Skalierungsfunktion der Originalgröße verwendet (wählen Sie
    die Ankreuzfelder für "osize" unter "Rotieren und Spiegeln"). Die
    Standardeinstellung der Auflösung ist -1. Die Werte können geändert
    werden. Klicken Sie die Schaltfläche “Umzeichnen” ("redraw"), um die
    Änderungen im Bild-Editor zu übernehmen.
- "Rotieren und Spiegeln" ist die nächste Spalte. Die Ankreuzfelder
    für den Anwendungsmodus können hier geändert werden. Die
    Standardeinstellung ist “Anpassen” ("fit"), d. h. das Bild wird
    entsprechend der Bildschirmgröße skaliert. Ein weiterer Wert ist
    “Ausschneiden” ("clip"), der das Bild nicht skaliert, sondern nur
    einen Ausschnitt des Bildes zeigt. Der Rotationswinkel des Bildes
    kann auch manuell gesetzt werden. Klicken Sie die Schaltfläche
    “Umzeichnen” ("redraw"), um die Änderungen im Bild-Editor zu
    übernehmen. Das Bild wird im Uhrzeigersinn um den entsprechenden in
    Grad angegebenen Winkel.
- "Farbe" ermöglicht es Ihnen, die Farben, die Helligkeit und den
    Kontrast des Bildes zu ändern. Es gibt zwei Möglichkeiten, die
    Farben zu verändern. Verschiedene numerische Werte auf drei Achsen
    führen zu Änderungen in Farbintensität und -schattierung:
  - Durch Addition (RGB): Diese Eigenschaft funktioniert wie ein
        Helligkeitsregler für jeden Farbkanal und addiert einen Wert zu
        jeder Pixelfarbe. Die drei Achsen stehen für die primären Farben
        des Lichts rot, grün und blau. Die Werte können zwischen -255
        und +255 liegen. (und höher, wenn die Multiplikation angewendet
        wird).
  - Durch Multiplikation (RGB): Diese Eigenschaft funktioniert wie
        der Kontrastregler für jeden Farbkanal und multipliziert den
        Wert jeder Pixelfarbe. Hier können Sie den Sättigungsgrad der
        primären Farben des Lichts rot, grün und blau im Bild durch
        Veränderung der Achsennummern variieren. Die Werte können
        zwischen -8 und +8 liegen.
  - Der Wert der Gesamthelligkeit kann ebenfalls durch einen
        numerischen Wert verändert werden. Dieser Wert wird zu allen
        Pixelfarbwerten addiert. Positive Werte machen das Bild heller,
        negative Werte dunkler. Die Werte können zwischen -255 und +255
        liegen (und höher, wenn Kontrast verwendet wird).
  - Der Gesamtkontrast kann ebenfalls verändert werden. Dieser Wert
        wird mit allen Pixelfarbwerten multipliziert. Positive Werte
        erhöhen den Kontrast, negative Werte senken ihn. Die Werte
        können zwischen -8 und +8 liegen. Wenn Sie den Kontrast in
        dieser Einstellung erhöhen, müssen Sie die erhöhten
        Gesamtpixelwerte durch negative Helligkeitswerte kompensieren.
        Der Schieberegler für Kontrast in der Werkzeugleiste macht dies
        automatisch.
- "Größe und Skalierung" bietet verschiedene Möglichkeiten, um den
    gezoomten Bereich zu ändern (relative Höhe, Breite und Verschiebung)
    und Gesamtgröße des Bildes (Zielhöhe und -breite):

1.

- relative Höhe des Zoombereichs als Anteilswert der Bildhöhe.
        Wert \]0-1\].
- Zielbreite des Bildes in Pixel
- zusätzlicher Skalierungsfaktor für die Zielgröße, die es Ihnen
        erlaubt, das gesamte Bild zu vergrößern
- relative Y-Verschiebung des Zoombereichs als Anteilswert der
        Bildhöhe. Wertebereich \]0-1\].
- relative X-Verschiebung des Zoombereichs als Anteilswert der
        Bildbreite. Wertebereich \]0-1\].
- Zielhöhe des Bildes in Pixel

Das Kontextmenü ist in der Eigenschaften-Sicht deaktiviert. Klicken
Sie ![](https://dev2.dariah.eu/wiki/download/attachments/7439890/Bildschirmfoto+2012-01-30+um+11.56.17.png?version=1&modificationDate=1331563180506) in
der Werkzeugleiste, um die gewählte Eigenschaft-Sicht an die aktuelle
Auswahl anzuheften. Das nach unten gerichtete Dreieck bietet Ihnen die
Möglichkeit, eine neue Eigenschaften-Sicht zu öffnen oder gleichermaßen
die Eigenschaft-Sicht an die aktuelle Auswahl anzuheften.
