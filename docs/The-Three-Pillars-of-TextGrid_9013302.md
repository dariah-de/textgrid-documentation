# The Three Pillars of TextGrid

last modified on Apr 08, 2013

The main constituents of the TextGrid middleware are the three utilities
TG-auth\*, TG-search and TG-crud (figure 3) – implemented as web
services.

(inser image here) Figure 3: The TextGrid Utilities

TG-auth\* covers two aspects. With “N” replacing the asterisk, it
provides for autheNtication of users in the TextGrid environment. With
“Z”, it serves as an authoriZation engine. As already mentioned, for
authorization, TextGrid uses a role-based access control solution called
[OpenRBAC](http://www.openrbac.de/en_startup.xml) where permissions are
stored in an LDAP database. With logging in to TextGrid, a session id is
generated, which is passed around between the utilities and services, to
be used to check permissions with TG-auth\*.

TG-search provides several interfaces for text retrieval and metadata
search. The capabilities of searching across XML data and the semantic
(RDF) index are explained in the section before.

TG-crud is a web service to create, retrieve, update and delete TextGrid
objects. It is the interface to storing information to the grid
environment, the search indices (see above) and the role based access
control system (RBAC) using TG-auth\*. TG-crud also checks access
permissions and ensures that the TextGrid repository stays consistent.
Furthermore, it uses the Adaptor Manager to convert XML documents into
the TextGrid baseline encoding, which also are stored in the XML
database for efficient structural search. Additionally, the Adaptor
Manager is responsible for extracting relation information from
metadata, TEI files and the generated baseline-encoded files (such as
contained links to other TextGrid objects and XML schema references) and
storing them to the RDF triple store. As may be easily comprehensible,
TG-crud bundles most of the application logic of TextGrid and its
middleware.

A detailed description of these utilities is given in [Report 3.5](http://www.textgrid.de/fileadmin/TextGrid/reports/R3_5-manual-tools.pdf) (first
project phase).
