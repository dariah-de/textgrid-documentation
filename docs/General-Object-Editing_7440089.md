# General Object Editing

last modified on Feb 28, 2018

Dieses Kapitel bietet einen Überblick über die grundlegenden
Editionswerkzeuge in TextGrid. Während des Editionsprozesses können
diese für die meisten notwendigen Funktionen verwendet werden.

- [XML Editor](XML-Editor_7439318.md)
- [oXygen](oXygen_7440092.md)
- [CSS Editor](CSS-Editor_19337257.md)
- [Text Text Link Editor](Text-Text-Link-Editor_10618227.md)
- [Text Image Link Editor](Text-Image-Link-Editor_7439374.md)
- [Unicode Character Table](Unicode-Character-Table_7439299.md)
- [MSTextTextLinkeditor](MSTextTextLinkeditor_34344147.md)
- [MSOxygenXMLEditor](MSOxygenXMLEditor_34344150.md)
- [MSTextGridRepPreview](MSTextGridRepPreview_38093121.md)
