# Open ANNEX

last modified on Mai 04, 2016

Sie können ANNEX auf zwei verschiedene Weisen öffnen:

1. Wählen Sie “Werkzeuge \> Sicht anzeigen \> ANNEX” in der Menüleiste.
2. Klicken Sie ![](attachments/40220762/40436907.png "annex.png") in
    der Werkzeugleiste.

## Attachments

- [annex.png](attachments/8127628/8192318.png) (image/png)  
- [annex.png](attachments/8127628/8192317.png) (image/png)  
