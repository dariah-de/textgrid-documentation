# RDF Objekt Editor

Created on Jul 28, 2015

|                                    |                                                                                                                                            |
|------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | RDF Object Editor                                                                                                                          |
| Beschreibung                       | TextGridLab plugin for the RDF Object Editor                                                                                               |
| Logo                               |                                                                                                                                            |
| Lizenz                             | [http://www.gnu.org/licenses/lgpl-3.0.txt](http://www.gnu.org/licenses/lgpl-3.0.txt)                                                       |
| Systemanforderungen                |                                                                                                                                            |
| Lizenz                             | LGPL 3                                                                                                                                     |
| Sourcecode                         | [https://projects.gwdg.de/projects/blumenbachiana-textgridlab-plugin](https://projects.gwdg.de/projects/blumenbachiana-textgridlab-plugin) |
| Projekte, die das Plugin verwenden |                                                                                                                                            |
| Beispieldateien                    |                                                                                                                                            |
| Installationsanleitung             |                                                                                                                                            |
| Dokumentation                      |                                                                                                                                            |
| Screenshots                        |                                                                                                                                            |
| Tutorials                          |                                                                                                                                            |
| Entwickler                         |                                                                                                                                            |
| Mitentwickler                      |                                                                                                                                            |
| Homepage/Kontakt                   |                                                                                                                                            |
| Bugtracker                         |                                                                                                                                            |

## Attachments:

- [screenshotSADE.png](attachments/40220519/40436743.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/40220519/40436744.png)
(image/png)  
- [digilib-logo-big.png](attachments/40220519/40436745.png) (image/png)  
- [meise_64.png](attachments/40220519/40436746.png) (image/png)  
