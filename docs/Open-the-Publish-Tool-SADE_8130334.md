# Open the Publish Tool SADE

last modified on Mai 03, 2016

Öffnen Sie das Publikationswerkzeug SADE auf eine der folgenden Weisen:

1. Wählen Sie "Werkzeuge \> SADE Publish" in der Menüleiste
2. Klicken Sie ![](attachments/40220509/40436736.gif) in der
    Werkzeugleiste
3. Rechtsklicken Sie ein Objekt und wählen Sie "Publish to SADE"

## Attachments

- [141-SADE-Publish-Perspektive.gif](attachments/8130334/8192287.gif)
(image/gif)  
- [Textgrid_Icons_131-161.zip](attachments/8130334/9240612.zip)
(application/zip)  
- [153-Web-Preview.gif](attachments/8130334/9240613.gif) (image/gif)  
