# Open the Text Text Link Editor

last modified on Mai 03, 2016

Der Text-Text-Link-Editor kann auf verschiedene Weisen geöffnet werden:

1. Wählen Sie “Werkzeuge \> Text-Text-Link-Editor” in der Menüleiste.
2. Klicken Sie ![](attachments/40220612/40436813.gif) in der
    Werkzeugleiste.
3. Doppelklicken Sie ein Text-Text-Link-Objekt
    im [Navigator](40220331.md) oder
    der [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md).
4. Rechtsklicken Sie ein Text-Text-Link-Objekt im Navigator oder der
    Suchergebnisse-Sicht und wählen Sie „Öffnen mit \>
    Text-Text-Link-Editor" im Kontextmenü.

## Attachments

- [Textgrid_Icons_2012-05-2.zip](attachments/10618241/10780717.zip)
(application/zip)  
- [142-oeffne-TTLE-Perspektive.gif](attachments/10618241/10780718.gif)
(image/gif)  
