# Using the MEI Score Editor

last modified on Mai 04, 2016

The following text describes how to work with the MEI Score Editor. For
this purpose, the use of the items of
the [Palette](Palette_7439859.md)will be demonstrated and  the
properties of the elements from the [Outline
View](Outline-View-of-the-MEI-Score-Editor_7439872.md) will be
mentioned. Since the MEI Score Editor creates and modifies MEI
documents, this subchapter follows the hierarchical structure of an MEI
document.

*Please note:* The manipulation of the score is done in the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) or,
respectively, with the context menu of the [Score View](Musical-Score-View_7439823.md). Creation of new elements can be
achieved either by dragging new elements from
the [Palette](Palette_7439859.md) into the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) for creation or
by using the context menu of an element in the Outline View/Score View.
When you have marked an element in the Outline View, you can also delete
it using the context menu or modify its properties using the [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md).

- [Create and Modify MEI Documents](Create-and-Modify-MEI-Documents_7439900.md)
- [Musical Variants](Musical-Variants_7439904.md)
