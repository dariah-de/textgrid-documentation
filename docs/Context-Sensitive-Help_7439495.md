# Context-Sensitive Help

last modified Feb 28, 2018

Es ist möglich, eine Art "Dynamische Hilfe" über die Menüleiste zu
initiieren. Eine Sicht, die über die Schaltflächen “Weiter” und “Zurück”
zur Navigation verfügt, öffnet sich mit einer kurzen Beschreibung der
Sicht oder des Editors, der oder die gerade geöffnet ist. Wenn Sie in
eine andere Sicht wechseln, ändert sich der Inhalt der Hilfe-Sicht
simultan.
