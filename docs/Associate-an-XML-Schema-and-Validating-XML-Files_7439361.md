# Associate an XML Schema and Validating XML Files

last modified on Mai 04, 2016

Sie können ein Schema nicht nur beim Erstellen Ihres XML-Dokuments mit
diesem assoziieren, sondern auch während Sie in
der [Quelle-Ansicht](Quelle-Ansicht_40220547.md) arbeiten. Wählen Sie
“XML \> Schema assoziieren” in der Menüleiste und wählen Sie eines der
aufgelisteten Schemata im Assistenten „Schema auswählen“. Klicken Sie
“OK”, um das Schema zu assoziieren. Um ein Schema zu assoziieren, können
Sie zwischen Schemata, auf die Sie in Projekten im Repository Zugriff
haben, und Schemata, die im integrierten XML-Katalog zur Verfügung
gestellt werden, auswählen. Um Ihr eigenes Schema ins Repository zu
importieren, siehe unter [Import](40220393.md). Derzeit werden nur
W3C-Schemata im TextGridLab unterstützt.

In der Quelle-Ansicht, werden Fehler in der allgemeinen XML-Syntax (wie
fehlende Anführungszeichen, Klammern oder schließende Tags) ebenso durch
rote wellenförmige Unterstreichung angezeigt wie Validierungsfehler
(wenn ein Schema spezifiziert ist). Um zu überprüfen, ob Ihr
XML-Dokument wohlgeformt und valide ist, können Sie es in jeder Sicht
durch Auswahl von “XML \> Validierungsfehler anzeigen” in der Menüleiste
anzeigen lassen. Die Validierungsfehler-Sicht wird unterhalb der
Editor-Sicht geöffnet und zeigt im Falle eines Fehlers eine
Fehlermeldung an, die das fehlerhafte XML-Element identifiziert.
