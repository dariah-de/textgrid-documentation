# Status Bar

last modified on Feb 28, 2018

The status bar at the bottom of the screen on the left side always
displays the
icon ![](attachments/7439176/8192284.gif "new_fastview.gif") to
show views as "fast views". The user can choose custom "fast views" from
a list. The right corner at the bottom of the screen displays the
TextGrid User ID of the user currently logged in. There can be
additional information visible in this status bar, such as the title and
resource identifier of selected Projects or Objects. Depending on the
views that are open, the status bar may also display other details.

## Attachments

- [new_fastview.gif](attachments/7439176/8192284.gif) (image/gif)  
