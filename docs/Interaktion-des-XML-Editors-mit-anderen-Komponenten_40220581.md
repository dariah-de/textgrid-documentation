# Interaktion des XML-Editors mit anderen Komponenten

last modified on Aug 19, 2015

[*Zum Anfang der Metadaten*](#page-metadata-start)

Der [Navigator](40220331.md) wird verwendet, um Projekte und Objekte
zu öffnen und zu verwalten. Durch Doppelklicken werden XML-Dokumente
standardmäßig im XML-Editor geöffnet.

Der [Metadaten-Editor](Metadaten-Editor_40220458.md) wird verwendet,
um die Metadaten von Projekten und Objekten zu verwalten. Für die Suche
von XML-Dokumenten sind diese Informationen hilfreich. Der
Metadaten-Editor erlaubt Ihnen, einen TEI-Header aus den Metadaten zu
erstellen, den Sie in Ihren XML-Quelltext einfügen können.

Die
[Unicode-Zeichentabelle](Unicode-Zeichen-Tabelle_40220675.md)-Sicht
kann verwendet werden, um Unicode-Zeichen in ein XML-Dokument
einzufügen.

Der [Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md) erlaubt
Ihnen, Texte und Bilder miteinander zu verknüpfen. Der Text muss im
XML-Format vorliegen, der XML-Editor wird dann standardmäßig verwendet.

Über das Kontextmenü ist es außerdem möglich, mit anderen Modulen des
TextGridLab zu interagieren. Dies gilt standardmäßig für die
[Wörterbuchsuche](40220702.md), es ist außerdem möglich für die
Werkzeuge [Lemmatisierer](Lemmatisierer_40220718.md),
[LEXUS](40220734.md), [Cosmas II](40220746.md),
[ANNEX](40220758.md) und
[SADE](Publikationswerkzeug-SADE_40220503.md), sofern sie installiert
sind.
