#: Texte kollationieren

last modified on Aug 20, 2015

1. Um Texte zu kollationieren, muss ein Kollationierungsset durch
    [Öffnen](40220885.md) des Kollationierers und Klicken
    von ![](attachments/40220895/40436952.gif "155-Datei-Icon-Collation-Set.gif")
    in der Werkzeugleiste festgelegt werden.
2. Wählen Sie ein Projekt und einen Titel sowie weitere Metadaten für
    das Set in dem Assistenten aus, der sich jetzt öffnet.
3. Ziehen Sie Objekte aus dem [Navigator](40220331.md) oder der
    [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md) und legen
    Sie diese in der Kollationierungssetliste ab. Derzeit kann nur
    Klartext (.txt) auf diese Weise kollationiert werden.
4. Speichern Sie das Kollationierungsset durch Drücken von \[Strg+S\].
5. Klicken
    Sie ![](attachments/40220895/40436951.gif "157-starte-Kollationierung.gif")
    in der Werkzeugleiste.
6. Die Ergebnisse werden in einer Tabelle, die alle Textelemente
    ausrichtet, in einem Graphen aller Varianten und in Form von
    TEI-konformem XML-Text angezeigt.

## Attachments

- [157-starte-Kollationierung.gif](attachments/40220895/40436951.gif)
(image/gif)  
- [155-Datei-Icon-Collation-Set.gif](attachments/40220895/40436952.gif)
(image/gif)  
