# Metadaten-Vorlagen-Editor verwenden

last modified on Jul 28, 2015

Zunächst muss ein Projekt gewählt und “Weiter” geklickt werden. Danach
fügen Sie mit “Neues Element hinzufügen” einen neuen Eintrag hinzu und
geben einen neuen Elementnamen an, der nur aus einem Wort bestehen darf.
Danach wählen Sie im Aufklappmenü einen Datentyp für dieses Element. Die
Elemente können sein

- eine Zeichenkette (string): Zeichenketten umfassen einzelne Wörter
    oder Zeichen bis hin zu großen Textblöcken
- ein Datum (date)
- ein ganzzahliger Wert (integer)
- ein Wahrheitswert (boolean): die Werte für “falsch” und “wahr”
    müssen als “0” und “1” geschrieben werden
- eine Zeitangabe (time)
- eine Dezimalzahl (decimal): diese muss mit einem Dezimalpunkt
    geschrieben werden (beispielsweise, “3.14”)

Setzen Sie wenn gewünscht Häkchen in die Ankreuzfelder “Wiederholbar”
oder “Pflichtfeld”. Ein Element kann durch Klicken der entsprechenden
Schaltfläche “Entfernen” aus der Liste entfernt werden. Weitere Elemente
können durch Klicken der Schaltfläche “Neues Element hinzufügen”
hinzugefügt werden. Klicken Sie “Fertigstellen”, um Ihre Eingabe zu
speichern, oder “Abbrechen”, um den Assistenten zu schließen. Klicken
Sie “Zurück”, wenn Sie ein anderes Projekt wählen wollen. Nachdem Sie
den Vorgang mit “Fertigstellen” abgeschlossen haben, werden die neuen
Eingabefelder für alle TextGrid-Objekte des Projekts im Metadaten-Editor
angezeigt.

![](attachments/40220486/40436734.png)

|                               |
|-------------------------------|
| **Metadaten-Vorlagen-Editor** |

## Attachments

- [mde-metadatatemplateeditor-wizard.png](attachments/40220486/40436734.png)
(image/png)  
