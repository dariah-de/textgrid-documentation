# Shortcuts

last modified on Feb 28, 2018

Eine Liste von Tastaturkürzeln kann im Lab durch Auswahl von „Hilfe für
Tastenbelegung“ unter "Hilfe" oder durch Drücken von
Strg+Umschalttaste+L gefunden werden. Dies ist eine Liste der
wichtigsten Tastaturkürzel:

|                           |                                   |
|---------------------------|-----------------------------------|
| \[Alt+-\]                 | System-Menü anzeigen              |
| \[Alt+Umschalttaste+Q,Q\] | Sicht anzeigen                    |
| \[Strg+3\]                | Schnellzugriff                    |
| \[Strg+F6\]               | Nächster Editor                   |
| \[Strg+F7\]               | Nächste Sicht                     |
| \[Strg+F8\]               | Nächste Perspektive               |
| \[Strg+F10\]              | Sichten-Menü anzeigen             |
| \[Strg+M\]                | Aktive(n) Sicht/Editor maximieren |
| \[Strg+S\]                | Speichern                         |
| \[Strg+Umschalttaste+F6\] | Vorheriger Editor                 |
| \[Strg+Umschalttaste+F7\] | Vorherige Sicht                   |
| \[Strg+Umschalttaste+F8\] | Vorherige Perspektive             |
| \[Strg+Umschalttaste+E\]  | Zu Editor wechseln                |
| \[Strg+Umschalttaste+L\]  | Hilfe für Tastenbelegung          |
| \[Strg+Umschalttaste+S\]  | Alles Speichern                   |
| \[Strg+Umschalttaste+W\]  | Alles Schließen                   |
| \[Strg+W\]                | Schließen                         |
| \[F12\]                   | Editor aktivieren                 |
