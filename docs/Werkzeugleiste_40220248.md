# Werkzeugleiste

last modified on Aug 18, 2015

Die Werkzeugleiste gibt Ihnen Zugriff auf die am häufigsten verwendeten
Werkzeuge und Komponenten sowie einige Werkzeug-spezifische Funktionen,
die nur verfügbar sind, wenn bestimmte Werkezuge geöffnet sind. Diese
spezifischen Funktionen werden in den folgenden Kapiteln über diese
Werkzeuge (beispielsweise den Text-Bild-Link-Editor) beschrieben. Dies
ist eine Liste der permanent verfügbaren Punkte der Werkzeugleiste des
TextGridLab Werkzeugleiste mit Symbol und der entsprechenden Funktion:

|                                                                                    |                                                                                              |
|:-----------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------|
| ![](attachments/40220248/40436655.png "001-zeige-WelcomeView.png")                 | öffnet den [Startbildschirm](Startbildschirm-und-Login_40220219.md)                        |
| ![](attachments/40220248/40436653.png "plus-icon.png")                             | erlaubt Ihnen, neue Projekte und [Aggregationen](Aggregationen_40220430.md) zu erstellen   |
| ![](attachments/40220248/40436654.png "016-speichere-Datei.png")                   | speichert das momentan geöffnete Dokument                                                    |
| ![](attachments/40220248/40436652.png "072-zeige-AggregationPerspektive.png")      | öffnet den [Aggregationen-Editor](Aggregationen-Editor_40220440.md)                        |
| ![](attachments/40220248/40436651.png "073-zeige-WoerterbuchPerspektive.png")      | öffnet die [Wörterbuchsuche](40220702.md)                                                  |
| ![](attachments/40220248/40436650.png "069-zeige-BildLinkEditorPerspektive.png")   | öffnet den [Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md)                      |
| ![](attachments/40220248/40436649.png "074-zeige-ProjektNutzerVerwaltung.png")     | öffnet die [Projekt- und Benutzer-Verwaltung](Projekt--und-Benutzerverwaltung_40220359.md) |
| ![](attachments/40220248/40436648.png "070-zeige-SuchwerkzeugPerspektive2.png")    | öffnet die [Suche](Suche_40220282.md)                                                      |
| ![](attachments/40220248/40436647.png "071-zeige_XMLEditorPerspektive.png")        | öffnet den [XML-Editor](XML-Editor_40220521.md)                                            |
| **![](attachments/40220248/40436640.png "075-zeige-WorkflowPerspektive.png")**     | öffnet das [Workflow-Werkzeug](Werkzeugleiste_40220248.md)                                 |
| ![](attachments/40220248/40436646.png "080-zeige-MetadatenAnsicht.png")            | öffnet den [Metadaten-Editor](Metadaten-Editor_40220458.md)                                |
| **![](attachments/40220248/40436642.gif "077-zeige-MetadatenEntwurfsWizard.gif")** | öffnet den [Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md)              |
| ![](attachments/40220248/40436645.png "081-zeige-SonderzeichenAnsicht.png")        | öffnet die [Unicode-Zeichen-Tabelle](Unicode-Zeichen-Tabelle_40220675.md)                  |
| ![](attachments/40220248/40436644.png "103-zeige-Hilfe.png")                       | öffnet die [Hilfe](Hilfe_40220301.md)                                                      |
| **![](attachments/40220248/40436643.png "036-schliesse-TextGridLab.png")**         | schließt die momentan geöffnete Perspektive                                                  |

|                                |
|--------------------------------|
| **Symbole der Werkzeugleiste** |

Wenn anderen Module wie CollateX installiert sind, erscheinen deren
Symbole ebenfalls in der Werkzeugleiste. Diese Funktionen werden in den
Kapiteln über die fachspezifischen Werkzeuge erläutert.

## Attachments

- [075-zeige-WorkflowPerspektive.png](attachments/40220248/40436640.png)
(image/png)  
- [075-zeige-WorkflowPerspektive2.png](attachments/40220248/40436641.png)
(image/png)  
- [077-zeige-MetadatenEntwurfsWizard.gif](attachments/40220248/40436642.gif)
(image/gif)  
- [036-schliesse-TextGridLab.png](attachments/40220248/40436643.png)
(image/png)  
- [103-zeige-Hilfe.png](attachments/40220248/40436644.png) (image/png)  
- [081-zeige-SonderzeichenAnsicht.png](attachments/40220248/40436645.png)
(image/png)  
- [080-zeige-MetadatenAnsicht.png](attachments/40220248/40436646.png)
(image/png)  
- [071-zeige_XMLEditorPerspektive.png](attachments/40220248/40436647.png)
(image/png)  
- [070-zeige-SuchwerkzeugPerspektive2.png](attachments/40220248/40436648.png)
(image/png)  
- [074-zeige-ProjektNutzerVerwaltung.png](attachments/40220248/40436649.png)
(image/png)  
- [069-zeige-BildLinkEditorPerspektive.png](attachments/40220248/40436650.png)
(image/png)  
- [073-zeige-WoerterbuchPerspektive.png](attachments/40220248/40436651.png)
(image/png)  
- [072-zeige-AggregationPerspektive.png](attachments/40220248/40436652.png)
(image/png)  
- [plus-icon.png](attachments/40220248/40436653.png) (image/png)  
- [016-speichere-Datei.png](attachments/40220248/40436654.png)
(image/png)  
- [001-zeige-WelcomeView.png](attachments/40220248/40436655.png)
(image/png)  
