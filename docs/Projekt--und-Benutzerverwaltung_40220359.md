# Projekt- und Benutzerverwaltung

last modified on Jul 28, 2015

Die Projekt- und Benutzerverwaltung erlaubt Ihnen, Projekte zu erstellen
und zu verwalten, Nutzer zu einem Projekt hinzuzufügen und ihnen Rollen
zuzuweisen.

- [Projekt- und Benutzerverwaltung öffnen](40220371.md)
- [Projekt- und Benutzerverwaltung-Perspektive](Projekt--und-Benutzerverwaltung-Perspektive_40220376.md)
- [Navigator-Sicht als Bestandteil der Projekt- und Benutzerverwaltung-Perspektive](Navigator-Sicht-als-Bestandteil-der-Projekt--und-Benutzerverwaltung-Perspektive_40220378.md)
- [Benutzerverwaltung-Sicht](Benutzerverwaltung-Sicht_40220383.md)
- [Projekt- und Benutzerverwaltung verwenden](Projekt--und-Benutzerverwaltung-verwenden_40220385.md)
- [Interaktion der Projekt- und Benutzerverwaltung mit anderen Komponenten](Interaktion-der-Projekt--und-Benutzerverwaltung-mit-anderen-Komponenten_40220387.md)
- [Import (de)](40220393.md)
- [Export (de)](40220406.md)
