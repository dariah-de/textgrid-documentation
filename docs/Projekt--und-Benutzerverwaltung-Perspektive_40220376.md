# Projekt- und Benutzerverwaltung-Perspektive

last modified on Aug 19, 2015

Die Perspektive ist in zwei Sichten unterteilt. Der
[Navigator](40220331.md) befindet sich auf der linken und die
[Benutzerverwaltung-Sicht](Benutzerverwaltung-Sicht_40220383.md) auf
der rechten Seite.

![](attachments/40220376/40436714.png)

|                                     |
|-------------------------------------|
| **Projekt- und Benutzerverwaltung** |

## Attachments

- [pum-navigatorlang.png](attachments/40220376/40436714.png) (image/png)  
