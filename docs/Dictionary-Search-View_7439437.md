# Dictionary Search View

last modified on Mai 03, 2016

Um ein Wort nachzuschlagen, füllen Sie die Eingabemaske der
Wörterbuchsuche wie folgt aus:

- Geben Sie ein Schlüsselwort im ersten Eingabefeld ein. Die Suche
    berücksichtigt keine Groß- und Kleinschreibung.
- Sie können die Zahl der angezeigten Ergebnisse beschränken.
    Standardmäßig werden die ersten zehn Ergebnisse angezeigt.
- Wählen sie zwischen exakter und unscharfer (“fuzzy”) Suche. In der
    unscharfen Suche werden auch ähnliche Zeichenketten gefunden.
- Setzen Sie Häkchen in den Ankreuzfeldern der Wörterbücher, die
    durchsucht werden sollen.
- Um die Suche zu starten, klicken Sie die Schaltfläche “Suche
    starten”.
- Sie können Platzhalter verwenden. Das Asterisk-Zeichen (\*) steht
    dabei für eine beliebige Zeichenkette (auch null Zeichen) und das
    Fragezeichen (?) steht für genau ein Zeichen. Sie können diese
    Platzhalter beliebig kombinieren.
