# Objektverwaltung und Administration

last modified on Jul 28, 2015

In diesem Abschnitt werden die Instrumente zur Verwaltung von Projekten,
Objekten und Nutzern erklärt. In diesem Zusammenhang wird auch
erläutert, wie der Import- und Export-Vorgang sowie die Aggregation und
Publikation von Objekten abläuft und wie ihre Metadaten verwaltet
werden.

- [Navigator (de)](40220331.md)
- [Projekt- und Benutzerverwaltung](Projekt--und-Benutzerverwaltung_40220359.md)
- [Revisionen- und Sperre-Mechanismus](Revisionen--und-Sperre-Mechanismus_40220412.md)
- [TextGrid-Objekte](TextGrid-Objekte.md)
- [Aggregationen-Editor](Aggregationen-Editor_40220440.md)
- [Metadaten-Editor](Metadaten-Editor_40220458.md)
- [Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md)
- [Publish (de)](40220493.md)
- [Publikationswerkzeug SADE](Publikationswerkzeug-SADE_40220503.md)
- [MSSADEPublishTool (de)](40220517.md)
- [RDF Objekt Editor](RDF-Objekt-Editor_40220519.md)
