# Warum Sie TextGrid nutzen sollten

last modified on Aug 18, 2015

Das TextGridLab und das TextGridRep wurden in Zusammenarbeit mit
GeisteswissenschaftlerInnen entwickelt und spiegeln die Bedürfnisse von
ForscherInnen wider, die die wissenschaftlichen Möglichkeiten, die sich
durch digitale Methoden bieten, individuell oder in einer Gemeinschaft
geographisch verteilter WissenschaftlerInnen erforschen möchten. Viele
geisteswissenschaftliche Projekte behandeln zunehmend komplexe
Forschungsfragen und große Datenmengen rufen WissenschaftlerInnen mit
unterschiedlichem Hintergrund und Erfahrungsschatz auf den Plan;
interdisziplinäre Forschung und kollaborative Partnerschaft werden durch
die Nutzung eines integrierten Arbeitsbereichs wie TextGrid in denen
eine Projekt- und Benutzer-Verwaltung ein integraler Bestandteil ist,
vereinfacht. Mit der Nutzung von TextGrid können ForscherInnen in einer
sicheren Umgebung, in der Forschungsdaten sicher und zuverlässig
gespeichert werden, komfortabel und produktiv zusammenarbeiten. Die in
TextGrid verfügbaren Werkzeuge und Dienste sind flexibel und erweiterbar
und können an verschiedene Forschungsmethoden angepasst werden.
Zusätzlich werden diese Werkzeuge gepflegt und upgedatet, um eine
langfristige Verfügbarkeit und Nachnutzbarkeit der Forschungsdaten
sicherzustellen.

Eine virtuelle Forschungsumgebung wie TextGrid erlaubt neue Formen von
Zusammenarbeit und Forschung in einem sicheren und zuverlässigen
Arbeitsbereich. TextGrid bietet die folgenden Merkmale, die die
geisteswissenschaftliche Forschungsarbeit erleichtern:

**Administration und Workflow-Organisation**

Diese besteht aus Kerndienste, um Projekte zu verwalten und zu
organisieren:

- eine anspruchsvolle Benutzerverwaltung, die die Zuweisung und
    Verwaltung von Rollen und Zugriffsrechten für verschiedene
    Projektpartner managt.
- ein effizientes Dokumentenverwaltungssystem, das Projekte und
    Objekte auf verschiedenen Ebenen effektiv verwaltet und die
    komplexen Beziehungen zwischen ihnen wiedergibt.
- ein Workflow-Werkzeug, das die halbautomatische Verarbeitung von
    Daten ermöglicht.
- ein Versionsmanagement, das sicherstellt, dass die
    Arbeitsfortschritte über zwischenzeitliche Revisionen umfassend
    dokumentiert werden. Dokumente, die in einer endgültigen Form im
    TextGrid Repository "veröffentlicht" werden, können nicht gelöscht
    werden; Aktualisierungen und neue Versionen können jedoch
    hinzugefügt werden.

**Dezentralisierter Arbeitsbereich**

NutzerInnen können auf das TextGridLab und das Rep ortsunabhängig
zugreifen und in komplexen Forschungsprojekten zusammenarbeiten.

**Standardisierung**

Das kontrollierte Metadaten-Vokabular und offene Standards erleichtern
den Austausche von Daten, Textsuche und digitale Archivierung.

**Erweiterbarkeit**

TextGrid ist erweiterbar und erlaubt NutzerInnen, vorhandene Werkzeuge
zu verbessern oder neue Werkzeuge und Dienste entweder als externe
Webdienste oder als interaktive Werkzeuge über die
Eclipse-Rich-Client-Plattform zu integrieren.

**Modularität**

Das TextGridLab ist modular aufgebaut, der XML-Editor dient dabei als
Kernelement, auf dem die virtuelle Forschungsumgebung aufbaut.
Zahlreiche Werkzeuge und Dienste sind mit dem XML-Editor verbunden und
entsprechend der individuellen Bedürfnisse des Projekts eingerichtet und
kombiniert werden. Manche Werkzeuge erlauben das Einfügen von Elementen
in den Quelltext im XML-Editor; andere haben eine strukturelle
Verbindung zum XML-Editor. Das offene Interface erlaubt zudem die
Integration neuer, speziell entwickelter Applikationen. Diese modulare
Struktur erleichtert Arbeitsprozesse in der virtuellen
Forschungsumgebung.

**Zuweisung von Nutzerrechten**

TextGrid stellt eine ausgeklügelte rollenbasierte Zugriffskontrolle
(role-based access control, OpenRBAC) zur Verwaltung der Nutzerrechte
zur Verfügung. Jedes TextGrid-Projekt muss mindestens einen
Projekt-Manager zugewiesen haben, und diese Rolle gibt das Recht,
anderen NutzerInnen Rollen zuzuweisen und zu entfernen. Nur diejenigen
NutzerInnen, denen in einem Projekt eine Rolle zugewiesen wurde, haben
Zugriff auf dessen unveröffentlichte Inhalte. Durch dieses System können
Nutzerrollen auch temporär vergeben werden, wenn beispielsweise eine
vorübergehende Mitarbeit im Projekt nötig ist, um eine bestimmte Aufgabe
auszuführen.. Auf diese Weise können der oder die Projekt-Manager den
Zugriff auf die Projekt-Daten kontrollieren und damit die sichere und
zuverlässige Zusammenarbeit und Datenspeicherung in einer weltweit
zugreifbaren Forschungsumgebung garantieren.

**Verteilte Speicherung**

Das TextGrid Repository verfügt über ein System verteilter Speicherung,
das aus zwei Hauptteilen besteht. Die erste Erstellung und die
Bearbeitung von Dateien wird im TextGridRep gespeichert. Die Daten
werden gespeichert und in einem Suchindex gesichert und haben keinen
persistenten Identifizierer (persistent identifier, PID). Der erste
Suchindex (Suchindex 1) ist dynamisch, was bedeutet, dass er Daten
enthält, auf die nur von TextGridLab-NuzterInnen zugegriffen werden
kann, denen durch einen Projekt-Manager Nutzerrechte zugewiesen worden
sind. Diese Daten können nur von den NutzerInnen verändert oder gelöscht
werden, denen das Recht zu löschen oder die Rolle als Bearbeiter
zugewiesen worden ist.

Nachdem der Projekt-Manager sich für die Veröffentlichung der Daten in
ihrem endgültigen Format entschlossen hat, werden die Daten in den
zweiten Suchindex (Suchindex 2) transferiert. Die Daten verschwinden
dann aus dem Suchindex 1 und werden mit einem einzigartigen PID
versehen, um sie eindeutig referenzierbar zu mache. Die Dateien, die
transferiert und "veröffentlicht" worden sind, können nicht mehr
verändert oder gelöscht werden. Spätere Änderungen an den Daten können
nur durch Kopieren oder Re-Importieren einer Datei in Suchindex 1
durchgeführt werden.

Suchindex 2 ist für jedermann mit Zugang zum World Wide Web zugreifbar.
Ein Charakteristikum ist seine statische Eigenschaft, die bedeutet, dass
seine Daten unveränderlich sind. Große Datenmengen können auch von
anderen Archiven oder Publikationsservern hochgeladen und durch das
TextGridRep verfügbar gemacht werden. Sowohl TextGridLab-NutzerInnen als
auch die breite Öffentlichkeit können den Suchindex 2 durchsuchen. Nach
der Verwendung des
[*Suchwerkzeugs*](https://doc.textgrid.de/search.html?q=Search)
im TextGridLab erhält man eine Liste von Treffern. Diese Suchergebnisse
sind ein akkurates Abbild des Inhalts des TextGridRep. Sie werden in
exakt derselben Reihenfolge angezeigt, in der auch die Ergebnisse einer
Suchanfrage dargestellt werden, die auf der TextGridRep-Website ohne
Login ins TextGridLab durchgeführt wurde.
