# SADE Publish Tool

last modified on Okt 29, 2015

**[For a more userfriendly version, please see the related multilingual article in the TextGrid Lab User Manual: ](https://wiki.de.dariah.eu/display/TextGrid/Publish+Tool+SADE)[Publish Tool SADE](Publish-Tool-SADE_8129840.md)**

## Get started

1. Download and unzip the SADE Publish Server tool:
    [http://ci.de.dariah.eu/jenkins/job/SADE/lastSuccessfulBuild/artifact/dist-utils/antbuild/build/sade-textgrid.tar.gz](http://ci.de.dariah.eu/jenkins/job/SADE/lastSuccessfulBuild/artifact/dist-utils/antbuild/build/sade-textgrid.tar.gz)
2. Browse to the folder "bin" and execute run.bat (on Windows) or
    run.sh (on Unix)

alternative:

1. Register an account at
    [http://sade.textgrid.de/exist/apps/SADE/textgrid/sign-up.html](http://sade.textgrid.de/exist/apps/SADE/textgrid/sign-up.md)
2. Wait for the confirmation email

## View your instance

1. Open a browser and go to
    [http://localhost:8080](http://localhost:8080)
  1. this is the eXist-db dashboard
  2. click on "SADE" go switch to the SADE app and our default
        project including the default template

alternative:

1. click on the link in the email

## Add data to your instance

1. Start the TextGridLab
2. Install the "SADE Publish Tool" from the Marketplace
3. Restart the TextGridLab
4. Go to the preferences and edit the values on the "SADE Publish Tool"
  1. URL: <http://localhost:8080/exist/apps/textgrid-connect/publish/textgrid/>
  2. User: admin
  3. leave the password filed empty

Users of the reference installation please use the vaules provieded in
the email.  
Now you can use the context menu in the navigator to publish data to
your instance.

The software generates a list of your published data like the navigator
within the Lab. Open the menu via a click on the title of website (top
left).  

**You should change the adminstrators password immiedialty on local installations!**  

## Attachments

- [screen2.png](attachments/8128058/8192117.png) (image/png)  
