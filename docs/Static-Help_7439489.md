# Static Help

last modified on Feb 28, 2018

Die allgemeine nicht-spezifische Hilfe kann durch Auswahl von “Hilfe” in
der Menüleiste geöffnet werden. Sie können außerdem “Inhaltsverzeichnis
der Hilfetexte” ![](attachments/40220305/40436667.gif "103-zeige-Hilfe.gif") ,
“Spickzettel” ![](attachments/40220305/40436666.gif "104-zeige-Merkzettel2.gif") ,
“Dynamische Hilfe” und
“Schlüsselwort-Suche” ![](attachments/40220305/40436664.gif "sinfocenter_obj.gif") auswählen.

- [Help Contents](Help-Contents_7439491.md)
- [Cheat Sheets](Cheat-Sheets_7439493.md)
- [Context-Sensitive Help](Context-Sensitive-Help_7439495.md)
- [Keyword Search](Keyword-Search_7439497.md)

## Attachments

- [103-zeige-Hilfe.gif](attachments/7439489/8192237.gif) (image/gif)  
- [104-zeige-Merkzettel2.gif](attachments/7439489/8192238.gif)
(image/gif)  
- [105-zeige-SchluesselwortSuche.gif](attachments/7439489/8192239.gif)
(image/gif)  
- [sinfocenter_obj.gif](attachments/7439489/8192285.gif) (image/gif)  
