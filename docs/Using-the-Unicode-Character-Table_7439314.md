# Using the Unicode Character Table

last modified on Mai 03, 2016

Ein typischer Anwendungsfall für die Unicode-Zeichen-Tabelle ist das
Einfügen spezieller Zeichen in ein XML-Dokument, beispielsweise Σ
(u+2211). Ein Beispiel, wie das Unicode-Zeichen "Σ" in ein XML-Dokument
eingefügt werden kann, ist hier schrittweise beschrieben:

1. Öffnen Sie die [XML-Editor Perspektive](XML-Editor-Perspektive_40220525.md).
2. Doppelklicken Sie das gewünschte XML-Dokument
    im [Navigator](40220331.md), um es zu öffnen.
3. Klicken oder doppelklicken Sie den versteckten Reiter
    “Unicode-Zeichen-Tabelle” neben dem Navigator und
    dem [Metadaten-Editor](Metadaten-Editor_40220458.md), um die
    Unicode-Zeichen-Tabelle-Sicht zu öffnen.
4. Klicken Sie an die Stelle im XML-Dokument, an der das spezielle
    Zeichen eingefügt werden soll.
5. Wählen Sie Σ aus der Tabelle mit Mathematischen Zeichen und Klicken
    Sie “Einfügen”.
6. Das Zeichen Σ wird in das XML-Dokument eingefügt.
