# Fehlerbehebung

last modified on Aug 18, 2015

## Schwierigkeiten

In der Regel kann das TextGridLab einfach heruntergeladen, ausgepackt
und gestartet werden. Wenn's doch mal Probleme gibt: Hier gibt es ein
paar Lösungshinweise für Probleme, die nur auf bestimmten Systemen
auftreten. **Derzeit sind uns keine Windows-8-spezifischen Probleme mit
TextGrid bekannt.**

- [Schwierigkeiten?](#Fehlerbehebung-Schwierigkeiten?)
  - [Das TextGridLab startet nicht: JVM-Fehler 13 (Windows/Linux)](#Fehlerbehebung-DasTextGridLabstartetnicht:JVM-Fehler13(Windows/Linux))
  - [Vor dem ersten Start: TextGridLab.app kann nicht geöffnet werden (OS X)](#Fehlerbehebung-VordemerstenStart:TextGridLab.appkannnichtgeöffnetwerden(OSX))
  - [Browserbasierte Komponenten funktionieren nicht (Linux)](#Fehlerbehebung-BrowserbasierteKomponentenfunktionierennicht(Linux))
  - [Probleme beim Entpacken des TextGridLab (nur Windows XP)](#Fehlerbehebung-ProblemebeimEntpackendesTextGridLab(nurWindowsXP))
  - [Erweiterte Unicode-Zeichen werden nicht korrekt angezeigt (Windows XP)](#Fehlerbehebung-ErweiterteUnicode-Zeichenwerdennichtkorrektangezeigt(WindowsXP))
  - [Hilfesystem funktioniert nicht (Windows)](#Fehlerbehebung-Hilfesystemfunktioniertnicht(Windows))
  - [Installation von (z.B.) oXygen bricht nach einer bestimmten Zeit reproduzierbar ab (Windows)](#Fehlerbehebung-Installationvon(z.B.)oXygenbrichtnacheinerbestimmtenZeitreproduzierbarab(Windows))
  - [Anti-Virus-Software (Windows)](#Fehlerbehebung-Anti-Virus-Software(Windows))
- [Fortgeschrittene Installationsszenarien](#Fehlerbehebung-FortgeschritteneInstallationsszenarien)
  - [Das TextGridLab für mehrere Nutzer installieren](#Fehlerbehebung-DasTextGridLabfürmehrereNutzerinstallieren)
  - [Deploy a portable TextGridLab including Java](#Fehlerbehebung-DeployaportableTextGridLabincludingJava)

### Das TextGridLab startet nicht: JVM-Fehler 13 (Windows/Linux)

Sie verwenden die falsche Architekturvariante (32/64-Bit) des
TextGridLab für Ihre Plattform. Laden Sie die andere herunter, sie wird
voraussichtlich problemlos starten.

Die TextGridLab-Variante muss zur Java-Variante auf Ihrem System passen.
**Windows-Benutzer benötigen in der Regel die 32-Bit-Version**, denn
Oracles Standard-Java-Installationsprogramm scheint auch auf
64-Bit-Rechnern eine 32-Bit-Java-Umgebung zu installieren.

### Vor dem ersten Start: TextGridLab.app kann nicht geöffnet werden (OS X)

Es kann vorkommen, dass vor dem ersten Start nach dem Entpacken auf OS X
ein Hinweis angezeigt wird, dass das Programm nicht geöffnet werden
könne, da es von einem nicht zertifizierten Entwickler stammt.

In diesem Fall klicken Sie mit der rechten Maustaste auf TextGridLab.app
und wählen Sie im Kontextmenü "Öffnen". Dieser Schritt ist nur dieses
eine Mal notwendig.

### Browserbasierte Komponenten funktionieren nicht (Linux)

Bleibt unter Linux der Willkommensbildschirm leer und beim Login kommen
Fehlermeldungen, so muss das Paket [WebKit-GTK 1.x](apt://libwebkitgtk-1.0-0 "Installiert das Paket libwebkitgtk-1.0-0, falls auf Ihrem System unterstützt")
installiert werden. Auf Ubuntu bzw. Debian können Sie das mit dem o.g.
Link tun oder indem Sie an der Kommandozeile

    sudo apt-get install libwebkitgtk-1.0-0

eingeben
\[[TG-1903](https://develop.sub.uni-goettingen.de/jira/browse/TG-1903)\].
Auf Fedora \[[\#9433](https://projects.gwdg.de/issues/9433)\] muss
zusätzlich ein symbolischer Link vom alten Namen `libwebkit-1.0.so.2`
gesetzt werden:

``` syntaxhighlighter-pre
sudo yum install webkitgtk.i686
cd /usr/lib      # odr cd /usr/lib64 auf 64-Bit-Versionen
ln -s libwebkitgtk-1.0.so.0 libwebkit-1.0.so.2
```

Für TextGridLab-Versionen bis 2.0.5 muss zusätzlich der SWT-Patch
installiert werden, wie in [Bug \#9717 Kommentar
4](https://projects.gwdg.de/issues/9717#note-4) beschrieben. Aktuelle
TextGridLab-Versionen enthalten den Patch bereits.

### Probleme beim Entpacken des TextGridLab (nur Windows XP)

Wenn man unter Windows XP mit dem eingebauten Entpacker das
TextGridLab-ZIP extrahieren möchte und als Zielverzeichnis einen relativ
langen Pfad wählt
(`C:\Dokumente und Einstellungen\Maximilian Mustermann\Downloads\TextGridLab-2.0.4-win32`)
, kann es zu merkwürdigen Fehlern kommen: Der Extrahierassistent fragt
nach einem Passwort, und beim Entpacken per Drag&Drop fehlen einige
Dateien (z.B. textgridlab.exe). Es gibt zwei alternative Lösungen:

- einen besseren Entpacker benutzen, z.B.
    [7-Zip](http://www.7-zip.de/)
- in einen kürzeren Pfad entpacken, z.B. `C:\TEMP`

### Erweiterte Unicode-Zeichen werden nicht korrekt angezeigt (Windows XP)

Unicode-Zeichen jenseits der BMP (d.h., Codepoints über 0xFFFF wie etwa
Notensymbole oder gotische Schriftzeichen) werden mit sogenannten
*Surrogatpaaren* codiert, d.h. durch zwei aufeinanderfolgende
Sonderzeichen aus speziellen Unicode-Blöcken. Windows XP unterstützt die
Anzeige dieser Zeichen, dies ist jedoch im Auslieferungszustand nicht
eingeschaltet. (In neueren Windowsversionen sowie unter Mac OS X und
Linux ist die Unterstützung vorhanden und aktiviert).

Wenn unter Windows XP statt bestimmter Unicodezeichen zwei Ersatzzeichen
angezeigt werden, schalten Sie Surrogate-Unterstützung wie folgt ein:

1.Laden Sie die Datei
    [Unicode-Surrogate-Support.reg](attachments/18809102/18940134.reg)
    herunter.
2.Doppelklicken Sie die Datei und bestätigen Sie das Dialogfenster
3.Starten Sie Ihr Windows neu

(vgl. auch [den entsprechenden Wikipedia-Eintrag](http://de.wikipedia.org/wiki/Hilfe:UTF-8-Probleme#Warum_werden_bei_Unicode-Charakteren_.C3.BCber_U.2B10000_trotz_installierter_Schriften_zwei_Rechtecke_angezeigt.3F))
\[[TG-1940](https://develop.sub.uni-goettingen.de/jira/browse/TG-1940)\]

### Hilfesystem funktioniert nicht (Windows)

Windows-Benutzern, die eine Persönliche Firewall benutzen, kann es
passieren, dass das Inhaltsverzeichnis der Hilfe und die Hilfetexte
selbst nicht angezeigt werden können. Der Grund dafür ist, dass das
TextGridLab-Hilfesystem einen lokalen Webserver startet, der dann die
Hilfetexte wahlweise Ihrem Lieblingsbrowser oder einem internen
Hilfefenster ausliefert.

Um das Hilfesystem in solchen Fällen zum Laufen zu bringen, müssen Sie
das TextGridLab in Ihrer persönlichen Firewall freischalten. Alternativ
können Sie unser [Online-Handbuch](User-Manual-2.0.md)
verwenden.

### Installation von (z.B.) oXygen bricht nach einer bestimmten Zeit reproduzierbar ab (Windows)

Deaktivieren Sie vorübergehend Ihren Virenscanner (Problem mit Avira
beobachtet).

Hintergrund: Um oXygen zu installieren muss eine recht große (\~ 100MB)
Plugin-Datei von deren Servern heruntergeladen werden. Normalerweise ist
das kein Problem, Aviras Antiviren-Filter scheint indes diesen Download
zunächst an einem temporären Ort zwischenzuspeichern und die Datei nach
dem kompletten Download nach Viren zu durchsuchen, bevor es sie an den
Installationsmechanismus des TextGridLab weiterleitet. Für das Lab sieht
das aus, als reagierten die oXygen-Server nicht auf die Anfrage des
Labs, und es wird nach einer gewissen Zeit mit einem Timeout-Fehler
abbrechen.

### Anti-Virus-Software (Windows)

Deaktivieren Sie vorübergehend Ihren Virenscanner (Problem mit Avira
beobachtet).

Neben den Problemen bei der Installation von oXygen treten auch
Schwierigkeiten bei der Nutzung des TextGridLabs auf. Die betreffende
Komponente aus Avira ist der "Browser-Schutz", welcher das Laden von
externen Ressourcen im Lab verhindert. So kann zum Beispiel die
Vorschauansicht nicht geöffnet werden, bzw. ihr Status verbleibt bei
"55%". Eine vorübergehende Deaktivierung des Virenscanners **und** des
Browser-Schutzes schafft Abhilfe. Man kann das TextGridLab auch auf eine
Whitelist setzten und somit jegliches scannen unterbinden. Beachten Sie
hier, dass auch die externen Ressourcen auf die Whitelist des
"Browser-Schutzes" gesetzt werden müssen.

## Fortgeschrittene Installationsszenarien

### Das TextGridLab für mehrere Nutzer installieren

Das TextGridLab benötigt einen beschreibbaren Bereich auf Ihrer
Festplatte, um Einstellungen und Arbeitsdaten zu speichern. Es legt
standardmäßig einen Ordner mit dem Namen workspace im aktuellen
Verzeichnis an – dies ist üblicherweise der Ordner, von dem aus das
TextGridLab gestartet wurde. Wenn das Lab in diesem Ordner nicht
schreiben kann (beispielsweise weil ein Administrator das TextGridLab
für alle Nutzer in einem Verzeichnis mit nur-lesendem Zugriff
installiert), müssen Sie eine Alternative angeben. Hierfür gibt es
folgende Möglichkeiten:

1. Als Administrator können Sie die Datei textgridlab.ini im Ordner des
    TextGridLab bearbeiten. Geben Sie *vor* der Zeile, die *den
    Ausdruck -*jvmArgs, enthält, folgende zwei Zeilen ein:

    [TABLE]

2. Diese Befehle bewirken, dass das TextGridLab das Verzeichnis
    TextGridLab-Workspace im persönlichen Verzeichnis des aktuellen
    Benutzers verwendet. Die erste Zeile muss dabei immer -data lauten,
    die zweite Zeile enthält das Zielverzeichnis, dabei ist @user.home
    ein Makro, das auf das Benutzerverzeichnis verweist.

3. Alternativ können Sie das TextGridLab mit den Argumenten
    -data @user.home/TextGridLab-Workspace (oder ähnliches) starten.

### Deploy a portable TextGridLab including Java

Manchmal besteht der Wunsch, das TextGridLab in einer Umgebung
einzusetzen, in der auf dem Computer keine aktuelle Version von Java
installiert ist – beispielsweise in einem Workshop. In diesem Fall ist
es möglich, eine TextGridLab-Installation zu erstellen, die ihre eigene
Java-Laufzeitumgebung beinhaltet, beispielsweise auf USB-Laufwerken.
Speichern Sie hierfür die JRE in ein Unterverzeichnis des TextGridLab
mit dem Namen jre. Das TextGridLab wird automatisch diese
Java-Installation verwenden.

- [Schwierigkeiten?](#Fehlerbehebung-Schwierigkeiten?)
  - [Das TextGridLab startet nicht: JVM-Fehler 13 (Windows/Linux)](#Fehlerbehebung-DasTextGridLabstartetnicht:JVM-Fehler13(Windows/Linux))
  - [Vor dem ersten Start: TextGridLab.app kann nicht geöffnet werden (OS X)](#Fehlerbehebung-VordemerstenStart:TextGridLab.appkannnichtgeöffnetwerden(OSX))
  - [Browserbasierte Komponenten funktionieren nicht (Linux)](#Fehlerbehebung-BrowserbasierteKomponentenfunktionierennicht(Linux))
    - [Probleme beim Entpacken des TextGridLab (nur Windows XP)](#Fehlerbehebung-ProblemebeimEntpackendesTextGridLab(nurWindowsXP))
    - [Erweiterte Unicode-Zeichen werden nicht korrekt angezeigt (Windows XP)](#Fehlerbehebung-ErweiterteUnicode-Zeichenwerdennichtkorrektangezeigt(WindowsXP))
    - [Hilfesystem funktioniert nicht (Windows)](#Fehlerbehebung-Hilfesystemfunktioniertnicht(Windows))
    - [Installation von (z.B.) oXygen bricht nach einer bestimmten Zeit reproduzierbar ab (Windows)](#Fehlerbehebung-Installationvon(z.B.)oXygenbrichtnacheinerbestimmtenZeitreproduzierbarab(Windows))
    - [Anti-Virus-Software (Windows)](#Fehlerbehebung-Anti-Virus-Software(Windows))
- [Fortgeschrittene Installationsszenarien](#Fehlerbehebung-FortgeschritteneInstallationsszenarien)
  - [Das TextGridLab für mehrere Nutzer installieren](#Fehlerbehebung-DasTextGridLabfürmehrereNutzerinstallieren)
  - [Deploy a portable TextGridLab including Java](#Fehlerbehebung-DeployaportableTextGridLabincludingJava)

## Attachments

- [Unicode-Surrogate-Support.reg](attachments/40220224/40436626.reg)
(text/x-ms-regedit)  
