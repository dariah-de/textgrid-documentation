# Unicode-Zeichen-Tabelle

last modified on Jul 31, 2015

Die Unicode-Zeichen-Tabelle ermöglicht es Ihnen, Zeichen aus den
Unicode-Zeichensätzen zu suchen, zu kopieren und in den aktiven Editor
oder die Zwischenablage einzufügen. Sie können Ihre eigenen von Ihnen
angepassten Sätze von Unicode-Zeichen für die Nutzung in der
Unicode-Zeichen-Tabelle erstellen.

- [Unicode-Zeichen-Tabelle öffnen](40220677.md)
- [Unicode-Zeichen-Tabelle-Sicht](Unicode-Zeichen-Tabelle-Sicht_40220679.md)
- [Unicode-Zeichen-Tabelle verwenden](Unicode-Zeichen-Tabelle-verwenden_40220689.md)
- [Interaktion der Unicode-Zeichen-Tabelle-Sicht mit anderen Komponenten](Interaktion-der-Unicode-Zeichen-Tabelle-Sicht-mit-anderen-Komponenten_40220691.md)
