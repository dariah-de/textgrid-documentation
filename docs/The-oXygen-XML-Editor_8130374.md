# The oXygen XML Editor

last modified on Mai 04, 2016

Der oXygen-XML-Editor ist ausführlich beschrieben unter

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)

Wenn oXygen in TextGrid geöffnet ist, wirkt sich dies auf die Menüleiste
und die Werkzeugleiste des TextGridLab aus.

- [Menu Bar of the oXygen XML Editor](Menu-Bar-of-the-oXygen-XML-Editor_8130376.md)
- [Toolbar of the oXygen XML Editor](Toolbar-of-the-oXygen-XML-Editor_8130378.md)
- [Context Menu of the oXygen XML Editor](Context-Menu-of-the-oXygen-XML-Editor_8130380.md)
