# Text-Bild-Link-Editor verwenden

last modified on Aug 03, 2015

Die folgenden Kapitel beschreiben den Verknüpfungsvorgang von Bildern
und Texten mit dem Text-Bild-Link-Editor.

- [Bild und Text öffnen](40220661.md)
- [Arbeitsbereich wählen](40220663.md)
- [Markierungen im Bild erstellen](Markierungen-im-Bild-erstellen_40220665.md)
- [Verknüpfungen zwischen Text und Bild erstellen](40220667.md)
- [Verknüpfungen zwischen Text und Bild korrigieren und löschen](40220669.md)
- [Text-Bild-Link-Objekt öffnen und bearbeiten](40220671.md)
- [Verknüpfungen in ein anderes Projekt kopieren](50630210.md)
