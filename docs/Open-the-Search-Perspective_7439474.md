# Open the Search Perspective

last modified on Feb 28, 2018

Die Suche-Perspektive kann auf verschiedene Weisen geöffnet werden:

- Klicken Sie das Symbol auf
    dem [Startbildschirm](Startbildschirm-und-Login_40220219.md)
- Wählen Sie “Werkzeuge \> Suche” in der Menüleiste
- Klicken
    Sie ![](attachments/40220287/40436661.png "070-zeige-SuchwerkzeugPerspektive2.png") in
    der Werkzeugleiste

## Attachments

- [070-zeige-SuchwerkzeugPerspektive2.png](attachments/7439474/7766191.png)
(image/png)  
