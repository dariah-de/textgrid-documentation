# Context Menu of the WYSIWYM View

last modified on Mai 04, 2016

Das Kontextmenü der WYSIWYM-Ansicht ist dem [*Kontextmenü der Quelle-Ansicht*](https://doc.textgrid.de/search.html?q=Context+Menu+of+the+Source+View) ähnlich.
Allerdings stehen manche Optionen nur hier zur Verfügung, beispielsweise
„Element einfügen“. Über das Kontextmenü ist es außerdem möglich, mit
anderen Modulen des TextGridLab zu interagieren. Dies gilt standardmäßig
für
die [*Wörterbuchsuche*](https://doc.textgrid.de/search.html?q=Dictionary+Search),
es ist außerdem möglich für die
Werkzeuge [*Lemmatisierer*](https://doc.textgrid.de/search.html?q=Lemmatizer), [*LEXUS*](https://doc.textgrid.de/search.html?q=LEXUS), [*Cosmas
II*](https://doc.textgrid.de/search.html?q=Cosmas+II), [*ANNEX*](https://doc.textgrid.de/search.html?q=ANNEX) und [*SADE*](https://doc.textgrid.de/search.html?q=Publish+Tool+SADE),
sofern sie installiert sind.
