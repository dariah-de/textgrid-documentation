# Interaction of Cosmas II with Other Components

last modified on Mai 04, 2016

Cosmas II kann direkt aus
dem [XML-Editor](XML-Editor_40220521.md) über das Kontextmenü
der [Quelle-Sicht](Quelle-Ansicht_40220547.md) oder
der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) geöffnet werden.
