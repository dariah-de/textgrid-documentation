# Revisionen verwenden

last modified on Jul 28, 2015

Eine neue Revision eines geöffneten und veränderten Dokuments kann durch
Auswahl von “Datei \> Als neue Revision speichern” in der Menüleiste
erstellt werden. Wenn ein Nutzer eine Datei speichern möchte, die von
einem anderen Nutzer verändert worden ist, gibt TextGrid eine Warnung
aus. Der Nutzer kann dann seine neue Version als neues Objekt oder eine
neue Revision speichern.

Alle Revisionen eines Objekts können durch Rechtsklicken des Objekts im
Navigator oder der Suchergebnisse-Sicht und durch Auswahl von
“Revisionen anzeigen” im Kontextmenü angezeigt werden. Eine Liste mit
allen Revisionen wird angezeigt mit Informationen wie Titel des
Dokuments, Projekt, von wem die Daten bereitgestellt werden und wann die
Revision erstellt wurde. Jede Revision kann durch einen URI, der die
Revisionsnummer beinhaltet, d. h. die Zahl nach dem Punkt am Ende der
Zeichenkette (beispielsweise *textgrid:17od6.5*), referenziert werden.
Um auf die früheste verfügbare Revision zu referenzieren, müssen Sie
nach dem URI ohne Revisionsnummer suchen (beispielsweise
*textgrid:17od6*).
