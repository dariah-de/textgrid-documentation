# Menu Bar of the MEI Score Editor Perspective

last modified on Mai 04, 2016

When you open the Note Editor and make changes in the current document,
certain items in the menu bar will be activated.

- [Edit](Edit_7439840.md)
- [Window](Window_7439842.md)
