# Advanced Search

last modified on Feb 28, 2018

Im “Erweiterte Suche”-Modus können Sie eine Suche mithilfe der zur
Verfügung gestellten Eingabemaske durchführen. Diese Eingabemaske
besteht aus drei verschiedenen Bereichen: Metadaten, Volltext und
Baseline Encoding. Jeder Bereich kann über ein Kontrollkästchen
ausgewählt werden. Ausgewählte Bereiche werden in der Suchanfrage
berücksichtigt. Die Metadaten- und Volltext-Suche können durch
Aktivierung beider Kontrollkästchen miteinander kombiniert werden,
während die Suche mit Baseline Encoding nur getrennt durchgeführt werden
kann.

- In der erweiterten Volltext-Suche wird nur der Textbereich eines
    Objekts durchsucht. Geben Sie den Suchbegriff in das Eingabefeld
    ein. Wenn Sie mehr als ein Wort eintragen, werden die Worte durch
    den logischen Operator „and“(„und“) miteinander verknüpft. Durch die
    Spezifikation des Wortabstands können Sie festlegen, ob die Wörter
    nur in einem Dokument gefunden werden sollen oder ob sie einen
    bestimmten Abstand voneinander haben sollen.
- Der Bereich Metadaten bietet bei der Erweiterten Suche die
    Möglichkeit, über verschiedene Metadatenfelder zu suchen. Die
    Eingabemaske ist als Matrix aufgebaut, jede Zeile repräsentiert
    dabei die Suche nach einem Metadatenfeld. Das Feld kann über ein
    Aufklappmenü am Zeilenanfang ausgewählt und der Suchbegriff in das
    Eingabefeld eingetragen werden. Diese Eingabefelder sind durch den
    logischen Operator “or” (“oder”) miteinander verknüpft, die
    verschiedenen Zeilen über den logischen Operator „and“ („und“). Auf
    die suchbaren Metadatenfelder können über ein Aufklappmenü
    zugegriffen werden. Sie bestehen aus:
  - Daten bereitgestellt von: eine Person, die Daten nach TextGrid
        übermittelt, identifiziert durch ihre Nutzer-ID
        (beispielsweise [*testuser@textgrid.de*](mailto:testuser@textgrid.de))
  - Identifizierer: ein Identifizierer in einem nicht
        TextGrid-bezogenen Kontext (beispielsweise eine ISBN oder ISSN,
        eine Signatur, eine Registrierung oder eine Inventarnummer)
  - Anmerkungen: Zusätzliche Informationen bezüglich des Objekts
  - Projekt: das TextGrid-Projekt, zu dem das Objekt gehört
  - Rechteinhaber: die Person oder Organisation, die das
        Urheberrecht für ein Objekt besitzt
  - TextGrid-URI: der interne “Uniform Resource Identifier” eines
        Objekts
  - Titel: der Titel eines Objekts

Die aufklappbaren Checklisten “Texttypen”, “Inhaltstypen” und “Sprachen”
enthalten auswählbare Baumstrukturen, die genutzt werden können, um die
Suche einzugrenzen. Unter Zuhilfenahme des Felds “Datum der Erstellung”
können Sie einen zeitlichen Rahmen angeben.

- Mit der Baseline-Encoding-Suche
    (siehe [*baseline-all-en.pdf*](https://dev2.dariah.eu/wiki/download/attachments/7439482/baseline-all-en.pdf?version=1&modificationDate=1358512078403&api=v2))
    können nur TextGrid-Objekte mit Baseline Encoding gefunden werden.
    Die Eingabemaske ist als Matrix aufgebaut. In das erste Eingabefeld
    einer Zeile muss das gesuchte Element eingetragen werden. In die
    weiteren Eingabefelder der Zeile kann der gesuchte Inhalt
    eingetragen werden. Die Eingabefelder sind mit dem logischen
    Operator „or“ („oder“) verknüpft, die verschiedenen Zeilen über den
    logischen Operator „and“ („und“).

## Attachments

- [baseline-all-en.pdf](attachments/7439482/12419651.pdf)
(application/octetstream)  
