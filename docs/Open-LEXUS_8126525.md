#Open LEXUS

last modified on Mai 04, 2016

Sie können LEXUS auf verschiedene Weisen öffnen:

1. Wählen Sie “Werkzeuge \> Sicht anzeigen \> LEXUS” in der Menüleiste.
2. Klicken
    Sie **![](attachments/40220738/40436898.png "lexus.png")** in der
    Werkzeugleiste.
3. Wenn ein Dokument in
    der [Quelle-Sicht](Quelle-Ansicht_40220547.md) oder
    der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) des [XML-Editor](XML-Editor_40220521.md) geöffnet
    ist, kann LEXUS im Kontextmenü ausgewählt werden.

## Attachments

- [lexus.png](attachments/8126525/8192311.png) (image/png)  
