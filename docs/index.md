# TextGrid – Virtual Research Environment for the Humanities

The joint project [TextGrid](https://textgrid.de/) aims to support
access to and exchange of data in the arts and humanities by means of
modern information technology. These pages offer up-to-date
documentation for TextGrid users, developers, and administrators; more
information and introductory material can be found on the [TextGrid homepage](https://textgrid.de).

## User Documentation

### [User Manual 2.0](User-Manual-2.0.md)

### [Nutzerhandbuch 2.0](Nutzerhandbuch-2.0.md)

### TextGrid Tutorials

- [TextGrid Tutorials](https://textgrid.de/de/web/guest/tutorials): Web-, Print- & Video-Version

## Technical Documentation

### Architecture

1. [Introduction](Architecture-Introduction_9013280.md)
2. TextGrid Basics
    1. [The Logical View](The-Logical-View_9013285.md)
    2. [The Physical View](The-Physical-View_9013294.md)
    3. [Search Index and Baseline Encoding](Search-Index-and-Baseline-Encoding_9013298.md)
    4. [The Three Pillars of TextGrid](The-Three-Pillars-of-TextGrid_9013302.md)
    5. [Rights Management Issues and Publication](Rights-Management-Issues-and-Publication_9013304.md)
3. [Metadata](Metadata.md)
    1. Used technologies
    2. Used formats
    3. Used frameworks, bibs, server, apps, etc.
    4. Concept Index

### Frontend: TextGrid Laboratory

- TextGridLab is based on the [Eclipse Rich Client Platform](https://www.eclipse.org/), currently on the 4.x stream (Eclipse 4.4 / Juno), based on the traditional RCP model (not e4).
- [Understanding Updates](Understanding-TextGridLab-Updates_11473539.md)

#### Getting Started

- Setup your [TextGridLab Development Environment](TextGridLab-Development-Environment_8127335.md)
- Accessing the Repository / [TextGridLab Data Model](TextGridLab-Data-Model_38085351.md) and [Core Plug-Ins](https://gitlab.gwdg.de/dariah-de/textgridlab/core)

#### Creating a tool for the TextGridLab

- Getting started: Setup your [TextGridLab Development Environment](TextGridLab-Development-Environment_8127335.md)
- [Typical Structure of a TextGridLab tool](Typical-Structure-of-a-TextGridLab-tool_40221300.md)
- [Accessing TextGrid Data Structures from your Lab plugin](Accessing-TextGrid-Data-Structures-from-your-Lab-plugin_40221315.md)
- [Contributing to the UI](Contributing-to-the-UI_40221329.md)
- [Integrating Browser Based Tools](Integrating-Browser-Based-Tools_40221335.md)

### Backend: TextGrid Repository Services

The main site of the technical documentation you can find here:
[https://textgridlab.org/doc/](https://dev.textgridlab.org/doc/)

#### Core Services

- [TG-auth\*](https://textgridlab.org/doc/services/submodules/tg-auth/docs/)
- [TG-crud](https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/)
- [TG-search](https://textgridlab.org/doc/services/submodules/tg-search/docs/)

#### Import and Dissemination

- [TextGrid Import](https://textgridlab.org/doc/services/submodules/kolibri/kolibri-addon-textgrid-import/docs/)
- [TextGrid Aggregator](https://textgridlab.org/doc/services/submodules/aggregator/docs/)
- [TG-oaipmh](https://textgridlab.org/doc/services/submodules/oai-pmh/docs_tgrep/)
- [TG-digilib](https://textgridlab.org/doc/services/submodules/tg-digilib/docs_tgrep/)
- [IIIF Implementation](https://textgridlab.org/doc/services/submodules/tg-iiif-metadata/docs_tgrep/)
- [Triplestore Queries](https://textgridlab.org/doc/services/submodules/tg-iiif-metadata/docs_tgrep/)

#### Extended APIs

- [TG-pid](https://textgridlab.org/doc/services/submodules/tg-pid/docs_tgrep/)
- [TG-publish](https://textgridlab.org/doc/services/submodules/kolibri/kolibri-tgpublish-service/docs/)

#### Service Clients

- [TextGrid Java Clients](https://textgridlab.org/doc/services/submodules/textgrid-java-clients/docs/)
- [TextGrid Python Clients](https://dariah-de.pages.gwdg.de/textgridrep/textgrid-python-clients/docs/)

#### Resolving

- [Resolving TextGrid URIs and PIDs](https://textgridlab.org/doc/services/resolver.html)

## TextGrid Repository

### [Organisational Infrastructure](https://textgridlab.org/doc/services/organizational-infrastructure.html)

...documents mission, organisational infrastructure, and long-term
preservation issues as well as the integration of the designated
community of the TextGrid Repository.

### [Data Policies](https://textgridlab.org/doc/services/data-policies.html)

...deals with collection development and preservation policies, data
quality and re-use, and ethical and disciplinary norms.

### [Digital Object Management](https://textgridlab.org/doc/services/digital-object-management.html)

...gives an overview about the main technologies and procedures used for
an appropriate data management. Import and publish workflows are
described and related to the repository architecture of TextGrid.

## Resources

### TextGrid

- Repository: [TextGridRep](https://textgridrep.org/)
- Homepage: [TextGrid](https://textgrid.de/)
- Download Client Software: [TextGridLab](https://textgrid.de/download)

### Development

- Sources: [TextGridLab GitLab](https://gitlab.gwdg.de/dariah-de/textgridlab/textgrid-laboratory), [TextGridRep GitLab](https://gitlab.gwdg.de/dariah-de/textgridrep)
- Bug reports and feature requests: [TextGridLab Issues](https://gitlab.gwdg.de/dariah-de/textgridlab/textgrid-laboratory/-/issues)
- Continuous Integration: [TextGridLab](https://ci.de.dariah.eu/jenkins/view/TextGridLab%20Build/)

### Contact

- User Support and Feedback: [textgrid-support@gwdg.de](mailto:textgrid-support@gwdg.de)
- Cooperation and Collaboration requests: [anfragen@textgrid.de](mailto:anfragen@textgrid.de)
