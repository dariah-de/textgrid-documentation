# Properties View of the MEI Score Editor

last modified on Mai 04, 2016

This view shows the modifiable properties of the elements from
the [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) in
two columns (i.e. attributes of selected elements in the Outline View
and their appropriate values).

The information displayed in the Properties View will change depending
on the preselected (highlighted) element in the Outline View. The
properties “ID” and "N" (number) are basic and static but their values
depend on the preselected element type. If you make changes in the Value
column, the modifications will be displayed immediately in the [Score View](Musical-Score-View_7439823.md). To validate all modifications in
the Value columns, please click anywhere in the [Score View](Musical-Score-View_7439823.md).

![](attachments/7439882/9240717.png)

|                       |
|-----------------------|
| MEISE Properties View |

- [Features of the Properties View](Features-of-the-Properties-View_7439886.md)
- [Title Bar of the MEISE Properties View](Title-Bar-of-the-MEISE-Properties-View_7439890.md)
- [Context Menu of the MEISE Properties View](Context-Menu-of-the-MEISE-Properties-View_7439895.md)

## Attachments

- [meise-f8_properties view.png](attachments/7439882/8192023.png) (image/png)  
- [mei-propertiesview.png](attachments/7439882/9240717.png) (image/png)  
