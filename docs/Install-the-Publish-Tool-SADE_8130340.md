# Install the Publish Tool SADE

last modified on Mai 03, 2016

Die Installation eines Plugins wird im TextGrid Benutzerhandbuch
eingehend beschrieben und ist abrufbar unter
[https://doc.textgrid.de/search.html?q=Installing+Additional+Tools](https://doc.textgrid.de/search.html?q=Installing+Additional+Tools).
In diesem Fall muss man das Plugin „SADE Publish Tool" auswählen und
installieren. Anschließend bekommt man ein neues Icon in der
Symbolleiste angezeigt, im Kontextmenü der im Navigator angezeigten
Objekte erscheint ein die Option „Publish to SADE" und Nutzerinnen
können in den [Einstellungen](Preferences_9012401.md) die
Konfiguration des Plugins vornehmen.
