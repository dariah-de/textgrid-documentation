# Software installieren und entfernen

last modified on Aug 18, 2015

**Software direkt von Update-Websites installieren**

Um eine Update-Website zu durchsuchen und Software, die eventuell noch
nicht im [Marketplace](40220207.md) zur Verfügung steht, direkt von
der Seite zu installieren, müssen Sie:

1. den Assistenten über den Unterpunkt „Neue Software installieren…“ im
    Menü „Hilfe“ öffnen,
2. die Update-Website, die Sie durchsuchen wollen, aus dem Aufklappmenü
    unter “Arbeiten mit:” auswählen, oder ihren URL (siehe obige Liste)
    in das Eingabefeld eingeben und die Eingabetaste drücken,
3. in der Baumstruktur darunter das/die Werkzeug(e) auswählen, das/die
    Sie installieren wollen und
4. die weiteren Schritte im Assistenten unter Verwendung der
    Schaltflächen „Weiter“ und „Fertigstellen“ durchführen.

Nach der Installation von Software werden Sie das TextGridLab neu
starten müssen.

Sie sind dabei nicht auf Werkzeuge des TextGridLab beschränkt –
theoretisch können Sie auf diese Weise jedes beliebige Eclipse-Plug-in
in das TextGridLab installieren. Werkzeuge, die weder auf unseren
Update-Websites noch in unserem Marketplace angeboten werden, wurden
jedoch nicht auf ihre Funktionsfähigkeit im TextGridLab geprüft und
funktionieren daher möglicherweise nicht.

**Werkzeuge entfernen**

Sie können Werkzeuge, die Sie nicht mehr benötigen, über den Assistenten
„Installationsdetails“ aus Ihrer Installation des TextGridLab entfernen:

1. Wählen Sie den Unterpunkt “Info über TextGridLab” im Menü „Hilfe“
    (oder „TextGridLab“ unter Mac OS).
2. Klicken Sie die Schaltfläche “Installationsdetails, um den
    entsprechenden Assistenten zu öffnen.
3. Wählen Sie unter dem Reiter “Installierte Software” das oder die
    Werkzeug(e) aus, das Sie deinstallieren wollen, und klicken Sie
    „Deinstallieren“.
4. Der Assistent “Deinstallieren” listet alle Elemente auf, die
    deinstalliert werden sollen. Klicken Sie „Fertigstellen“.
5. Nach dem Deinstallieren sollten Sie das TextGridLab wie empfohlen
    neu starten.

Nehmen Sie zur Kenntnis, dass Sie keine Werkzeuge entfernen können, die
zur den Kernkomponenten des TextGridLab zählen oder von anderen
Werkzeugen benötigt werden.
