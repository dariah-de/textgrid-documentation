# Unicode-Zeichen-Tabelle-Sicht

last modified on Jul 31, 2015

Die Unicode-Zeichen-Tabelle besteht aus seiner Werkzeugleiste und einem
Textfeld in der Kopfzeile, einer Tabelle im Hauptteil und einem
Informationsfeld mit Schaltflächen in der Fußleiste.

![](attachments/40220679/40436863.png "uct-chartable-view.png")

|                             |
|-----------------------------|
| **Unicode-Zeichen-Tabelle** |

- [Hauptteil der Unicode-Zeichen-Tabelle-Sicht](Hauptteil-der-Unicode-Zeichen-Tabelle-Sicht_40221970.md)
- [Informationsfeld der Unicode-Zeichen-Tabelle-Sicht](Informationsfeld-der-Unicode-Zeichen-Tabelle-Sicht_40221972.md)
- [Zeichensatz-Anpassung-Editor](Zeichensatz-Anpassung-Editor_40220687.md)
- [Werkzeugleiste und Kopfzeile der Unicode-Zeichen-Tabelle-Sicht](Werkzeugleiste-und-Kopfzeile-der-Unicode-Zeichen-Tabelle-Sicht_40221968.md)

**Troubleshooting:** If you are using Windows XP and the character table
displays duplicate note symbols or two rectangles in each cell, [you need to enable support for Unicode Surrogate Characters in your operation system](https://develop.sub.uni-goettingen.de/jira/browse/TG-1940?focusedCommentId=28474&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#action_28474).
To do so,

1. download the file
    [Unicode-Surrogate-Support.reg](attachments/40220679/40436862.reg),
2. double-click it and confirm the dialog box,
3. restart Windows XP.

## Attachments

- [Unicode-Surrogate-Support.reg](attachments/40220679/40436862.reg)
(text/x-ms-regedit)  
- [uct-chartable-view.png](attachments/40220679/40436863.png)
(image/png)  
