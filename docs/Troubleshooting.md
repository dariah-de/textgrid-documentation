# TextGridLab Troubleshooting

last modified on Dez 14, 2023

In der Regel kann das TextGridLab einfach heruntergeladen, ausgepackt und gestartet werden. Wenn's doch mal Probleme gibt: Hier gibt es ein paar Lösungshinweise für Betriebssystem-übergreifende und auch -spezifische Probleme. Ältere und für aktuelle Versionen des TextGridLab nicht mehr relevante Hinweise finden Sie auf der Seite [Troubleshooting (old)](Troubleshooting_old.md).

## Übergreifend

### Heimatinstitutionen zurücksetzen

Wenn bei der Auswahl in der Anmeldung die Heimatinstitution nicht geändert werden kann, lässt sich dies auf diesem Weg bewerkstelligen:

1. Den internen Browser des TextGridLab vom Menu aus aufrufen (dort    wird die Information als Cookie gespeichert): **Werkzeuge** \> **Sicht anzeigen** \> **Andere** \> **Allgemein** \> **Interner Web Browser**

2. In diesem Browser zur Seite [https://auth.de.dariah.eu/CDS/WAYF](https://auth.de.dariah.eu/CDS/WAYF) navigieren.

3. Organisation zurücksetzen bzw. die korrekte setzen.

### Das TextGridLab startet nicht: JVM-Fehler 13 (Windows/Linux)

Sie verwenden die falsche Architekturvariante (32/64-Bit) des TextGridLab für Ihre Plattform. Laden Sie die andere herunter, sie wird voraussichtlich problemlos starten.

Die TextGridLab-Variante muss zur Java-Variante auf Ihrem System passen. **Windows-Benutzer benötigen in der Regel die 32-Bit-Version**, denn Oracles Standard-Java-Installationsprogramm scheint auch auf 64-Bit-Rechnern eine 32-Bit-Java-Umgebung zu installieren.

### XML Vorschau funktioniert nicht: TransformerConfigurationException

In den Lab-Einstellungen schauen Sie nach **XML** \> **Experimentelle Features** und ändern die Adresse der XSLT Datei in: [https://raw.githubusercontent.com/TEIC/Stylesheets/released/html/html.xsl](https://raw.githubusercontent.com/TEIC/Stylesheets/released/html/html.xsl)

Danach öffnen Sie die Datei erneut.

## Windows

### TextGridLab startet nicht: The TextGridLab executable launcher was unable to locate its companion shared library

Sie haben das TextGridLab nicht korrekt entpackt. Nach dem download, benutzen Sie bitte ein [funktionierendes Entpackprogramm wie z.B. 7zip](http://a%20proper%20unzip%20tool%20like%207zip), um den Inhalt des TextGridLab-Archivs auzupacken – öffnen Sie die ZIP-Datei nicht einfach im Explorer, und benutzen Sie insbesondere unter XP nicht das eingebaute Entpackprogramm. Verschieben Sie niemals Teile des TextGridLab-Ordners –
verschieben Sie immer den Ordner mit allen Unterordner als Ganzes. Wenn Sie das TextGridLab auf ihrem Desktop oder in Ihrem Startmenü wollen, erstellen Sie ggf. eine Verknüpfung.

Entpacken Sie das TextGridLab nicht in einem sehr tief verschachtelten Verzeichnis: Windows hat eine Maximallänge für Pfadnamen, und das TextGridLab muss aus technischen Gründen für einige Dateien längere Pfadnamen verwenden. Die von der o.g. Fehlermeldung bemängelte Datei
findet sich z.B. relativ zum TextGridLab-Ordner in einem Unterverzeichnis wie `plugins\org.eclipse.equinox.launcher.win32.win32.x86_64_1.1.200.v20150204-1316\eclipse_1608.dll`.

### Hilfesystem funktioniert nicht

Windows-Benutzern, die eine Persönliche Firewall benutzen, kann es passieren, dass das Inhaltsverzeichnis der Hilfe und die Hilfetexte selbst nicht angezeigt werden können. Der Grund dafür ist, dass das TextGridLab-Hilfesystem einen lokalen Webserver startet, der dann die Hilfetexte wahlweise Ihrem Lieblingsbrowser oder einem internen Hilfefenster ausliefert.

Um das Hilfesystem in solchen Fällen zum Laufen zu bringen, müssen Sie das TextGridLab in Ihrer persönlichen Firewall freischalten. Alternativ können Sie unser [Online-Handbuch](User-Manual-2.0.md) verwenden.

### Installation von (z.B.) oXygen bricht nach einer bestimmten Zeit reproduzierbar ab

Deaktivieren Sie vorübergehend Ihren Virenscanner (Problem mit Avira beobachtet).

Hintergrund: Um oXygen zu installieren muss eine relativ große Plugin-Datei von deren Servern heruntergeladen werden. Normalerweise ist das kein Problem, Aviras Antiviren-Filter scheint indes diesen Download zunächst an einem temporären Ort zwischenzuspeichern und die Datei nach dem kompletten Download nach Viren zu durchsuchen, bevor es sie an den Installationsmechanismus des TextGridLab weiterleitet. Für das Lab sieht das aus, als reagierten die oXygen-Server nicht auf die Anfrage des Labs, und es wird nach einer gewissen Zeit mit einem Timeout-Fehler abbrechen.

### Anti-Virus-Software

Deaktivieren Sie vorübergehend Ihren Virenscanner (Problem mit Avira beobachtet).

Neben den Problemen bei der Installation von oXygen treten auch Schwierigkeiten bei der Nutzung des TextGridLabs auf. Die betreffende Komponente aus Avira ist der „Browser-Schutz“, welcher das Laden von externen Ressourcen im Lab verhindert. So kann zum Beispiel die Vorschauansicht nicht geöffnet werden, bzw. ihr Status verbleibt bei „55%“. Eine vorübergehende Deaktivierung des Virenscanners **und** des Browser-Schutzes schafft Abhilfe. Man kann das TextGridLab auch auf eine Whitelist setzten und somit jegliches scannen unterbinden. Beachten Sie hier, dass auch die externen Ressourcen auf die Whitelist des „Browser-Schutzes“ gesetzt werden müssen.

### Marketplace Popup-Fenster öffnet sich nicht oder hat keinen Inhalt

Falls das Fenster für den Eclipse Marketplace sich nicht öffnet oder keinen Inhat hat, wenn Sie **Hilfe** \> **Eclipse Marketplace** klicken, stellen Sie bitte sicher, dass der Pfadname Ihres TextGridLab-Installationsordners kein Leerzeichen im Namen hat.

### TLS-Fehler bei Login im TG-lab (Windows 10)

Bei dem Fehler im TG-lab „Keine sichere Verbindung mit dieser Seite möglich. Ihre TLS-Sicherheitseinstellungen weisen keine Standardwerte auf, was ebenfalls diesen Fehler verursachen könnte.“ gehen Sie bitte in Ihre Einstellungen:

**Internetoptionen** \> **Erweitert** und aktivieren weiter unten unter **Sicherheit** den Punkt „TLS 1.2 verwenden“.

## Linux

### TODO (UPDATE)! Browserbasierte Komponenten funktionieren nicht

Bleibt unter Linux der Willkommensbildschirm leer und beim Login kommen Fehlermeldungen, so muss das Paket [WebKit-GTK 1.x](apt://libwebkitgtk-1.0-0 "Installiert das Paket libwebkitgtk-1.0-0, falls auf Ihrem System unterstützt") installiert werden. Auf Ubuntu bzw. Debian können Sie das mit dem o.g. Link tun oder indem Sie an der Kommandozeile

    sudo apt-get install libwebkitgtk-1.0-0

eingeben \[[TG-1903](https://develop.sub.uni-goettingen.de/jira/browse/TG-1903)\]. Auf Fedora \[[\#9433](https://projects.gwdg.de/issues/9433)\] muss zusätzlich ein symbolischer Link vom alten Namen `libwebkit-1.0.so.2` gesetzt werden:

``` syntaxhighlighter-pre
sudo yum install webkitgtk.i686
cd /usr/lib      # oder cd /usr/lib64 auf 64-Bit-Versionen
ln -s libwebkitgtk-1.0.so.0 libwebkit-1.0.so.2
```

Für TextGridLab-Versionen bis 2.0.5 muss zusätzlich der SWT-Patch installiert werden, wie in [Bug \#9717 Kommentar 4](https://projects.gwdg.de/issues/9717#note-4) beschrieben. Aktuelle TextGridLab-Versionen benötigen den Patch nicht.

#### Fedora 28

webkit1 has been removed from the repository. We provide the needed packages and you may install them with

**webkitgtk on fedora** Quelle erweitern

``` syntaxhighlighter-pre
cd /tmp
wget --output-document=webkit.tar.gz https://wiki.de.dariah.eu/download/attachments/18809102/webkit%2Bdependencies.fedora28.tar.gz\?api\=v2
tar -xzf webkit.tar.gz
sudo dnf install --assumeyes ./libicu-last-50.1.2-11.el6.remi.x86_64.rpm
sudo dnf install --assumeyes ./lib64webp4-0.3.1-2.mga4.x86_64.rpm
sudo dnf install --assumeyes ./webkitgtk-2.4.9-3.el7.nux.x86_64.rpm
```

a dnf update or yum update may removes the older webkit when other packages provide the newer on. This happens because of the group «fedora-obsolete-packages». in this case use

``` syntaxhighlighter-pre
sudo dnf install --allowerasing ./webkitgtk-2.4.9-3.el7.nux.x86_64.rpm
```

to reset the webkit installation.

### UPDATE! Fehler „MOZILLA_FIVE_HOME not set“

Kommt beim Start des TextGridLab die oben genannte Fehlermeldung fehlt wahrscheinlich die libwebkit-gtk. Versuchen Sie den Lösungsvorschlag [Browserbasierte Komponenten funktionieren nicht (Linux)](#Troubleshooting-BrowserbasierteKomponentenfunktionierennicht(Linux)) anzuwenden.

### TODO (NEW)! Probleme mit Wayland

./textgridlab.sh

## Max OS

### Vor dem ersten Start: TextGridLab.app kann nicht geöffnet werden

Es kann vorkommen, dass vor dem ersten Start nach dem Entpacken auf OS X ein Hinweis angezeigt wird, dass das Programm nicht geöffnet werden könne, da es von einem nicht zertifizierten Entwickler stammt.

In diesem Fall klicken Sie mit der rechten Maustaste auf TextGridLab.app und wählen Sie im Kontextmenü „Öffnen“. Dieser Schritt ist nur ein Mal notwendig.

### TextGridLab started nicht: Es ist ein Fehler aufgetren

Das Fehlerfenster zeigt an, dass weitere Informationen im angegebenen Log zu finden sind. Bitte öffnen Sie die angegebene Logdatei. Wenn da ein Eintrag am Ende der Datei wie dieser steht:

    Caused by: java.lang.IllegalStateException: The platform metadata area could not be written:`/private/var/folders/yr/gtbbgl915jbg74s4kb3_q3qm0000gn/T/AppTranslocation/1231F14F-D417-4A89-B029-31B546924E99/d/TextGridLab.app/Contents/MacOS/workspace/.metadata`.

By default the platform writes its content under the current working directory when the platform is launched. Use the -data parameter to specify a different content area for the platform.

Dann bitte folgendes machen: Auf die TextGridLab.app mit der rechten Maustaste klicken und auf „Paketinhalt zeigen“ klicken. Gehen Sie in den Ordner „Contents“ und klicken mit der rechten Maustaste auf „Info.plist“ und wählen **Öffnen mit** \> **Anderem Programm** und wählen TextEdit aus der Liste. Finden Sie die Zeile `<string>-showlocation</string>` ziemlich am Ende der Datei. Fügen Sie darunter folgende neue Zeile ein:

    <string>-data</string><string>~/.TextGridLab</string>

Speichern sie nun die Datei ab, schließen das Finder Fenster und starten sie das Program TextGridLab erneut.

### Plugin-Installation (aus dem Marketplace) schlägt fehl

Wenn ein:e Administrator:in das TextGridLab im Ordner **Applications** für ein:n Nutzer:in ohne Administrationsrechte installiert hat, schlägt die Plugin-Installation fehl. Normale Nutzer:innen dürfen keine Applikationen in diesem Ordner ändern!

Lösung:

1. Bitten Sie eine:n Administrator:in, das Plugin für alle Nutzer:innen als Administrator:in zu installieren. Das Plugin ist dann für alle Nutzer:innen verfügbar.

2. Installieren Sie TG Lab lokal: Am einfachsten ist es, eine Kopie der TextGrid Lab-Anwendung zu erstellen. Öffnen Sie dazu den Finder, klicken Sie links auf Programme, suchen Sie TextGridLab, klicken Sie es mit der linken Maustaste an und halten Sie die Maustaste gedrückt. Halten Sie dann die ALT-Taste auf der Tastatur gedrückt (Sie müssen ein grünes Plus-Symbol sehen) und ziehen Sie die Anwendung auf den Desktop. Lassen Sie nun beides los, die Maustaste und die ALT-Taste der Tastatur. Mit dieser Aktion erstellen Sie eine lokale Kopie der Anwendung. Starten Sie dann die Anwendung vom Desktop aus mit einem Doppelklick und installieren Sie die Plugins nach Belieben.

## Advanced Installation Scenarios

### Install the TextGridLab once for multiple users

The TextGridLab needs a writeable area on your hard disk to store settings and working data. By default, it will create and use a folder named `workspace` in the current working directory – this will typically be the TextGridLab's application folder. If this directory is not writeable (e.g., because an administrator installs the TextGridLab for all users to a read-only directory), you need to tell it an alternative. There are two ways to do so:

1. As an administrator, you can edit the file `textgridlab.ini` in the TextGridLab application directory. *Before* the line containing `-jvmArgs`, add two lines:

    ``` syntaxhighlighter-pre
    -data
    @user.home/TextGridLab-Workspace
    ```

    This will cause the TextGridLab to use the directory `TextGridLab-Workspace` in the current user's personal directory. The first line must always be `-data`, the second line contains the target directory; `@user.home` is a macro that will be expanded to the user's home directory.

2. Alternatively, you can start the TextGridLab with the command line arguments `-data @user.home/TextGridLab-Workspace` (or likewise).

## Attachments

- [Unicode-Surrogate-Support.reg](attachments/18809102/18940134.reg) (text/x-ms-regedit)  
- [webkit+dependencies.fedora28.tar.gz](attachments/18809102/64970800.gz) (application/x-gzip)  
