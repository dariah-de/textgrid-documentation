# Vorschau-Ansicht

last modified on Aug 19, 2015

Wenn Sie ein XML-Objekt mit einem adäquaten XSLT-Dokument assoziieren,
zeigt die Vorschau-Ansicht den HTML-Text an, der aus diesem Vorgang
resultiert. Um die XSLT-Datei mit einem XML-Dokument zu assoziieren,
fügen Sie ihrem XML-Quelltext eine Stylesheet-Deklaration hinzu:

``` syntaxhighlighter-pre
<?xml-stylesheet type="text/xsl" href="URI of the Stylesheet"?>
```

Der "URI des Stylesheets" kann auch der URI des XSLT-Objekts in TextGrid
sein (Rechtsklicken Sie das Stylesheet im Navigator und wählen Sie *URI kopieren,* um seinen URI in die Zwischenablage zu kopieren). Das
Kontextmenü der Vorschau-Ansicht ist identisch mit dem Ihres
Betriebssystems.

Das TextGridLab bündelt eine Version von Saxon's Home Edition, Sie
können hier daher XSLT 2.0-Stylesheets verwenden.

TEXTGRIDLAB ≥ 2.3Wenn es keine \<?xml-stylesheet?\>-Deklaration gibt und
Sie ein TEI-Dokument bearbeiten, wird die Vorschau-Ansicht versuchen,
Ihr XML-Dokument unter Verwendung des Standard-Stylesheets anzuzeigen.
In diesem Fall wird standardmäßig das [TEI's HTML-Stylesheets (von Sebastian Rahtz)](https://github.com/TEIC/Stylesheets) verwendet, Sie
können dies aber in den [Benutzervorgaben](Benutzervorgaben_40220277.md) unter *XML \>
Experimental Features* einstellen.

## Aus Ihrem Stylesheet auf TextGrid-Ressourcen zugreifen

Der XSLT-Prozessor, der HTML produziert, kann TextGrid-URIs auflösen und
daher unter der Voraussetzung der entsprechenden Zugriffsrechte direkt
auf TextGrid-Dokumente zugreifen. Das bedeutet, dass Sie beispielsweise
die XPath-Funktion document() unter Verwendung der TextGrid URIs nutzen
können, d. h.

``` syntaxhighlighter-pre
<xsl:apply-templates select="document(@url)"/>
```

funktioniert mit @url = 'textgrid:4711.0'.

TEXTGRIDLAB \> 2.1.1 Der eingebettete Browser, der den resultierenden
HTML-Text darstellt, ist nicht in der Lage, textgrid: URIs direkt
umzusetzen. Wenn Sie beispielsweise wollen, dass der Browser Bilder
anzeigt, auf die Sie in Ihrem Quelldokument in einer Form wie \<tei:pb
facs="textgrid:4711.0"/\> verweisen, müssen Sie dies zuerst in einen
http(s)-URL übersetzen. Um Sie dabei zu unterstützen, übergibt die
Vorschau-Ansicht dem XSLT-Prozessor einen Parameter namens
graphicsURLPattern. Der Wert dieses Parameters enthält eine
Zeichenkette, in der wiederum die spezielle Teilzeichenkette @URI@
enthalten ist, und wenn Sie den Ausdruck @URI@ durch den TextGrid-URI
des Bildes ersetzen, erhalten Sie einen https-URL, den Ihr Browser
verwenden kann (und der bereits Informationen zur Authentifizierung
enthält). Das folgende XSLT-Fragment kann den oben erwähnten Fall pb
umsetzen:

``` syntaxhighlighter-pre
<xsl:param name="graphicsURLPattern"/>

<xsl:template match="tei:pb[@facs]">
  <img class="facsimile" src="{replace($graphicsURLPattern, '@URI@', @facs)}"/>
</xsl:template>
```
