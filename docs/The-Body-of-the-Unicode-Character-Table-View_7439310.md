# The Body of the Unicode Character Table View

last modified on Mai 03, 2016

Die Tabelle im Hauptteil zeigt alle Zeichen, die im gewählten
Zeichensatz enthalten sind, in einzelnen Feldern an und erlaubt Ihnen,
ein beliebiges davon durch Klicken auszuwählen. Wenn Sie Windows XP als
Betriebssystem verwenden, können Probleme bei der Darstellung und dem
Einfügen der Zeichen auftreten. Durch Rechtsklicken des ausgewählten
Zeichens öffnet sich ein Kontextmenü, um das gewählte Zeichen in die
Zwischenablage zu kopieren.
