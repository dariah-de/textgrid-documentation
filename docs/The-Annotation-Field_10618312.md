# The Annotation Field

last modified on Mai 03, 2016

Unterhalb des Text-Text-Link-Feldes befindet sich das Annotationsfeld.
Klicken Sie darin, um einen Kommentar zur ausgewählten
Text-Text-Verknüpfung hinzuzufügen.
