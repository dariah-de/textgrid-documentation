# XML-Editor verwenden

last modified on Jul 31, 2015

Einige der wichtigsten Operationen, für die der XML-Editor vorgesehen
ist, werden in den folgenden Abschnitten erläutert.

- [XML-Dokumente öffnen](40220575.md)
- [XML-Dokumente erstellen](XML-Dokumente-erstellen_40220577.md)
- [XML-Schema assoziieren und XML-Dokumente validieren](XML-Schema-assoziieren-und-XML-Dokumente-validieren_40220579.md)
