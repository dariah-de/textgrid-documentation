# Interaktion von Revisionen mit anderen Komponenten

last modified on Aug 19, 2015

Im Falle von Editionen, Kollektionen und anderen Aggregationen können
Sie diese [Objekte](TextGrid-Objekte.md) rechtsklicken und
“Bearbeiten” wählen. Dann öffnet sich der
[Aggregationen-Editor](Aggregationen-Editor_40220440.md). Wenn Sie das
fragliche Objekt in der Aggregationen-Editor-Sicht rechtsklicken und
"Revision" wählen, haben Sie die Möglichkeit, auf die neueste Revision
zu verweisen, die Revision, die mit dem ausgewählten Objekt
übereinstimmt, oder auf eine beliebige andere Revision aus der Liste.
