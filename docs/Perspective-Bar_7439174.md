# Perspective Bar

last modified on Feb 28, 2018

The perspective bar is at the top of the screen on the right side,
underneath the menu bar, unless the layout of the toolbars has been
modified. The perspective bar allows access to perspectives that are
currently open. It also provides a way to open a new perspective by clicking ![](attachments/7439174/8192210.png "new_persp.png"). By
right-clicking on one of the open tabs in the perspective bar, it is
possible to customize the perspective.

## Attachments

- [new_persp.png](attachments/7439174/8192210.png) (image/png)  
