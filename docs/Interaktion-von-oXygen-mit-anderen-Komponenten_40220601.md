# Interaktion von oXygen mit anderen Komponenten

last modified on Aug 20, 2015

Der oXygen-XML-Editor interagiert mit vielen Komponenten des TextGridLab
in ähnlicher Weise wie der [XML-Editor](XML-Editor_40220521.md). Über
das [Kontextmenü des oXygen-XML-Editors](40220597.md) können der
[Lemmatisierer](Lemmatisierer_40220718.md) und – sofern installiert –
das Publikationswerkzeug [SADE](Publikationswerkzeug-SADE_40220503.md)
genutzt werden. Im Gegensatz zum TextGrid-XML-Editor können die
[Wörterbuchsuche](40220702.md), [LEXUS](40220734.md), [Cosmas II](40220746.md) und [ANNEX](40220758.md) nicht in Verbindung mit
dem oXygen-XML-Editor genutzt werden.
