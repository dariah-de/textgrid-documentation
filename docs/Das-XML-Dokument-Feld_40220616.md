# Das XML-Dokument-Feld

last modified on Jul 31, 2015

Wenn ein XML-Dokument mit dem Text-Text-Link-Editor geöffnet ist, wird
das Dokument in einer eigenen Spalte dargestellt. Wenn mehrere
XML-Dokumente geöffnet sind, werden sie in parallelen Spalten
dargestellt. Verwenden Sie Ihre Maus, um ein Fragment des Dokuments
auszuwählen. Über das Kontextmenü können Sie nach dem Fragment in Google
suchen oder es kopieren.
