# Benutzerverwaltung-Sicht

last modified on Jul 28, 2015

Wenn ein neues Projekt erstellt wurde, können Projektmitglieder
hinzugefügt und ihnen Rollen zugewiesen werden. Um hinzugefügt werden zu
können, müssen sich die Nutzer zuvor mindestens einmal im TextGridLab
angemeldet haben.

In TextGrid gibt es vier mögliche Rollen für Benutzer, von denen jede
spezifische Rechte innehat:

|                          |                                                    |
|--------------------------|----------------------------------------------------|
| Projekt-Manager          | Rechte vergeben und Veröffentlichen von Ressourcen |
| Berechtigung zum Löschen | Löschen von Ressourcen                             |
| Editor                   | Lesender und schreibender Zugriff auf Ressourcen   |
| Beobachter               | Nur-lesender Zugriff auf Ressourcen                |


|                 |                                       |                                                                                                                                   |
|-----------------|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
|                 | Projektrechte                         | Ressourcenrechte                                                                                                                  |
|                 |                                       |                                                                                                                                   |
| Delegieren      | Nutzerrechte für das Projekt zuweisen | Zugriffsrechte für Ressourcen bearbeiten                                                                                          |
| Veröffentlichen |                                       | Ressourcen veröffentlichen. Die Ressourcen werden dann öffentlich zugänglich und können nicht mehr gelöscht oder verändert werden |
| Löschen         |                                       | Unveröffentlichte Ressourcen löschen                                                                                              |
| Erstellen       | Neue Objekte erstellen                | Neue Ressourcen im Projekt erstellen                                                                                              |
| Schreiben       |                                       | Schreiben in (und damit bearbeiten von) unveröffentlichten Ressourcen                                                             |
| Lesen           |                                       | Lesen von Ressourcen im Projekt                                                                                                   |

Rechte in TextGrid sind nicht hierarchisch. Wenn Nutzer sämtliche Rechte
haben sollen, müssen ihnen die Rollen “Projekt-Manager”, “Berechtigung
zum Löschen” und “Editor” zugewiesen werden. Rollen mit ihren jeweiligen
Rechten können Nutzern auch wieder entzogen werden, wenn man die dafür
notwendigen Rechte besitzt.

## Attachments

- [114-Projekt-Nutzer.png](attachments/40220383/40436715.png)
(image/png)  
- [116-Kontakt-Nutzer.png](attachments/40220383/40436716.png)
(image/png)  
- [115-Suche-Nutzer.png](attachments/40220383/40436717.png) (image/png)  
