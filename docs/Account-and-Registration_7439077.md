# Account and Registration

last modified on Feb 26, 2019

Einige Funktionen sind ohne Login verfügbar, wie der lesende Zugriff auf
veröffentlichte Informationen und die Arbeit mit lokalen Dateien. Um
Ihre eigenen Daten im TextGrid Repository speichern zu können, müssen
Sie sich jedoch durch eine Nutzerkennung authentifizieren. Wenn Ihre
Institution an der DFN-AAI-Föderation teilnimmt, können Sie einfach Ihr
Institutslogin verwenden. Alternativ können Sie eine
TextGrid-Nutzerkennung auf der TextGrid-Website beantragen

*[https://textgrid.de/de/registrierung/download](https://textgrid.de/de/registrierung/download)*

Nachdem Ihre Registrierung bestätigt wurde, werden Sie eine E-Mail mit
Ihren Zugriffsinformationen erhalten.
