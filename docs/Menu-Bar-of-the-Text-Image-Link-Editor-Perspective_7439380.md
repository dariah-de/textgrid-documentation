# Menu Bar of the Text Image Link Editor Perspective

last modified on Mai 03, 2016

Wenn die Bild-Sicht aktiviert ist, können die Operationen
“Rückgängig” ![](attachments/40220632/40436821.png "037-zuruecknehme-Aenderung.png") und
“Wiederholen” ![](attachments/40220632/40436820.png "038-wiederhole-Aenderung.png") unter
dem Menüpunkt “Bearbeiten” in der Menüleiste verwendet werden. Wenn
der [XML-Editor](XML-Editor_40220521.md) im Hintergrund geöffnet ist,
werden alle Funktionen unter diesem Menüpunkt zur Verfügung gestellt.

## Attachments

- [037-zuruecknehme-Aenderung.png](attachments/7439380/8192255.png)
(image/png)  
- [038-wiederhole-Aenderung.png](attachments/7439380/8192256.png)
(image/png)  
