# Using Cosmas II

last modified on Mai 04, 2016

1. Öffnen Sie Cosmas II durch Auswahl von “Werkzeuge \> Sicht
    anzeigen \> Cosmas II” in der Menüleiste oder Klicken
    von ![](attachments/40220754/40436906.png "cosmas.png") in der
    Werkzeugleiste. Das Cosmas-Werkzeug öffnet sich unterhalb
    des [XML-Editors](XML-Editor_40220521.md).
2. Geben Sie ein Wort oder eine Wortfolge im Eingabefeld ein.
3. Klicken Sie “Suche”.
4. Sie können die Ergebnisse in die Zwischenablage kopieren, indem Sie
    diese rechtsklicken und “Gewählte Ergebnisse in Zwischenablage
    kopieren” oder “Gewählte Ergebnisse in CSV-Datei exportieren” im
    Kontextmenü auswählen. Wenn Sie die Ergebnisse in die Zwischenablage
    kopiert haben, wählen Sie “Einfügen” oder drücken Sie \[Strg+V\], um
    sie in einem Dokument einzufügen.

Alternativ können Sie Cosmas II über das Kontextmenü
der [Quelle-Sicht](Quelle-Ansicht_40220547.md) oder
der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) im XML-Editor öffnen.

1. Markieren Sie das Wort im XML-Editor und rechtsklicken Sie es. Das
    Kontextmenü öffnet sich.
2. Klicken Sie “In Cosmas II suchen“. Das Cosmas-Werkzeug öffnet sich
    unterhalb des XML-Editors und Ihre Suche wird umgehend starten. Das
    Ergebnis wird im Ergebnisfeld angezeigt.
3. Zum Kopieren oder Exportieren der Ergebnisse, siehe oben unter Punkt
    4.

## Attachments

- [cosmas.png](attachments/8126492/8192316.png) (image/png)  
