# Context Menu of the Image View

last modified on Mai 03, 2016

Das Kontextmenü der Bild-Sicht stellt mehrere Operationen zur Verfügung:

- "Text mit gewählter Markierung verknüpfen": verknüpft den gewählten
    Bildausschnitt mit dem markierten Text
- "Verknüpfung für ausgewählte Markierung(en) aufheben": hebt die
    gewählte Verknüpfung zwischen Bild und Text auf
- "Aktive Markierung(en) löschen": löscht die gerade aktiven
    Markierungen
- "Alle Markierungen/Verknüpfungen löschen": löscht alle Markierungen
    und alle Verknüpfungen in einem Bild
- "Zu verknüpftem Text springen": wählt den dazugehörigen Bereich im
    XML-Text aus
- "(Rechteck) duplizieren \> Aktives Rechteck horizontal duplizieren":
    erstellt rechts davon eine Kopie des aktiven Rechtecks. 
- "(Rechteck) duplizieren \> Aktives Rechteck vertikal duplizieren":
    erstellt unterhalb davon eine Kopie des aktiven Rechtecks
- "(Rechteck) duplizieren \> Aktives Rechteck vertikal (nach oben)
    duplizieren": erstellt oberhalb davon eine Kopie des aktiven
    Rechtecks
- "(Rechteck) duplizieren \> Aktives Rechteck horizontal (rückwärts)
    duplizieren": erstellt links davon eine Kopie des aktiven Rechtecks.
  "Andocklinien"
  - Ausrichtung für aktive Andocklinie
    - Horizontal: richtet die aktive Andocklinie horizontal aus
    - Vertikal: richtet die aktive Andocklinie vertikal aus
  - Alle Linien speichern: speichert alle Andocklinien
  - Alle Linien entfernen: entfernt alle Andocklinien
  - Alle Linien ein- oder ausblenden: blendet alle Andocklinien ein,
        wenn sie ausgeblendet sind, oder umgekehrt
  - Einstellungen: öffnet
        die [Andocklinien-Einstellungen](Funktionen-der-Bild-Sicht_40220641.md)
- ["Schriftmodus"](Funktionen-der-Bild-Sicht_40220641.md)
  - Schriftmodus ein- oder ausblenden: blendet den Schriftmodus ein,
        wenn er ausgeblendet ist, oder umgekehrt
  - Schriftmodus für aktive Markierung(en) wählen: ändert
        Schriftmodus für die gewählte Markierung
  - Schriftmodus für aktives Dokument wählen: ändert Schriftmodus
        für alle Markierungen, die noch keinen Schriftmodus haben, und
        für neu erstellte Markierungen
- ["Rotation](Funktionen-der-Bild-Sicht_40220641.md)"
  - Rotationsmodus (de)aktivieren zum Rotieren einer Markierung mit
        der Maus
  - Rotation in
        der [Statusleiste](Statusleiste-der-Bild-Sicht_40220647.md) ein-
        oder ausblenden
  - Rotation für aktive Markierung(en) mittels Rollbalken
- "Text-Ebenen"
  - Ebenen editieren: öffnet
        den [Ebenen-Editor](Funktionen-der-Bild-Sicht_40220641.md)
  - Ebenen für ausgewählte Markierung(en) editieren: ordnet die
        Auswahl einer Ebene zu
  - Alle Ebenen einblenden: alle Ebenen werden eingeblendet
- "Bildgröße"
  - Originalgröße anzeigen: zeigt das Bild in Originalgröße an
  - Vertikal anpassen: passt die Höhe des Bilds an die Höhe der
        Sicht an
  - Horizontal anpassen: passt die Breite des Bilds an die Breite
        der Sicht an
  - An Fenstergröße anpassen: passt die Höhe bzw. Breite des Bilds
        an die Höhe bzw. Breite der Sicht an
