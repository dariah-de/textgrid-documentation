# Allgemeine Objektbearbeitung

last modified on Mai 03, 2016

Dieses Kapitel bietet einen Überblick über die grundlegenden
Editionswerkzeuge in TextGrid. Während des Editionsprozesses können
diese für die meisten notwendigen Funktionen verwendet werden.

- [XML-Editor](XML-Editor_40220521.md)
- [oXygen (de)](40220583.md)
- [CSS-Editor](CSS-Editor_40220603.md)
- [Text-Text-Link-Editor](Text-Text-Link-Editor_40220608.md)
- [Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md)
- [Unicode-Zeichen-Tabelle](Unicode-Zeichen-Tabelle_40220675.md)
- [Kopie von MSTextTextLinkeditor](Kopie-von-MSTextTextLinkeditor_40220693.md)
- [Kopie von MSOxygenXMLEditor](Kopie-von-MSOxygenXMLEditor_40220695.md)
- [Kopie von MSTextGridRepPreview](Kopie-von-MSTextGridRepPreview_40220697.md)
