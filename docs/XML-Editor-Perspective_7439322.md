# TextGrid : XML Editor Perspective

last modified on Mai 04, 2016

Sobald Sie die XML-Editor-Perspektive geöffnet haben, sehen Sie drei
Bereiche:

1. der [Navigator](40220331.md) und zwei versteckte Reiter auf der
    linken Seite,
2. die [Editor-Sicht](Editor-Sicht_40220539.md) in der Mitte, die
    leer ist, bi sein XML-Dokument geöffnet oder erstellt wird,
3. die beiden Sichten ’[Gliederung](Gliederung-Sicht_40220561.md)’
    und ’[Eigenschaften](Eigenschaften-Sicht_40220567.md)’ auf der
    rechten Seite, die leer sind, wenn kein XML-Dokument geöffnet ist.

- [Features of the XML Editor](Features-of-the-XML-Editor_7439324.md)
- [Menu Bar of the XML Editor Perspective](Menu-Bar-of-the-XML-Editor-Perspective_7439326.md)
- [Toolbar of the XML Editor Perspective](Toolbar-of-the-XML-Editor-Perspective_7439328.md)
- [Status Bar of the XML Editor Perspective](Status-Bar-of-the-XML-Editor-Perspective_7439330.md)
- [Shortcuts of the XML Editor Perspective](Shortcuts-of-the-XML-Editor-Perspective_7439332.md)
