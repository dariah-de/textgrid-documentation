# Philological Tools

last modified on Feb 28, 2018

Dieses Kapitel beschreibt Werkzeuge für Forschung im Bereich Textkritik,
insbesondere für die Kollationierung von Textzeugen und die Darstellung
von Glossen.

- [CollateX](CollateX_7440112.md)
- [MSCollateX](MSCollateX_34344155.md)
