# Markierungen im Bild erstellen

last modified on Aug 03, 2015

Zuerst muss ein Bildausschnitt gewählt werden. Jedes Wort, jeder Bereich
oder jede Sequenz des Bildes kann im Rechteck- (Standard) oder
Polygon-Modus gewählt werden. Im Rechteck-Modus kann durch Klicken an
einer Ecke des gewünschten Bildbereichs eine Rechteck-Markierung durch
Ziehen erstellt werden, solange die linke Maustaste gedrückt bleibt. Im
Polygon-Modus kann eine Markierung ebenfalls durch Klicken und Ziehen
erstellt werden. Beim Loslassen der linken Maustaste wird die nächste
Ecke des Polygons platziert, vervollständigt werden kann es durch
Doppelklicken. Eine Markierung, die noch nicht mit einem Text verknüpft
ist, wird mit einer gestrichelten Umrandung dargestellt.
