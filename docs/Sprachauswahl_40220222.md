# Sprachauswahl

last modified on Aug 18, 2015

Wenn Sie die Sprache für das TextGrid Laboratory festlegen wollen,
können Sie seine Konfigurationsdatei "textgridlab.ini" öffnen und

  -Duser.language=en

am Ende der Datei eintragen. Speichern Sie die Datei und starten Sie das
Laboratory erneut. In diesem Beispiel ist die Standardsprache englisch,
für eine deutsche Nutzeroberfläche tragen Sie nach dem
Gleichheitszeichen de ein.
