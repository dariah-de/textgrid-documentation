# Open CollateX

last modified on Mai 04, 2016

CollateX kann auf verschiedene Weisen geöffnet werden:

1. Wählen Sie "Werkzeuge \> CollateX" in der Menüleiste.
2. Klicken
    Sie ![](attachments/40220885/40436939.gif "154-zeige-CollateX.gif") in
    der Werkzeugleiste.
3. Doppelklicken Sie ein Kollationierungsset oder ein Äquivalenzset
    im [Navigator](40220331.md) oder
    der [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md)
4. Rechtsklicken Sie ein Kollationierungsset oder ein Äquivalenzset im
    Navigator oder der Suchergebnisse-Sicht und wählen Sie "Öffnen
    mit \> CollateX" im Kontextmenü.

## Attachments

- [154-zeige-CollateX.gif](attachments/8129959/8192325.gif) (image/gif)  
