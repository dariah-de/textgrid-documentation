# Statusleiste der XML-Editor-Perspektive

last modified on Aug 19, 2015

Wenn ein Dokument im XML-Editor geöffnet ist, werden Sie in der Leiste
am unteren Ende des Sicht darüber informiert, ob das Dokument
“Schreibbar” ist, ob [”Intelligentes Einfügen”](Funktionen-des-XML-Editors_40220527.md) aktiviert ist und
die aktuellen Koordinaten des Cursors anzeigen
([Quelle-Ansicht](Quelle-Ansicht_40220547.md)).
