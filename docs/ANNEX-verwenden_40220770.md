# ANNEX verwenden

last modified on Aug 04, 2015

Für weitere Informationen über das Erstellen und Bearbeiten
audiovisueller Dokumenten mit ELAN und ANNEX, siehe

[http://www.lat-mpi.eu/tools/elan/](http://www.lat-mpi.eu/tools/elan/)

[http://www.lat-mpi.eu/tools/annex/](http://www.lat-mpi.eu/tools/annex/)
