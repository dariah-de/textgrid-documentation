# SADE Template Privacy Policy

last modified on Jun 09, 2022

## Datenschutzhinweis

Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei
der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein
lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht
möglich.

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten
Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich
angeforderter Werbung und Informationsmaterialien wird hiermit
ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich
ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von
Werbeinformationen, etwa durch Spam-Mails, vor.

## Anbieter

Anbieter dieser Internetpräsenz ist im Rechtssinne die Niedersächsische
Staats- und Universitätsbibliothek Göttingen, siehe
[Impressum](https://de.dariah.eu/impressum).

## Personenbezogene Daten

Bei der Benutzung der DARIAH-DE Webseite und aller DARIAH-DE Dienste
werden nur die jeweils nötigen persönlichen Daten erhoben. Viele
Angebote sind auch ohne Angabe von persönlichen Daten nutzbar. Soweit
Daten erhoben werden, geschieht dies nur mit Ihrer Einwilligung.

Die Erhebung von Daten erfolgt nur nach Registrierung in der DARIAH AAI.
Zur Nutzung von [geschützten DARIAH-DE Angeboten](https://de.dariah.eu/access-to-protected-resources-with-your-home-organisation-s-user-account)
ist eine Registrierung unter Angabe von Name, E-Mail-Adresse und
Institution erforderlich. Diese, sowie weitere optionale dort
gespeicherte Daten, können Sie im [DARIAH SelfService](https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl)
abrufen. Die Daten werden erhoben, um eine individuelle Nutzung der
gewünschten Dienste zu ermöglichen und den Anforderungen der [DFN AAI, Verlässlichkeitsklasse Basic](https://wiki.aai.dfn.de/de:degrees_of_reliance), zu genügen. Ihre
Daten können bei der Nutzung von Diensten in der DFN AAI sowie an die
DARIAH AAI angebundene Services von DARIAH-EU-Partnern an die
entsprechenden Betreiber weitergeben werden.

Sämtliche personenbezogenen Daten werden nur solange gespeichert, wie
dies für den jeweils genannten Zweck erforderlich ist. Hierbei werden
bestehende gesetzliche Aufbewahrungsfristen berücksichtigt.

## Datenempfänger

Datenempfänger innerhalb der Universität ist die Niedersächsische
Staats- und Universitätsbibliothek.

Weitere Datenempfänger sind die [Gesellschaft für wissenschaftliche Datenverarbeitung Göttingen](https://www.gwdg.de/), [DAASI International](https://daasi.de), der [Lehrstuhl Medieninformatik der Otto-Friedrich-Universität Bamberg,](https://www.uni-bamberg.de/minf/)
die [Max Planck Computing & Data Facilities](http://www.mpcdf.mpg.de/)
und das [Karlsruher Institut für Technologie.](https://www.kit.edu/)

## Informationen zur Verarbeitung personenbezogener Daten

### Name und Kontaktdaten der Verantwortlichen

Der Präsident  
Prof. Dr. Metin Tolan  
Wilhelmsplatz 1  
37073 Göttingen  
+49 551 39-2100 (Tel.)  
[praesident@uni-goettingen.de](mailto:praesident@uni-goettingen.de) 

### Kontaktdaten des betrieblichen Datenschutzbeauftragten

Prof. Dr. Andreas Wiebe  
Lehrstuhl für Bürgerliches Recht  
Wettbewerbs- und Immaterialgüterrecht  
Medien- und Informationsrecht  
Platz der Göttinger Sieben 6  
D-37073 Göttingen  
+49 551 39-7381 (Tel.)  
+49 551 39-4437 (Fax)  
[datenschutz@uni-goettingen.de](mailto:datenschutz@uni-goettingen.de)

### Zwecke und Rechtsgrundlagen der Verarbeitung

Ihre Daten werden erhoben, um Ihr Anliegen zu bearbeiten. Ihre Daten
werden auf Grundlage von Art. 6 Abs. 1e erhoben.

### Empfänger der personenbezogenen Daten

Um Ihre Anfrage zu beantworten, werden Ihre personenbezogenen Daten an
die innerhalb der SUB Göttingen zuständige Personengruppe übermittelt.

### Dauer der Speicherung der personenbezogenen Daten

Ihre Daten werden gespeichert, um die Dienste zu erbringen, mit denen
Sie uns beauftragen. Sobald Sie sich abmelden oder um eine Löschung der
Daten bitten, werden Ihre Daten nur noch so lange gespeichert, bis alle
laufenden Vorgänge abgeschlossen sind und dann gelöscht, es sei denn
gesetzliche Vorgaben erfordern eine längere Speicherung.

### Betroffenenrechte

Nach der EU-Datenschutzgrundverordnung stehen Ihnen folgende Rechte
zu:  
Werden Ihre personenbezogenen Daten verarbeitet, so haben Sie das Recht,
Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art.
15 DSGVO). Sollten unrichtige personenbezogene Daten verarbeitet werden,
steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO). Liegen die
gesetzlichen Voraussetzungen vor, so können Sie die Löschung oder
Einschränkung der Verarbeitung verlangen sowie Widerspruch gegen die
Verarbeitung einlegen (Art. 17, 18 und 21 DSGVO).  
Wenn Sie die erforderlichen Daten nicht angeben, kann Ihr Antrag nicht
bearbeitet werden.  
Wenn Sie in die Datenverarbeitung eingewilligt haben oder ein Vertrag
zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe
automatisierter Verfahren durchgeführt wird, steht Ihnen gegebenenfalls
ein Recht auf Datenübertragbarkeit zu (Art. 20 DSGVO). Sollten Sie von
Ihren oben genannten Rechten Gebrauch machen, prüft die Niedersächsische
Staats- und Universitätsbibliothek Göttingen, ob die gesetzlichen
Voraussetzungen hierfür erfüllt sind.  
Zur Ausübung Ihrer Rechte wenden Sie sich bitte an den betrieblichen
Datenschutzbeauftragten.

Bei datenschutzrechtlichen Beschwerden können Sie sich an die zuständige
Aufsichtsbehörde wenden:

Die Landesbeauftragte für den Datenschutz Niedersachsen  
Prinzenstraße 5  
30159 Hannover  
+49 551 120 4500 (Tel.)  
+49 551 120 4599 (Fax)  
[poststelle@lfd.niedersachsen.de](mailto:poststelle@lfd.niedersachsen.de)

### Widerrufsrecht bei Einwilligung

Wenn Sie in die Verarbeitung durch die Niedersächsische Staats- und
Universitätsbibliothek Göttingen durch eine entsprechende Erklärung
eingewilligt haben, können Sie die Einwilligung jederzeit für die
Zukunft widerrufen. Die Rechtmäßigkeit der aufgrund der Einwilligung bis
zum Widerruf erfolgten Datenverarbeitung wird durch diesen nicht
berührt.

## Auskunft, Löschung, Sperrung

Sie haben jederzeit das Recht auf unentgeltliche Auskunft über Ihre
gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und
den Zweck der Datenverarbeitung sowie ein Recht auf Berichtigung,
Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum
Thema personenbezogene Daten können Sie sich jederzeit über die im
Impressum angegeben Adresse des Webseitenbetreibers an uns wenden.

## Statistische Daten für den Betreiber der Website

Diese Website nutzt die Open-Source-Software Matomo für die statistische
Auswertung von Besucherzugriffen. Matomo ist datenschutzkonform
konfiguriert, Daten werden anonymisiert gespeichert. Die zu diesem Zweck
verwendeten "Sitzungs-Cookies" sind Textdateien, die auf Ihrem Computer
gespeichert werden und die eine Analyse der Benutzung der Webseite durch
Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre
Benutzung dieses Internetangebotes werden auf dem Server der GWDG
gespeichert. Die IP-Adresse wird sofort nach der Verarbeitung und vor
deren Speicherung anonymisiert. Die von Matomo erfassten Daten werden in
anonymisierter Form dazu verwendet, unser Angebot zu verbessern.
IP-Adressen werden in Matomo ohne das letzte Tupel gespeichert. Wir
wissen aus welchem Netz eine Anfrage kam, nicht jedoch von welchem
Rechner.

Diese "Sitzungs-Cookies" beinhalten keine personenbezogenen Daten und
verfallen nach Ablauf der Sitzung. Techniken, wie zum Beispiel
Java-Applets oder Active-X-Controls, die es ermöglichen, das
Zugriffsverhalten der Nutzer nachzuvollziehen, werden nicht
eingesetzt.  
  
Im Einzelnen werden über jeden Zugriff/Abruf folgende Daten gespeichert:

- die ersten beiden Teile der IP-Adresse
- Datum und Uhrzeit
- User-Agent der verwendeten Software
- aufgerufene Seite/Name der abgerufenen Datei
- übertragene Datenmenge
- Meldung, ob der Zugriff/Abruf erfolgreich war.

Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung
der über Sie erhobenen Daten durch Matomo in der zuvor beschriebenen Art
und Weise und zu dem zuvor benannten Zweck einverstanden.

## Openstreetmap

Einige DARIAH-DE-Dienste nutzen Karten von
[Openstreetmap.org](http://Openstreetmap.org), betrieben von der
Openstreetmap Foundation, 132 Maney Hill Road, Sutton Coldfield, West
Midlands, B72 1JU, United Kingdom. Beim Aufruf der entsprechenden
Dienste und Seiten werden die Karten direkt von den Servern von
Openstreetmap abgerufen. Dabei wird Openstreetmap mitgeteilt, welche
Karten Sie anschauen und von welchen unserer Dienste Sie das tun.
Details zum Datenschutz bei Openstreetmap finden Sie in den
[Datenschutzhinweisen von Openstreetmap](https://wiki.openstreetmap.org/wiki/Privacy_Policy).

## Maps-For-Free

Einige DARIAH-DE-Dienste nutzen Karten von
[Maps-For-Free.com](http://Maps-For-Free.com), betrieben von Hans
Braxmeier, Donaustrasse 13, 89231 Neu-Ulm. Beim Aufruf der
entsprechenden Dienste und Seiten werden die Karten direkt von den
Servern von [Maps-For-Free.com](http://Maps-For-Free.com) abgerufen.
Dabei wird [Maps-For-Free.com](http://Maps-For-Free.com) mitgeteilt,
welche Karten Sie anschauen und von welchen unserer Dienste Sie das tun.
Details zum Datenschutz bei
[Maps-For-Free.com](http://Maps-For-Free.com) finden Sie in den
[Datenschutzhinweisen von Maps-For-Free.com](https://maps-for-free.com/html/about.md).

## Pelagios Commons

Einige DARIAH-DE-Dienste nutzen Karten von Pelagios Commons, betrieben
von Johan Åhlfeldt mit Unterstützung des Pelagios Projekts. Beim Aufruf
der entsprechenden Dienste und Seiten werden die Karten direkt von den
Servern von Pelagios Commons abgerufen. Dabei wird Pelagios Commons
mitgeteilt, welche Karten Sie anschauen und von welchen unserer Dienste
Sie das tun. Details zum Datenschutz bei Pelagios Commons finden Sie in
den [Datenschutzhinweisen von Pelagios Commons](http://pelagios.org/maps/greco-roman/about.md).

## Eurostat

Einige DARIAH-DE-Dienste nutzen Karten von Eurostat, betrieben von der
Europäischen Kommission. Beim Aufruf der entsprechenden Dienste und
Seiten werden die Karten direkt von den Servern der Europäischen
Kommission abgerufen. Dabei wird der Europäischen Kommission mitgeteilt,
welche Karten Sie anschauen und von welchen unserer Dienste Sie das tun.
Details zum Datenschutz der Europäischen Kommission finden Sie in den
[Datenschutzhinweisen der Europäischen Kommission](https://ec.europa.eu/info/privacy-policy_en).

## Google Fonts

Wir binden die Schriftarten („Google Fonts“) des Anbieters Google LLC,
1600 Amphitheatre Parkway, Mountain View, CA 94043, USA, ein.
Datenschutzerklärung:
[https://www.google.com/policies/privacy/](https://www.google.com/policies/privacy/),
Opt-Out:
[https://adssettings.google.com/authenticated](https://adssettings.google.com/authenticated).

Quelle: Erstellt mit [Datenschutz-Generator.de von RA Dr. Thomas Schwenke](https://datenschutz-generator.de/).

## Cookies

Die Internetseiten verwenden teilweise so genannte Cookies. Cookies
richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren.
Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und
sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem
Rechner abgelegt werden und die Ihr Browser speichert.  
Die meisten der von uns verwendeten Cookies sind so genannte
„Session-Cookies“. Sie werden nach Ende Ihres Besuchs automatisch
gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie
diese löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim
nächsten Besuch wiederzuerkennen.  
Sie können Ihren Browser so einstellen, dass Sie über das Setzen von
Cookies informiert werden und Cookies nur im Einzelfall erlauben, die
Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie
das automatische Löschen der Cookies beim Schließen des Browsers
aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität
dieser Website eingeschränkt sein.

## Server-Log-Files

Der Provider der Seiten erhebt und speichert automatisch Informationen
in so genannten Server-Log-Files, die Ihr Browser automatisch an uns
übermittelt. Dies sind:

- Browsertyp und Browserversion
- verwendetes Betriebssystem
-  Referrer URL
- Hostname des zugreifenden Rechners
- Uhrzeit der Serveranfrage

Diese Daten sind nicht bestimmten Personen zuordenbar. Eine
Zusammenführung dieser Daten mit anderen Datenquellen wird nicht
vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen,
wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt
werden.

## Twitter

Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden.
Diese Funktionen werden angeboten durch die Twitter Inc., 1355 Market
Street, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von
Twitter und der Funktion „Re-Tweet“ werden die von Ihnen besuchten
Websites mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt
gegeben. Dabei werden auch Daten an Twitter übertragen. Wir weisen
darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt
der übermittelten Daten sowie deren Nutzung durch Twitter erhalten.
Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von
Twitter unter
[https://twitter.com/privacy](https://twitter.com/privacy).  
Ihre Datenschutzeinstellungen bei Twitter können Sie in den
Konto-Einstellungen unter:
[https://twitter.com/account/settings](https://twitter.com/account/settings)
ändern.

## YouTube

Unsere Website nutzt Plugins der von Google betriebenen Seite YouTube.
Betreiber der Seiten ist die YouTube, LLC, 901 Cherry Ave., San Bruno,
CA 94066, USA. Wenn Sie eine unserer mit einem YouTube-Plugin
ausgestatteten Seiten besuchen, wird eine Verbindung zu den Servern von
YouTube hergestellt. Dabei wird dem YouTube-Server mitgeteilt, welche
unserer Seiten Sie besucht haben.  
Wenn Sie in Ihrem YouTube-Account eingeloggt sind ermöglichen Sie
YouTube, Ihr Surfverhalten direkt Ihrem persönlichen Profil zuzuordnen.
Dies können Sie verhindern, indem Sie sich aus Ihrem YouTube-Account
ausloggen.  
Weitere Informationen zum Umgang von Nutzerdaten finden Sie in der
Datenschutzerklärung von YouTube unter:
[https://www.google.de/intl/de/policies/privacy](https://www.google.de/intl/de/policies/privacy)

Quelle: [e-recht24.de](https://www.e-recht24.de)
