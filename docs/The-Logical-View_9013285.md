# The Logical View

last modified on Mai 30, 2012

## The Logical View

### Project and Aggregation

Starting the TextGrid Lab, the eclipse-based TextGrid client
application, one of the first tools the TextGrid user is confronted with
is the omnipresent Navigator (figure 1). Offering the same (and some
additional) functionality as an ordinary file browser, this tool
provides access to the objects in the grid.

(insert image here) Figure 1: The TextGrid Navigator

The top-level elements of the hierarchical structure displayed by the
Navigator are the so- called Projects. The Project serves as container
for the (role-based) rights management – comparable with the Bucket in
[Amazon S3](http://docs.amazonwebservices.com/AmazonS3/latest/dev/index.html?UsingAuthAccess.md) or
the Context in
[eSciDoc](https://www.escidoc.org/JSPWiki/en/ContentModel%203%20http://www.ifla.org/en/publications/functional-requirements-for-bibliographic-records).
Any TextGrid object belongs to a project. TextGrid users can start a new
project in order to create new or copy existing objects. The creator of
a project can select other TextGrid users, associate them with the
project and assign specific roles to them. A Project consists of an
arbitrary number of objects (Simple Object) and Aggregations, (virtual)
folders, that can contain objects and other Aggregations in turn.
Aggregations are also objects, though specialised ones. Every object is
coupled with a set of metadata.

### Object Types: Item, Edition, Work and Collections

In order to avoid redundancy, misspellings etc. and to keep the
management of bibliographic metadata straightforward and simple,
bibliographic metadata is spread on three types of Objects: Item,
Edition and Work – inspired by the [FRBR model](http://www.ifla.org/en/publications/functional-requirements-for-bibliographic-records).
While – in terms of metadata – Item is the most generic object type that
is not necessarily a bibliographic object in a narrower sense (could
also be an aggregation), Edition and Work objects must conform to a
specific metadata sub-schema. While Edition is modelled as an
Aggregation (with a specific set of metadata), a Work object is modelled
as an empty Simple Object with work-specific metadata. The connection
between instances of these three object types is modelled via relations
be- tween Item and Edition (aggregates), and Edition and Work
(isEditionOf), cf. figure 2:

(insert image here) Figure 2: Bibliographic Hierarchy.

Besides Item, Edition and Work, the [TextGrid metadata schema](http://www.textgrid.info/schemas/textgrid-metadata_2010.xsd) provides
for a fourth object type, the Collection. Not unlike the Edition object,
a Collection is also modelled as an Aggregation, but with a more
generic, non-bibliographic metadata set. For instance, a Collection can
be used to aggregate non-bibliographic objects (or mash up bibliographic
and non-bibliographic Items) and assign them to a common temporal or
spatial scope – or even provide a description for this selection.
