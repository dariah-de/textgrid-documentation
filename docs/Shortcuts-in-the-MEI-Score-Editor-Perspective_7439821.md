# Shortcuts in the MEI Score Editor Perspective

last modified on Mai 04, 2016

|              |                                                           |
|--------------|-----------------------------------------------------------|
| Del          | Deletes All Currently Selected Elements From the Document |
| Strg+Shift+1 | Starts the "Insert Measures" Tool                         |
| Strg+Shift+2 | Starts the "Insert Staffdefs" Tool                        |
| Strg+Shift+4 | Starts the "Score Image Export" Tool                      |
| Strg+Shift+5 | Starts the "MEI-prune model" Tool                         |
| Strg+Shift+6 | Opens the "Manage Sources" Dialog                         |
