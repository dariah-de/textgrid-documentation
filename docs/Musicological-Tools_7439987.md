# Musicological Tools

last modified on Feb 28, 2018

Als leistungsfähiges Werkzeug für die Erstellung digitaler
musikwissenschatlicher Editionen bietet TextGrid den Noteneditor MEISE.
Die Dokumentation dieses Werkzeugs liegt derzeit nur auf Englisch vor.

[Zur englischen Dokumentation](Musicological-Tools_7439987.md)
