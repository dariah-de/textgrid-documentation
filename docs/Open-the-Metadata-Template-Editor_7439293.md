# Open the Metadata Template Editor

last modified on Mai 03, 2016

Um den Metadaten-Vorlagen-Editor zu öffnen, wählen Sie “Werkzeuge” in
der Menüleiste. Alternativ klicken Sie  in der Werkzeugleiste klicken.
Sie können auch eines der Projekte, für das Sie die
"Projekt-Manager"-Rolle innehaben,
im [Navigator](40220331.md) rechtsklicken und “Metadatenbeschreibung
bearbeiten (Template Editor)” wählen.

## Attachments

- [077-zeige-MetadatenEntwurfsWizard.png](attachments/7439293/7766106.png)
(image/png)  
