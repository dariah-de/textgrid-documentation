# Lemmatisierer verwenden

last modified on Aug 04, 2015

Sie können entweder eine Wortform lemmatisieren oder ein ganzes Dokument
lemmatisieren.

- [Dokument lemmatisieren](Dokument-lemmatisieren_40220730.md)
- [Wortform lemmatisieren](Wortform-lemmatisieren_40220728.md)
