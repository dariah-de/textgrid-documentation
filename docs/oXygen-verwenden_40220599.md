# oXygen verwenden

last modified on Aug 20, 2015

Für weitere Informationen über die Arbeit mit dem oXygen-XML-Editor,
siehe

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)

Der oXygen-XML-Editor kann als Alternative zum [TextGrid XML-Editor](XML-Editor_40220521.md) verwendet werden.
