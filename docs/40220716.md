# Interaktion der Wörterbuchsuche mit anderen Komponenten

last modified on Aug 04, 2015

Die Funktionalität des Wörterbuchsuche-Werkzeug kann auch während der
Arbeit an einem XML-Dokument genutzt werden. Öffnen Sie dafür die
Wörterbuchsuchergebnisse -Sicht. Jedes beliebige Wort in der
Quelle-Sicht des XML-Editors kann über “Suche in Wörterbüchern” im
Kontextmenü nachgeschlagen werden.
