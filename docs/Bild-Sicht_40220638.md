# Bild-Sicht

last modified on Aug 03, 2015

Die Bild-Sicht ist der Arbeitsbereich des TextGridLab zur Bearbeitung
von Bildern. Einige seiner Funktionen können zudem zur Verknüpfung von
Bild- und Textelementen verwendet werden.

- [Funktionen der Bild-Sicht](Funktionen-der-Bild-Sicht_40220641.md)
- [Menüleiste der Bild-Sicht](40220643.md)
- [Titelleiste der Bild-Sicht](Titelleiste-der-Bild-Sicht_40220645.md)
- [Statusleiste der Bild-Sicht](Statusleiste-der-Bild-Sicht_40220647.md)
- [Tastenkürzel der Bild-Sicht](40220649.md)
- [Kontextmenü der Bild-Sicht](40220651.md)
