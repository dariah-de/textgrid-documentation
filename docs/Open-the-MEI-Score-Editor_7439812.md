# Open the MEI Score Editor

last modified on Mai 04, 2016

A perspective must be open to contain the Note Editor. The only way to
start the Score Editor is by opening or creating an MEI document as
described below:

- Create a new MEI note sheet via the
    toolbar *![](attachments/7439812/7766150.png "meise-createNewMEIfile.png") *\>
    Item \> MEI Notesheet”. Select a Project in the “Create a new
    TextGrid Object” dialog box. Click “Finish” or “Next” to select a
    title for the new Object. Pressing the button “Finish” will open the
    Note Editor with a simple unnamed MEI document, together with the
    XML Editor’s [Outline](Outline-View_7439342.md)and [Properties
    View](Properties-View_7439349.md).
- Click on an existing MEI Item in the [Navigator
    View](Navigator-View_7439192.md) and the editor will open.
- Via the context menu: Open the [XML Editor
    Perspective](XML-Editor-Perspective_7439322.md). Choose an MEI
    Item in the Navigator. Right-click it, choose “Open with...” \> and
    select “MEI Score Editor”.
- In this way, you can also open an MEI Item with the [XML
    Editor](XML-Editor_7439318.md) in order to work with the source
    code.

## Attachments

- [meise-createNewMEIfile.png](attachments/7439812/7766150.png)
(image/png)  
