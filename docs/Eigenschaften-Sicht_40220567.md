# Eigenschaften-Sicht

last modified on Aug 19, 2015

Die Eigenschaften-Sicht zeigt in zwei Spalten die Attribute und die
entsprechenden Werte an. Sie können die Werte bearbeiten, indem Sie ein
Element in der Gliederung-Sicht oder der Entwurf-, Quelle- oder
WYSIWYM-Ansicht auswählen und den gewünschten Wert direkt in das
entsprechende Textfeld der Wert-Spalte eintragen. Drücken Sie die
Eingabetaste, um Änderungen an Attributen zu bestätigen. Es wird
empfohlen, dass Sie Attribute in der Eigenschaften-Sicht bearbeiten,
wenn Sie in der [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md)
arbeiten, das Attribute in der WYSIWYM-Ansicht nicht angezeigt werden.

![](attachments/40220567/40436806.png "xml-properties-view.png")

|                         |
|-------------------------|
| **Eigenschaften-Sicht** |

- [Titelleiste der Eigenschaften-Sicht](Titelleiste-der-Eigenschaften-Sicht_40220569.md)
- [Kontextmenü der Eigenschaften-Sicht](40220571.md)

## Attachments

- [xml-properties-view.png](attachments/40220567/40436806.png)
(image/png)  
