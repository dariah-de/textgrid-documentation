# Aggregationen-Editor verwenden

last modified on Aug 19, 2015

Um in einer Aggregation, Kollektion oder Edition eine Referenz auf ein
Objekt zu erstellen, ziehen Sie diese im [Navigator](40220331.md) und
legen Sie sie im Baum, der im Aggregationen-Editor angezeigt wird, ab.
In diesem Fall wird das Objekt in die Aggregation verschoben. Wenn Sie
beim Ziehen des Objekts in die Aggregation die Strg-Taste gedrückt
halten, wird das Objekt in die Aggregation kopiert. In diesem Fall
bleibt das Original-Objekt unverändert und eine Kopie wird in der
Aggregation gespeichert. Sie können im Navigatorbaum oder im Feld “Teil
der Edition(en)” der Metadaten-Sicht des Elements sehen, zu welcher
Edition ein Element gehört. Speichern Sie die Änderungen durch Drücken
von \[Strg+S\], durch Klicken
von ![](attachments/40220450/40436728.png "save.png") in der
Werkzeugleiste oder durch Auswahl von “Datei \> Speichern” in der
Menüleiste. Ansonsten wird ein Assistent Sie daran erinnern, die
Änderungen zu speichern, wenn Sie den Aggregationen-Editor schließen. 

## Attachments

- [save.png](attachments/40220450/40436728.png) (image/png)  
