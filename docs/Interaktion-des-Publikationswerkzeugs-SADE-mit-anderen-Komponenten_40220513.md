# Interaktion des Publikationswerkzeugs SADE mit anderen Komponenten

last modified on Aug 19, 2015

Nach der Installation kann SADE mit "Publish to SADE" aus dem
Kontextmenü des [Navigators](40220346.md), dem
[XML-Editor](XML-Editor_40220521.md) und – sofern installiert – dem
[XML-Editor oXygen](40220597.md). geöffnet werden.
