# Open the Metadata Editor

last modified on Mai 03, 2016

Der Metadaten-Editor ist standardmäßig Bestandteil
des [XML-Editors](XML-Editor_40220521.md) und
der [Aggregationen-Editor](Aggregationen-Editor_40220440.md)-Perspektive
und kann in anderen Perspektiven auf verschiedene Weisen zugegriffen
werden:

1. durch Auswahl von “Werkzeuge \> Sicht anzeigen \> Metadaten” in der
    Menüleiste oder
2. durch Klicken von  in der Werkzeugleiste.

Um die Metadaten eines Objekts in den Metadaten-Editor zu laden, müssen
Sie sich authentifizieren und ein TextGrid-Objekt zur Bearbeitung
auswählen.

![](attachments/40220460/40436729.png)

|                            |
|----------------------------|
| **Metadaten-Editor-Sicht** |

## Attachments

- [080-zeige-MetadatenAnsicht.png](attachments/7439272/7766093.png)
(image/png)  
- [mde-metadataeditor-view.png](attachments/7439272/9240675.png)
(image/png)  
- [mde-metadataeditor-view.png](attachments/7439272/7766094.png)
(image/png)  
