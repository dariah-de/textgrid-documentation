# Publikationswerkzeug SADE verwenden

last modified on Jul 28, 2015

Wenn keine Zieladresse im Internet angegeben ist, wird eine Nachricht
eingeblendet. Bevor das Publikationswerkzeug verwendet werden kann, muss
eine SADE-Instanz festgelegt werden. Diese kann nach Auswahl von
"Fenster \> Benutzervorgaben" in der Menüleiste im Assistenten unter dem
Menüpunkt "Sade Publisher" gewählt werden. Ein Formular erlaubt Ihnen,
die Instanz über einen URL festzulegen und einen Nutzernamen samt
Passwort zu speichern. Sobald dieser Vorgang abgeschlossen ist, können
Objekte durch Auswahl von "Publish to SADE" im Kontextmenü eines Objekts
veröffentlicht werden. Für weitere Informationen, siehe

[http://www.bbaw.de/telota/projekte/digitale-Editionen/sade](http://www.bbaw.de/telota/projekte/digitale-editionen/sade)

Standardmäßig lautet der Benutzername für die eXist-Datenbank "admin",
eine Passworteingabe ist nicht erforderlich.
