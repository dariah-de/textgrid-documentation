# Properties View

last modified on Mai 04, 2016

Die Eigenschaften-Sicht zeigt in zwei Spalten die Attribute und die
entsprechenden Werte an. Sie können die Werte bearbeiten, indem Sie ein
Element in der Gliederung-Sicht oder der Entwurf-, Quelle- oder
WYSIWYM-Ansicht auswählen und den gewünschten Wert direkt in das
entsprechende Textfeld der Wert-Spalte eintragen. Drücken Sie die
Eingabetaste, um Änderungen an Attributen zu bestätigen. Es wird
empfohlen, dass Sie Attribute in der Eigenschaften-Sicht bearbeiten,
wenn Sie in
der [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md) arbeiten, das
Attribute in der WYSIWYM-Ansicht nicht angezeigt werden.

![](attachments/40220567/40436806.png "xml-properties-view.png")

|                         |
|-------------------------|
| **Eigenschaften-Sicht** |

- [Title Bar of the Properties View](Title-Bar-of-the-Properties-View_7439351.md)
- [Context Menu of the Properties View](Context-Menu-of-the-Properties-View_7439353.md)

## Attachments

- [xml-properties-view.png](attachments/7439349/7766142.png) (image/png)  
