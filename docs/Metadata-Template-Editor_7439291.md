# Metadata Template Editor

last modified on Mai 03, 2016

Der Metadaten-Vorlagen-Editor erlaubt Ihnen, für Ihre Projekte eigene
Felder im Metadaten-Editor anzulegen. Sie müssen die
"Projekt-Manager"-Rolle innehaben, um mit diesem Editor zu arbeiten.

- [Open the Metadata Template Editor](Open-the-Metadata-Template-Editor_7439293.md)
- [Using the Metadata Template Editor](Using-the-Metadata-Template-Editor_7439295.md)
- [Interaction of the Metadata Template Editor View with Other Components](Interaction-of-the-Metadata-Template-Editor-View-with-Other-Components_7439297.md)
