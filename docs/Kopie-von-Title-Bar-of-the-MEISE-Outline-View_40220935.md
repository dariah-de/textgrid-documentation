# Kopie von Title Bar of the MEISE Outline View

Created on Jul 24, 2015

The Outline View title bar contains a
button **![](attachments/40220935/40437036.png "icon-reading-active.png")**
which enables switching between different previously declared sources.
These sources themselves define different variants which can been seen
in the Musical Score View while switching. See [Musical Variants](Musical-Variants_7439904.md).

The Filter function ![](attachments/40220935/40437037.png "filter.png")
of the title bar is located in the downward triangle.

## Attachments

- [icon-reading-active.png](attachments/40220935/40437036.png)
(image/png)  
- [filter.png](attachments/40220935/40437037.png) (image/png)  
- [meise-zoominzoomout.png](attachments/40220935/40437038.png)
(image/png)  
