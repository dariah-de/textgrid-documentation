# Kopie von Palette

Created on Jul 24, 2015

The Palette will open every time the Score View is activated. It
consists of the instruments and items which you can use in the Outline
View. Practical instructions for using these instruments (drag & drop
onto the Outline View), are described in the section about “[Using the MEI Score Editor](Using-the-MEI-Score-Editor_7439898.md)“ . You can
hide the Palette by clicking the white triangle at the top right or by
moving it to the other side of the Score View by dragging it like a
view.

![](attachments/40220925/40436990.png)

|         |
|:-------:|
| Palette |

- [Kopie von Features of the Palette](Kopie-von-Features-of-the-Palette_40220927.md)
- [Kopie von Context Menu of the Palette](Kopie-von-Context-Menu-of-the-Palette_40220929.md)

## Attachments

- [mei-paletteview.png](attachments/40220925/40436990.png) (image/png)  
- [palette.PNG](attachments/40220925/40436991.png) (image/png)  
- [meise-f1_palette.png](attachments/40220925/40436992.png) (image/png)  
