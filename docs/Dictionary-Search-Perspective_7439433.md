# Dictionary Search Perspective

last modified on Mai 03, 2016

Die Perspektive ist in drei Sichten unterteilt: die
Wörterbuchsuche-Sicht oben links, die Wörterbuchsuchergebnisse-Sicht
oben rechts und de Wörterbuch-Grid-Sicht unten.

![](attachments/40220706/40436894.png)

|                                 |
|---------------------------------|
| **Wörterbuchsuche-Perspektive** |

## Attachments

- [dic-dictionarysearch.png](attachments/7439433/9240701.png)
(image/png)  
- [dic-dictionarysearch.bmp](attachments/7439433/9240700.bmp)
(image/bmp)  
- [dic-dictionarysearch.png](attachments/7439433/7766180.png)
(image/png)  
