# CSS-Editor

last modified on Jul 31, 2015

Standardmäßig gibt es keinen speziellen CSS-Editor im TextGrid. Sie
können jedoch einen CSS-Editor von der Eclipse-Indigo-Update-Website
installieren:

1. Wählen Sie "Hilfe \> Neue Software installieren…" in der Menüleiste.
2. Tragen Sie den URL
    [*http://download.eclipse.org/releases/indigo*](http://download.eclipse.org/releases/indigo)
    in das Feld “Arbeit mit” ein.
3. Wählen Sie in der Liste, die dann darunter angezeigt wird, in der
    Kategorie *"Web, XML, ...*" den Unterpunkt *"Web Page Editor"*
    (vorletzter Eintrag).
4. Schließen Sie den Assistenten und starten Sie das TextGridLab neu.

Danach werden vorhandene CSS-Objekte mit dem CSS-Editor inklusive
Syntaxhervorhebung (“syntax highlighting”), Autovervollständigung, einer
Gliederung- und einer Eigenschaften-Sicht geöffnet.
