# Publish View

last modified on Mai 03, 2016

Die Publish-Sicht besteht aus mehreren Bereichen.

- Der obere Bereich informiert Sie über den Status Ihrer Objekt(e).
    Nach der Initialisierung der Sicht werden Sie darauf hingewiesen,
    dass die Edition oder Kollektion jetzt geprüft werden kann.
- Der untere Bereich erlaubt Ihnen, zu “Prüfen”, zu “Veröffentlichen”
    (nach dem Prüfen) und “Ab\[zu\]brechen”. Nach Klicken der
    Schaltfläche “Prüfen” wechseln die Informationen, die im oberen
    Bereich angezeigt werden, von “Die Kollektion/Edition kann jetzt
    geprüft werden ” zu “Die Kollektion/Edition ist noch nicht für die
    Veröffentlichung geeignet” oder “Die Kollektion/Edition ist jetzt
    für die Veröffentlichung geeignet ”.
- In den farbigen Kästchen im mittleren Bereich können Sie sehen, wie
    viele OKs (Zulassungen) , Warnungen oder Fehler aufgetreten sind.
    Weitere Informationen werden unterhalb angezeigt. Der mittlere
    Bereich ist in drei Spalten (“Name”, “Status” und “Aktion”)
    unterteilt. Wenn eine Warnung oder ein Fehler aufgetreten ist,
    werden genauere Informationen in der Spalte “Aktionen” der Liste
    angezeigt. Durch Klicken eines Eintrags im Feld “Aktionen” einer
    Fehlermeldung erscheint eine Schaltfläche “Aktion ausführen”.
    Klicken Sie diese, um zur Quelle des Fehlers oder der Warnung
    weitergeleitet zu werden.
- Wenn keine Warnungen oder Fehler im mittleren Bereich angezeigt
    werden, wird “ok” in der Spalte “Status” angezeigt und die
    Schaltfläche “Veröffentlichen” im unteren Bereich wird aktiviert.
    Klicken Sie “Veröffentlichen”, um den Vorgang abzuschließen. Dieser
    Vorgang ist unwiderruflich, d. h. er kann nicht rückgängig gemacht
    werden.

![](attachments/40220497/40436735.png)

|                   |
|-------------------|
| **Publish-Sicht** |

## Attachments

- [pub-proofednotsuitable.png](attachments/7439463/9240678.png)
(image/png)  
- [pub-proofednotsuitable.png](attachments/7439463/7766190.png)
(image/png)  
