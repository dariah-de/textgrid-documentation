# Generische Metadaten

last modified on Mai 03, 2016

Die folgenden Metadaten sind generisch. Das heißt, dass sie in den
Metadatenformularen aller TextGrid-Objekte angezeigt werden:

1. Title(s) (Objekt-)Titel
2. Identifier(s) Identifikatoren
3. Format Format
4. Notes Notizen

Das Pflichtelement "Title" soll für den Titel und eventuelle Untertitel
des Objekts verwendet werden. Sie können mit den entsprechenden
Schaltflächen Titel hinzufügen oder entfernen.

Fügen Sie eine Art von Identifikator für das Objekt aus einem
kontrollierten Vokabular hinzu und wählen Sie im Aufklappmenü zwischen
ISBN, ISSN, URL oder Kalliope (= RNA). ISBN, ISSN, URL und Kalliope sind
allgemeine Standardwerte, es können auf Wunsch auch andere Typen
hinzugefügt werden. Weitere Elemente von Typ "Identifikator" können mit
den entsprechenden Schaltflächen hinzugefügt werden.

Verwenden Sie das Element "Format", um die benötigten Informationen über
den MIME-Type (= Internet Media Type) anzugeben. In den meisten Fällen
wird ein Standardwert vorgeschlagen:

| **generelle Text-/Bild-Formate, Anwendungen** | **TextGrid-spezifische Formate**                   | **XML-bezogene Formate**               | **Workflow-spezifische Formate**                             |
|-----------------------------------------------|----------------------------------------------------|----------------------------------------|--------------------------------------------------------------|
| Klartext (text/plain)                         | Aggregation (text/tg.aggregation+xml)              | XML-Dokument (text/xml)                | TextGrid-Workflow-Dokument (text/tg.workflow+xml)            |
| JPEG-Bild (image/jpeg)                        | Kollektion (text/tg.Kollektion+tg.aggregation+xml) | XML-Schema (text/xsd+xml)              | TextGrid-Dienstbeschreibung (text/tg.servicedescription+xml) |
| GIF-Bild (image/gif)                          | Edition (text/tg.Edition+tg.aggregation+xml)       | XSLT-Stylesheet (text/xml+xslt)        |                                                              |
| TIFF-Bild (image/tiff)                        | Werk (text/tg.work+xml)                            | Cascading Stylesheet (CSS) (text/css)  |                                                              |
| PNG-Bild (image/png)                          | Element (text/tg.item+xml)                         | DTD (application/xml-dtd)              |                                                              |
| ZIP-Archiv (application/zip)                  | Text-Bild-Link-Objekt (text/linkeditorlinkedfile)  | MEI-Dokument (text/mei+xml)            |                                                              |
|                                               |                                                    | Kollationsset (text/collation-set+xml) |                                                              |

Das Feld “Notes” kann für jede andere Art von Information genutzt
werden.
