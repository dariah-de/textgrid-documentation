# Bildvorschau-Sicht

last modified on Aug 03, 2015

Die Bildvorschau-Sicht öffnet sich zeitgleich mit der Bild-Sicht. Sie
besteht aus einem Schieberegler, um den Zoomfaktor in der Bild-Sicht
anzupassen, und einem Markierungsrahmen, um den aktiven Bildausschnitt
in der Bild-Sicht mit der Maus zu verschieben. Klicken Sie auf den
Bildausschnitt, auf den Sie fokussieren möchten.

![](attachments/40220655/40436841.png "tble-thumb-view.png")

|                        |
|------------------------|
| **Bildvorschau-Sicht** |

## Attachments

- [tble-thumb-view.png](attachments/40220655/40436841.png) (image/png)  
