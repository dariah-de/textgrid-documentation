# Context Menu of the Design View

last modified on Mai 04, 2016

Das Kontextmenü der Entwurf-Ansicht erlaubt Ihnen, auf die meisten
Funktionen der Ansicht durch Rechtsklicken zuzugreifen.

- Um ein XML- Objekt zu löschen, rechtsklicken Sie es und wählen Sie
    “Löschen”.
- Um eine DOCTYPE-Deklaration in Ihr Dokument einzufügen,
    rechtsklicken Sie eine beliebige Position in der Entwurf-Ansicht und
    wählen Sie “DTD-Informationen hinzufügen...”.
- Um neue oder bereits vorhandene Namensbereiche im Dokument zu
    bearbeiten, rechtsklicken Sie eine beliebige Position in der
    Entwurf-Ansicht und wählen Sie “Namensbereiche bearbeiten...”.
- Verarbeitungsanweisungen können nach der Auswahl von
    “Verarbeitungsanweisung hinzufügen” in einem Assistenten bearbeitet
    werden.
- Um ein Element an einer bestimmten Position relative zu einem
    Element im Dokument einzufügen, rechtsklicken Sie es und wählen Sie
    eines der vorgeschlagenen Elemente. Wenn Sie “Untergeordnetes
    Element hinzufügen”, “Einfügen vor” oder “Einfügen nach” auswählen,
    können Sie die folgenden Elemente hinzufügen:
- einen Kommentar
  - eine Verarbeitungsanweisung
  - PCDATA ![](attachments/40220545/40436783.gif "file_obj.gif")
  - einen CDATA-Abschnitt
  - ein neues
        Element ![](attachments/40220545/40436784.gif "element.gif")
- Sie können einem Element Attribute hinzufügen, indem Sie es
    rechtsklicken und “Neues Attribut…” wählen und dann Attribute aus
    der Vorschlagsliste auswählen.
- Um Attribute zu bearbeiten, rechtsklicken Sie das Attribut in der
    Entwurf-Ansicht und wählen Sie “Attribut bearbeiten...”.

Die Listen vorgeschlagener Elemente und Attribute werden von einer
Inhaltshilfe zur Verfügung gestellt. Vorschläge kommen von einem
referenzierten Inhaltsmodell, wenn ein Schema für das Dokument
festgelegt ist, oder aus dem XML-Katalog, wenn kein Schema festgelegt
ist. Die wird Ihnen “sinnvollere” Vorschläge anbieten wie beispielsweise
spezifische Kind-Elemente innerhalb eines gegebenen Elements, sowie
notwendige Attribute vorschlagen.

## Attachments

- [element.gif](attachments/8129677/8192261.gif) (image/gif)  
- [file_obj.gif](attachments/8129677/8192262.gif) (image/gif)  
