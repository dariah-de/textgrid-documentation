# XML-Editor-Perspektive

last modified on Aug 19, 2015

Sobald Sie die XML-Editor-Perspektive geöffnet haben, sehen Sie drei
Bereiche:

1. der [Navigator](40220331.md) und zwei versteckte Reiter auf der
    linken Seite,
2. die [Editor-Sicht](Editor-Sicht_40220539.md) in der Mitte, die
    leer ist, bi sein XML-Dokument geöffnet oder erstellt wird,
3. die beiden Sichten ’[Gliederung](Gliederung-Sicht_40220561.md)’
    und ’[Eigenschaften](Eigenschaften-Sicht_40220567.md)’ auf der
    rechten Seite, die leer sind, wenn kein XML-Dokument geöffnet ist.

- [Funktionen des XML-Editors](Funktionen-des-XML-Editors_40220527.md)
- [Menüleiste der XML-Editor-Perspektive](40220529.md)
- [Werkzeugleiste der XML-Editor-Perspektive](Werkzeugleiste-der-XML-Editor-Perspektive_40220531.md)
- [Statusleiste der XML-Editor-Perspektive](Statusleiste-der-XML-Editor-Perspektive_40220533.md)
- [Tastaturkürzel der XML-Editor-Perspektive](40220535.md)
