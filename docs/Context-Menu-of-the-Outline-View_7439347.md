# Context Menu of the Outline View

last modified on Mai 04, 2016

Das Kontextmenü der Gliederung-Sicht stellt mehrere Operationen zur
Verfügung:

- Einen [Adapter assoziieren](Funktionen-des-XML-Editors_40220527.md) ![](attachments/40220565/40436805.png "064-verbinde-Adaptor2.png")
- Einen    URI ![](attachments/40220565/40436803.png "026-kopiere-URI3.png") oder
    ein
    URI-Fragment ![](attachments/40220565/40436801.png "066-kopiere-URlFragment2.png") kopieren
- Löschen ![](attachments/40220565/40436797.png "delete.png"),
    (Technische) [Metadaten anzeigen](Metadaten-Editor_40220458.md) (![](attachments/40220565/40436799.png "029-zeige-TechnischeMetadaten.png") und ![](attachments/40220565/40436800.png "028-neulade-Metadaten.png")), [Revisionen anzeigen](Revisionen--und-Sperre-Mechanismus_40220412.md) und
    CRUD-Warnungen
    anzeigen![](attachments/40220565/40436798.png "warning.png")
- Nach SADE publizieren
- Operationen ausführen, die mit dem Kontextmenü
    der [Entwurf-Ansicht](Entwurf-Ansicht_40220541.md) durchgeführt
    werden können.

## Attachments

- [064-verbinde-Adaptor2.png](attachments/7439347/8192244.png)
(image/png)  
- [026-kopiere-URI.png](attachments/7439347/8192245.png) (image/png)  
- [026-kopiere-URI3.png](attachments/7439347/8192246.png) (image/png)  
- [066-kopiere-URlFragment.png](attachments/7439347/8192247.png)
(image/png)  
- [066-kopiere-URlFragment2.png](attachments/7439347/8192248.png)
(image/png)  
- [028-neulade-Metadaten.png](attachments/7439347/8192249.png)
(image/png)  
- [029-zeige-TechnischeMetadaten.png](attachments/7439347/8192250.png)
(image/png)  
- [warning.png](attachments/7439347/8192251.png) (image/png)  
- [delete.png](attachments/7439347/8192252.png) (image/png)  
