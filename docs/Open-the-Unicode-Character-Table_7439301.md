# Open the Unicode Character Table

last modified on Mai 03, 2016

Die Unicode-Zeichen-Tabelle öffnet sich standardmäßig automatisch als
Teil der [XML-Editor Perspektive](XML-Editor-Perspektive_40220525.md).
Zusätzlich kann Sie auf die folgenden Weisen geöffnet werden:

- Klicken
    Sie ![](attachments/40220677/40436861.png "accessories-character-map.png") in
    der Werkzeugleiste.
- Wählen Sie “Werkzeuge  \> Sicht anzeigen \> Unicode-Zeichen-Tabelle”
    in der Menüleiste.

Die Unicode-Zeichen-Tabelle öffnet sich automatisch mit dem XML-Editor
und befindet sich dann als versteckter Reiter hinter
dem [Navigator](40220331.md) in der linken Sicht.

## Attachments

- [accessories-character-map.png](attachments/7439301/7766108.png)
(image/png)  
- [081-zeige-SonderzeichenAnsicht.gif](attachments/7439301/8192301.gif)
(image/gif)  
