# Werkzeugleiste des oXygen-XML-Editors

last modified on Jul 31, 2015

Wenn der oXygen-XML-Editor geöffnet ist, werden Elemente aus der
Werkzeugleiste dieses Editors in der Werkzeugleiste des TextGridLab
hinzugefügt. Diese können beispielsweise zur Validierung und
Transformation verwendet werden. Für weitere Informationen, siehe

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)
