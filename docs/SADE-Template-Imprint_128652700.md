# SADE Template Imprint

last modified on Jun 09, 2022

## Impressum

### Anbieter

Anbieter dieser Internetpräsenz ist im Rechtssinne die Niedersächsische
Staats- und Universitätsbibliothek Göttingen:

Georg-August-Universität Göttingen  
Niedersächsische Staats- und Universitätsbibliothek Göttingen  
Platz der Göttinger Sieben 1  
37073 Göttingen

Tel.: +49 (0) 551 - 39 5212  
Fax: +49 (0) 551 - 39 5222  
sekretariat(at)[sub.uni-goettingen.de](http://sub.uni-goettingen.de)  
[www.sub.uni-goettingen.de](https://www.sub.uni-goettingen.de/)

### Vertreter

Die Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB
Göttingen) wird vertreten durch den leitenden Direktor Prof. Dr. Wolfram
Horstmann.

Die SUB Göttingen ist als zentrale Einrichtung eine organisatorische,
rechtlich nicht selbständige Einheit der Georg-August-Universität
Göttingen. Die Georg-August-Universität Göttingen ist eine Körperschaft
des öffentlichen Rechts. Sie wird durch den Präsidenten Prof. Dr. Metin
Tolan gesetzlich vertreten:

Georg-August-Universität Göttingen  
Wilhelmsplatz 1  
37073 Göttingen  
Tel.: +49 (0)551 / 39-0  
Fax: +49 (0)551 / 39-9612  

### Zuständige Aufsichtsbehörde

Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts  
Stiftungsausschuss Universität (§§ 59 Abs. 2, 60 Abs. 2 Satz 2 Nr. 7, 60
a Abs. 1 NHG)  
Wilhelmsplatz 1  
37073 Göttingen

Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE
152 336 201

Inhaltlich Verantwortlicher gemäß § 55 Abs. 2 RStV:  
Prof. Dr. Wolfram Horstmann (Projektleitung)  
Platz der Göttinger Sieben 1  
37073 Göttingen  
Tel.: +49 (0)551 - 39 33866  
Fax: +49 (0)551 - 39 33856  
E-Mail:
horstmann\[at\][sub.uni-goettingen.de](http://sub.uni-goettingen.de)

#### Zuständiger Datenschutzbeauftragter

[Datenschutzbeauftragter der Universität Göttingen (ohne UMG)](https://www.uni-goettingen.de/en/582169.md)

#### Haftungshinweis

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung
für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten
sind ausschließlich deren Betreiber verantwortlich
