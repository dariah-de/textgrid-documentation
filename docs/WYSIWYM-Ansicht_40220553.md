# WYSIWYM-Ansicht

last modified on Aug 19, 2015

Die WYSIWYM (=“What You See Is What You Mean”)-Ansicht stellt eine mit
einem Textverarbeitungsprogramm vergleichbare Schnittstelle zur
Verfügung und versteckt die XML-Tags vor dem Benutzer. Sie verwendet das
TEI-Schema und ein Standard-CSS-Stylesheet zur Festlegung des
Dokumentlayouts.

Um andere XML-Formate zu bearbeiten oder ein nutzerspezifisch
angepasstes Layout festzulegen, können Sie Ihr eigenes Schema und
CSS-Stylesheet verwenden. Ihr Stylesheet sollte Element vom Typ
Paragraph als “display:block” festlegen.

Wenn Ihr Dokument nicht als TEI-Dokument erkannt worden ist und kein
assoziiertes Stylesheet gefunden wurde, haben Sie die Möglichkeit, ein
[Stylesheet zu assoziieren](Funktionen-des-XML-Editors_40220527.md),
bevor Sie fortfahren. Verwenden Sie das
[Import-Werkzeug](40220393.md), um zuerst ein CSS zu importieren oder
um ohne Assoziieren eines Stylesheets fortzufahren (nicht empfohlen).

![](attachments/40220553/40436788.png)

|                 |
|-----------------|
| WYSIWYM-Ansicht |

- [Funktionen der WYSIWYM-Ansicht](Funktionen-der-WYSIWYM-Ansicht_40220555.md)
- [Kontextmenü der WYSIWYM-Ansicht](40220557.md)

## Attachments

- [xml-wysiwymview.png](attachments/40220553/40436788.png) (image/png)  
- [xml-blockinlinemarker-detail.png](attachments/40220553/40436789.png)
(image/png)  
- [inlinemarkers.png](attachments/40220553/40436790.png) (image/png)  
- [blockmarkers.png](attachments/40220553/40436791.png) (image/png)  
