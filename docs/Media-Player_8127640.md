# Media Player

last modified on Mai 04, 2016

Unter dem Medium werden verschiedene Steuerelemente angezeigt, die es
Ihnen erlauben, die Lautstärke anzupassen, das Video abzuspielen oder
anzuhalten, es vor- oder zurückzuspulen sowie die Einstellungen für die
Pufferzeit zu ändern. Rechts von der Videoanzeige werden
Medieninformationen inklusive Ressource, Mediendatei und Laufzeit
angezeigt. Wenn ein Abschnitt des Videos ausgewählt worden ist, wird die
Start- und Endzeit des Abschnitts ebenfalls hier angezeigt. Sowohl der
Media Player als auch die Informationsanzeige können minimiert werden.
