# Open the XML Editor

last modified on Mai 04, 2016

Um den XML-Editor zu öffnen

- wählen Sie “XML-Editor” auf dem Startbildschirm oder
- wählen Sie “Werkzeuge \> XML-Editor” in der Menüleiste oder
- klicken
    Sie ![](attachments/40220523/40436747.png "071-zeige_XMLEditorPerspektive.png") in
    der Werkzeugleiste.

## Attachments

- [071-zeige_XMLEditorPerspektive.png](attachments/7439320/7766118.png)
(image/png)  
