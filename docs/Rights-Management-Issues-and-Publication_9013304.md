# Rights Management Issues and Publication

Created on Mai 30, 2012

## Rights Management Issues and Publication

The role-based rights management and the user's ability to invite others
to join her Project according to pre- or individually defined roles is
one of the greatest strengths (i.e. enabling collaboration) of TextGrid
– but is also the source of some sophisticated application logic slowing
down things slightly. TG-search, for instance, has to ask for every
match of a user's query whether this user is allowed to “find” this
object. For this (and other) reasons, there‟s another, freely accessible
search index where published objects are fed in. Since published objects
are „frozen‟. Accordingly, the now immutable object is being moved from
the dynamic (frequent I/O operations) to a static grid storage instance.
Being published, an object is associated with a persistent identifier
([Handle](http://www.handle.net)), and its metadata and format is being
validated. Furthermore, this object must be part of an Edition or
Collection to assure a certain quality level of the metadata.

(insert image here) Figure 4: TextGrid infrastructure and publishing
workflow
