# Using the Import Tool

Das Import-Werkzeug kann nicht nur verwendet werden, um lokale Dateien
als TextGrid-Objekte zu importieren, sondern auch um sie danach mit oder
ohne Metadaten zu re-importieren.

- [Import Local Files](Import-Local-Files_8130304.md)
- [Re-Import TextGrid Objects](Re-Import-TextGrid-Objects_8128480.md)
