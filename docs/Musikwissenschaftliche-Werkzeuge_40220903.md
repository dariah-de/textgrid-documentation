# Musikwissenschaftliche Werkzeuge

last modified on Nov 23, 2015

Als leistungsfähiges Werkzeug für die Erstellung digitaler
musikwissenschatlicher Editionen bietet TextGrid den Noteneditor MEISE.
Die Dokumentation dieses Werkzeugs liegt derzeit nur auf Englisch vor.

[Zur englischen Dokumentation](Musicological-Tools_7439987.md)
