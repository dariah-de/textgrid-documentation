# Das Lemmatisierer-Werkzeug

last modified on Aug 04, 2015

Das Lemmatisierer-Werkzeug besteht aus drei Elementen: ein Eingabefeld,
ein Konfigurationsfeld und ein Ergebnisfeld. Wenn Sie “Werkzeuge \>
Sicht anzeigen \> Wortform lemmatisieren” wählen, ist das Eingabefeld
für eine deutsche Wortform vorgesehen. Wenn Sie “Werkzeuge \> Sicht
anzeigen \> Datei lemmatisieren” wählen, öffnet sich der
“BatchLemmatisierer” zur Lemmatisierung eines ganzen Dokuments und Sie
können abhängig vom Format Ihres Dokuments zwischen der Lemmatisierung
von ASCII-Klartext, einer deutschen Wortform-Liste oder tokenisiertem
TEI-konformem XML wählen. Klicken Sie “Eingabedatei spezifizieren”, um
eine Datei von Ihrem Computer auszuwählen und hochzuladen. Stellen Sie
sicher, dass Sie das entsprechende Dateiformat gewählt haben.

Im Konfigurationsfeld wird die aktuelle Form der Analyse gezeigt.
Klicken Sie “Konfiguration einstellen”, um diese zu ändern. Ein
Assistent öffnet sich, in dem Sie das Ausgabeformat und das Lexikon
auswählen können. Sie können verschiedene Optionen für die Analyse
festlegen: nur Lemmatisierung, Disambiguierung, Schätzer für unbekannte
Wortformen, unscharfe (“fuzzy”) Suche und ZLib-Kompression.

Nach Klicken der Schaltfläche “Lemmatisierer starten”, werden die
Ergebnisse im Ergebnisfeld unterhalb des Konfigurationsfelds angezeigt.
Wenn Sie das Ergebnis der Lemmatisierung einer Datei speichern möchten,
klicken Sie die Schaltfläche “Ausgabe speichern” am unteren Ende des
Ergebnisfelds.
