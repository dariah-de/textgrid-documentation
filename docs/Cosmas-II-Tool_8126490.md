# Cosmas II Tool

last modified on Mai 04, 2016

Das Cosmas II-Werkzeug besteht aus zwei Elementen: einem Eingabefeld und
einem Ergebnisfeld. Geben Sie ein Wort oder eine Wortfolge im
Eingabefeld und drücken Sie die Eingabetaste oder klicken Sie die
Schaltfläche “Suche”, um eine Anfrage abzuschicken, oder klicken Sie den
nach unten gerichteten Pfeil am Ende des Eingabefelds, um ein
Aufklappmenü zu öffnen und ein Wort oder eine Wortfolge auszuwählen,
nach der Sie bereits zuvor in dieser Sitzung gesucht hatten. Die Suche
berücksichtigt keine Groß- und Kleinschreibung.

![](attachments/40220752/40436904.png) 

|                        |
|------------------------|
| **Cosmas II-Werkzeug** |

Die Ergebnisse werden im unteren Feld mit Ihrer Quelle, einer
KWIC-Ansicht (Key Word in Context), die das gesuchte Schlüsselwort in
seinem Kontext zeigt, und einer Referenz angezeigt. Ergebnisse
verschiedener Anfragen werden in verschiedenen Reitern angezeigt.
Verwenden Sie das Kontextmenü, um ausgewählte Ergebnisse in die
Zwischenablage zu kopieren, sie als CSV-Datei zu exportieren oder um
alle Ergebnisse auszuwählen.

## Attachments

- [textgrid-manual-screenshot-cosmas01.jpg](attachments/8126490/8192402.jpg)
(image/jpeg)  
- [lit-cosmasiiview.png](attachments/8126490/9240703.png) (image/png)  
