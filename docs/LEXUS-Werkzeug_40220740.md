# LEXUS-Werkzeug

last modified on Aug 04, 2015

Das LEXUS-Werkzeug besteht aus drei Elementen: einem Eingabefeld, einem
Bereich, in dem Sie verschiedene Optionen für die Erweiterte Suche
einstellen können, und einem Ergebnisfeld. Geben Sie ein Wort oder eine
Wortfolge im Eingabefeld ein und drücken Sie die Eingabetaste oder
klicken Sie die Schaltfläche “Suche”, um eine Anfrage abzuschicken. Sie
können auch das Aufklappmenü am Ende des Eingabefelds verwenden und ein
Wort oder eine Wortfolge auswählen, nach der Sie bereits zuvor in dieser
Sitzung gesucht hatten. Die Suche berücksichtigt Groß- und
Kleinschreibung.

Sie können erweiterte Optionen einstellen, nachdem Sie den Bereich unter
dem Eingabefeld durch Öffnen der Zeile unterhalb des Suchterms geöffnet
haben. Ein Ziellexikon kann ausgewählt werden. Derzeit werden Daten vom
deutschen Wörterbuch elexiko
([http://www.owid.de/wb/elexiko/start.html](http://www.owid.de/wb/elexiko/start.md))
vom Institut für Deutsche Sprache in Mannheim, Germany sowie einem
Wichita Lexikon vom Max-Planck-Institut für Psycholinguistik
([http://www.lat-mpi.eu/tools/lexus/](http://www.lat-mpi.eu/tools/lexus/))
in Nijmegen zur Verfügung gestellt.

Die Zieldatenkategorien, die gewählt werden können, hängen von der
Struktur des gewählten Lexikons ab. Bezogen auf die Zieldatenkategorie
können verschiedene Suchbedingungen eingestellt werden: "ist genau“,
"enthält“, "beginnt mit“ und "endet mit“ ("is“, "contains“, "begins
with“ und "ends with“). Zusätzlich zu diesen Optionen können Sie über
das Ankreuzfeld "negiert“ ("is negated“) Ihre Anfrageoptionen umkehre.

Die Suchergebnisse werden im Ergebnisfeld unterhalb des Optionen-Menüs
angezeigt. Ergebnisse verschiedener Anfragen werden in verschiedenen
Reitern angezeigt. Auf der linken Seite des Ergebnisses finden Sie eine
dem gewählten Lexikon entsprechende Ordnerbaumstruktur. Der
Lexikoneintrag selbst wird auf der rechten Seite angezeigt.

![](attachments/40220740/40436899.png)

|                    |
|--------------------|
| **LEXUS-Werkzeug** |

## Attachments

- [lit-lexusview.png](attachments/40220740/40436899.png) (image/png)  
- [textgrid-manual-screenshot-lexus01.jpg](attachments/40220740/40436900.jpg)
(image/jpeg)  
