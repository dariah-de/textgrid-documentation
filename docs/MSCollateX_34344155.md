# MSCollateX

last modified on Mai 04, 2016

|                                    |                                                                                                                                                                                                                                                                                                   |
|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | CollateX                                                                                                                                                                                                                                                                                          |
| Beschreibung                       | CollateX ist ein Programm zur Kollationierung von Texten um einen kritischen Apparat zu erzeugen. Es ist der Nachfolger von Peter Robinsons Collate und wird von verschiedenen Partnerinstitutionen und Einzelpersonen im Rahmen der europäischen Initiative "Interedition" gemeinsam entwickelt. |
| Logo                               | ![](attachments/34344155/34472171.png)                                                                                                                                                                                                                                                            |
| Lizenz                             | [http://www.gnu.org/licenses/lgpl-3.0.txt](http://www.gnu.org/licenses/lgpl-3.0.txt)                                                                                                                                                                                                              |
| Systemanforderungen                |                                                                                                                                                                                                                                                                                                   |
| Sourcecode                         |                                                                                                                                                                                                                                                                                                   |
| Projekte, die das Plugin verwenden |                                                                                                                                                                                                                                                                                                   |
| Beispieldateien                    |                                                                                                                                                                                                                                                                                                   |
| Installationsanleitung             | [Install CollateX](Install-CollateX_8129948.md)                                                                                                                                                                                                                                                 |
| Dokumentation                      | [CollateX](CollateX_7440112.md)                                                                                                                                                                                                                                                                 |
| Screenshots                        |                                                                                                                                                                                                                                                                                                   |
| Tutorials                          |                                                                                                                                                                                                                                                                                                   |
| Entwickler                         |                                                                                                                                                                                                                                                                                                   |
| Mitentwickler                      |                                                                                                                                                                                                                                                                                                   |
| Homepage/Kontakt                   |                                                                                                                                                                                                                                                                                                   |
| Bugtracker                         |                                                                                                                                                                                                                                                                                                   |

## Attachments

- [collatex_logo_small.png](attachments/34344155/34472171.png)
(image/png)  
- [meise_64.png](attachments/34344155/34472172.png) (image/png)  
- [digilib-logo-big.png](attachments/34344155/34472173.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/34344155/34472174.png)
(image/png)  
- [screenshotSADE.png](attachments/34344155/34472175.png) (image/png)  
