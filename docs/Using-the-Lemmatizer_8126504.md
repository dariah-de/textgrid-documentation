# Using the Lemmatizer

last modified on Mai 04, 2016

Sie können entweder eine Wortform lemmatisieren oder ein ganzes Dokument
lemmatisieren.

- [Lemmatize a Wordform](Lemmatize-a-Wordform_8126506.md)
- [Lemmatize a Document](Lemmatize-a-Document_8126509.md)
