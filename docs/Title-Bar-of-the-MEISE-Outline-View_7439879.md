# Title Bar of the MEISE Outline View

last modified on Mai 04, 2016

The Outline View title bar contains a
button **![](attachments/7439879/8192197.png "icon-reading-active.png")** which
enables switching between different previously declared sources. These
sources themselves define different variants which can been seen in the
Musical Score View while switching. See [Musical Variants](Musical-Variants_7439904.md).

The Filter function ![](attachments/7439879/8192113.png "filter.png") of
the title bar is located in the downward triangle.

## Attachments

- [meise-zoominzoomout.png](attachments/7439879/8192034.png) (image/png)  
- [filter.png](attachments/7439879/8192113.png) (image/png)  
- [icon-reading-active.png](attachments/7439879/8192197.png) (image/png)  
