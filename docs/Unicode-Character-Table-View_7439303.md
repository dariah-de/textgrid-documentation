# Unicode Character Table View

last modified on Mai 03, 2016

Die Unicode-Zeichen-Tabelle besteht aus seiner Werkzeugleiste und einem
Textfeld in der Kopfzeile, einer Tabelle im Hauptteil und einem
Informationsfeld mit Schaltflächen in der Fußleiste.

![](attachments/40220679/40436863.png "uct-chartable-view.png")

|                             |
|-----------------------------|
| **Unicode-Zeichen-Tabelle** |

- [Toolbar and Header of the Unicode Character Table View](Toolbar-and-Header-of-the-Unicode-Character-Table-View_7439306.md)
- [The Body of the Unicode Character Table View](The-Body-of-the-Unicode-Character-Table-View_7439310.md)
- [The Information Field of the Unicode Character Table View](The-Information-Field-of-the-Unicode-Character-Table-View_7439312.md)
- [Custom Charset Editor](Custom-Charset-Editor_7439308.md)

## Attachments

- [uct-chartable-view.png](attachments/7439303/7766109.png) (image/png)  
- [Unicode-Surrogate-Support.reg](attachments/7439303/15566865.reg)
(text/x-ms-regedit)  
