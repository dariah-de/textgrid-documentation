# Kopie von MSTextGridRepPreview

Created on Jul 23, 2015

|                                    |                                                                                                                                                                       |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | TextGridRep-Vorschau                                                                                                                                                  |
| Beschreibung                       | Die TextGridRep-Vorschau zeigt für noch nicht veröffentlichte TEI-Dokumente im TextGridLab, wie diese nach der Veröffentlichung im Rep (ungefähr) dargestellt würden. |
| Logo                               | [http://textgridrep.de/images/textgridrep_logo.png](http://textgridrep.de/images/textgridrep_logo.png)                                                                |
| Lizenz                             | [http://www.gnu.org/licenses/lgpl-3.0.txt](http://www.gnu.org/licenses/lgpl-3.0.txt)                                                                                  |
| Systemanforderungen                |                                                                                                                                                                       |
| Sourcecode                         |                                                                                                                                                                       |
| Projekte, die das Plugin verwenden |                                                                                                                                                                       |
| Beispieldateien                    |                                                                                                                                                                       |
| Installationsanleitung             |                                                                                                                                                                       |
| Dokumentation                      | Nach der Installation entsteht eine neue [Perspektive](https://wiki.de.dariah.eu/display/tgarchiv1/Perspektiven), die das Vorschaufenster und den Navigator zeigt.    |
| Screenshots                        |                                                                                                                                                                       |
| Tutorials                          |                                                                                                                                                                       |
| Entwickler                         |  Thorsten Vitt                                                                                                                                                        |
| Mitentwickler                      |                                                                                                                                                                       |
| Homepage/Kontakt                   | [https://projects.gwdg.de/projects/aggregator-lab](https://projects.gwdg.de/projects/aggregator-lab)                                                                  |
| Bugtracker                         | [http://projects.gwdg.de/projects/tg](http://projects.gwdg.de/projects/tg)                                                                                            |
