# Open the Publish View

last modified on Mai 03, 2016

Um eine Edition oder Kollektion zu veröffentlichen, rechtsklicken Sie
diese im [Navigator](40220331.md) und wählen Sie “In TextGridRep
veröffentlichen”. Die Publish-Sicht wird dann geöffnet.
