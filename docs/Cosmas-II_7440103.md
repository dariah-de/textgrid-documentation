# Cosmas II

last modified on Mai 04, 2016

COSMAS steht für COrpus Search, Management and Analysis System. Es
handelt sich um eine Volltext-Datenbank für linguistisch und
lexikographisch motivierte Anfragen im Korpus des Instituts für Deutsche
Sprache (IDS). Anfragen können deutsche Wörter, Wortklassen, Wortabstand
und- Position betreffen. Ergebnisse können nach Zeit, Land und Thema
sortiert sowie nach Verwendungsmustern analysiert werden. Das Deutsches
Referenzkorpus, auf das über COSMAS zugegriffen werden kann, besteht aus
ungefähr 3 Milliarden Wörter aus deutschen Zeitungen, Zeitschriften und
Bücher verschiedener Genres. Für weitere Informationen, siehe

[http://www.ids-mannheim.de/cosmas2/](http://www.ids-mannheim.de/cosmas2/)

Aus urheberrechtlichen Gründen ist zum Zeitpunkt der Erstellung dieses
Texts der Zugriff auf das Korpus vom TextGridLab aus auf eine geringe
Anzahl von Texten beschränkt.

- [Install Cosmas II](Install-Cosmas-II_8130749.md)
- [Open Cosmas II](Open-Cosmas-II_8126488.md)
- [Cosmas II Tool](Cosmas-II-Tool_8126490.md)
- [Using Cosmas II](Using-Cosmas-II_8126492.md)
- [Interaction of Cosmas II with Other Components](Interaction-of-Cosmas-II-with-Other-Components_8126494.md)
