# TextGridHttp Server Plugin

last modified on Feb 28, 2018

This plugin allows to retrieve your personal TextGrid objects via HTTP.
This is useful if you use XSLT to HTML transformations for previews of
the TEI data and want to include images and JavaScripts which reside in
the TextGrid repository, too. Without this plugin it is not possible to
includes links to other TextGrid repository objects because the
integrated webbrowser cannot resolve the TextGrid URLs.

## Installation

Please start TextGridLab and click on Marketplace. Now go to the entry
"TextGridHttpServer" and click on install. After accpting the license
conditions and restart of the TextGrid Lab the plugin is installed.

TextGrid Lab starten und auf Marketplace klicken. Nun zum Eintrag von
"TextGridHttpServer" scrollen und auf "Install" klicken, nach
akzeptieren der Lizenzbedingungen und Neustarten der TextGrid Lab
Umgebung steht das Plugin zur Verfügung.

### Verify the installation

Please open any available web browser on your system and enter the
following URL: [http://localhost:9090](http://localhost:9090). You will
see the following page:

![](https://raw.githubusercontent.com/Hannah-Arendt-Project/TextGridHttpServerPlugin/master/gh-imgs/indexpage.png)

The installation is now complete. If this page does not appear, please
restart TextGrid Lab again. If the Windows firewall asks for permission,
please accept it. The HTTP server is only accessible via your local
machine and is not a security thread.

## Usage

You need to add your desired images and javascript files into the
TG-repository. Now you can edit your XSLT/HTML files to link to those
files. Please copy the internal TextGrid URI from the object you would
like to reference to into the clipboard. Navigate to the desired object
in the TG-navigator, right click on it and select copy URI.

Now you can edit your XSLT/HTML documents. For example if you want to
include a javascript file as output from a XSLT transformation edit your
XSLT transformation like this:

    <script type="text/javascript" src="http://localhost:9090/textgrid:323ab.0"></script>

The address [http://localhost:9090](http://localhost:9090) refers to the
local HTTP server. The part after the / refers to the TextGrid Objekt
URI (which you have copied into the clipboard on the step before).

The same principle applies to image tags. Instead of

    <img src="logo.jpg"/> 

you write

    <img src="http://localhost:9090/textgrid:323ac.0"/> 

into you document. The textgrid:323ac.0 is the TextGrid URI from the
file logo.jpg, which is stored in the TG-repository.

Now you can test your XSLT transformation e.g. with Oxygen via XML →
Apply Transformation scenarios. Or you can open the HTML file with the
internal web browser.

## Further usage

This plugin may be also useful in connection with SADE. You can retrieve
all objects from your repository as long as you have TextGrid Lab open.
So you don't need to hard code any login information into your SADE
project.

## Further informationen and source code

Can be found on the Github page:
[https://github.com/Hannah-Arendt-Project/TextGridHttpServerPlugin](https://github.com/Hannah-Arendt-Project/TextGridHttpServerPlugin)
