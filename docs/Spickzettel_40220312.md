# Spickzettel

last modified on Aug 18, 2015

Spickzettel können entworfen werden, um NutzerInnen bei der Durchführung
einer bestimmten Aufgabe zu unterstützen, indem sie die Abfolge der
notwendigen Schritte auflisten. Derzeit werden keine Spickzettel für das
TextGridLab zur Verfügung gestellt. Sie können Spickzettel von einer
lokalen Datei oder einem URL auswählen. Für weitere Informationen, siehe

[*http://Hilfe.eclipse.org/indigo/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Freference%2Fref-cheatsheets.htm*](http://help.eclipse.org/indigo/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Freference%2Fref-cheatsheets.htm)
