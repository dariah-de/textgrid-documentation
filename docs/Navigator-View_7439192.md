# Navigator View

Die Navigator-Sicht wird für die Verwaltung von TextGrid-Projekten und
-Objekten verwendet. Alle Projekte, auf die der Nutzer Zugriff hat,
werden mit einem Ordnersymbol in der Baumansicht, die durch Klicken der
Knotenpunkte aus- oder zugeklappt werden kann, angezeigt. Es wird zudem
ein Symbol  angezeigt, das den Inhalt des TextGrid Repository im
Navigator repräsentiert. Die Inhalte werden nach den Namen der Autoren
sortiert aufgelistet. Der Navigator verwendet verschiedene Symbole für
verschiedene Objekttypen, zum Beispiel:

|                                                                                      |                       |
|--------------------------------------------------------------------------------------|:---------------------:|
| ![](attachments/40220337/40436703.png "xml.png")                                     |     XML-Dokument      |
| **![](attachments/40220337/40436696.gif "113-Dateityp-MEI.gif")**                    |     MEI-Dokument      |
| ![](attachments/40220337/40436692.gif)                                               |    Klartext-Objekt    |
| ![](attachments/40220337/40436702.png "image.png")                                   |      Bild-Objekt      |
| **![](attachments/40220337/40436695.png "069-zeige-BildLinkEditorPerspektive.png")** | Text-Bild-Link-Objekt |
| ![](attachments/40220337/40436701.png "108-ist-Edition.png")                         |        Edition        |
| ![](attachments/40220337/40436700.png "130-ist_Werk.png")                            |         Werk          |
| ![](attachments/40220337/40436699.png "109-ist-Collection.png")                      |      Kollektion       |
| ![](attachments/40220337/40436698.png "107-ist-Aggregation.png")                     |      Aggregation      |

Für weitere Informationen zu “Edition”, “Werk”, “Kollektion” und
“Aggregation”, siehe [Objekte](Objekte_40220426.md).

- [Menu Bar of the Navigator View](Menu-Bar-of-the-Navigator-View_7439194.md)
- [Toolbar of the Navigator View](Toolbar-of-the-Navigator-View_7439198.md)
- [Context Menu of the Navigator View](Context-Menu-of-the-Navigator-View.md)

## Attachments

- [xml.png](attachments/7439192/7766075.png) (image/png)  
- [image.png](attachments/7439192/7766076.png) (image/png)  
- [108-ist-Edition.png](attachments/7439192/7766077.png) (image/png)  
- [130-ist_Werk.png](attachments/7439192/7766078.png) (image/png)  
- [109-ist-Collection.png](attachments/7439192/7766079.png) (image/png)  
- [107-ist-Aggregation.png](attachments/7439192/7766080.png) (image/png)  
- [163-TextGrid-Repository.gif](attachments/7439192/8192211.gif)
(image/gif)  
- [113-Dateityp-MEI.gif](attachments/7439192/8192212.gif) (image/gif)  
- [069-zeige-BildLinkEditorPerspektive.png](attachments/7439192/8192213.png) (image/png)  
- [tree_explorer.gif](attachments/7439192/8192273.gif) (image/gif)  
- [nav_stop.gif](attachments/7439192/8192274.gif) (image/gif)  
- [file_obj.gif](attachments/7439192/8192275.gif) (image/gif)  
