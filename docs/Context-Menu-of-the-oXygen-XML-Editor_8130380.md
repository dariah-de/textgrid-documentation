# Context Menu of the oXygen XML Editor

last modified on Mai 04, 2016

Wenn der oXygen-XML-Editor geöffnet ist, wird das Kontextmenü des
TextGrid-XML-Editors mit Elementen aus dem Kontextmenü des oXygen
Editors angereichert. Für weitere Informationen, siehe

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)
