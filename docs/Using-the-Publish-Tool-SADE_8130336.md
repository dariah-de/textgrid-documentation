# Using the Publish Tool SADE

last modified on Mär 07, 2017

## Allgemeine Komponenten

### eXist-db

eXist ist eine XML-Datenbank, das heißt es können XML-Dokumente abgelegt
und mit XML- Technologien wie XQuery und XSLT abgefragt und
weiterverarbeitet werden. Bei größeren Datenmengen lässt sich die Suche
durch das Anlegen eines Index beschleunigen
([http://exist-](http://exist-)
[db.org/exist/apps/doc/indexing.xml](http://db.org/exist/apps/doc/indexing.xml)).
Das aktuelle eXist 2.2 bietet einen neugestalteten Range- Index
([http://exist-db.org/exist/apps/wiki/blogs/eXist/NewRangeIndex](http://exist-db.org/exist/apps/wiki/blogs/eXist/NewRangeIndex)).
Mit diesem lassen sich schnellere Suchen in einzelnen XML-Feldern oder
Attributen realisieren. Von dieser Entwicklung profitiert insbesondere
die facettierte Suche. eXist bringt nicht nur die Datenbank und die
Unterstützung der XML-Technologien mit, es wurden auch bereits einige
relevante Programme geschrieben, deren Integration sinnvoll ist. Diese
sind zumeist über ein eigenes öffentliches Repositorium verfügbar. Ein
dort zur Verfügung gestellter Beispieldatensatz umfasst die Werke
William Shakespeares und zeigt schon exemplarisch die Nutzung mit
TEI-Dokumenten.

#### Template Engine

Die Template Engine von SADE ist ein Fork einer frühen Version der eXist
Template Engine. Teilweise wurden Änderungen der Template Engine aus
neueren eXist Versionen in die SADE Template Engine zurückportiert. Die
Template Engine eröffnet die Möglichkeit, SADE Module zu schreiben die
unabhängig von CSS und Webseitendesign mit verschiedenen Darstellungen
funktionieren. So gibt es z.B. für die facetierte Suche ein Bootstrap
Template, es wären aber auch Templates mit anderen CSS Frameworks
möglich, die Funktionalität des Moduls bleibt in allen Darstellungen
gleich.

#### SADE

SADE als Software innerhalb der eXist-db nutzt diese Template Engine und
XAR-Module. XAR Module sind Pakete, in denen XML-Ressourcen wie
XQuery-Scripte, XSLT-Stylesheets und XML zusammengefasst werden, und
sich so gebündelt als Apps oder zur Funktionalitätserweiterung in eXist
installieren lassen. Das in eXist genutzte XAR-Format ist eine
modifizierte Version des eXpath package-Formates
([http://expath.org/spec/pkg](http://expath.org/spec/pkg)). Die
Distribution dieser Pakete kann als Einzel- Upload oder über
Paket-Repositorien erfolgen. Die Nutzung der eXist Template Engine
stellt die Unabhängigkeit solcher XAR-Module vom Layout der
dargestellten Webseite sicher. In einer Konfigurationsdatei lassen sich
Module, Templates und Pfade zu XML-Daten festlegen und so eigene Portale
mit den jeweils benötigten Komponenten und einem individuellem Design
zusammenstellen. Damit bleiben auch innerhalb der Datenbank Daten,
Datenverarbeitung und Visualisierung voneinander getrennt. Komponenten,
die in digitalen Editionen in der Regel benötigt werden, wie eine
Ansicht von TEI- Transformationen, eine Suche und die Navigation,
gehören zum Kern des Projektes und können so gemeinsam weiterentwickelt
werden. Der Austausch eigener Entwicklungen, wie beispielsweiseeine
Zeitleiste oder eine Landkarten-basierte Visualisierung, können als
XAR-Module der Community zur Verfügung gestellt werden. Im
TextGrid-Kontext gibt es derzeit zwei Projekte, die frühere Versionen
von SADE in modifizierter Form einsetzen: Blumenbach-Online, die
digitale Edition der Notizbücher Theodor Fontanes und die “Bibliothek
der Neologie”.

#### Digilib

Digilib (Sourceforge) ist eine serverseitige Software zur Manipulation
von Bildern. Mit dieser ist es unter anderem möglich, Bilder in
unterschiedlichen Skalierungen und Formaten oder auch nur einzelne
Bildausschnitte vom Server abzurufen. Es gibt bei digilib einen Service,
der die Operationen auf den Bildern vornimmt und eine REST-API
bereitstellt sowie einen webbasierten Client, der die Nutzerinteraktion
mit den vom Service angebotenen Bildern und Bildbearbeitungsfunktionen
erlaubt.  
Digilib ist schon seit der zweiten Förderphase in TextGrid verfügbar,
die Integration wurde 2013, im Rahmen eines von der Mellon-Foundation
geförderten Projektes zur Integration von IIIF in TextGrid, stark
verbessert, wodurch auch eine Optimierung der Geschwindigkeit erreicht
werden konnte. Für im TextGrid gehöstete Bilder kann in Zukunft der
TextGrid-seitige digilib-Service anstelle einer SADE-integrierten
Version zu verwendet werden. Diese Lösung hat einige Vorteile: So können
mit ihr in TextGrid Digitalisate direkt in dem Format, in dem sie den
Scan-Prozess verlassen – dem TIFF-Format – abgelegt werden. Eine
Vorhaltung der Bilder im JPEG-Format, wie es bei der SADE-integrierten
Version für die Publikation unerlässlich wäre, ist nun nicht mehr nötig.
Die Bilder selbst brauchen bei der Publikation aus dem TextGridLab nicht
zur SADE-Installation kopiert zu werden. Bei der Ausgabe von Dokumenten,
die Bilder enthalten, werden die Objekte anhand der TextGrid-URI von
digilib als JPEG in einer zweckmäßigen Auflösung (100px für Thumbnails,
1500px für die Ansicht von Seitenscans) abgerufen. So können in Zukunft
Geschwindigkeitsoptimierungen zentral über den TextGrid-Service
vorgenommen werden, die einzelnen SADE-Server werden von der
Bildumrechnung entlastet. Die Unabhängigkeit von SADE von dem
integrierten digilib vereinfacht die Nutzung des DARIAH-eXist-Hosting,
da dadurch für eine funktionierende Installation nur eine
eXist-Datenbank vorhanden sein muss.

### TextGrid-spezifische Komponenten

Die nachfolgend aufgeführten Komponenten funktionieren als Module in der
eXist-Datenbank, funktionieren aber auch ohne die Hauptkomponente SADE.
Weiterhin folgen noch die Erweiterungen von SADE selbst in einem
anschließenden Abschnitt. Alle Quellcodes sind unter
[http://github.com/ubbo/](http://github.com/ubbo/) frei Verfügbar.

#### tg-client

Die meisten Komponenten von TextGrid lassen sich per
REST/SOAP-Schnittstelle oder Webinterface ansteuern. Das XQuery-Skript
stellt all diese Funktionen in der Datenbank zur Verfügung. So kann man
aus allen anderen Modulen oder ggf. auch von der Weboberfläche aus auf
Authentifizierungsdienste und mehr zugreifen. Es bietet derzeit
Anknüpfungen an:

- die SPARQL-Schnittstelle von tg-search  
- die von tg-crud bereitgestellten Metadaten-Objekte  
- die von tg-crud bereitgestellten Daten-Objekte  
- den Authentifizierungsdienst tg-auth (inkl. einer zwischengespeicherten Session-ID, um unnötige Last bei tg-auth zu vermeiden) und es bietet zudem eine Funktion, um die URI-Prefixes "textgrid:" aus den Objektreferenzen zu entfernen.

Damit stehen die Kernservices von TextGrid innerhalb der Datenbank zur
Verfügung  
und können in andere Module eingebunden werden.

#### tg-connect

Dieses Skript wird bei der Dokumentenübertragung genutzt. Es ist für das
holen und ablegen der Dokumente verantwortlich und bedient sich intensiv
der von tg-client bereitgestellten Funktionen. Es greift dabei die aus
dem TextGrid Laboratory vom Publish-Tool SADE (TextGridLab Plugin)
übertragenen Informationen ab und verarbeitet diese, überträgt also die
Dokumente aus dem TextGrid Repository in diese SADE-Instanz.
Abschließend wird das Skript zum Aufbau der Menüführung abgerufen.

#### tg-menu

Mit dem Modul tg-menu wird die aus dem TextGrid Laboratory innerhalb des
Navigators zu sehende hierarchische Struktur aufgebaut. Diese wird dann
als Menü in die digitale Präsentationsoberfläche eingebaut. Dabei wird
weiterhin auf die Trennung von Daten und Layout geachtet. Primär wird
ein XML-Dokument erzeugt, welches diese Struktur nachempfindet und in
der dem Projekt zugehörigen Kollektion abgelegt. Um schließlich konform
mit dem jeweiligen Template, dem Layout der digitalen Edition, zu sein,
ist ein weiterer Verarbeitungsschritt nötig, ein XSLT, welches in der
Projekt-Kollektion abgelegt sein muss und den Namen des gewählten
Templates tragen sollte, wird automatisch aufgerufen. Dadurch können
flexible Layouts und Designs eingesetzt werden.

#### digilib-Proxy

Bei der ursprünglichen Version der TextGrid Sade-Publish-Komponente
wurden die Bilder beim Publizieren im lokalen digilib-Verzeichnis der
SADE-Installation abgelegt. Daher brauchte die Authentifizierung bei
TextGrid nur beim Publizieren der Bilder vorgenommen werden. In Zukunft
kann der TextGrid digilib-Service für die Bilddarstellung genutzt
werden, was einer Authentifizierung bei jeder Anzeige von Bildern
bedarf.Dies gilt für den Fall, dass ein Rechtemanagement für die
Bildabfrage nötig ist, beispielsweise bei nur für die betreffende
Edition freigegebenen Bilddokumenten. Daher wurde in XQuery ein digilib
Proxy implementiert, ein Skript, welches einen gültigen TextGrid-Account
nutzen kann und diese Abfragen koordiniert. Anfragen an digilib aus dem
Portal werden an den Proxy gestellt, dieser meldet einen hinterlegten
TextGridNutzeraccount am TextGrid Repository an, bezieht eine SessionID
und und damit die Berechtigung, das abzurufende Bild sehen zu dürfen.
Diese Berechtigung wird an den Endnutzer weitergegeben, die zugehörige
Session-ID bleibt aber versteckt.

> Exkurs: Funktionsaccounts  
> Schon jetzt werden von den beiden genannten Projekten sogenannte
> Funktionsaccounts benutzt, deren Daten in der Konfiguration von SADE
> angegeben werden müssen. Dabei handelt es sich um TextGrid-Accounts,
> denen in den betreffenden Projekten lediglich Beobachter-Status
> zukommt. Derzeit kann jeder TextGrid Nutzeraccount TextGrid-Projekte
> erstellen und Daten publizieren. Da für alle TextGrid Accounts ein
> realer Nutzer hinterlegt ist, ist hier die Haftung, Verantwortlichkeit
> etc. geklärt. Ein im SADE-Projektportal hinterlegter Nutzeraccount
> könnte aber potentiell allen Nutzern, die erweiterte Zugangsrechte
> innerhalb des Portals haben, bekannt werden. Hier bedarf es eines noch
> stärker beschränkten Accounttypes. Das sind Nutzeraccounts, die
> explizit nur Leserechte an Projekten, denen sie zugeordnet sind, haben
> können.

#### SADE Module

Es wurden drei neue Module für SADE programmiert: eines für die
Navigation, ein Viewer und eine  
facettierte Suche. Alle drei Module nutzen die Template Engine.

##### Navigation

Um die Inhalte des SADE-Portals in einer eigenen Menüführung auffindbar
zu machen, wurde das Navigationsmodul entwickelt. Mit diesem Modul ist
es möglich, in einer XML-Datei die Menüstruktur des Portals festzulegen.
Einzelne Links werden mit einem Namen und einem Verweis versehen,
relative Links bleiben im Portal, externe Links sind ebenfalls möglich.
Für dieses Modul wurde eine Nutzerdokumentation erstellt. Die
Portal-Navigation kann in einer einfachen XML-Struktur beschrieben
werden. Beispiel - XML Konfiguration und die resultierende Menüansicht
(im Bootstrap Template):

``` syntaxhighlighter-pre
<navigation>
<item label="Text" link="index.html?id=text.md"/>
<submenu label="Links">
<item label="textgrid.de" link="http://textgrid.de "/>
</submenu>
</navigation>
```

![ Mit dem Navigationsmodul erstelltes Menü](attachments/8130336/41681307.jpg " Mit dem Navigationsmodul erstelltes Menü")

Derzeit können an dieser Stelle keine Untermenüs verschachtelt werden.

##### Viewer

Es wurde ein Viewer-Modul programmiert, welches die Datei-Endung prüft
und zwischen HTML-, Markdown- und XML-Dokumenten unterscheidet. Für
diese drei Dokumenttypen sind Anweisungen hinterlegt, die jeweils zu
einer HTML-Ausgabe führen. Im Falle eines XML- Dokuments wird noch
geprüft, ob es sich um ein mit dem in TextGrid integrierten
Text-Bild-Link- Editor erstelltes Dokument handelt, oder eine andere
Struktur vorliegt, bei der dann entsprechend des Namespaces ein
passendes XSLT-Stylesheet ausgewählt wird. So ist es mit demselben
Viewer beispielsweise möglich, Markdown, TEI und Blumenbach-Objekt-XML
darzustellen. Der Viewer unterstützt auch die seitenweise Ausgabe von
TEI-Dokumenten, wobei keine im Dokument zuvor stehenden Elemente
berücksichtigt werden und somit auch potentiell semantisch übergreifende
leere Elemente vernachlässigt werden. Die Markdown-Darstellung wurde
integriert, um einzelne Portaltexte, die allerdings derzeit nicht
durchsuchbar sind, schnell und einfach zu erstellen. Die Hilfe- und
Tutorialdokumentationen der TextGrid-SADE-Referenzinstallation sind in
Markdown verfasst. TEI-Dokumente werden mit Hilfe von Stylesheets
(bereitgestellt von der TEI-C, [http://www.tei-c.org/Tools/Stylesheets/](http://www.tei-c.org/Tools/Stylesheets/))
in die Webseite eingebunden. In der Projekt-Konfigurationsdatei können
eigene Stylesheets zur Darstellung anstelle der integrierten Stylesheets
angegeben werden. Dafür ist in der config.xml die Sektion Multiviewer
zuständig. Hier kann pro namespace des Dokument root Knotens ein
Stylesheet angegeben werden.

Auszug aus config.xml

``` syntaxhighlighter-pre
<module key="multiviewer">
<param key="xslt">
<!-- Nutze das eigene Stylesheet aus dem xslt Ordner im
Projektverzeichis (local=true) für den TEI namespace -->
<stylesheet
namespace="http://www.tei-c.org/ns/1.0"
local="true"
location="xslt/my-tei-stylesheet.xsl" />
<!-- Eigenes stylesheet für den XML dokumente mit eigenem Namespace,
Übergabe eines Parameters an das XSLT -->
<stylesheet
namespace="http://my.project.org/specific-namespace/1.1"
local="true"
location="xslt/my-project.xslt.xsl">
<parameters>
<param name="imgParams" value="?dh=200"/>
</parameters>
</stylesheet>
</param>
</module>
```

###### SemToNotes-Integration

Für die Darstellung synoptischer Ansichten (mit Hilfe des
Text-Bild-Link-Editors erarbeitete Verknüpfungen) wurden
Softwarekomponenten aus dem Projekt "SemToNotes"
([https://github.com/HKIKoeln/SemToNotes](https://github.com/HKIKoeln/SemToNotes)
und
[http://hkikoeln.github.io/SemToNotes/](http://hkikoeln.github.io/SemToNotes/))
in die SADE-Umgebung integriert. SemToNotes ist ein Annotations- und
Visualisierungstool, welches für die Beschreibung und Darstellung
topologischer Strukturen in Bilddokumenten und deren Verknüpfungen mit
inhaltlichen Informationen geeignet ist. Publiziert ein Nutzer eine mit
dem Text-Bild-Link-Editor erstellte Datei sowie die zugehörigen TEI und
Bilddokumente, können die TBLE Objekte mit Hilfe des SemToNotes Viewers
dargestellt werden. Die integrierte Suche unterstützt diese
Objektverknüpfungen und bietet bei Volltexttreffern auch TBLE-Objekte
welche mit der Textstelle verknüpft sind an.

![](attachments/8130336/41681308.jpg)

Illustration 2: Ergebnis der mit Hilfe des Text-Bild-Link-Editors
erstellten Daten

##### Faceted Search

Für die Suche in publizierten XML-Daten wurde ein SADE-Modul
geschrieben, welches die Suche durchführt und entsprechend seiner
Konfiguration Facetten bildet. In der Konfiguration lassen sich für die
einzelnen zu bildenden Facetten XPath-Ausdrücke angeben. Facetten lassen
sich in der Webseite per Klick auf aus dem Such-Ergebnis ausschließen.
Mit Klick auf die Facette lässt sich das Ergebnis auf diese Facette
einschränken.Für die Darstellung eines einzelnen Ergebnistreffers wird
ein XSLT verwendet, das Metadaten aus dem gefundenen Dokument extreiert
und beim Suchtreffer darstellt. Das bietet eine flexible Möglichkeit,
eigene Metadatenfelder aus dem TEI Dokument bei der Suchtrefferanzeige
auszugeben. Konfiguriert wird dieses in der config.xml des Projektes, im
module mit key="faceted-search".

Auszug aus config.xml

``` syntaxhighlighter-pre
<module key="faceted-search">
[...]
<!-- Benutze ein eigenes XSLT für die Trefferanzeige -->
<param key="result-xslt">/db/sade-projects/my-project/xslt/search-
hit.xslt</param>
<param key="facets">
<!-- Vorgegebene Facetten, können geändert oder gelöscht werden -->
<facet key="persons" title="Person">
<xpath>tei:author</xpath>
</facet>
<facet key="keywords" title="Schlagwort">
<xpath>tei:term</xpath>
</facet>
<facet key="dates" title="Zeit">
<xpath>tei:date/@when</xpath>
</facet>
<facet key="places" title="Publikationsort">
<xpath>tei:pubPlace</xpath>
</facet>
<!-- Eigene Facette -->
<facet key="personstext" title="Person im Text">
<xpath>tei:persName</xpath>
<xpath>tei:rs</xpath>
</facet>
</param>
</module>
```

## Attachments

- [tg-sade-pub2.jpg](attachments/8130336/41681307.jpg) (image/jpeg)  
- [tg-sade-pub3.jpg](attachments/8130336/41681308.jpg) (image/jpeg)  
