# Toolbar of the XML Editor Perspective

last modified on Mai 04, 2016

Wenn der XML-Editor geöffnet ist, werden abhängig von der gerade
gewählten Ansicht zusätzliche Funktionen in der Werkzeugleiste aktiv.

**Block-Marker anzeigen / Inline-Marker anzeigen (WYSIWYM-Ansicht)**

Klicken Sie in der [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md) die
Schaltfläche **![](attachments/40220531/40436771.gif "161-Block-Tags-einblenden.gif")** ,
um Block Marker anzuzeigen, und klicken
Sie ![](attachments/40220531/40436770.gif "160-Inline-Tags-einblenden.gif") ,
um alle Inline-Marker anzuzeigen.

**Nächste Annotation / Vorherige Annotation (Quelle-Ansicht)**

Durch Klicken
von ![](attachments/40220531/40436779.png "icon_annotation_next.png") oder ![](attachments/40220531/40436778.png "icon_annotation_prev.png") kann
die nächste oder vorherige Annotation gefunden werden. Durch Klicken der
kleinen schwarzen Dreiecke neben diesen Schaltflächen wird eine Liste
von Entitäten angezeigt, die ausgewählt werden können. Nur die
markierten Arten von Objekten werden gefunden. Sie können Hinzufügungen
und Änderungen ebenso auswählen wie Fehler, Warnungen und Informationen.
Sie haben außerdem die Möglichkeit, zur nächsten oder vorherigen
Annotation zu springen.

**Grammatikeinschränkungen Ein und Aus (Quelle-Ansicht)**

Wenn Sie ein XML-Dokument bearbeiten, das einen Satz von Einschränkungen
oder Regeln über eine DTD oder ein XML-Schema festlegt, können Sie die
Einschränkungen durch Klicken
von ![](attachments/40220531/40436777.png "constrain-on.png") oder ![](attachments/40220531/40436776.png "constrain-off.png") ein-
oder ausschalten. Wenn die Einschränkungen eingeschaltet sind,
verhindert der XML-Editor das Einfügen von Elementen, Attributen oder
Attributwerten, die durch die Regeln des XML-Schemas oder der DTD nicht
erlaubt sind, und verhindert das Entfernen von notwendigen oder
vordefinierten Sets von Tags und Werten.

**Abhängigkeiten erneut laden (Quelle-Ansicht)**

Wenn Ihr XML-Dokument mit einer DTD oder einem XML-Schema assoziiert
ist, das sich geändert hat, können Sie die Abhängigkeiten durch Klicken
von ![](attachments/40220531/40436775.png "reload-dependencies.png") erneut
laden.

## Attachments

- [blockmarkers.png](attachments/7439328/7766120.png) (image/png)  
- [inlinemarkers.png](attachments/7439328/7766121.png) (image/png)  
- [icon_annotation_next.png](attachments/7439328/7766122.png)
(image/png)  
- [icon_annotation_prev.png](attachments/7439328/7766123.png)
(image/png)  
- [constrain-on.png](attachments/7439328/7766124.png) (image/png)  
- [constrain-off.png](attachments/7439328/7766125.png) (image/png)  
- [reload-dependencies.png](attachments/7439328/7766126.png) (image/png)  
- [F+.png](attachments/7439328/7766128.png) (image/png)  
- [F+.png](attachments/7439328/7766127.png) (image/png)  
- [F-.png](attachments/7439328/7766129.png) (image/png)  
- [cancel.png](attachments/7439328/8192230.png) (image/png)  
- [161-Block-Tags-einblenden.gif](attachments/7439328/8192291.gif)
(image/gif)  
- [160-Inline-Tags-einblenden.gif](attachments/7439328/8192292.gif)
(image/gif)  
- [125-Einfuege_Lieblingselement_in_XML.gif](attachments/7439328/8192293.gif)
(image/gif)  
- [126-Loesche_Lieblingselement_in_XML.gif](attachments/7439328/8192294.gif)
(image/gif)  
