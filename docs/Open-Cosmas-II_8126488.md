# Open Cosmas II

last modified on Mai 04, 2016

Sie können Cosmas II auf verschiedene Weisen öffnen:

1. Wählen Sie “Werkzeuge \> Sicht anzeigen \> Cosmas II” in der
    Menüleiste.
2. Klicken
    Sie ** ![](attachments/40220750/40436903.png "cosmas.png")** in der
    Werkzeugleiste.
3. Wenn ein Dokument in
    der [Quelle-Sicht](Quelle-Ansicht_40220547.md) oder
    der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) des [XML-Editor](XML-Editor_40220521.md) geöffnet
    ist, kann Cosmas II im Kontextmenü ausgewählt werden.

## Attachments

- [cosmas.png](attachments/8126488/8192315.png) (image/png)  
- [cosmas.png](attachments/8126488/8192314.png) (image/png)  
