# Import-Werkzeug öffnen

last modified on Jul 28, 2015

Eine Perspektive muss geöffnet sein, um dieses Werkzeug zu nutzen.
Wählen Sie “Datei \> Lokale Dateien importieren ...” in der Menüleiste.

![](attachments/40220397/40436718.png)

|        |
|--------|
| Import |

## Attachments

- [imp-import.png](attachments/40220397/40436718.png) (image/png)  
