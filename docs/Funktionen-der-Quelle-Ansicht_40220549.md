# Funktionen der Quelle-Ansicht

last modified on Aug 19, 2015

**Syntaxhervorhebung**

Elemente wie Attribute und Werte sowie Kommentare werden in der
Quelle-Ansicht unterschiedlich hervorgehoben, um die Orientierung im
Quelltext und die Entdeckung von Syntaxfehlern zu vereinfachen. Sie
können die Syntaxhervorhebung in den Editor-relevanten Benutzervorgaben
anpassen.

**Unbegrenztes Rückgängigmachen und Wiederholen**

Um in einem Dokument gemachte Änderungen rückgängig zu Machen oder zu
wiederholen, Klicken Sie “Bearbeiten \> Rückgängig” oder “Bearbeiten \>
Wiederholen” in der Menüleiste. Alternativ können Sie in der
Editor-Sicht rechtsklicken und den entsprechenden Menüpunkt
“Textänderungen rückgängig machen” auswählen. Sie können alle
Änderungen, die Sie über die gesamte Sitzung hinweg an einer Datei
vorgenommen haben, rückgängig machen und beliebig oft wiederholen.

**Knotenauswahl**

Abhängig von der Cursorposition im Editor hebt die Knotenauswahl die
entsprechenden Tags sowie die Zeilen, die ein Knoten einschließt, in
einer senkrechten Leiste am linken Rand der Quelle-Ansicht hervor.

**Kommentar umschalten und Blockkommentar hinzufügen oder entfernen**

Sie können einen XML-Kommentar durch Rechtsklick und Auswahl von
“Quelle \> Kommentar umschalten” ein- oder ausblenden. Um einen
Abschnitt des Quelltextes zu kommentieren, rechtsklicken Sie den
ausgewählten Bereich und wählen Sie “Quelle \> Blockkommentar
hinzufügen”. Um einen Kommentar zu entfernen, wählen Sie “Quelle \>
Blockkommentar entfernen”.

**Echtzeit-Validierung**

Während Sie das Dokument bearbeiten, werden Fehler in der allgemeinen
XML- Syntax (wie fehlende Anführungszeichen, Klammern oder schließende
Tags) ebenso durch rote wellenförmige Unterstreichung angezeigt wie
Validierungsfehler (wenn ein Schema spezifiziert ist). Ein rotes Symbol
am oberen Ende der senkrechten Leiste am rechten Rand der Quelle-Ansicht
erscheint, wenn das Dokument Fehler enthält. Um eine Fehlerbeschreibung
zu erhalten, bewegen Sie den Mauszeiger über die roten Symbole, die in
der senkrechten Leiste am rechten Rand der Quelle-Ansicht angezeigt
werden. Ein gelbes Symbol steht für eine Warnung.

![](attachments/7439338/7766133.png "xml-validationerror-detail.png")

|                          |
|--------------------------|
| **Echtzeit-Validierung** |

**Intelligenter Einfügemodus**

Die Eingabe von Elementen und Kommentaren wird durch den Intelligenten
Einfügemodus unterstützt: Wenn Sie ein Element eingeben, vervollständigt
der Intelligente Einfügemodus automatisch das schließende Tag. In
ähnlicher Form wird beim Beginnen eines XML-Kommentars mit \<!-- dieser
durch das schließende Tag --\> vervollständigt. Der Cursor bleibt dabei
zwischen den Kommentar-Zeichen, so dass Sie direkt mit der Eingabe des
Kommentartextes fortfahren können. Auf dieselbe Weise werden öffnende
Anführungszeichen durch schließende Anführungszeichen vervollständigt,
während der Cursor dazwischen bleibt, so dass Sie direkt mit der Eingabe
des Attributswerts fortfahren können. Um den Intelligenten Einfügemodus
ein- oder auszuschalten, wählen Sie “Bearbeiten \> Intelligenter
Einfügemodus” in der Menüleiste und wählen Sie die gewünschte
Einstellung aus.

**QuickInfo**

Die QuickInfo wird angezeigt, wenn Sie den Mauszeiger über ein Element
oder Attribut ziehen. Sie zeigt das Inhaltsmodell des aktuellen Elements
oder Attributs sowie zusätzliche Dokumentationsinformationen an, wenn
das assoziierte Schema das Element oder Attribut beinhaltet. Durch
Ziehen des Mauszeigers über einen Textabschnitt mit einer roten
wellenförmigen Unterstreichung wird eine Beschreibung des Problems
angezeigt. Sie können die Einstellungen für die QuickInfo in den
Editor-relevanten Benutzervorgaben verändern.

![](attachments/7439338/7766134.png "xml-hoverhelp-detail.png")

|               |
|---------------|
| **QuickInfo** |

**Inhaltshilfe**

Wenn Sie die öffnende Klammer \< eines Elements in der Quelle-Ansicht
eingeben, blendet Ihnen die Inhaltshilfe eine Liste vorgeschlagener
Elemente ein, die an dieser Position zulässig sind. Sie können die
Inhaltshilfe durch Auswahl von “Bearbeiten \> Inhaltshilfe” in der
Menüleiste aktivieren. Wenn Sie die Inhaltshilfe aktivieren, während
sich der Cursor an der Position für ein Attribut befindet, wird eine
Liste vorgeschlagener Attribute eingeblendet, die an dieser Position
zulässig sind. Die Vorschläge kommen von einem referenzierten
Inhaltsmodell, wenn ein Schema für das Dokument festgelegt ist, oder aus
dem XML-Katalog. Sie können die Inhaltshilfe in den Editor-relevanten
Benutzervorgaben anpassen.

**Schnellkorrektur**

Wählen Sie “Bearbeiten \> Schnellkorrektur” in der Menüleiste, um eine
Liste vorgeschlagener Korrekturen einzublenden, nachdem Sie den Cursor
auf einen Textabschnitt positioniert haben, der mit einer roten
wellenförmigen Unterstreichung angezeigt wird. Alternativ können Sie den
unterstrichenen Text rechtsklicken und “Schnellkorrektur” auswählen.
Wenn an der ausgewählten Position keine Probleme oder Warnungen
vorliegen, sind keine Vorschläge der Schnellhilfe verfügbar.

**Formatierung**

Um das XML-Dokument lesbarer und zur Vorbereitung des Drucks zu
gestalten, können Sie die Formatierungsoptionen in den Editor-relevanten
Benutzervorgaben ändern. Sie können die Formatierung für das gesamte
Dokument durch Rechtsklicken eines nicht hervorgehobenen Bereichs Ihres
Dokuments und Auswahl von “Quelle \> Formatieren” anpassen. Um die
Formatierung nur für einen hervorgehobenen Bereich anzupassen,
rechtsklicken Sie in den ausgewählten Bereich und wählen Sie “Quelle \>
Aktive Elemente formatieren”.

**Dokument bereinigen**

Einfache Fehler im XML-Dokument wie fehlende notwendige Attribute,
fehlende öffnenden oder schließende Tags oder Attribute ohne
Anführungszeichen können durch eine XML-Bereinigung korrigiert werden.
Sie können den Assistenten “Dokument bereinigen” durch Rechtsklicken im
Editor und Auswahl von “Quelle \> Dokument bereinigen” öffnen.
Alternativ können Sie “XML \> Quelle \> Dokument bereinigen” in der
Menüleiste auswählen. Afterwards, set your Cleanup preferences in the
dialog box; you can further select ’Format source’ in the box to apply
formatting to the document.

**Team und lokale Bearbeitungshistorie**

Die lokale Bearbeitungshistorie eines XML-Dokuments wird
aufrechterhalten, wenn Sie ein Dokument erstellen oder bearbeiten. Diese
Historie ist “Team \> Lokales Protokoll anzeigen” im Kontextmenü
verfügbar. Jedes Mal, wenn Sie die Datei bearbeiten und speichern, wird
eine Kopie gespeichert, so dass Sie die aktuelle Datei durch eine
frühere Bearbeitung ersetzen oder sogar eine gelöschte Datei
wiederherstellen können. Sie können außerdem die Inhalte aller lokalen
Bearbeitungen vergleichen. Außerdem können Sie unter “Team” im
Kontextmenü einen “Patch anwenden”. Wählen Sie im Assistenten
“Patch-Eingabespezifikation” die Position der Ressource (Zwischenablage,
Datei, URL oder Arbeitsbereich) aus, an der der Patch generiert wurde.
Diese Ressource sollte dieselben Datei-Revisionen beinhalten wie die
Zusammenstellung, auf Basis derer der Patch generiert wurde. Für weitere
Informationen, siehe die Eclipse-Dokumentation:

[*http://www.eclipse.org/documentation/*](http://www.eclipse.org/documentation/)

**Benutzervorgaben**

Sie können die XML-Editor-relevanten Benutzervorgaben durch
Rechtsklicken in der Quelle-Ansicht und Auswahl von
“[Benutzervorgaben](Benutzervorgaben_40220277.md)” bearbeiten. Die
folgenden Einstellungen können bearbeitet werden:

- Farbgebung für Syntax: Klappen Sie den Baum “XML \> XML-Dateien \>
    Editor” auf der linken Seite im Assistenten “Einstellungen” aus und
    wählen Sie “Farbgebung für Syntax”.
- QuickInfo: Klappen Sie den Baum “Allgemein \> Editoren \> Editoren
    für strukturierten Text” auf der linken Seite im Assistenten
    “Einstellungen” aus. Wählen Sie im Assistenten “Editoren für
    strukturierten Text” den Reiter “QuickInfo” und wählen die
    gewünschten Einstellungen aus.
- Inhaltshilfe: Klappen Sie dem Baum “XML \> XML-Dateien \> Editor”
    auf der linken Seite im Assistenten “ Einstellungen” aus und wählen
    Sie “Inhaltshilfe”.
- Formatierung: Klappen Sie den Baum “XML \> XML-Dateien” auf der
    linken Seite im Assistenten “Einstellungen” aus und wählen Sie
    “Editor”.

Für weitere Informationen, siehe die Eclipse-Dokumentation:

[*http://www.eclipse.org/documentation/*](http://www.eclipse.org/documentation/)
