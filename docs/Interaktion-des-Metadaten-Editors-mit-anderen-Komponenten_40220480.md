# Interaktion des Metadaten-Editors mit anderen Komponenten

last modified on Aug 19, 2015

Der Metadaten-Editor ist Bestandteil des
[XML-Editors](XML-Editor_40220521.md). Er zeigt die Metadaten der
[Objekte](TextGrid-Objekte.md), die gerade geöffnet sind. Die
Struktur der Metadaten hängt von der Art des Objekts ab, auf die sie
sich bezieht. Sie kann mit dem
[Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md) für
die speziellen Bedürfnisse angepasst werden.
