# ANNEX (de)

last modified on Nov 23, 2015

ANNEX ist ein Flash-basiertes Werkzeug, das Ihnen ermöglicht
Video-Ressourcen synchron zur ihren Annotationen zu betrachten. Es
verwendet Dateien, die mit ELAN
([http://www.lat-mpi.eu/tools/elan/](http://www.lat-mpi.eu/tools/elan/))
erstellt wurden. In der ANNEX-Sicht können Sie zwischen der Anzeige
einer oder mehrerer Annotationsschichten und unterschiedlichen Frame
Setups (sichten) wählen. Einzelne Abschnitte eines Videos können
ausgewählt und wiedergegeben werden. Vor- und Zurückspulen anhand der
Sekundenanzeige in einem Video ist ebenfalls möglich.

- [ANNEX installieren](ANNEX-installieren_40220760.md)
- [ANNEX öffnen](40220762.md)
- [Das ANNEX-Werkzeug](Das-ANNEX-Werkzeug_40220764.md)
- [Media Player (de)](40220766.md)
- [Bedienelemente](Bedienelemente_40220768.md)
- [ANNEX verwenden](ANNEX-verwenden_40220770.md)
- [Interaktion von ANNEX mit anderen Komponenten](Interaktion-von-ANNEX-mit-anderen-Komponenten_40220772.md)
