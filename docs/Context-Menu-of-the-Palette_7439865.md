# Context Menu of the Palette

last modified on Mai 04, 2016

The context menu of the Palette offers several commands:

- To arrange the workspace layout of the Palette, select “Layout \>”.
    The list arrangement of all icons is preselected. You can also
    choose between an arrangement in columns, a display of the icons
    only, or a list arrangement of all icons with details included.
- If you select “Use Large Icons” the palette actions will use
    large-scale icons where available.
- You can change some predefined settings of the Palette’s icons:
    Click “Customize…” and a new dialog box will open. All five units
    and their elements contained in the Palette are listed on the left
    in a tree structure. The bar at the top of the tree structure
    enables you to change the position of the entries in the Palette
    (“Move Down” , “Move Up” ). You can also delete single elements from
    the list. The “New” button allows you to create a new drawer. The
    settings of the Control Group and its components cannot be changed.
    For the other groups there are several possibilities to change their
    preferences. Please tick the boxes to hide the whole unit or just
    single elements of it. For a single unit, you can open the drawer at
    start-up or pin the drawer open at start-up. It is also possible to
    create or modify the names and descriptions of groups and elements.
    Click "Apply" to apply your changes.
- To change the settings of the Palette itself, click “Settings…” and
    a new dialog will open. If you click “Change...” you can change the
    font, its style, color, and the font size of the icon captions in a
    separate window. Otherwise, the default settings will be restored.
    You can choose between different layouts ("Columns", "List", "Icons
    only", "Details") via buttons or use large icons by ticking a
    checkbox. There are also options for the drawers of the Palette: The
    default setting is that the drawer will be closed if there is not
    enough workspace. Furthermore, you can decide whether it should be
    closed when opening another drawer or if it should remain open.
