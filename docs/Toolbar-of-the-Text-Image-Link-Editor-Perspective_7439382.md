# Toolbar of the Text Image Link Editor Perspective

last modified on Mai 03, 2016

Wenn die [Bild-Sicht](Bild-Sicht_40220638.md) aktiviert ist, stehen
zusätzliche Elemente in der Werkzeugleiste zur Verfügung:

- Klicken Sie ![](attachments/40220635/40436828.png "delete.png") , um
    aktive Markierungen zu löschen
- Klicken
    Sie ![](attachments/40220635/40436827.png "014-schliesse-Datei.png") ,
    um den Text-Bild-Link-Editor zurückzusetzen

Wenn auch eine Datei im [XML-Editor](XML-Editor_40220521.md) geöffnet
ist, werden weitere Schaltflächen in der Werkzeugleiste aktiviert:

- Klicken Sie ![](attachments/40220635/40436826.png "link.png") , um
    ein gewähltes Textsegment mit einer gewählten Markierung zu
    verknüpfen.
- Klicken
    Sie ![](attachments/40220635/40436825.png "unlink_obj.png") , um
    (eine) gewählte Verknüpfung(en) aufzuheben.
- Klicken Sie ![](attachments/40220635/40436824.png "save.png") (neben
    dem Verknüpfungssymbol), um das Text-Bild-Link-Objekt zu speichern.
- Klicken Sie ![](attachments/40220635/40436823.png "save-as.png") ,
    um das Text-Bild-Link-Objekt als... zu speichern.
- Klicken
    Sie ![](attachments/40220635/40436822.gif "152-Revisionen.gif") , um
    das Text-Bild-Link-Objekt als
    neue [Revision](https://wiki.de.dariah.eu/display/tgarchiv2/Revisionen) zu
    speichern.

## Attachments

- [delete.png](attachments/7439382/7766152.png) (image/png)  
- [014-schliesse-Datei.png](attachments/7439382/7766153.png) (image/png)  
- [link.png](attachments/7439382/7766154.png) (image/png)  
- [unlink_obj.png](attachments/7439382/7766155.png) (image/png)  
- [save.png](attachments/7439382/7766156.png) (image/png)  
- [save-as.png](attachments/7439382/7766157.png) (image/png)  
- [152-Revisionen.gif](attachments/7439382/8192391.gif) (image/gif)  
