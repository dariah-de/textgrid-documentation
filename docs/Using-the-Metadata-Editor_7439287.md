# Using the Metadata Editor

last modified on Mai 03, 2016

Öffnen Sie im Navigator das Kontextmenü eines beliebigen Objekts, das
Sie im Metadaten-Editor bearbeiten wollen. Wählen Sie hierfür "Metadaten
öffnen". Die entsprechende Eingabemaske öffnet sich. Füllen Sie alle
Eingabefelder mit den erforderlichen Metadaten.

Nachdem Sie alle Metadaten vervollständigt haben, kann der Datensatz zu
Klicken der Schaltfläche “Metadaten speichern” unten in der
Metadaten-Editor-Sicht gespeichert werden. Alle Einträge können durch
Klicken der Schaltfläche “Metadaten neu laden” neben der Schaltfläche
“Metadaten speichern” neu geladen werden.

Um die externen Metadaten in den Header eines TEI-Objekts zu kopieren,
klicken Sie die Schaltfläche “TEI-Header kopieren” unten in der
Metadaten-Editor-Sicht. Der Header kann in den
im [XML-Editor](XML-Editor_40220521.md) angezeigten Quellcode
eingefügt werden. Der Metadaten-Editor kann auch verwendet werden, um
zwischen bestimmten Objekten, namentlich Editionen und
Werken, [Referenzen zu erstellen](Referenzen-zwischen-Objekten_40220438.md).
