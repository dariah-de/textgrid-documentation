# Context Menu of the Aggregations Editor

last modified on Mai 03, 2016

Das Kontextmenü des Aggregationen-Editors bietet Ihnen neben den
Funktionen der Werkzeugleiste des Editors noch zwei zusätzliche
Möglichkeiten:

- Wählen Sie “Element umbenennen”, um ein beliebiges Element
    umzubenennen
- Unter “Revision” können Sie "Auf
    diese [Revision](https://doc.textgrid.de/search.html?q=Revisions+and+Locking+Mechanism) verweisen",
    "Auf neueste Revision verweisen" oder mit “Auf Revision verweisen …“
    auf eine beliebige andere existierende Revision verweisen

Wenn Sie die letzte Option wählen, öffnet sich ein neuer Assistent “Auf
Revision verweisen”, der alle Revisionen eines Objekts auflistet mit der
Revisionsnummer, Titel, Projekt, von wem die Daten bereitgestellt werden
und wann die Revision erstellt wurde. Wählen Sie durch Klicken ihrer
Nummer die Revision, auf die Sie verweisen wollen, und bestätigen Sie
mit “OK” oder doppelklicken Sie das gewünschte Element. Danach erscheint
die Revisionsnummer hinter dem entsprechenden Objekt im Editor.

![](attachments/40220448/40436727.png)

|                            |
|----------------------------|
| **Auf Revision verweisen** |

## Attachments

- [rev-referrevision-wizard.png](attachments/7439453/9240674.png)
(image/png)  
- [rev-referrevision-wizard.png](attachments/7439453/7766188.png)
(image/png)  
