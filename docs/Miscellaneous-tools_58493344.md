# Miscellaneous tools

last modified on Feb 28, 2018

In diesem Kapitel sind sonstige Werkzeuge aufgelistet, die für eine
digitale Edition hilfreich sein können.

- [TextGridHttpServer](TextGridHttpServer_58493355.md)
- [TextGridHttp Server Plugin](TextGridHttp-Server-Plugin_58493462.md)
