# Features of the Design View

last modified on Mai 04, 2016

**DTD-Informationen hinzufügen und bearbeiten**

Dieser Menüpunkt wird derzeit nicht unterstützt. Sie können als Abhilfe
Ihre DTD in ein XML-Schema konvertieren und es über “XML \> Schema
assoziieren...” assoziieren. 

**Schema-Informationen hinzufügen und bearbeiten**

Ein Namensbereich kann über das Kontextmenü in der Entwurf-Ansicht
gewählt werden. Wählen Sie “Namensbereich bearbeiten...” und der
Assistent “Schemainformationen bearbeiten” wird geöffnet. Die
Deklaration des Namensbereichs besteht aus einem Präfix, dem
Namensbereichsnamen und einem Positionshinweis. Klicken Sie
“Hinzufügen”, um fortzufahren. Im Assistenten “Namensbereichsdeklaration
hinzufügen” können Sie über Optionsfelder zwischen registrierten
Namensbereichen wählen oder einen neuen Namensbereich angeben. Wählen
Sie einen registrierten Namensbereich durch Klicken des Auswahlfelds
davor und Klicken Sie “OK”. Alternativ kann ein neuer Namensbereich
angegeben werden.

Die gewählte Namensbereichsdeklaration wird jetzt im Assistenten
“Schemainformationen bearbeiten” angezeigt. Klicken Sie “Bearbeiten...”.
Geben Sie das benötigte Präfix und den URI für die Deklaration des
Namensbereichs ein. Klicken Sie “Durchsuchen” für einen
Positionshinweis. Der Assistent “Datei auswählen” öffnet sich. Klicken
Sie “OK” oder “Abbrechen”, um den Vorgang abzuschließen und kehren Sie
zu Assistenten “Namensbereichsdeklaration hinzufügen” zurück.

Sobald der Namensbereich festgelegt ist, können Sie den Vorgang mit “OK”
abschließen. Wenn kein bestimmter Namensbereich hinzufügt werden soll,
klicken Sie “Abbrechen”. Dadurch kehren Sie zum Assistenten
“Schemainformationen bearbeiten” zurück, in dem der zuvor hinzugefügte
Namensbereich jetzt angezeigt wird.

Klicken Sie “Hinzufügen”, um weitere Namensbereiche hinzuzufügen. Wählen
Sie “Bearbeiten”, um bereits hinzugefügte Namensbereiche zu verändern.
Klicken Sie “Entfernen”, um sie zu entfernen. Schließen Sie den
Assistenten mit “OK” oder “Abbrechen”. Einmal hinzugefügt können
Namensbereiche durch Rechtsklick und Auswahl von “Namensbereiche
bearbeiten...” bearbeitet werden. Der Namensbereich kann auch durch
Linksklicken des entsprechenden Feldes in der Entwurf-Ansicht geändert
werden.

**Verarbeitungsanweisungen hinzufügen und bearbeiten**

Verarbeitungsanweisungen können durch Rechtsklicken der Stelle, an der
sie hinzugefügt werden sollen, und Auswahl “Untergeordnetes Element
hinzufügen”, “Einfügen vor” oder “Einfügen nach” und danach
“Verarbeitungsanweisung hinzufügen” eingefügt werden. Dieser Vorgang ist
nicht möglich, wenn Sie in der Baumstruktur auf Attribute rechtsklicken.
Eine Verarbeitungsanweisung wird mit ihrem Ziel und ihren Daten
angegeben. Das Eingabefeld "Ziel" wird verwendet, um die Applikation zu
identifizieren, zu der die Anweisungen gehören. Das Eingabefeld "Daten"
enthält die Anweisungen. Klicken Sie “OK”, um die Anweisung
hinzuzufügen. Eine vorhandene Verarbeitungsanweisung kann durch
Rechtsklick und Auswahl von “Verarbeitungsanweisung bearbeiten”
bearbeitet werden. Der Assistent ist identisch mit dem Assistenten
“Verarbeitungsanweisung hinzufügen”. Die Daten können auch durch
Linksklicken des entsprechenden Feldes in der Spalte „Inhalt“ geändert
werden.

**Elemente hinzufügen**

Rechtsklicken Sie das Element, unter dem ein neues Element eingefügt
werden soll. Sie müssen dann wählen, ob das neue Element vor oder nach
dem rechtsgeklickten Element eingefügt werden soll oder als
untergeordnetes (Kind-)Element ("Einfügen vor”, "Einfügen nach”,
“Untergeordnetes Element hinzufügen”). Klicken Sie dann “Neues
Element...”, geben Sie den Namen des Elements ein und klicken Sie “OK”.

**Attribute hinzufügen und bearbeiten**

Rechtsklicken Sie ein Element und wählen Sie “Attribut hinzufügen” und
“Neues Attribut”. Geben Sie den Namen und den Wert des Attributs ein und
klicken Sie “OK”. Mit einem vorhandenen Attribut kann in der
Entwurf-Ansicht durch Rechtsklick und Auswahl von “Attribut bearbeiten”
gearbeitet werden. Der Assistent hat dieselbe Struktur wie der Assistent
“Attribut hinzufügen”. Der Wert des Attributs kann auch durch
Linksklicken des entsprechenden Felds in der Spalte "Inhalt" der
Entwurf-Ansicht geändert werden.
