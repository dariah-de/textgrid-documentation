# Edition-spezifische Metadaten

last modified on Jul 28, 2015

Wenn Sie eine Edition erstellen, beinhaltet das Metadatenformular die
folgenden Felder:

1. Title(s) (Objekt-)Titel
2. Identifier(s) Identifikatoren
3. Edition of Edition von
4. Agent(s) Agent(en)
5. Source(s) Quelle(n)
6. Form(s) of Notation Notationsform(en)
7. Language(s) Sprache(n)
8. License/Copyright Lizenz/Urheberrecht

Das Element “Edition of” versieht das zugehörige Werk-Objekt mit einem
TextGrid-URI. Um eine Edition einem Werk zuzuweisen, verwenden Sie das
Feld “Edition of”. Relevante Objekte für diesen Vorgang werden durch
Klicken der Schaltfläche „Browsen …“ angezeigt. Wenn Sie ein Werk
wählen, wird sein TextGrid-URI im Eingabefeld angezeigt. Verwenden Sie
die Verknüpfung, um das Objekt zu öffnen, und die Schaltfläche, um die
Projekte durchzusehen und ein entsprechendes Werk-Objekt für die Edition
auszuwählen. Das Erstellen einer Referenz zwischen Elemente und
Editionen oder Kollektionen ist nur über den
[Aggregationen-Editor](https://doc.textgrid.de/search.html?q=Aggregations+Editor)
möglich.

Das Element "Agent" ist wiederholbar. Fügen Sie weitere Agenten durch
klicken der Schaltfläche “Weiteren Agent hinzufügen” hinzu. Tragen Sie
den Namen des Agenten als Wert ein und wählen Sie die entsprechende
Dublin Core Rolle im Aufklappmenü aus. Agenten können mit einem URI wie
beispielsweise PND oder FOAF identifiziert werden. Personennamen sollten
in der Form “Nachname, erster Vorname, zweiter Vorname” angegeben
werden.

Das Element "Source" kann eine Objektzitierung oder eine
bibliographische Zitierung sein. Eine Objektzitierung wird
beispielsweise im Falle eines Artefakts in einem Museum verwendet. Eine
bibliographische Zitierung wird beispielsweise für Bücher (d. h.
Monographien und Sammelbänder) oder Zeitschriftenartikel verwendet.
Klicken Sie eine der Schaltflächen, um diese Information hinzuzufügen
oder die entsprechenden Schaltflächen, um Metadaten für Quellen zu
entfernen.

Obekt-Zitierung: Eine Objekt-Zitierung kann aus einem Titel,
Informationen über den Contributor sowie über das Datum und einen
speziellen Identifikator bestehen. “Title” ist ein Pflichtfeld und
wiederholbar. “Objekt Date” und der “Objekt Identifikator”-Typ sind
ebenfalls Pflichtfelder. Das Element "Objekt Contributor" folgt der
Struktur aller "Agent"-Elemente; "Date" folgt der Struktur aller
"Date"-Elemente; und Objekt-Identifikator der Struktur aller anderen
[Identifikatoren](https://doc.textgrid.de/search.html?q=Metadata+Editor+View).

Bibliographische Zitierung: Eine bibliographische Zitierung besteht aus
den Elementen

1. Author(s) Autor(en)
2. Editor(s) Editor(en)
3. Title(s) of Edition Titel der Edition
4. Place(s) of Publication Ort der Veröffentlichung
5. Publisher(s) Herausgeber
6. Date of Publication Datum der Veröffentlichung
7. Edition Nr. Editionsnummer
8. Series Reihe
9. Volume Band
10. Issue Ausgabe
11. StartPage Anfangsseite
12. EndPage Endeseite
13. Bibliographic Identifier(s) Bibliographische(r) Identifikator(en)

Nur das Element "Title(s) of Edition", das Feld "StartPage", das zum
Element "Date of Publication" gehört, und der "Bibliographic
Identifikator"-Typ sind Pflichtfelder. "Title(s) of Edition",
"Author(s)", "Editor(s)", "Place(s) of Publication", "Publisher(s)" und
"Series" sind wiederholbar durch Klicken der Schaltfläche “Hinzufügen”.

Autor, Editor und Herausgeber sind mit einem URI wie beispielsweise PND
oder FOAF identifizierbar. Setzen Sie das Häkchen im Ankreuzfeld, Setzen
Sie das Häkchen im Ankreuzfeld, wenn es sich dabei um juristische
Personen handelt. Verwenden Sie das Feld “Name” für den Namen der
Person. Personennamen sollten in der Form “Nachname, erster Vorname,
zweiter Vorname” angegeben werden. Verwenden Sie das Feld “Value” für
den Namen des Ortes der Veröffentlichung. Für das [Datum der Veröffentlichung](https://doc.textgrid.de/search.html?q=Metadata+Editor+View),
können alle Möglichkeiten für "Date"-Elemente angewendet werden.

"Edition Nr.", "Series", "Volume" und "Issue" sind freie Felder, die wie
ihre bibliographischen Äquivalente verwendet werden. “StartPage” ist ein
Pflichtfeld, “EndPage” hingegen nicht. “Bibliographic Identifikator” ist
ein wiederholbares Element. Wählen Sie ISBN, ISSN oder URL als Typ im
Aufklappmenü. Der Wert muss im Feld “Identifikator(s)” eingefügt werden.

Nachdem Sie die Eingabe in alle Eingabefelder des Elements "Source"
abgeschlossen haben, wählen Sie die Notation im Aufklappmenü "Form(s) of
Notation" aus. Für Fälle mit mehr als einem Schreibstil ist das Element
wiederholbar.

Das Element “Language” ist wiederholbar für Dokumente mit mehr als einer
Sprache.

Der Lizenztyp oder die -Beschreibung kann im Feld “License/Copyright”
spezifiziert werden. Die Lizenz selbst kann mit einem URI identifiziert
werden.
