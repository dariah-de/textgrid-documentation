# Das Annotationsfeld

last modified on Jul 31, 2015

Unterhalb des Text-Text-Link-Feldes befindet sich das Annotationsfeld.
Klicken Sie darin, um einen Kommentar zur ausgewählten
Text-Text-Verknüpfung hinzuzufügen.
