# Aggregations Editor View

last modified Mai 03, 2016

Der Aggregationen-Editor ist Bestandteil einer Perspektive mit
dem [Navigator](40220331.md) und
dem [Metadaten-Editor](Metadaten-Editor_40220458.md). Wenn eine
Aggregation bearbeitet wird, können die Werkzeugleiste und das
Kontextmenü des Aggregationen-Editors verwendet werden.

![](attachments/40220444/40436721.png)

|                          |
|--------------------------|
| **Aggregationen-Editor** |

- [Toolbar of the Aggregations Editor](Toolbar-of-the-Aggregations-Editor_7439451.md)
- [Context Menu of the Aggregations Editor](Context-Menu-of-the-Aggregations-Editor_7439453.md)

## Attachments

- [pum-aggregationcomposer.png](attachments/7439449/7766183.png)
(image/png)  
- [pum-aggregationeditor.png](attachments/7439449/9240673.png)
(image/png)