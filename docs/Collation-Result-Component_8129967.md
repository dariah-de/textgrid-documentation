# Collation Result Component

last modified by on Mai 04, 2016

Wenn eine Kollationierung abgeschlossen ist, wird das Ergebnis im
unteren Bereich
der [CollateX-Perspektive](Die-CollateX-Perspektive_40220887.md) angezeigt.
Diese Komponente stellt das Ergebnis auf drei verschiedene Weisen dar:

1. Die
    Alignment-Tabelle ![](attachments/40220891/40436950.gif "159-Kollationierungsergebnis-tabellarische-Ansicht.gif") zeigt
    jedes Element eines Texts und vergleicht es mit den Textpassagen
    anderer Texte. Jede Varianz wird hervorgehoben.
2. Die Varianten werden auch in einem
    Graphen ![](attachments/40220891/40436949.gif "158-View-Icon-Kollationierungsergebnis.gif") dargestellt,
    der durch Klicken des zweiten Reiters der Komponenten eingesehen
    werden kann. Ziehen und verschieben Sie Elemente mit der Maus,
    sofern gewünscht.
3. Die Ergebnisse werden außerdem in Form von TEI-konformem XML-Text
    ausgegeben.

## Attachments

- [159-Kollationierungsergebnis-tabellarische-Ansicht.gif](attachments/8129967/8192379.gif)
(image/gif)  
- [158-View-Icon-Kollationierungsergebnis.gif](attachments/8129967/8192380.gif)
(image/gif)
