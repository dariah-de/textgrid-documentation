# Context Menu of the Source View

last modified on Mai 04, 2016

Das Kontextmenü der Quelle-Ansicht bietet durch Rechtsklicken Zugriff
auf die meisten Funktionen der Sicht. Über dieses Menü können Sie

- Änderungen rückgängig machen und auf den letzten gespeicherten Stand
    des XML-Dokuments zurückgreifen sowie den aktuellen Stand speichern
- Das Dokument oder Teile davon im [Navigator](40220331.md), dem
    Projektexplorer,
    der [Gliederung-Sicht](Gliederung-Sicht_40220561.md) oder
    der [Eigenschaften-Sicht](Eigenschaften-Sicht_40220567.md) durch
    Auswahl von “Anzeigen in” hervorheben
- Ausschneiden, Kopieren und Einfügen wählen
- Schnellkorrektur starten
- Kommentare verwalten, formatieren oder das Dokument bereinigen als
    Unterpunkte von “Quelle”
- Die [Eigenschaften-Sicht](Eigenschaften-Sicht_40220567.md) öffnen
- Die Subversion-Optionen von Eclipse unter “Team”, “Vergleichen mit”
    oder “Ersetzen durch” nutzen
- [Einen Adapter assoziieren](Funktionen-des-XML-Editors_40220527.md)
- Einen URI oder ein URI-Fragment kopieren, das Dokument
    löschen, [Metadaten](Metadaten-Editor_40220458.md) anzeigen oder
    neu laden
    und [Revisionen](Revisionen--und-Sperre-Mechanismus_40220412.md) sowie
    CRUD-Warnungen anzeigen lassen, die Sie über Datenbankprobleme
    informieren
- Für den Editor relevante Benutzervorgaben auswählen
- Ein Wort in den [Wörterbüchern](40220702.md) nachschlagen

Über das Kontextmenü ist es außerdem möglich, mit anderen Modulen des
TextGridLab zu interagieren. Dies gilt standardmäßig für
die [Wörterbuchsuche](40220702.md), es ist außerdem möglich für die
Werkzeuge [Lemmatisierer](Lemmatisierer_40220718.md), [LEXUS](40220734.md), [Cosmas
II](40220746.md), [ANNEX](40220758.md) und [SADE](Publikationswerkzeug-SADE_40220503.md),
sofern sie installiert sind.
