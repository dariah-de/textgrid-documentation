# Dictionary Search Results View

last modified on Mai 03, 2016

In der oberen rechten Ecke wird die Anzahl der Treffer angezeigt. Als
Hinweis auf den Inhalt wird die erste Zeile des Artikels (bis zu 13
Wörter des Lemmas) angezeigt. Wenn die Zahl der Treffer das Limit
übersteigt, können Sie sich die weiteren Ergebnisse durch Klicken der
Schaltflächen “Weiter” oder “Zurück” anzeigen lassen.
