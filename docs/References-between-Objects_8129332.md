# References between Objects

last modified on Mai 03, 2016

Sie können Relationen oder Referenzen zwischen verschiedenen Arten von
Objekten auf zwei verschiedene Weisen erstellen.

1. Um eine Edition einem Werk zuzuweisen, verwenden Sie das
    Metadatenformular für das "Edition"-Objekt. Um die Eingabemaske für
    die Metadaten zu öffnen, rechtsklicken Sie eine Edition
    im [Navigator](40220331.md) und wählen Sie "Metadaten öffnen".
    Der [Metadaten-Editor](Metadaten-Editor_40220458.md) öffnet sich.
    Scrollen Sie zum Feld "Edition von” und klicken Sie die Schaltfläche
    "Browsen ...". Die relevanten Objekte für diesen Vorgang werden
    angezeigt. Wenn Sie ein Werk auswählen, wird dessen TextGrid-URI im
    Eingabefeld angezeigt. Jetzt wird eine Relation zwischen Werk und
    Edition hergestellt.
2. Das Erstellen einer Referenz zwischen Elementen und Editionen oder
    Kollektionen ist nur mit
    dem [Aggregationen-Editor](Aggregationen-Editor-verwenden_40220450.md) möglich.
    Rechtsklicken Sie eine Edition oder eine Kollektion im Navigator und
    wählen Sie "Bearbeiten". Sobald sich der Aggregationen-Editor
    öffnet, können ein oder mehrere Elemente aus dem Navigator gezogen
    und in der entsprechenden Edition oder Kollektion abgelegt werden.
    Sie können im Navigatorbaum oder im Feld “Teil der Edition(en)” der
    Metadaten-Sicht des Elements sehen, zu welcher Edition ein Element
    gehört. Öffnen Sie die Metadaten eines Elements auf die oben
    beschriebene Weise, um zu sehen, ob es zu einer Edition gehört.
