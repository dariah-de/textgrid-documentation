# Auswahl- und Kontextmenü

Die Inhalte und Funktionen der Nutzeroberfläche hängen von der aktuellen
Auswahl ab. Um ein Word oder ein Objekt in einer Liste oder Baumstruktur
auszuwählen, klicken Sie es einmal, es wird dann hervorgehoben. Um
mehrere Objekte auszuwählen, verwenden Sie Tasten- und
Maus-Kombinationen, die in ihrem Betriebssystem dafür vorgesehen sind
(bei Betriebssystem von Microsoft können Sie die Taste "Strg" gedrückt
halten und mit der Maus klicken, während Sie bei Mac OS die Taste "cmd"
verwenden), oder verwenden Sie die "Umschalttaste", um eine Reihe von
Objekten auszuwählen. Sie können die beiden Tasten auch mit den
Pfeiltasten und der Leertaste kombinieren, um Elemente ohne Verwendung
der Maus auszuwählen. In einem Texteditor können Sie Elemente zum
Hervorheben einfach durch Ziehen der Maus oder durch Verwendung der
Umschalttaste mit den Pfeiltasten auswählen.

Das Kontextmenü des TextGridLab enthält verschiedene Befehle, die sich
abhängig von der/den momentan geöffneten Perspektive(n) oder Sicht(en)
unterscheiden. Die folgenden Kapitel werden die Funktionen, auf die
durch das Kontextmenü zugegriffen werden kann, ausführlich beschreiben.
