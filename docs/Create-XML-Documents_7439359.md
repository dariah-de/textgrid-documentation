# Create XML Documents

last modified on Mai 04, 2016

Sie können ein neues XML-Dokument erstellen, indem Sie entweder
“Datei \> Neues Objekt” in der Menüleiste auswählen oder durch
Rechtsklicken eines Projekts im [Navigator](40220331.md) und Auswahl
von “Neues Objekt” im Kontextmenü. Wählen Sie im sich öffnenden
Assistenten “Neues TextGrid-Objekt”, ein Zielprojekt und den Dokumenttyp
(XML-Dokument, XML-Schema oder XSLT-Stylesheet). Klicken Sie “Weiter”
und weisen Sie im Metadatenformular Metadaten
zu. [Metadaten](https://wiki.de.dariah.eu/display/tgarchiv1/Metadaten) können
auch später hinzugefügt werden. Sie können nun “Weiter” klicken, um ein
Schema mit Ihrem XML-Dokument zu assoziieren, oder “Fertigstellen”, um
den Assistenten zu verlassen. Um die Änderungen im Dokument zu
speichern, können Sie “Datei \> Speichern” wählen, das
Symbol ![](attachments/40220577/40436812.png "save.png") in der
Werkzeugleiste klicken oder in der XML-Editor-Sicht rechtsklicken und im
Kontextmenü “Speichern” wählen.

## Attachments

- [save.png](attachments/7439359/7766148.png) (image/png)  
