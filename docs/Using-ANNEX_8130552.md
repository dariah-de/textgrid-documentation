# Using ANNEX

last modified on Mai 04, 2016

Für weitere Informationen über das Erstellen und Bearbeiten
audiovisueller Dokumenten mit ELAN und ANNEX, siehe

[http://www.lat-mpi.eu/tools/elan/](http://www.lat-mpi.eu/tools/elan/)

[http://www.lat-mpi.eu/tools/annex/](http://www.lat-mpi.eu/tools/annex/) 
