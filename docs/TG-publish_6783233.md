# TG-publish

last modified on Jul 14, 2016

**The TextGrid TG-publish Service Documentation has moved [HERE](https://textgridlab.org/doc/services/submodules/kolibri/kolibri-tgpublish-service/docs/index.md)!**
