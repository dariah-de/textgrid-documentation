# Working with older versions of oXygen

Created on Dez 20, 2013

When you install oXygen using the
[Marketplace](Marketplace_8130167.md), usually the newest version is
installed, and it will be included in the usual
[update](Updating_11472516.md) mechanism. If you would like to stick
with an older version of oXygen, the following steps will help you:

## Undoing an Update

You can undo a TextGridLab update and revert to an earlier version by
the following steps:

1. Save your unfinished work – TextGridLab will be restarted in the
    following steps.
2. Open the *About TextGridLab* dialog from the Help menu (or, on Mac
    OS X, from the TextGridLab menu).
3. Click the *Installation Details* button.
4. Select the *Installation History* button.
5. In the upper list, select the update level you wish to restore. The
    list below shows the installation contents of this level for review.
6. Click the *Revert* button and confirm the dialogs following. After a
    TextGridLab restart, the old version level will be restored.

## Manually installing an older version of oXygen

1. Open *Help* → *Install New Software*
2. From the dropdown list at the top of the window, choose the oXygen
    entry
3. In the lower part of the dialog, uncheck both *Show only the latest
    version* and *Group by category*
4. In the list in the middle of the dialog, check the checkbox next to
    the oXygen version you wish to install (and optionally the
    corresponding language pack for translations)
5. Finish click next twice, confirm the license and click finish to
    install oXygen

Afterwards, you may wish to exempt oXygen from the usual update
mechanism:

## Exempting oXygen from TextGridLab updates

1. Open the [Preferences](Preferences_9012401.md) dialog from the
    Window menu or from the TextGridLab menu (Mac OS X).
2. Select *Install/Update* → *Available Software Sites*
3. From the list, remove the checkmark in front of the oXygen entry
4. Click *Ok* to confirm your changes and close the dialog.
