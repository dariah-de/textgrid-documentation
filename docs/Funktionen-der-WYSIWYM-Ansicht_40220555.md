# Funktionen der WYSIWYM-Ansicht

last modified on Jul 31, 2015

**Block- und Inline-Marker**

Standardmäßig werden keine Tags in der WYSIWYM-Ansicht angezeigt. Um die
Block-Marker anzuzeigen, die eingebettete Abschnitte („divisions“) einer
XML-Datei anzeigen, klicken
Sie **![](attachments/40220555/40436793.gif "161-Block-Tags-einblenden.gif")**
in der Werkzeugleiste. Um die Inline-Marker anzuzeigen, die die
einzelnen Elemente eines XML-Dokuments markieren, klicken
Sie ![](attachments/40220555/40436792.gif "160-Inline-Tags-einblenden.gif")
in der Werkzeugleiste.

![](attachments/7439340/7766139.png "xml-blockinlinemarker-detail.png")

|                              |
|------------------------------|
| **Block- und Inline-Marker** |

**Element einfügen**

Sie können neue Elemente durch Rechtsklicken an einer gewählten Position
im Editor durch Auswahl von “Element einfügen” im Kontextmenü oder über
“XML \> Element(e)
einfügen” ![](attachments/40220555/40436794.png "052-einfuege-Elemente.png")
in der Menüleiste einfügen. Die Aktivierung der Schaltfläche
"Inline-Marker anzeigen" in der Werkzeugleiste wird empfohlen. Wenn bei
der Auswahl von “Element(e) einfügen” Text im Editor hervorgehoben ist,
wird das neue Element das hervorgehobene Textfragment umschließen. In
beiden Fällen erscheint nach der Auswahl von “Element(e) einfügen“ eine
Liste von Elementen, die an dieser Position erlaubt sind. Sie können die
Hoch ↑- und Runter ↓-Tasten auf Ihrer Tastatur verwenden, um ein Element
auszuwählen, um die Eingabetaste drücken oder ein Element in der Liste
doppelklicken, um es einzufügen. Um ein Element einzufügen, das in der
Vorschlagsliste nicht angezeigt wird, geben Sie den Namen des Elements
in das Eingabefeld ein und klicken Sie “Neu eingeben”**.**

## Attachments

- [160-Inline-Tags-einblenden.gif](attachments/40220555/40436792.gif)
(image/gif)  
- [161-Block-Tags-einblenden.gif](attachments/40220555/40436793.gif)
(image/gif)  
- [052-einfuege-Elemente.png](attachments/40220555/40436794.png)
(image/png)  
