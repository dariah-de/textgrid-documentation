# Collection-specific Metadata

last modified on Mai 03, 2016

Das Metadatenformular für Kollektionen enthält die folgenden Elemente:

1. Title(s) (Objekt-)Titel
2. Identifier(s) Identifikatoren
3. Collector(s) Sammler
4. Abstract(s) Zusammenfassung
5. Collection Description(s) Beschreibung(en) der Kollektion
6. Spatial(s) Ortsangabe(n)
7. Temporal(s) Zeitangabe(n)
8. Subject(s) Thema/en
9. Notes Notizen

Verwenden Sie das Element "Collector" für einen DC Relator Term als
Rollenattribut, einen identifizierenden URI als "id"-Attribut und den
Namen des Agenten als Wert. Das Element ist ein Pflichtfeld und
wiederholbar.

Das Element "Abstract" für übergreifende Informationen über ein Objekt
ist optional und nicht wiederholbar. Das Feld "Collection Description"
ist optional und wiederholbar.

Räumliche, zeitliche und fachliche Schlüsselwörter können hinzugefügt
werden. Sie bestehen aus einem Identifikator, der mit einem
kontrollierten Vokabular verknüpft sein sollte und einem Wert, mit dem
er dargestellt werden soll. Diese Felder werden wiederholbar durch
Klicken der Schaltfläche “Weiteren Identifikator hinzufügen”.
