# Open Image and Text

last modified on Apr 07, 2017

Doppelklicken von XML- und Bild-Objekten
im [Navigator](40220331.md) öffnet das XML-Dokument
im [XML-Editor](XML-Editor_40220521.md) und das Bild in
der [Bild-Sicht](Bild-Sicht_40220638.md). Alternativ können
XML-Dokumente durch Auswahl von “Zu Text-Bild-Link-Editor hinzufügen” im
Kontextmenü des Navigators geöffnet werden.

Dieses Werkzeug funktioniert nur mit dem TextGrid XML-Editor.
BenutzerInnen des Oxygen-Plugins müssen darauf achten, das XML-Dokument
nicht mit Oxygen geöffnet zu haben. Nutzen Sie dafür bitte im
Kontextmenü des Navigators "Öffnen mit → TextGrid XML-Editor". Sie
können über die "Benutzervorgaben" auch den TextGrid XML-Editor als
Standard-Tool für Objekte des Typs "xml" einstellen. Öffnen Sie ein
TBLE-Objekt und öffnet sich automatisch der Oxygen-Editor, so sollten
Sie diesen schließen und das XML-Objekt erneut im TextGrid XML-Editor
öffnen.
