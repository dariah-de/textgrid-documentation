# Interaktion der Projekt- und Benutzerverwaltung mit anderen Komponenten

last modified on Aug 19, 2015

Die Benutzerverwaltung interagiert mit dem [Navigator](40220331.md),
der hauptsächlich zur Verwaltung von Projekte und Objekte. Verwendet
wird. Die Navigator-Sicht ist Bestandteil der Projekt- und
Benutzerverwaltung-Perspektive. Durch Rechtsklicken eines Projekts im
Navigator kann die Benutzerverwaltung geöffnet werden.
