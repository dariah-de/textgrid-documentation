# Typical Structure of a TextGridLab tool

last modified on Jul 28, 2015

To get a tool ready to be installable from the TextGridLab Marketplace,
you need to provide a *p2 repository* with your tooling as an
installable unit, typically as a feature. A p2 repository is Eclipse's
way of providing software: It is a directory structure with your tool
and metadata, provided somewhere on a public server. This is also called
an *update site*.

Like many Eclipse projects, TextGrid tools use the Maven plugin
[Tycho](http://eclipse.org/projects/tycho) to build our tools. Have a
look at the rather simple [TextGridRep Preview](https://projects.gwdg.de/projects/aggregator-lab) project that
may serve as a starting point (please look at the `develop` branch).

The root `pom.xml` defines the overall structure, we only look at some
details.

``` syntaxhighlighter-pre
    <parent>
    <groupId>info.textgrid.lab</groupId>
    <artifactId>parent</artifactId>
    <version>2.1.0</version>
    <relativePath>../parent</relativePath>
    </parent>
```

We recommend inheriting from this parent POM. It is deployed to the
DARIAH Nexus (see below) and it contains many of the basic definitions
used for our stuff.

``` syntaxhighlighter-pre
    <modules>
    <module>info.textgrid.lab.aggregator</module> <!-- plugin -->
    <module>info.textgrid.lab.aggregator.feature</module>
    <module>aggregator-repository</module>  <!-- p2 repo -->
    </modules>
```

This simple project defines three subprojects: The first one is a plugin
that contains your actual code. The second one is a *feature* that
simply specifies which plugins and features constitute your project and
which are required. This is the starting point for the later
installation. The third one builds the update site – you add your
feature to the `category.xml` in there and run `mvn package` in the root
folder and it creates the update site ready to deploy in
`aggregator-repository/target/repository`.

You'll need to specify some sources for import:

``` syntaxhighlighter-pre
    <repositories>

      <repository>
    <id>dariah.nexus</id>
    <name>DARIAH Nexus Public Repository</name>
    <url>http://dev.dariah.eu/nexus/content/groups/public</url>
    <releases>
      <enabled>true</enabled>
    </releases>
    <snapshots>
      <enabled>true</enabled>
    </snapshots>
      </repository>
```

This *Maven* repository contains the parent POM.

``` syntaxhighlighter-pre
      <repository>
    <id>lab-core</id>
    <layout>p2</layout>
    <url>${lab.repository.core}</url>
      </repository>

      <repository>
    <id>eclipse</id>
    <layout>p2</layout>
    <url>${lab.repository.eclipse}</url>
      </repository>

      <repository>
    <id>textgridlab-dependencies</id>
    <layout>p2</layout>
    <url>${lab.repository.dependencies}</url>
      </repository>
    </repositories>
```

Here we have three p2 repositories defined: `lab-core` contains the
current TextGridLab's core plugins, `eclipse` points to the
corresponding Eclipse release and `textgridlab-dependencies` contains
some external and TextGrid non-lab libraries, packaged for consumption
in Eclipse-based applications.

To get started, I recommend copying this project and renaming stuff from
`aggregator` to your tooling name in the various `.xml` files. Use the
Eclipse editors to adjust the `feature.xml` and `category.xml` and keep
the version numbers in the corresponding `pom.xml`s in sync. The real
development work will happen in the plugin project, though.

(Some older documentation, focusing on TextGrid internal development,
[can be found on the sub pages](https://doc.textgrid.de/search.html?q=TextGridLab+Modular+Build).)
