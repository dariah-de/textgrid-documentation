# Statusleiste der Bild-Sicht

last modified on Aug 20, 2015

Die Statusleiste der Bild-Sicht gibt Auskunft über (von links nach
rechts)

- den [aktivierten Modus](Werkzeugkasten_40220657.md)
- den Content Type oder das Objektformat des geöffneten Bilds
- den Namen des geöffneten Text-Bild-Link-Objekts
- ein
    Warndreieck ![](attachments/40220647/40436840.png "warning.png") ,
    wenn es noch unverknüpfte Markierung(en) gibt
- den Rotationswinkel der gewählten Markierung, der im Assistenten
    “[Rotationswinkel
    einstellen](Funktionen-der-Bild-Sicht_40220641.md)” nach Klicken
    dieses Feldes geändert werden kann.
- den RGB-Farbwert an den Koordinaten des Mauszeigers in der
    Bild-Sicht
- eingeblendete oder aktivierte Ebenen. Der
    [Ebenen-Editor](Funktionen-der-Bild-Sicht_40220641.md) öffnet sich
    nach Klicken dieses Feldes

## Attachments

- [warning.png](attachments/40220647/40436840.png) (image/png)  
