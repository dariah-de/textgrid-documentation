# Einleitung

last modified on Feb 26, 2019

In der Einleitung werden die Vorzüge des TextGrid Labs vorgestellt.
Außerdem werden die besonderen Begrifflichkeiten der auf Eclipse
basierenden Oberfläche eingeführt. Dazu gehören Perspektive und Ansicht
sowie die Such- und die Hilfefunktion.

- [Inbetriebnahme](Inbetriebnahme_40220196.md)
- [Nutzeroberfläche](40220240.md)
- [Suche](Suche_40220282.md)
- [Hilfe](Hilfe_40220301.md)
