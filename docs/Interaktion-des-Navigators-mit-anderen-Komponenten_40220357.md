# Interaktion des Navigators mit anderen Komponenten

last modified on Aug 19, 2015

Als Werkzeug zur Verwaltung von Projekten und Objekten interagiert der
Navigator mit der [Projekt- und Benutzerverwaltung](Projekt--und-Benutzerverwaltung_40220359.md), dem
[Metadaten-Editor](Metadaten-Editor_40220458.md) und dem
[Aggregationen-Editor](Aggregationen-Editor_40220440.md).

Die [Projekt- und Benutzerverwaltung](Projekt--und-Benutzerverwaltung_40220359.md) kann
aus dem Kontextmenü der Navigator-Sicht geöffnet werden. Die Projekt-
und Benutzerverwaltung wird außerdem standardmäßig nach dem Erstellen
neuer Projekte geöffnet.

Aus dem Kontextmenü der Navigator-Sicht kann auch der
[Metadaten-Editor](Metadaten-Editor_40220458.md) geöffnet werden sowie
Metadaten neu geladen werden. Die Metadaten-Editor-Schnittstelle ist
Bestandteil des Assistenten zum Erstellen neuer Objekte.

Der Navigator ist standardmäßig Bestandteil der
[Aggregationen-Editor](Aggregationen-Editor_40220440.md)-Perspektive.
Diese wird insbesondere zum Erstellen von Referenzen zwischen Objekten
und Aggregationen wie Editionen oder Kollektionen verwendet. Wenn Sie
eine Edition, Kollektion oder andere Aggregation rechtsklicken und
“Bearbeiten” wählen, öffnet sich der Aggregationen-Editor.

Wenn [SADE](Publikationswerkzeug-SADE_40220503.md) installiert ist,
kann ein Objekt über das Kontextmenü des Navigators mit SADE
veröffentlicht werden.
