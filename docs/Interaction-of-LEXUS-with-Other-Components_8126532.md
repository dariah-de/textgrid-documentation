# Interaction of LEXUS with Other Components

last modified on Mai 04, 2016

Eine Suche in LEXUS kann direkt aus
dem [XML-Editor](XML-Editor_40220521.md) über das Kontextmenü
der [Quelle-Sicht](Quelle-Ansicht_40220547.md),
der [WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) oder
der [Vorschau](Vorschau-Ansicht_40220559.md) initialisiert werden.
