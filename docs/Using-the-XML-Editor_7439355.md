# Using the XML Editor

last modified on Mai 04, 2016

Einige der wichtigsten Operationen, für die der XML-Editor vorgesehen
ist, werden in den folgenden Abschnitten erläutert.

- [Open XML Documents](Open-XML-Documents_7439357.md)
- [Create XML Documents](Create-XML-Documents_7439359.md)
- [Associate an XML Schema and Validating XML Files](Associate-an-XML-Schema-and-Validating-XML-Files_7439361.md)
