# Musical Score View

last modified on Mai 04, 2016

At least one perspective must be open to contain the Musical Score View.
As soon as an MEI Object has been created or opened, the view will
appear. In this area the musical scores that you can open from the
TextGrid Repository, or create and modify with the Palette and the
Outline View, will be displayed.

The encoded note text is displayed in a WYSIWYM View to display the
encoding properly. All changes in the document can be made in this
WYSIWYG-style. The Score View is primarily meant for the two-dimensional
display of the rendering, as drag & drop operations are disabled.
Elements in the note sheet should be modified with the assistance of the
Editor’s [Palette](Palette_7439859.md) and
the [Outline](Outline-View-of-the-MEI-Score-Editor_7439872.md) and [Properties
View](Properties-View-of-the-MEI-Score-Editor_7439882.md) . Single
elements in the Score View as notes or rests can be selected and appear
in blue by left-clicking on them.  

More complex interventions in the source code of an MEI document will
require the [XML Editor](XML-Editor_7439318.md). To open the source
code of an MEI Object, please use its context menu in
the [Navigator](Navigator_7439188.md) and choose “Open with \> XML
Editor”.

![](attachments/7439823/9240710.png)

|                          |
|--------------------------|
| MEISE Musical Score View |

The context menu is enabled on the Score View, but direct user
manipulation via drag & drop is disabled. It offers the following
options:

- Undo ![](attachments/7439823/9240617.png) and
    Redo ![](attachments/7439823/9240618.png) changes .
- If single elements are selected in the Score View, they can be
    deleted with ![](attachments/7439823/9240620.gif).
- The user can duplicate preselected elements with
    "Clone" ![](attachments/7439823/9240624.png).
- "Run as \> Run Configurations…" opens a new window. There the user
    will be able to create new configurations for the launch and define
    the settings.
- You can validate your MEI document by clicking “Validate”.
- Use some Eclipse-specific options by selecting “Team”, “Compare
    With”, or “Replace With”.
- To associate an adaptor to your document, please select “Associate
    an Adaptor...”. An associated adaptor can translate the given
    documents to the TextGrid Baseline Encoding. In the new dialog box
    the selected file is presented with information about title,
    project, contributor, and creation date. You can mark the box below
    to make the assignment persistent when clicking “OK”. In the lower
    list you can select the adaptor by clicking on it. Here you can also
    receive additional information. To reverse your selection, you can
    mark the button under this list.
- To save your changes, click ![](attachments/7439823/9240621.gif) on
    the bottom of the context menu.

## Attachments

- [renderview.PNG](attachments/7439823/8192181.png) (image/png)  
- [eclipse-icons.zip](attachments/7439823/9240615.zip) (application/zip)  
- [eclipse-icons.zip](attachments/7439823/9240616.zip) (application/zip)  
- [eclipse-icons.zip](attachments/7439823/9240619.zip) (application/zip)  
- [undo_edit.png](attachments/7439823/9240617.png) (image/png)  
- [redo_edit.png](attachments/7439823/9240618.png) (image/png)  
- [eclipse-icons.zip](attachments/7439823/9240614.zip) (application/zip)  
- [042-loesche-Auswahl.gif](attachments/7439823/9240620.gif) (image/gif)  
- [016-speichere-Datei.gif](attachments/7439823/9240621.gif) (image/gif)  
- [action-clone.png](attachments/7439823/9240624.png) (image/png)  
- [mei-musicalscoreview.png](attachments/7439823/9240710.png)
(image/png)  
