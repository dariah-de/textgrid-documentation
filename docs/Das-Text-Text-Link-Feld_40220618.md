# Das Text-Text-Link-Feld

last modified on Jul 31, 2015

Dieses Feld wird verwendet, um Verknüpfungen zwischen XML-Fragmenten zu
erstellen. Wenn ein Fragment ausgewählt ist, kann eine Verknüpfung, die
auf dieses Fragment referenziert, durch Klicken der Schaltfläche "Neu"
erstellt werden. Wenn ein anderes Fragment ausgewählt ist, klicken Sie
„Hinzufügen“, um eine Verknüpfung zwischen den Fragmenten zu erstellen.
Mit „Löschen“ können Fragmente in Verknüpfungsobjekten sowie die
Verknüpfungsobjekte selbst gelöscht werden.
