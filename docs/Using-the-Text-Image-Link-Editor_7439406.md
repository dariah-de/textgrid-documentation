# Using the Text Image Link Editor

last modified on Mai 03, 2016

Die folgenden Kapitel beschreiben den Verknüpfungsvorgang von Bildern
und Texten mit dem Text-Bild-Link-Editor.

- [Open Image and Text](Open-Image-and-Text_7439408.md)
- [Select Workspace](Select-Workspace_7439412.md)
- [Create Selections in the Image](Create-Selections-in-the-Image_7439414.md)
- [Create Links between Text and Image](Create-Links-between-Text-and-Image_7439416.md)
- [Correct and Delete Links between Text and Image](Correct-and-Delete-Links-between-Text-and-Image_7439418.md)
- [Open and Edit a Text Image Link Object](Open-and-Edit-a-Text-Image-Link-Object_8129810.md)
