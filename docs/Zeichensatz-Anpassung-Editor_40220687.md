# Zeichensatz-Anpassung-Editor

last modified on Jul 31, 2015

Dieser Editor zeigt in der Kopfzeile eine Liste der momentan verfügbaren
Zeichensätze an. Der Hauptteil beinhaltet eine Werkzeugleiste zum
Einfügen, Löschen und Speichern der Zeichensätze. Die Fußzeile zeigt die
Werte der momentan gewählten Zeichensätze und deren Veränderung durch
die folgenden Optionen:

- Klicken
    Sie ![](attachments/40220687/40436873.png "Clipboard02.png") , um
    einen neuen Satz zu erstellen.  
    Um ein Set zu löschen, klicken
    Sie ![](attachments/40220687/40436870.png "Clipboard06.png") oder
    rechtsklicken Sie das Set, um es aus der Liste zu löschen.
- Klicken
    Sie ![](attachments/40220687/40436869.png "Clipboard05.png") , um
    die Liste zu aktualisieren.
- Des Weitere können Sie Sets außerhalb von TextGrid speichern und
    externe Sets durch Klicken der
    Symbole ![](attachments/40220687/40436876.png "document-save-as.png")
    oder ![](attachments/40220687/40436875.png "document-open.png")
    importieren.

Der Name und das Symbol des Zeichensatzes können ebenso geändert werden
wie der Inhalt, der entweder aus einem vordefinierten Unicode-Blocksatz
oder einer vom Benutzer angepassten Liste von Unicode-Zeichen besteht.
Diese Zeichen können als dezimaler Unicode-Codepoint (e.g. “56789”), als
hexadezimaler Unicode-Codepoint mit einem Präfix (beispielsweise
“U+1D100”) oder direkt aus der Zwischenablage eingefügt werden. Die
gewählten Zeichen müssen durch Semikolons getrennt werden. Klicken Sie
“Schließen”, um den Zeichensatz zu speichern.

![](attachments/40220687/40436874.png "uct-charseteditor-wizard.png")

|                                  |
|----------------------------------|
| **Zeichensatz-Anpassung-Editor** |

## Attachments

- [Clipboard05.png](attachments/40220687/40436869.png) (image/png)  
- [Clipboard06.png](attachments/40220687/40436870.png) (image/png)  
- [Clipboard04.png](attachments/40220687/40436871.png) (image/png)  
- [Clipboard03.png](attachments/40220687/40436872.png) (image/png)  
- [Clipboard02.png](attachments/40220687/40436873.png) (image/png)  
- [uct-charseteditor-wizard.png](attachments/40220687/40436874.png)
- [document-open.png](attachments/40220687/40436875.png) (image/png)  
- [document-save-as.png](attachments/40220687/40436876.png) (image/png)  
