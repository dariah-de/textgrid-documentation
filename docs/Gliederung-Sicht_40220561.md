# Gliederung-Sicht

last modified on Jul 31, 2015

Die Gliederung-Sicht erleichtert die Navigation in großen
XML-Dokumenten, indem sie einen Überblick über die Baumstruktur des
Dokuments gibt.

![](attachments/40220561/40436795.png "xml-outline-view.png")

|                      |
|----------------------|
| **Gliederung-Sicht** |

- [Titelleiste der Gliederung-Sicht](Titelleiste-der-Gliederung-Sicht_40220563.md)
- [Kontextmenü der Gliederung-Sicht](40220565.md)

## Attachments

- [xml-outline-view.png](attachments/40220561/40436795.png) (image/png)  
