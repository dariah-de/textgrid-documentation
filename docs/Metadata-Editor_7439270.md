# Metadata Editor

last modified on Mai 03, 2016

Der Metadaten-Editor wird verwendet, um die grundlegenden
Metadaten-Elemente von TextGrid-Objekten zu erstellen und zu verwalten.
Diese Metadaten werden in TextGrid für projektübergreifende Suchen
genutzt. Das Metadaten-Eingabeformular kann mit
dem [Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md) an
die individuellen Bedürfnisse angepasst werden.

- [Open the Metadata Editor](Open-the-Metadata-Editor_7439272.md)
- [Metadata Editor View](Metadata-Editor-View_7439274.md)
- [Using the Metadata Editor](Using-the-Metadata-Editor_7439287.md)
- [Interaction of the Metadata Editor with Other Components](Interaction-of-the-Metadata-Editor-with-Other-Components_7439289.md)
