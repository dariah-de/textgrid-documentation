# Menu Bar of the oXygen XML Editor

last modified on Mai 04, 2016

Wenn der oXygen-XML-Editor geöffnet ist, gibt es eine zweite
"XML"-Option in der Menüleiste des TextGridLab, die den Charakteristika
von oXygen entspricht. In "Werkzeuge \> Sicht anzeigen \> Andere …" gibt
es ebenfalls Komponenten von oXygen, die durch Doppelklicken geöffnet
werden können. Für weitere Informationen über das oXygen-XML-Menü und
die Komponenten des Editors, siehe

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)

Beachten Sie, dass die erste Menüleiste "XML" nicht verwendet werden
kann, solange oXygen geöffnet ist, da sich dieses Menü ausschließlich
auf den TextGrid-XML-Editor bezieht. Es ist daher nicht möglich, ein
Schema aus eigenen Projekten oder generische Schemas wie TEI P5, das in
TextGrid mitgeliefert wird, auszuwählen und Dokumenten zuzuweisen, die
im TextGrid-XML-Editor geöffnet sind. Um ein Schema zuzuweisen, wählen
Sie den Unterpunkt "Schema assoziieren" aus der zweiten Menüleiste "XML"
in Verbindung mit dem TextGrid-URI des entsprechenden Objekts.
