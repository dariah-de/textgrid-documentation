# Open oXygen

last modified on Mai 04, 2016

Der oXygen-XML-Editor kann durch Rechtsklicken eines Objekts und Auswahl
von “oXygen” geöffnet werden. Wenn oXygen als Standardeditor eingestellt
ist, hat das Doppelklicken des Objekts denselben Effekt.
