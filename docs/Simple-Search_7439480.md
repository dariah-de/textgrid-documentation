# Simple Search

last modified on Feb 28, 2018

Im Eingabefeld der Einfache Suche (die standardmäßig geöffnet wird)
können Sie Ihre Suchbegriffe eingeben. Es gibt drei
verschiedene Suchmodi:

- "Suche im Volltext": nur der Volltext eines Objekts wird durchsucht
- "Suche in Metadaten": nur die Metadaten eines Objekts wird
    durchsucht
- "Suche in beidem": sowohl der Volltext als auch die Metadaten eines
    Objekts werden durchsucht

In der einfachen Volltext-Suche, können Suchzeichenketten durch
Verwendung der Operatoren “and” und “or” kombiniert werden. Eingaben
ohne diese Konjunktionen werden als mit “and” verknüpft interpretiert.

Die einfache Metadaten-Suche funktioniert auf die gleiche Weise wie die
Volltext-Suche. Die Suchzeichenkette kann mit “and” und “or” kombiniert
werden. Eine weitere Möglichkeit, nach Metadaten zu suchen ist, den
Metadatentyp zu spezifizieren. Die Notation
lautet *Metadaten-Tag:Suchbegriff*. Alle möglichen Metadaten-Tags können
mit dieser Option verwendet werden.
