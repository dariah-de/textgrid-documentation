# TextGridLab 2.0.x Release HOWTO

last modified on Okt 02, 2013

We maintain a set of maintenance releases for TextGridLab 2.0

**WORK IN PROGRESS**

## Bug Fixing / Development

1. Everything that needs to be fixed for a 2.0.x release needs a proper
    focused issue report in our Jira.
2. Bug fixes should be applied to the `trunk` (nightly) code first.
3. Each commit should be focused, and it SHOULD mention the relevant
    issue number (TG-1234) in its commit message  
    1. If you accidently forget to include the issue number with a
        relevant commit, you MUST add a comment including the relevant
        revision number(s) of your commit(s) in the bug report
4. You should increase the relevant artifact versions accordingly
5. If your bug should be included in yet unreleased TextGridLab 2.0.n,
    include the relevant release in the bug reports *fix for* field

## Preparing the Release Candidate

The 2.0.x train is maintained in the branch
[http://develop.sub.uni-goettingen.de/repos/textgrid/branches/releases/TextGridLab-2.0.x](http://develop.sub.uni-goettingen.de/repos/textgrid/branches/releases/TextGridLab-2.0.x).
There is an automatic build
at [http://dev.digital-humanities.de/ci/job/lab-2.0.x/](http://dev.digital-humanities.de/ci/job/lab-2.0.x/).
Example description for releasing *TextGridLab 2.0.2*.

1. Checkout the branch
2. Adjust the version number by running `./setversion.sh 2.0.2` in the
    branch directory. Requires xmlstarlet & performs the following
    edits:  
    Adjust the version number in the following places (yes I know this
    sucks):
    - base/info.textgrid.lab.feature.base/pom.xml:`<version>` element
    - base/info.textgrid.lab.feature.base/feature.xml: `version`
        attribute
    - base/base-repository/pom.xml:`<archiveFileName>` element
    - base/base-repository/info.textgrid.lab.core.application.base_product.product:
        `version` attribute
    - base/info.textgrid.lab.welcome/src/info/textgrid/lab/welcome/IntroXHTMLContentProvider.java:
        The version string for the welcome message
3. Generate a list of bugs for the version to be released using the
    issue filter. Sort by date resolved, increasing
4. For each bug:  
    1. determine the changeset(s) by checking the *SVN Updates* tab
            and the *Comments* for the bug report.
    2. Merge the changesets:

            ``` syntaxhighlighter-pre
            export trunk=https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/lab
            svn merge -c <changeset> $trunk
            ```

    3. resolve any conflicts, if neccessary.
    4. build and clean
    5. commit. In the commit message, you MUST mention the bug
            report and you SHOULD mention the changesets. It could be
            helpful to actually get the changeset's original log
            message, e.g., if you use vi as your editor for the commit
            message:

            ``` syntaxhighlighter-pre
            :r!svn log -c <changeset> $trunk
            ```

## Testing the Release Candidate

After the build with all included releases has finished, the RC should
be tested

1. Manual test against the features
2. Functional test
3. Test updates from previous lab version by  
    1. downloading a clean copy of the previous version of the lab
    2. adding the update site,
        [http://download.digital-humanities.de/updates/2.0.x/](http://download.digital-humanities.de/updates/2.0.x/)
    3. *Check for updates* or restart the lab
4. Call for user tests

## Release

When everything is tested we need to prepare the update site, upload the
product zips and update the download website.

### Prepare the Update Site

- The update sites are maintained on textgridlab.org
    in `/var/www/updates/stable`. We have the following directory
    structure:  
  - `stable/composite/2.0.5` etc. each contain an update site for
        the respective version *only*.
  - `stable/composite` is an composite update site of *all* these
        version-specific update sites.
  - `stable` is a (non-composite) mirror of `stable/composite`.

    To prepare this, there are a bunch of scripts linked to
    /usr/local/bin. Do the following, in this order, replacing 2.0.5
    with the appropriate version:

      1. in `stable/composite`,
        run `generate-aggregate-repo http://download.digital-humanities.de/updates/2.0.x 2.0.5`
        – this will mirror the repository from the integration build to
        the directory `2.0.5`.
      2. in `stable/composite`, run
        `generate-composite-p2-repo "TextGridLab Stable" 2.*` – this
        will create the composite repository metadata for the 2.\*
        subdirectories in the current (composite) directory.
      3. in `stable`,
        run `generate-aggregate-repo $PWD/composite candidate` – this
        will create an aggregate (= faster) mirror of the composite
        stuff in the `candidate` directory.
      4. For each directory or file in `candidate`, remove their
        respective counterpart from `stable` and move the version
        from `candidate` in place.
      5. remove the (now empty) `candidate` subdirectory.
- Details for managing the directory structure and
    the `composite*.xml` files are at [Update Site
    Management](Update-Site-Management_9929010.md).

### Prepare the Downloads

- The download site is on textgridlab.org at `/var/www/download`.
    Files are in subdirectories with the version number
- It is probably best to just mirror the original downloads from the
    automatic build

### Download Website

1. on textgridlab.org, cd into `/var/www/download/2.0.2` and run the
    [generate-lab-index.py](attachments/12194350/12419521.py) script
2. open the download page in the typo3 backend and  
    1. paste the generate-lab-index.py output into the download table
        field in the hero block, replacing the existing content
    2. write a short release note in German and English into the
        corresponding field
    3. generate the changelog in Jira:  
        1. go to the [TextGrid project's Versions list](https://develop.sub.uni-goettingen.de/jira/browse/TG#selectedTab=com.atlassian.jira.plugin.system.project%3Aversions-panel)
        2. click the version to release
        3. click the *Release Notes* link in the upper right
    4. paste the changelog into the corresponding field in typo3
    5. add last version to the list of archived versions

### Jira

1. Go to the [Manage Versions](https://develop.sub.uni-goettingen.de/jira/secure/project/ManageVersions.jspa?pid=10023)
    page
2. *Release* your version
3. add a new version

## Attachments

- [generate-lab-index.py](attachments/12194350/12419521.py)
(text/x-python)  
