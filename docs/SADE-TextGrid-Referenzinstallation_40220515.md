# SADE-TextGrid-Referenzinstallation

last modified on Jul 28, 2015

Es gibt eine Referenzinstallation von SADE für TextGrid, die unter
[http://141.5.100.153/exist/apps/SADE/textgrid/index.html](http://141.5.100.153/exist/apps/SADE/textgrid/index.md)
verfügbar ist. Diese dient Demonstrationszwecken und Sie können damit
auch eigene Veröffentlichungen testen. Wenn Sie sich dafür einen eigenen
Bereich unter
[http://141.5.100.153/exist/apps/SADE/textgrid/sign-up.html](http://141.5.100.153/exist/apps/SADE/textgrid/sign-up.md)
reservieren, erhalten Sie eine E-Mail mit der Bestätigung und allen
notwendigen Parametern.
