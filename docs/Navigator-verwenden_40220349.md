# Navigator verwenden

last modified on Aug 19, 2015

Einige der wichtigsten Aufgaben der Navigator-Sicht werden in den
folgenden Abschnitten behandelt.

- [Neue Projekte erstellen](Neue-Projekte-erstellen_40220351.md)
- [Neue Objekte erstellen](Neue-Objekte-erstellen_40220353.md)
