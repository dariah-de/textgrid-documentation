# Interaktion von Digilib mit anderen Komponenten

last modified on Aug 20, 2015

Digilib kann verwendet werden, um Bilder zu bearbeiten und zu speichern,
bevor sie im
[Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md) mit
XML-Dokumenten verknüpft werden.
