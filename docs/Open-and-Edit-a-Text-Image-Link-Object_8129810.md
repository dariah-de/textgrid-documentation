# Open and Edit a Text Image Link Object

last modified on Mai 03, 2016

Um ein neues verknüpftes Objekt im Navigator zu öffnen, rechtsklicken
Sie es und wählen Sie "Öffnen" oder doppelklicken Sie es mit der linken
Maustaste.

Sie können ein Text-Bild-Link-Objekt auch über "Bearbeiten" im
Kontextmenü des Navigators bearbeiten. Ein neuer Editor wird geöffnet.
Er zeigt die zwei Spalten "Referenzen im Objekt" auf der linken und
"Ersetzen durch" auf der rechten Seite an. Die linke Spalte listet die
Bild- und XML-Dokumente auf, auf die im Text-Bild-Link-Objekt verwiesen
wird. Mit den Schaltflächen im Bereich "Neue Objekte hinzufügen" am
rechten Rand können Sie neue Objekte oder lokale Dateien zur rechten
Spalte hinzufügen. Diese Objekte ersetzen die bisherigen in der linken
Spalte. Sie können auch Objekte aus den Listen beider Spalten mit der
Schaltfläche "Ausgewähltes Objekt löschen" löschen. Die Position eines
gewählten Objekts in der Liste kann durch "Hoch" und "Runter" geändert
werden. Alle Änderungen am Text-Bild-Link-Objekt können durch Klicken
auf "Änderungen übernehmen ..." übernommen werden.

## Attachments

- [021-oeffne-TGObjekt.png](attachments/8129810/8192268.png) (image/png)  
- [069-zeige-BildLinkEditorPerspektive.png](attachments/8129810/8192269.png)
(image/png)  
