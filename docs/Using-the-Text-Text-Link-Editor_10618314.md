# Using the Text Text Link Editor

last modified on Mai 03, 2016

Führen Sie diese Schritte durch, um eine Verknüpfung zwischen Fragmenten
von XML-Dokumenten zu erstellen:

1. Klicken Sie ![](attachments/40220622/40436815.gif) in der
    Menüleiste, um ein neues Element vom Typ "Text-Text-Link Objekt" zu
    erstellen.
2. Für diesen Vorgang muss ein Projekt ausgewählt und Metadaten
    hinzugefügt werden.
3. Nachdem der Text-Text-Link-Editor geöffnet wurde, doppelklicken Sie
    die relevanten XML-Dokumente.
4. Die ausgewählten XML-Dokumente werden in verschiedenen parallelen
    Spalten angezeigt.
5. Wählen Sie ein Textfragment aus einem XML-Dokument und klicken Sie
    "Neu", um eine neue Verknüpfung zu erstellen.
6. Wählen Sie ein anderes Textfragment und klicken Sie "Hinzufügen", um
    es zu einer ausgewählten Verknüpfung zusammenzufassen. Die momentan
    gewählte Verknüpfung wird kursiv dargestellt.
7. Sie können den Buchstaben "C" in Klammern vor der Verknüpfung
    klicken, um eine Farbe für alle Fragmente dieser Verknüpfung
    auszuwählen.
8. Sie können in das Annotationsfeld klicken und einen Kommentar zur
    ausgewählten Verknüpfung hinzufügen.

## Attachments

- [143-Erstelle-Textlink-Dokument.gif](attachments/10618314/20381989.gif)
(image/gif)  
- [020-erstelle-TGObjekt.gif](attachments/10618314/20381990.gif)
(image/gif)  
- [add.gif](attachments/10618314/20381991.gif) (image/gif)  
