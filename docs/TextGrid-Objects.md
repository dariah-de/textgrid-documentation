# TextGrid Objects

last modified on Feb 28, 2018

TextGrid verwaltet Objekte und die Arten von Relationen zwischen ihnen.
Diese Relationen drücken sich in Aggregationen, Editionen und
Kollektionen aus, die selbst ebenfalls TextGrid-Objekte sind. Um
Relationen zwischen Objekten zu erstellen, muss jedes Objekt
identifizierbar sein. Um eine weitere Identifikation der Objekte und
eine projektübergreifende [Suche](Suche_40220282.md) zu ermöglichen,
müssen die Objekte
durch [Metadaten](https://wiki.de.dariah.eu/display/tgarchiv1/Metadaten) beschrieben
werden. Für einen Überblick über [alle Metadaten-Kategorien](https://doc.textgrid.de/search.html?q=Metadata+Categories),
siehe die entsprechende Seite.

- [Objects](Objects_7439232.md)
- [Items](Items_7439236.md)
- [Aggregations](Aggregations_7439234.md)
- [Works](Works_7439240.md)
- [Editions](Editions_7439238.md)
- [Collections](Collections_7439242.md)
- [References between Objects](References-between-Objects_8129332.md)
