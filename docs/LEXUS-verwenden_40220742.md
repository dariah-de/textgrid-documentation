# LEXUS verwenden

last modified on Aug 20, 2015

1.Öffnen Sie LEXUS durch Auswahl von “Werkzeuge \> Sicht anzeigen \>
    LEXUS” in der Menüleiste oder Klicken
    von ![](attachments/40220742/40436901.png "lexus.png") in der
    Werkzeugleiste. Das LEXUS-Werkzeug öffnet sich unterhalb des
    [XML-Editors](XML-Editor_40220521.md).
2. Geben Sie ein Wort im Eingabefeld ein. Sie müssen das passende
    Lexikon ausgewählt haben. Derzeit ist das deutsche Lexikon als
    Standardwert voreingestellt.
3. Optional können Sie Erweiterte Suchoptionen für die
    Ziel-Datenkategorie einstellen. Im deutschen Lexikon ist die
    Standardkategorie "lzgA", welche das Lemma selbst bezeichnet; die
    Standard-Suchbedingung ist "is", d. h. nur das exakte Lemma wird im
    Lexikon abgefragt.
4. Klicken Sie “Suche”, um die Suche im Lexikon zu starten.

Alternativ können Sie LEXUS über das Kontextmenü der
[Quelle-Sicht](Quelle-Ansicht_40220547.md) oder der
[WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md) im XML-Editor öffnen.

1. Markieren Sie das Wort im XML-Editor und rechtsklicken Sie es. Das
    Kontextmenü öffnet sich.
2. Klicken Sie “In LEXUS suchen“. Das LEXUS-Werkzeug öffnet sich
    unterhalb des XML-Editors und Ihre Suche wird umgehend starten. Das
    Ergebnis wird im Ergebnisfeld angezeigt.

## Attachments

- [lexus.png](attachments/40220742/40436901.png) (image/png)  
- [082-zeige-WortformLemmatisiererAnsicht.gif](attachments/40220742/40436902.gif)
(image/gif)  
