# Dictionary Grid View

last modified on Mai 03, 2016

Um einen Eintrag im Wörterbuch nachzuschlagen, klicken Sie auf die in
der Wörterbuchsuchergebnisse-Sicht aufgelistete Kurzform. Der Eintrag
öffnet sich in der Wörterbuch-Grid-Sicht. Die Reiter am oberen Rand der
Wörterbuch-Grid-Sicht führen zu weiteren Informationen über das
Online-Wörterbuch und das Projekt, in das das Projekt eingebettet ist.
Das Feld am oberen Ende der linken Spalte kann für die Suche von Lemmas
verwendet werden. Diese Schnellansicht beginnt mit dem angefragten
Artikel. Es listet abhängig von deren Länge auch die folgenden Artikel
auf. Sie können alle verfügbaren Optionen nutzen, um genauere Suchen
durchzuführen. In dieser Sicht entspricht das Kontextmenü jenem ihres
Betriebssystems.
