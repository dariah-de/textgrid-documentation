# Text-Text-Link-Editor

last modified on Feb 26, 2019

Der Text-Text-Link-Editor steht im Marketplace nicht mehr zum Download
zur Verfügung.

Der Text-Text-Link-Editor dient der Verknüpfung von Fragmenten
verschiedener XML-Dokumente und speichert die Ergebnisse als
TEI-konforme Objekte. Der Editor validiert zudem bestehende
Verknüpfungen zwischen XML-Dokumenten.

- [Text-Text-Link-Editor installieren](Text-Text-Link-Editor-installieren_40220610.md)
- [Text-Text-Link-Editor öffnen](40220612.md)
- [Die Text-Text-Link-Editor-Perspektive](Die-Text-Text-Link-Editor-Perspektive_40220614.md)
- [Das XML-Dokument-Feld](Das-XML-Dokument-Feld_40220616.md)
- [Das Annotationsfeld](Das-Annotationsfeld_40220620.md)
- [Das Text-Text-Link-Feld](Das-Text-Text-Link-Feld_40220618.md)
- [Text-Text-Link-Editor verwernden](Text-Text-Link-Editor-verwernden_40220622.md)
