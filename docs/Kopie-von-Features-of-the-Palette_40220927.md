# Kopie von Features of the Palette

Created on Jul 24, 2015

The
Palette ![](attachments/40220927/40437015.png "Bildschirmfoto 2012-01-12 um 12.20.33.png")
includes a range of icons divided into several groups:

- Control Group: With the “Select”
    tool **![](attachments/40220927/40437014.png "Bildschirmfoto 2012-01-30 um 11.22.30.png")**
    it is possible to go to any position in the note sheet.
- "Create Staves":
    ![](attachments/40220927/40437023.png "meise_staffgroup.png") 
    creates a new group of bracketed or braced right hand and left hand
    staves. ![](attachments/40220927/40437022.png "meise-old_staffdeff.png")
    creates a new staff definition, i.e. a container for staff
    meta-information which includes meta-information about the clef’s
    pitch, the key signature, the label (right hand or left hand), and
    the time signature.
- "Create Containers":
    “Section” **![](attachments/40220927/40437005.gif "icon-section.gif")**
    ,
    “Measure” ![](attachments/40220927/40437021.png "002-zeige_Notentakt2.png")
    ,
    “Staff” **![](attachments/40220927/40437004.gif "icon-staff.gif")**
    and
    “Layer” **![](attachments/40220927/40437003.gif "icon-layer.gif")**
    : this group creates new containers in the note sheet which are
    themselves able to contain other data types. All comprised data in a
    container results in a meaningful whole.

         ![](attachments/40220927/40436996.png)

- "Create Events":
    “Note” ![](attachments/40220927/40437018.png "003-zeige-Note.png") ,
    “Rest” ![](attachments/40220927/40437017.png "004-zeige-Notenpause_3.png")
    , "MRest"
    **![](attachments/40220927/40437006.png "004-zeige-Notenpause.png")**,
    "Space" **![](attachments/40220927/40437002.gif "icon-space.gif")**,
    “Beam
    (group)” **![](attachments/40220927/40437008.png "111-zeige-Balkengruppe.png")**
    , “Chord
    (group)” **![](attachments/40220927/40437007.png "110-zeige_Akkordgruppe.png")**
    : this group contains the minimum unit of elements which can be
    created within a musical score in the Score View.

        ![](attachments/40220927/40436995.png)

- "Create Variants":
    “Apparatus” **![](attachments/40220927/40437001.gif "icon-apparatus.gif")**
    / “Reading”
    **![](attachments/40220927/40437000.gif "icon-reading.gif")**:  this
    group enables the encoding of variants, a specialty of the editor
    for musicological use.

        ![](attachments/40220927/40436994.png)

- "Create Additions": "Create
    Slur" **![](attachments/40220927/40436999.gif "icon-slur.gif")**
    "Create
    Tie" **![](attachments/40220927/40436999.gif "icon-slur.gif")**
    "Dynam" **![](attachments/40220927/40436998.gif "icon-dynam.gif")**
    : with this element group, useful music notation symbols can be
    added to the score.
      
    ![](attachments/40220927/40436993.png)  

The pin function on each of the units ("Pin open", "Unpin") allows you
to maintain the Palette state even after restarting the TextGrid Lab.

For more details about the application of the Palette, please see the
chapter [Using the MEI Score Editor](Using-the-MEI-Score-Editor_7439898.md). 

## Attachments

- [mei-palette-additions.png](attachments/40220927/40436993.png)
(image/png)  
- [mei-palette-variants.png](attachments/40220927/40436994.png)
(image/png)  
- [mei-palette-events.png](attachments/40220927/40436995.png)
(image/png)  
- [mei-palette-containers.png](attachments/40220927/40436996.png)
(image/png)  
- [createAdditions.PNG](attachments/40220927/40436997.png) (image/png)  
- [icon-dynam.gif](attachments/40220927/40436998.gif) (image/gif)  
- [icon-slur.gif](attachments/40220927/40436999.gif) (image/gif)  
- [icon-reading.gif](attachments/40220927/40437000.gif) (image/gif)  
- [icon-apparatus.gif](attachments/40220927/40437001.gif) (image/gif)  
- [icon-space.gif](attachments/40220927/40437002.gif) (image/gif)  
- [icon-layer.gif](attachments/40220927/40437003.gif) (image/gif)  
- [icon-staff.gif](attachments/40220927/40437004.gif) (image/gif)  
- [icon-section.gif](attachments/40220927/40437005.gif) (image/gif)  
- [004-zeige-Notenpause.png](attachments/40220927/40437006.png)
(image/png)  
- [110-zeige_Akkordgruppe.png](attachments/40220927/40437007.png)
(image/png)  
- [111-zeige-Balkengruppe.png](attachments/40220927/40437008.png)
(image/png)
- [section.png](attachments/40220927/40437009.png) (image/png)  
- [meise-f1_create variants.png](attachments/40220927/40437010.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.23.37.png](attachments/40220927/40437011.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.23.25.png](attachments/40220927/40437012.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.22.56.png](attachments/40220927/40437013.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.22.30.png](attachments/40220927/40437014.png) (image/png)  
- [Bildschirmfoto 2012-01-12 um 12.20.33.png](attachments/40220927/40437015.png) (image/png)  
- [Bildschirmfoto 2012-01-12 um 12.25.14.png](attachments/40220927/40437016.png) (image/png)  
- [004-zeige-Notenpause_3.png](attachments/40220927/40437017.png)
(image/png)  
- [003-zeige-Note.png](attachments/40220927/40437018.png) (image/png)  
- [meise-create_events.png](attachments/40220927/40437019.png)
(image/png)  
- [meise-create_containers.png](attachments/40220927/40437020.png)
(image/png)  
- [002-zeige_Notentakt2.png](attachments/40220927/40437021.png)
(image/png)  
- [meise-old_staffdeff.png](attachments/40220927/40437022.png)
(image/png)  
- [meise_staffgroup.png](attachments/40220927/40437023.png) (image/png)  
- [meise-f1_select.png](attachments/40220927/40437024.png) (image/png)  
