# The Mission of TextGrid

last modified on Feb 28, 2018

Das primäre Ziel von TextGrid ist, GeisteswissenschaftlerInnen eine
virtuelle Forschungsumgebung (VFU, englisch: VRE für Virtual Research
Environment) zur Verfügung zu stellen, in der verschiedene Werkzeuge und
Dienste für die Erstellung, Analyse, Bearbeitung und Veröffentlichung
von Texten und Bildern verfügbar sind. Sie besteht aus zwei
Hauptkomponenten: dem TextGrid Laboratory (TextGridLab) und dem TextGrid
Repository (TextGridRep). Die Werkzeuge und Dienste des TextGrid
Laboratory sind darauf ausgelegt, den Bedürfnissen textbasierter
geisteswissenschaftlicher Disziplinen wie Philologie, Linguistik,
Musikwissenschaften und Kunstgeschichte gerecht zu werden. TextGrid
unterstützt durch die Integration des TextGrid Repository, in dem
Forschungsdaten vorgehalten und zugänglich gemacht werden, zudem die
Speicherung und Nachnutzung von Forschungsdaten. Die Architektur von
TextGrid ist erweiterbar, was bedeutet, dass zusätzliche Werkzeuge und
Dienste einfach hinzugefügt und erweitert werden können.

Das TextGrid unterliegende Konzept ist ziemlich einfach:
GeisteswissenschaftlerInnen sind nicht mehr auf die Einschränkungen
ihres individuellen Arbeitsbereichs beschränkt, sondern können mit der
Sicherheit, dass ihre Daten in einer sicheren und zuverlässigen Umgebung
entsprechende der Richtlinien für gute wissenschaftliche Praxis
gespeichert werden, ortsunabhängig kollaborativ an Projekten mit anderen
ForscherInnen zusammenarbeiten. Das TextGrid Laboratory (Lab) ist eine
quelloffene Software (Open Source), die von einem beliebigen Computer
gestartet werden kann und integrierten Zugriff auf spezialisierte
Werkzeuge, Dienste und Inhalte bietet. Es bietet eine Auswahl getesteter
Werkzeuge, Dienste und Ressourcen, die den kompletten Arbeitsablauf
unterstützen, um beispielsweise eine textkritische Edition digital zu
erstellen. Zusätzlich zu den existierenden Werkzeugen und Diensten, die
bereits im TextGridLab verfügbar sind, können externe Funktionen in Form
von zusätzlichen Werkzeugen und Diensten hinzugefügt werden. Das
TextGrid Repository (Rep) ist ein Archiv, das die Langzeitspeicherung
und Nachnutzung von Forschungsdaten ermöglicht. Sobald Dokumente im
TextGridRep abgespeichert sind, können sie nicht mehr gelöscht oder
verändert werden. Updates und new editions may be added, but the
original documents will be maintained as published in order to follow
responsible research practices. Ein sogenannter PID-Dienst weist jedem
Objekt, das im Repository veröffentlicht wird, einen persistenten
Identifizierer (persistent identifier, PID) zu, einen einzigartigen
Code, der aus einer Folge von Zahlen und Buchstaben besteht und
Zitierbarkeit garantiert.

Alles in allem richtet sich TextGrid an drei hauptsächliche
Nutzergruppen: Geisteswissenschaftliche ForscherInnen, die an
Forschungsprojekten und digitalen Editionen arbeiten,
Software-Entwickler, die neue Dienste und Werkzeuge entwickeln und
implementieren und für ihre Entwicklungen eine integrierte quelloffene
Plattform finden möchten, und Inhaltsanbieter wie Archive und
Forschungsinstitutionen, die ihre Daten in TextGrid integrieren möchten,
um sie einem breiteren Publikum zur Verfügung zu stellen.
