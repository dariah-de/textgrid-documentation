# Interaction of the Metadata Editor with Other Components

last modified on Mai 03, 2016

Der Metadaten-Editor ist Bestandteil
des [XML-Editors](XML-Editor_40220521.md). Er zeigt die Metadaten
der [Objekte](TextGrid-Objekte.md), die gerade geöffnet sind.
Die Struktur der Metadaten hängt von der Art des Objekts ab, auf die sie
sich bezieht. Sie kann mit
dem [Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md) für
die speziellen Bedürfnisse angepasst werden.
