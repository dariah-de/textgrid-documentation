# Fachspezifische Werkzeuge and Dienste

last modified on Aug 04, 2015

In diesem Abschnitt werden die Werkzeuge für spezielle wissenschaftliche
Anforderungen erklärt.

- [Linguistik-Werkzeuge](Linguistik-Werkzeuge_40220700.md)
- [Philologische Werkzeuge](Philologische-Werkzeuge_40220879.md)
- [Musikwissenschaftliche Werkzeuge](Musikwissenschaftliche-Werkzeuge_40220903.md)
- [Werkzeuge für Bildwissenschaften](40220957.md)
- [Weitere Werkzeuge](Weitere-Werkzeuge_58493443.md)
