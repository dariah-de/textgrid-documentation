# Metadaten-Vorlagen-Editor

last modified on Jul 28, 2015

Der Metadaten-Vorlagen-Editor erlaubt Ihnen, für Ihre Projekte eigene
Felder im Metadaten-Editor anzulegen. Sie müssen die
"Projekt-Manager"-Rolle innehaben, um mit diesem Editor zu arbeiten.

- [Metadaten-Vorlagen-Editor öffnen](40220484.md)
- [Metadaten-Vorlagen-Editor verwenden](Metadaten-Vorlagen-Editor-verwenden_40220486.md)
- [Interaktion der Metadaten-Vorlagen-Editor-Sicht mit anderen Komponenten](Interaktion-der-Metadaten-Vorlagen-Editor-Sicht-mit-anderen-Komponenten_40220488.md)
