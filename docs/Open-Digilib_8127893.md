# Open Digilib

last modified on Mai 04, 2016

Um Bilder in Digilib zu öffnen, wählen Sie “Digilib” im Kontextmenü
eines Bildes im [Navigator](40220331.md).

Das Bild wird dann im Digilib-Editor angezeigt. Wenn der Digilib-Editor
geöffnet ist, werden verschiedene Schaltflächen zur Veränderung des
Bildes in der Werkzeugleiste angezeigt.

Für fortgeschrittene Bearbeitungsmöglichkeiten müssen Sie die
Eigenschaften-Sicht öffnen. Sie können entweder sie entweder indirekt
durch Öffnen des XML-Editors im Hintergrund öffnen oder “Werkzeuge \>
Sicht anzeigen \> Andere...” in der Menüleiste wählen. Sobald sich der
Assistent “Sicht anzeigen” öffnet, wird eine Reihe von Ordnern
angezeigt. Öffnen Sie den ersten Ordner “Allgemein” und wählen Sie
“Eigenschaften”.

Klicken Sie auf das Bild im Digilib-Editor und der Assistent
„Eigenschaften“ wird für die Bearbeitung geöffnet. Jetzt ist Ihr
Arbeitsbereich fertiggestellt.

![](attachments/40220963/40437067.png)

|                         |
|-------------------------|
| **Digilib-Perspektive** |

## Attachments

- [Neues Bild.png](attachments/8127893/8192331.png) (image/png)  
- [digilib.png](attachments/8127893/8192332.png) (image/png)  
- [digilib1.png](attachments/8127893/8192333.png) (image/png)  
- [dl-digilib.png](attachments/8127893/9240719.png) (image/png)  
