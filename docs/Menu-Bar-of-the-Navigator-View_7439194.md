# Menu Bar of the Navigator View

last modified on Feb 28, 2018

Manche Funktionen, die essenziell zur Verwaltung von Projekten und
Objekten sind, stehen auch in der Menüleiste unter dem Menüpunkt “Datei”
zur Verfügung:

- Wählen Sie “Neues Objekt”, um ein neues TextGrid-Objekt zu
    erstellen, und “Neues Projekt”, um ein neues TextGrid-Projekt zu
    erstellen.
- Objekte können mit “Objekt öffnen” geöffnet werden, aber auch mit
    “URI öffnen...”. Dieser Identifikator kann auch mit “URI kopieren”
    kopiert werden.
- Projekte können von Nutzern, die die dafür
    notwendigen [Rechte](Projekt--und-Benutzerverwaltung_40220359.md) besitzen,
    deaktiviert, reaktiviert oder gelöscht werden.
