# Source View

last modified on Mai 04, 2016

In der Quelle-Ansicht können Sie den Quelltext eines Dokuments direkt
manuell bearbeiten sowie Elemente und Attribute einfügen oder entfernen.
Zeilen können durch Klicken der Symbole + und – am linken Rand geöffnet
und geschlossen werden. Eine Schema-abhängige Inhaltshilfe unterstützt
Sie durch eingeblendete Hinweise und eine Liste von Vorschlägen beim
Hinzufügen neuer Elemente.

![](attachments/40220547/40436785.png)

|                |
|----------------|
| Quelle-Ansicht |

- [Features of the Source View](Features-of-the-Source-View_8129705.md)
- [Context Menu of the Source View](Context-Menu-of-the-Source-View_8129709.md)

## Attachments

- [xml-sourceview.png](attachments/7439338/9240681.png) (image/png)  
- [xml-validationerror-detail.png](attachments/7439338/7766133.png)
(image/png)  
- [xml-hoverhelp-detail.png](attachments/7439338/7766134.png)
(image/png)  
- [xml-sourceview.png](attachments/7439338/7766132.png) (image/png)  
