# Neue Objekte erstellen

last modified on Jul 28, 2015

Wählen Sie “Datei \> Neues Objekt” in der Menüleiste oder “Neues Objekt”
im Kontextmenü des Navigators, um in einem ausgewählten Projekt ein
TextGrid-Objekt zu erstellen.

![](attachments/40220353/40437118.png)

|                      |
|----------------------|
| **Objekt erstellen** |

Um ein Objekt zu erstellen, müssen ein Projektordner und ein Objekttyp
gewählt werden. Wenn ein Projekt im Navigator vorausgewählt war, wird
dieses Projekt im Assistenten “Neues TextGrid-Objekt” vorgeschlagen.
Nach Klicken der Schaltfläche “Weiter” wird die
[*Metadaten-Editor*](https://doc.textgrid.de/search.html?q=Metadata+Editor)-Schnittstelle
geöffnet. Vervollständigen Sie die Metadaten und klicken Sie “Weiter”.
Abhängig vom Dokumenttyp unterscheiden sich die folgenden Schritte. Wenn
es sich um ein XML-Dokument handelt, können Sie ein Schema wählen.
Nachdem alle Pflichtfelder ausgefüllt wurden, können Sie zum Abschließen
des Vorgangs “Fertigstellen” klicken.

## Attachments

- [pum-createobject-wizard.png](attachments/40220353/40436711.png)
(image/png)  
- [image2015-7-28 13:55:30.png](attachments/40220353/40437118.png) (image/png)  
