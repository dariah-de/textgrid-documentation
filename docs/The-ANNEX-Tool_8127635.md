# The ANNEX Tool

last modified on Mai 04, 2016

Der ANNEX-Arbeitsbereich verwendet die Daten aus der Annotationsdatei.
In der oberen linken Ecke wird die zur Annotation gehörende Mediendatei
durch ein QuickTime-Plugin angezeigt. In der Spalte links davon wird das
erste Bedienelement mit verschiedenen Optionen wie "Text", "Raster",
"Untertitel" und "Zeitleiste" angezeigt sowie "Schwingungsverlauf" und
"Kombiniert", die nicht immer eingeblendet werden. Rechts von der
Videodatei wird ein Feld mit "Medieninformationen" angezeigt. Im unteren
Bereich des Werkzeugs befindet sich der eigentliche Arbeitsbereich.

![](attachments/40220764/40436909.jpg "textgrid-manual-screenshot-annex01.jpg") 

|                    |
|--------------------|
| **ANNEX-Werkzeug** |

## Attachments

- [textgrid-manual-screenshot-annex01.jpg](attachments/8127635/8192403.jpg)
(image/jpeg)  
- [lit-annexview.png](attachments/8127635/9240704.png) (image/png)  
