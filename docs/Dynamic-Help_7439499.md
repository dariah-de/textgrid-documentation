# Dynamic Help

last modified on Feb 28, 2018

In den meisten Sichten finden Sie eine Schaltfläche mit einem
Fragezeichen, um die "Dynamische Hilfe" zu öffnen. Alternativ können Sie
diese immer über “Hilfe \> Dynamische Hilfe” in der Menüleiste oder
durch Klicken des "Hilfesymbols"  in der Werkzeugleiste öffnen.
Beispielsweise führt Sie das blaue verknüpfte Wort “Hilfe” in
der [Benutzerverwaltung-Sicht](Benutzerverwaltung-Sicht_40220383.md) zu
Informationen über die Zuweisung von Rollen. Ebenso führt Sie der blaue
verknüpfte Ausdruck “Hinweise zur Suche” in
der [Suche-Sicht](Suche-Sicht_40220291.md) zu einer Dynamischen Hilfe.

## Attachments

- [help_contents.gif](attachments/7439499/9240577.gif) (image/gif)  
