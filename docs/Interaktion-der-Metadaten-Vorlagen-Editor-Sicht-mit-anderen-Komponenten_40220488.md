1.  [TextGrid](index.md)
# Interaktion der Metadaten-Vorlagen-Editor-Sicht mit anderen Komponenten

last modified on Aug 19, 2015

Der Metadaten-Vorlagen-Editor erlaubt Ihnen, die Metadaten für die
Bedürfnisse eines Projekts anzupassen. Die Schnittstelle des
[Metadaten-Editors](Metadaten-Editor_40220458.md) ändert sich
entsprechend der Anpassungen, die im Metadaten-Vorlagen-Editor gemacht
wurden.
