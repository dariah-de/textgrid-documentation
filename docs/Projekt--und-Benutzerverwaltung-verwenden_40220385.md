# Projekt- und Benutzerverwaltung verwenden

last modified on Aug 19, 2015

Führen Sie diese Schritte durch, um neue Mitglieder zu Ihrem Projekt
hinzuzufügen:

1. Suchen Sie nach Nutzern durch Name, E-Mail-Adresse oder
    Organisation. Der Platzhalter \* steht dabei für eine beliebige
    Zeichenkette. Sie können alle registrierten suchbaren Nutzer
    durchsuchen, indem Sie nur das Asterisk-Zeichen (\*) eintragen. Wenn
    Sie “Benutzer suchen” klicken oder die Eingabetaste drücken, wird
    das Kontrollkästchen “Suchergebnisse anzeigen” automatisch aktiviert
    und die Ergebnisse oberhalb in einer Liste von Nutzern mit dem
    Symbol ![](attachments/7439226/7766089.png "115-Suche-Nutzer.png")
    angezeigt. Außerdem können Sie nach neuen Mitgliedern suchen, indem
    Sie die Nutzer auflisten, mit denen Sie bereits in einem Projekt
    zusammenarbeiten. Aktivieren Sie dafür das Kontrollkästchen
    “Kontakte anzeigen”. Ihre Kontaktpersonen werden mit einem
    speziellen
    Symbol ![](attachments/7439226/7766090.png "116-Kontakt-Nutzer.png")
    markiert.
2. Wählen Sie die [Rollen](Benutzerverwaltung-Sicht_40220383.md) für
    die zukünftigen Projektmitglieder durch aktiveren der
    Kontrollkästchen neben jedem Nutzer. Noch nicht übernommene
    Änderungen werden in roten Kästchen dargestellt.
3. Sobald Sie “Änderungen übernehmen” klicken, wird der Hintergrund der
    Kästchen weiß und das Symbol vor den Namen ändert sich in das
    Symbol ![](attachments/7439226/7766091.png "114-Projekt-Nutzer.png") .
    Die Nutzer sind nun Mitglieder in Ihrem Projekt. Klicken Sie
    “Änderungen rückgängig machen”, um noch nicht übernommene Änderungen
    rückgängig zu machen.
