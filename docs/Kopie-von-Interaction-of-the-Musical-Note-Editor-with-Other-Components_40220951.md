# Kopie von Interaction of the Musical Note Editor with Other Components

Created on Jul 24, 2015

MEI documents that were created or modified with the MEI Score Editor
can also be opened with the [XML Editor](XML-Editor_7439318.md) to
display and/or modify their source code.
