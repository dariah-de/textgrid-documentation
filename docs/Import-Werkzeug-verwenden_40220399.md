# Import-Werkzeug verwenden

last modified on Jul 28, 2015

Das Import-Werkzeug kann nicht nur verwendet werden, um lokale Dateien
als TextGrid-Objekte zu importieren, sondern auch um sie danach mit oder
ohne Metadaten zu re-importieren.

- [Lokale Dateien importieren](Lokale-Dateien-importieren_40220402.md)
- [Re-Import von TextGrid-Objekten](Re-Import-von-TextGrid-Objekten_40220404.md)
