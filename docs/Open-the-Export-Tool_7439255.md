# Open the Export Tool

last modified on Mai 03, 2016

Eine Perspektive muss geöffnet sein, um dieses Werkzeug zu nutzen.
Wählen Sie “Datei \> Aus TextGrid exportieren ...” in der Menüleiste
oder wählen Sie ein oder mehrere Objekt(e) im Navigator oder der
Suchergebnisse-Sicht und wählen Sie “Aus TextGrid exportieren …”.
