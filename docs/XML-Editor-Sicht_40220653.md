# XML-Editor-Sicht

last modified on Aug 20, 2015

Um Bilder und XML-Dokumente zu verknüpfen, muss der
[XML-Editor](XML-Editor_40220521.md) geöffnet werden. Er ist
standardmäßig Bestandteil der Text-Bild-Link-Editor-Perspektive.
