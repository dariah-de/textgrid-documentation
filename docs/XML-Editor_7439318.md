# XML Editor

last modified on Mai 03, 2016

Der XML Editor ist ein interaktives Werkzeug zur Bearbeitung von
bestehenden und zur Erstellung von neuen XML-Dokumenten sowie der
Annotation derselben. XML ist eine Auszeichnungssprache, wobei in den
Auszeichnungselementen die Bedeutung des Inhaltes festgelegt wird und
optional auch dessen Darstellung. Mit XML können Daten in einem Text
organisiert und klassifiziert werden. Somit wird der
Informationsaustausch zwischen Programmen erleichtert, und durch die
Maschinenlesbarkeit sind sowohl Text als auch Struktur gut durchsuchbar.
Mehr Informationen zu XML gibt es auf

[http://www.w3.org/XML/](http://www.w3.org/XML/)

Innerhalb des TextGridLab stehen zwei XML Editoren zur Auswahl: der von
Eclipse mitgelieferte und ein speziell für TextGrid angepaßter. Dieser
basiert auf dem "[Eclipse Web Tools Platform Project](http://www.eclipse.org/webtools)" und dem Editor
"[Vex](http://wiki.eclipse.org/Vex)" (John Krasnay et al).

Das Handbuch erläutert die [TextGrid XML Editor Perspektive](XML-Editor-Perspective_7439322.md) und seine Bestandteile
im einzelnen und enthält einige Beispiele zur [Benutzung des XML Editors](Using-the-XML-Editor_7439355.md).

- [Open the XML Editor](Open-the-XML-Editor_7439320.md)
- [XML Editor Perspective](XML-Editor-Perspective_7439322.md)
- [Editor Field](Editor-Field_7439334.md)
- [Outline View](Outline-View_7439342.md)
- [Properties View](Properties-View_7439349.md)
- [Using the XML Editor](Using-the-XML-Editor_7439355.md)
- [Interaction of the XML Editor with Other Components](Interaction-of-the-XML-Editor-with-Other-Components_7439363.md)
