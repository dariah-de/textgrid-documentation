# Keyword Search

last modified on Feb 28, 2018

Die Schlüsselwort-Suche kann über einen eigenen Menüpunkt gestartet
werden. Geben Sie ein oder mehrere Wörter in das Eingabefeld ein und
klicken Sie “Los”. Klicken Sie das Auswahlfeld am rechten Rand des
Eingabefelds, um eine Liste der verwendeten Ausdrücke der vorherigen
Suchanfragen anzuzeigen. Sie können Platzhalter und logische Operatoren
(AND, OR und NOT) verwenden. Das Asterisk-Zeichen (\*) steht für eine
beliebige Zeichenkette, das Fragezeichen (?) für genau ein Zeichen und
” ” können Sie verwenden, um einen Ausdruck abzugrenzen.

- Klicken
    Sie ![](attachments/40220318/40436687.png "search-result-categories.png") ,
    um Ergebniskategorien anzuzeigen.
- Klicken
    Sie ![](attachments/40220318/40436686.png "search-result-descriptions.png") ,
    um Ergebnisbeschreibungen anzuzeigen.
- Verwenden
    Sie ![](attachments/40220318/40436685.png "back.png") und ![](attachments/40220318/40436684.png "forward.png"),
    um zu navigieren.

Klicken Sie auf das nach unten gerichtete Dreieck in der Werkzeugleiste
der Sicht, um zwischen verschiedenen Optionen zu wählen, die während der
Schlüsselwort-Suche verfügbar sind:

- Wenn Sie “Inhalte” auswählen: Die Ergebnisse werden in einer
    Baumstruktur angezeigt. Verwenden Sie die Symbole “+” und “-”, um
    einen Zweig zu öffnen oder zu schließen. Klicken
    Sie ![](attachments/40220318/40436683.png "collapse-all-nodes.png") in
    der Werkzeugleiste, um alle Zweige zu schließen.
- Durch Auswahl von “Verwandte Themen” werden Einträge ausgewählt, in
    denen der/die momentan geöffnete Editor oder Sicht oder diskutiert
    werden.
- Wählen Sie “Lesezeichen” aus, um ihre mit einem Lesezeichen
    versehenen Einträge zu durchsuchen.
- Sie können außerdem im "Index" suchen.

Das Ergebnis der Suche wird darunter aufgelistet. Sie können den Titel
eines Eintrags anklicken, um ihn zu öffnen. Wenn Sie einen Eintrag in
der Hilfe öffnen, bietet Ihnen die Werkzeugleiste der Sicht die
Möglichkeit,

- den Eintrag in einem externen Fenster anzuzeigen, das im
    “Inhaltsverzeichnis der Hilfetexte” geöffnet wird,
- den Eintrag in den Hilfethemen in Baumstruktur anzuzeigen,
- den Eintrag zu drucken und ein Lesezeichen zu setzen,
- die Suchbegriffe durch klicken hervorzuheben,
- mit den Schaltflächen “Zurück” und “Weiter” zu navigieren.

Die Fußzeile des Fensters "Inhaltsverzeichnis der Hilfetexte" bietet
Ihnen dieselben Optionen wie die Kopfzeile der Schlüsselwort-Suche:
“Inhalte”, “Suchen”, “Verwandte Themen”, “Lesezeichen” und
“Verzeichnis”.

## Attachments

- [search-result-categories.png](attachments/7439497/7766206.png)
(image/png)  
- [search-result-descriptions.png](attachments/7439497/7766207.png)
(image/png)  
- [back.png](attachments/7439497/7766208.png) (image/png)  
- [forward.png](attachments/7439497/7766209.png) (image/png)  
- [collapse-all-nodes.png](attachments/7439497/7766210.png) (image/png)  
- [search-lamp.png](attachments/7439497/7766211.png) (image/png)  
