# Collation Set and Equivalence Set Component

last modified on Mai 04, 2016

Im oberen Bereich
der [CollateX-Perspektive](Die-CollateX-Perspektive_40220887.md) werden
standardmäßig Kollations- und Äquivalenzsets angezeigt. Wenn diese Sets
nicht ausgewählt sind, ist der obere Bereich leer. Wenn Sie ein
Kollationierungsset erstellen möchten, klicken
Sie ![](attachments/40220889/40436948.gif "155-Datei-Icon-Collation-Set.gif") in
der Werkzeugleiste, um die Kollationierungssetliste zu öffnen, und
ziehen Sie TextGrid-Objecke aus dem [Navigator](40220331.md) oder
der [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md) und legen
Sie diese in der Liste ab. Das Kollationierungsset ist das Set aller
Textzeugen, die bei der Kollationierung berücksichtigt werden sollen.
Die Texte müssen als Klartext (.txt) vorliegen. Um ein Äquivalenzset zu
erstellen, klicken
Sie ![](attachments/40220889/40436947.gif "156-Datei-Icon-Ausnahmeliste.gif") in
der Werkzeugleiste, um die Äquivalenzsetliste zu öffnen, und ziehen Sie
Tokens aus der Kollationsergebnis-Komponente und legen Sie diese in der
Liste ab. Das Äquivalenzset legt alle Elemente, die als identisch
behandelt werden sollen, fest. Varianz zwischen diesen Elementen wird
vom Werkzeug ignoriert.

## Attachments

- [155-Datei-Icon-Collation-Set.gif](attachments/8129965/8192329.gif)
(image/gif)  
- [156-Datei-Icon-Ausnahmeliste.gif](attachments/8129965/8192330.gif)
(image/gif)  
