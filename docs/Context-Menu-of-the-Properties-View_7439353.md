# Context Menu of the Properties View

last modified on Mai 04, 2016

Das Kontextmenü der Eigenschaften-Sicht erlaubt Ihnen, den Wert einer
gegebenen Eigenschaft zu kopieren und wiederherzustellen. Wenn ein
Element der Gliederung-Sicht ausgewählt ist und Sie dann in das Textfeld
der Wert-Spalte der Eigenschaften-Sicht klicken, wird Ihnen dasselbe
Kontextmenü angezeigt wie in Ihrem Betriebssystem.
