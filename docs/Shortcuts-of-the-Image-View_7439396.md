# Shortcuts of the Image View

last modified on Mai 03, 2016

Die folgenden Tastenkürzel sind verfügbar:

|                                   |                                                                                            |
|-----------------------------------|--------------------------------------------------------------------------------------------|
| \[Bild Hoch\] und \[Bild Runter\] | Bild hoch- oder runterbewegen                                                              |
| \[+\]                             | Hineinzoomen                                                                               |
| \[-\]                             | Herauszoomen                                                                               |
| \[0\]                             | Originalgröße                                                                              |
| \[C\]                             | Aktives Rechteck horizontal nach rechts duplizieren                                        |
| \[Strg+A\]                        | [Alle Markierungen auswählen](Funktionen-der-Bild-Sicht_40220641.md)                     |
| \[Strg+T\]                        | [Werkzeugkasten](Werkzeugkasten_40220657.md) öffnen oder schließen                       |
| \[Strg+V\]                        | [Aktive Andocklinie duplizieren](Funktionen-der-Bild-Sicht_40220641.md)                  |
| \[Strg+Y\]                        | Wiederholen                                                                                |
| \[Strg+Z\]                        | Rückgängig                                                                                 |
| \[Umschalttaste + C\]             | Aktives Rechteck vertikal nach unten duplizieren                                           |
| \[Umschalttaste + U\]             | Aktives Rechteck vertikal nach oben duplizieren                                            |
| \[Umschalttaste + B\]             | Aktives Rechteck horizontal nach links duplizieren                                         |
| \[Umschalttaste + H\]             | [Horizontale Ausrichtung der aktiven Andocklinie](Funktionen-der-Bild-Sicht_40220641.md) |
| \[Umschalttaste + L\]             | [Alle Text-Ebenen einblenden](Funktionen-der-Bild-Sicht_40220641.md)                     |
| \[Umschalttaste + P\]             | [Rotationswinkel ein- oder ausblenden](Funktionen-der-Bild-Sicht_40220641.md)            |
| \[Umschalttaste + S\]             | [Schriftmodus ein- oder ausblenden](Funktionen-der-Bild-Sicht_40220641.md)               |
| \[Umschalttaste + R\]             | [Rotationsmodus (de)aktivieren](Funktionen-der-Bild-Sicht_40220641.md)                   |
| \[Umschalttaste + V\]             | [Vertikale Ausrichtung der aktiven Andocklinie](Funktionen-der-Bild-Sicht_40220641.md)   |
| \[Leertaste + Maus-Links\]        | Bild bewegen                                                                               |
| \[Tab\]                           | [Zwischen Markierungen wechseln](Funktionen-der-Bild-Sicht_40220641.md)                  |
