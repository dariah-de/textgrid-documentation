# Statusleiste

last modified on Aug 18, 2015

Die Statusleiste am unteren Ende des Bildschirms zeigt auf der linken
Seite immer das Symbol
![](attachments/40220253/40436657.gif "new_fastview.gif"), um eine
"Sicht als Schnellsicht an\[zu\]zeigen". Hiermit können Sie sich
sogenannte "Schnellsichten" aus einer Liste einrichten. In der rechten
unteren Ecke des Bildschirms wird die TextGrid-Benutzer-ID des gerade
eingeloggten Benutzers angezeigt. In dieser Statusleiste können weitere
Informationen angezeigt werden, wie der Titel und der URI ausgewählter
Projekte oder Objekte. Abhängig von den geöffneten Sichten können in der
Statusleiste auch andere Informationen angezeigt werden.

## Attachments

- [new_fastview.gif](attachments/40220253/40436657.gif) (image/gif)  
