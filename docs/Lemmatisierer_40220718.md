# Lemmatisierer

last modified on Aug 04, 2015

Der Lemmatisierer erlaubt Ihnen, sich das zu einer bestimmten deutschen
Wortform gehörende Lemma ausgeben zu lassen sowie die unflektierte
Wortform zu analysieren. Der Lemmatisierer basiert auf der deutschen
Computermorphologie SMOR

[http://www.ims.uni-stuttgart.de/projekte/gramotron/PAPERS/LREC04/smor.pdf](http://www.ims.uni-stuttgart.de/projekte/gramotron/PAPERS/LREC04/smor.pdf)

und auf der morphologischen Lexikonkomponente MORPHISTO:

[http://www1.ids-mannheim.de/lexik/TextGrid/morphisto/](http://www1.ids-mannheim.de/lexik/TextGrid/morphisto/)

- [Lemmatisierer installieren](Lemmatisierer-installieren_40220720.md)
- [Lemmatisierer öffnen](40220722.md)
- [Das Lemmatisierer-Werkzeug](Das-Lemmatisierer-Werkzeug_40220724.md)
- [Lemmatisierer verwenden](Lemmatisierer-verwenden_40220726.md)
- [Interaktion des Lemmatisierers mit anderen Komponenten](Interaktion-des-Lemmatisierers-mit-anderen-Komponenten_40220732.md)
