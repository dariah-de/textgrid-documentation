# Open the Aggregations Editor

last modified on Mai 03, 2016

Der Aggregationen-Editor kann durch Erstellen einer neuen Aggregation
oder durch Bearbeiten einer bereits existierenden Aggregation, Edition,
oder Kollektion geöffnet werden. Der Editor öffnet sich

- durch Auswahl von “Datei \> Neu” in der Menüleiste und Erstellen
  einer Aggregation
- durch Klicken von "![](attachments/40220442/40436720.png) \>
  Aggregation" in der Werkzeugleiste
- durch Rechtsklicken einer Aggregation, Edition oder Kollektion
    im [Navigator](https://wiki.de.dariah.eu/pages/viewpage.action?pageId=40220331) und
    Auswahl von “Bearbeiten”

Wenn “Datei \> Neu” in der Menüleiste oder das
Symbol ![](attachments/40220442/40436720.png) und danach „Aggregation“
gewählt wird, öffnet sich der Assistent “Neues TextGrid-Objekt“. Nach
Abschluss des Vorgangs öffnet sich der Aggregationen-Editor.

Sie können den Aggregationen-Editor auch in einer separaten Perspektive
öffnen:

- Klicken Sie "Aggregationen" auf dem Startbildschirm
- Wählen Sie “Werkzeuge \> Aggregationen” in der Menüleiste
- Klicken von ![](attachments/40220442/40436719.png) in der
    Werkzeugleiste

In diesen Fällen öffnet sich eine Perspektive mit dem Navigator, dem
leeren Metadaten-Editor und dem leeren Aggregationen-Editor. Um eine
neue Aggregation hinzuzufügen, wählen Sie “Datei \> Neu” in der
Menüleiste oder Klicken Sie  in der Werkzeugleiste. Um eine Aggregation
zu bearbeiten, rechtsklicken Sie diese im Navigator und wählen Sie
“Bearbeiten”.

## Attachments

- [plus-icon.png](attachments/7439447/7766181.png) (image/png)  
- [072-zeige-AggregationPerspektive.png](attachments/7439447/7766182.png)
(image/png)  
