# Features of the Palette

last modified on Mai 04, 2016

The
Palette ![](attachments/7439861/8192037.png "Bildschirmfoto 2012-01-12 um 12.20.33.png") includes
a range of icons divided into several groups:

- Control Group: With the “Select”
    tool **![](attachments/7439861/8192038.png "Bildschirmfoto 2012-01-30 um 11.22.30.png")** it
    is possible to go to any position in the note sheet.
- "Create
    Staves": ![](attachments/7439861/8192025.png "meise_staffgroup.png") 
    creates a new group of bracketed or braced right hand and left hand
    staves. ![](attachments/7439861/8192026.png "meise-old_staffdeff.png") creates
    a new staff definition, i.e. a container for staff meta-information
    which includes meta-information about the clef’s pitch, the key
    signature, the label (right hand or left hand), and the time
    signature.
- "Create Containers":
    “Section” **![](attachments/7439861/8192183.gif "icon-section.gif")** ,
    “Measure” ![](attachments/7439861/8192027.png "002-zeige_Notentakt2.png") ,
    “Staff” **![](attachments/7439861/8192184.gif "icon-staff.gif")** and
    “Layer” **![](attachments/7439861/8192185.gif "icon-layer.gif")** :
    this group creates new containers in the note sheet which are
    themselves able to contain other data types. All comprised data in a
    container results in a meaningful whole.

         ![](attachments/7439861/9240712.png)

- "Create Events":
    “Note” ![](attachments/7439861/8192030.png "003-zeige-Note.png") ,
    “Rest” ![](attachments/7439861/8192031.png "004-zeige-Notenpause_3.png") ,
    "MRest" **![](attachments/7439861/8192112.png "004-zeige-Notenpause.png")**,
    "Space" **![](attachments/7439861/8192186.gif "icon-space.gif")**,
    “Beam
    (group)” **![](attachments/7439861/8192110.png "111-zeige-Balkengruppe.png")** ,
    “Chord
    (group)” **![](attachments/7439861/8192111.png "110-zeige_Akkordgruppe.png")** :
    this group contains the minimum unit of elements which can be
    created within a musical score in the Score View.

        ![](attachments/7439861/9240713.png)

- "Create Variants":
    “Apparatus” **![](attachments/7439861/8192187.gif "icon-apparatus.gif")** /
    “Reading” **![](attachments/7439861/8192188.gif "icon-reading.gif")**: 
    this group enables the encoding of variants, a specialty of the
    editor for musicological use.

        ![](attachments/7439861/9240714.png)

- "Create Additions": "Create
    Slur" **![](attachments/7439861/8192189.gif "icon-slur.gif")** "Create
    Tie" **![](attachments/7439861/8192189.gif "icon-slur.gif")** "Dynam" **![](attachments/7439861/8192191.gif "icon-dynam.gif")** :
    with this element group, useful music notation symbols can be added
    to the score.**  
      
    ![](attachments/7439861/9240715.png)  
    **

The pin function on each of the units ("Pin open", "Unpin") allows you
to maintain the Palette state even after restarting the TextGrid Lab.

For more details about the application of the Palette, please see the
chapter [Using the MEI Score
Editor](Using-the-MEI-Score-Editor_7439898.md). 

## Attachments

- [meise-f1_select.png](attachments/7439861/8192024.png) (image/png)  
- [meise_staffgroup.png](attachments/7439861/8192025.png) (image/png)  
- [meise-old_staffdeff.png](attachments/7439861/8192026.png) (image/png)  
- [002-zeige_Notentakt2.png](attachments/7439861/8192027.png)
(image/png)  
- [meise-create_containers.png](attachments/7439861/8192028.png)
(image/png)  
- [meise-create_events.png](attachments/7439861/8192029.png) (image/png)  
- [003-zeige-Note.png](attachments/7439861/8192030.png) (image/png)  
- [004-zeige-Notenpause_3.png](attachments/7439861/8192031.png)
(image/png)  
- [Bildschirmfoto 2012-01-12 um 12.25.14.png](attachments/7439861/8192036.png) (image/png)  
- [Bildschirmfoto 2012-01-12 um 12.20.33.png](attachments/7439861/8192037.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.22.30.png](attachments/7439861/8192038.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.22.56.png](attachments/7439861/8192039.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.23.25.png](attachments/7439861/8192040.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.23.37.png](attachments/7439861/8192041.png) (image/png)  
- [meise-f1_create variants.png](attachments/7439861/8192043.png) (image/png)  
- [meise-f1_create variants.png](attachments/7439861/8192042.png) (image/png)  
- [section.png](attachments/7439861/8192106.png) (image/png)  
- [111-zeige-Balkengruppe.png](attachments/7439861/8192110.png)
(image/png)  
- [110-zeige_Akkordgruppe.png](attachments/7439861/8192111.png)
(image/png)  
- [004-zeige-Notenpause.png](attachments/7439861/8192112.png)
(image/png)  
- [icon-section.gif](attachments/7439861/8192183.gif) (image/gif)  
- [icon-staff.gif](attachments/7439861/8192184.gif) (image/gif)  
- [icon-layer.gif](attachments/7439861/8192185.gif) (image/gif)  
- [icon-space.gif](attachments/7439861/8192186.gif) (image/gif)  
- [icon-apparatus.gif](attachments/7439861/8192187.gif) (image/gif)  
- [icon-reading.gif](attachments/7439861/8192188.gif) (image/gif)  
- [icon-slur.gif](attachments/7439861/8192190.gif) (image/gif)  
- [icon-slur.gif](attachments/7439861/8192189.gif) (image/gif)  
- [icon-dynam.gif](attachments/7439861/8192191.gif) (image/gif)  
- [createAdditions.PNG](attachments/7439861/8192192.png) (image/png)  
- [mei-palette-containers.png](attachments/7439861/9240712.png)
(image/png)  
- [mei-palette-events.png](attachments/7439861/9240713.png) (image/png)  
- [mei-palette-variants.png](attachments/7439861/9240714.png)
(image/png)  
- [mei-palette-additions.png](attachments/7439861/9240715.png)
(image/png)  
