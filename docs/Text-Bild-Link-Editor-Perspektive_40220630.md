# Text-Bild-Link-Editor-Perspektive

last modified on Aug 20, 2015

Der Editor umfasst den [Navigator](40220331.md) auf der linken Seite,
dessen Kontextmenü Ihnen erlaubt, Bilder oder Texte zu öffnen – abhängig
von Ihren Zugriffsrechten. Auf der rechten Seite sind zwei Sichten, eine
oberhalb der anderen, die leer sind, wenn kein Bild oder Text gewählt
wurde. Sobald ein Bild geöffnet wird, kann es in der
[Bild-Sicht](Bild-Sicht_40220638.md) gesehen werden, welche die obere
Sicht auf der rechten Seite ist. Ihnen wird auch automatisch die
[Bildvorschau-Sicht](Bildvorschau-Sicht_40220655.md) unterhalb des
Navigators und der losgelöste
[Werkzeugkasten](Werkzeugkasten_40220657.md) geöffnet. Sobald ein
XML-Dokument geöffnet wird, wird es im
[XML-Editor](XML-Editor_40220521.md) unterhalb der Bild-Sicht
angezeigt. Insgesamt besteht die Perspektive zusätzlich zum generischen
Navigator aus einer Bild-Sicht zur Betrachtung des Faksimiles und seinen
Markierungen, der XML-Editor-Sicht zur Betrachtung seiner Transkription,
einer Bildvorschau-Sicht und einem Werkzeugkasten:

- Die Bild-Sicht zeigt das Bild oder einen zu verknüpfenden
    Bildausschnitt an und erlaubt das Markieren von Bildsegmenten
- Die Bildvorschau-Sicht zeigt eine verkleinerte Version des gesamten
    Bildes und den aktiven Bildausschnitt, der einfach verschoben und
    fokussiert werden kann und der in der Bild-Sicht vergrößert
    dargestellt wird
- Die XML-Editor-Sicht erlaubt Ihnen, Texte zu öffnen oder zu
    erstellen sowie Textabschnitte zur markieren und mit dem Bild zu
    verknüpfen
- Der Werkzeugkasten stellt Ihnen Funktionen zur Arbeit in der
    Bild-Sicht zur Verfügung

![](attachments/40220630/40437234.png)

![](plugins/servlet/confluence/placeholder/unknown-attachment "tble-perspective.png")

|                                       |
|---------------------------------------|
| **Text-Bild-Link-Editor-Perspektive** |

- [Menüleiste der Text-Bild-Link-Editor-Perspektive](40220632.md)
- [Werkzeugleiste der Text-Bild-Link-Editor-Perspektive](Werkzeugleiste-der-Text-Bild-Link-Editor-Perspektive_40220635.md)

## Attachments

- [image2015-8-3 9:35:49.png](attachments/40220630/40437234.png) (image/png)  
