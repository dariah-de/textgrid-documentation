# Context Menu of the Navigator View

last modified on Mai 03, 2016

Kontextmenüs öffnen sich, wenn Sie ein Projekt oder ein Objekt im
Objektbaum rechtsklicken. Diese ermöglichen Ihnen, Objekte zu verwalten
oder neue zu erstellen. Das Kontextmenü unterscheidet sich für Projekte
und Objekte und variiert abhängig vom gewählten Objekt.

Wenn ein Projektordner vorausgewählt ist, können Sie ihn deaktivieren,
reaktivieren oder löschen, wenn Sie die dafür notwendigen Rechte
besitzen. Das Projekt wird aus der Projektliste verschwinden, bis es
reaktiviert wird. Seine veröffentlichten Ressourcen werden weiterhin
lesbar bleiben. Sie können ein Projekt auch durch Auswahl der
entsprechenden Unterpunkte im Menü “Datei” in der Menüleiste
deaktivieren, reaktivieren oder löschen. Die [Projekt- und
Benutzerverwaltung](Projekt--und-Benutzerverwaltung_40220359.md) kann
direkt von hier durch Rechtsklicken eines Projekts geöffnet werden.

Um ein Objekt zu öffnen, doppelklicken Sie es oder rechtsklicken Sie es
und wählen Sie “Öffnen”. Abhängig von seinem Dokumenttyp wird es in
seinem Standardeditor geöffnet. Wählen Sie “Öffnen mit”, um einen
anderen Editor auszuwählen. Um ein Objekt zu löschen, rechtsklicken Sie
es und wählen Sie “Löschen”. Sie müssen dafür die notwendigen Rechte
besitzen. Alle Objekte können als Dateien von hier exportiert werden.
Wählen Sie “Revisionen anzeigen”, um die Revisionen der Datei anzusehen.
Sie können außerdem die dazugehörigen Metadaten öffnen oder neu laden
oder die technischen Metadaten ansehen. Für weitere Informationen
siehe [Benutzerrechteverwaltung](Projekt--und-Benutzerverwaltung_40220359.md), [Revisionen](Revisionen--und-Sperre-Mechanismus_40220412.md) und [Anpassung
der Metadaten](Metadaten-Editor_40220458.md).

Wenn
eine [Edition](Editionen_40220434.md), [Kollektion](Kollektionen_40220436.md) oder [Aggregation](Aggregationen_40220430.md) gewählt
ist, bietet das Kontextmenü den Menüpunkt “Bearbeiten” an.
Der [Aggregationen-Editor](Aggregationen-Editor_40220440.md) öffnet
sich, wenn sie hier klicken. Im Falle von Editionen und Kollektionen ist
die [Publikation](https://wiki.de.dariah.eu/display/tgarchiv1/Publikationen) über
das Kontextmenü ebenfalls möglich.

Das Kontextmenü des Navigators erlaubt Ihnen, [ein XML Dokument mit
einem Adaptor zu assoziieren](Funktionen-des-XML-Editors_40220527.md).
Bilder und XML-Dokumente können außerdem
zum [Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md) hinzugefügt
werden.

Um ein kopiertes Objekt in ein anderes Projekt einzufügen, können Sie
ein Objekt rechtsklicken und “Kopieren” wählen und dann ein Projekt
rechtsklicken und “Einfügen” wählen. Sie können außerdem den URI
(Uniform Resource Identifier) eines Projekts oder Objekts in die
Zwischenablage kopieren, damit diese auch in anderen Anwendungen als dem
TextGridLab genutzt werden können. Wenn Sie “CRUD-Warnungen anzeigen”
wählen, können Sie sehen, ob es Probleme mit der Datenbank gibt.
Wenn [SADE](Publikationswerkzeug-SADE_40220503.md) installiert ist,
kann ein Objekt über das Kontextmenü des Navigators mit SADE
veröffentlicht werden.
