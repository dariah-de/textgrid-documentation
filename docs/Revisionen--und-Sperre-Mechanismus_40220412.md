# Revisionen- und Sperre-Mechanismus

last modified on Jul 28, 2015

Revisionen sind ein Mittel, um ein Dokument in einem bestimmten Stadium
abzuspeichern, beispielsweise als Zwischenergebnis einer digitalen
Edition. Neue Revisionen werden ausdrücklich vom Nutzer erstellt und
alle Revisionen eines bestimmten Basis-Objekts liegen im selben Projekt.
TextGrid verwendet einen Mechanismus, um ein Objekt zu sperren, wenn ein
Nutzer damit arbeitet. Ein zweiter Nutzer erhält in diesem Fall eine
Nachricht, wenn er dasselbe Objekt öffnet und der erste Nutzer noch
damit arbeitet. Er hat dann nur Leserechte und kann das Dokument nicht
ändern.

- [Revisionen verwenden](Revisionen-verwenden_40220414.md)
- [Interaktion von Revisionen mit anderen Komponenten](Interaktion-von-Revisionen-mit-anderen-Komponenten_40220417.md)
