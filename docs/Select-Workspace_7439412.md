# Select Workspace

last modified on Mai 03, 2016

Der angezeigte Bildausschnitt kann mit dem Auswahlrechteck in der
Bildvorschau-Sicht und den Rollbalken ausgewählt werden. Der Zoomfaktor
kann mit den Schiebereglern in der Bildvorschau-Sicht oder den
Vergrößerungswerkzeugen geändert werden.
