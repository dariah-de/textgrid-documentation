# Correct and Delete Links between Text and Image

last modified on Mai 03, 2016

Verknüpfungen können jederzeit korrigiert oder gelöscht werden.

- In der Bild-Sicht werden verknüpfte Markierungen mit einer
    durchgezogenen Umrandung dargestellt.
- Eine Verknüpfung kann durch Doppelklicken der Markierung im Bild
    oder des Ankers im Text ausgewählt werden.
- Sie können Markierungen in der Bild-Sicht auch korrigieren. Wenn Sie
    den Mauszeiger auf eine aktive Markierung ziehen, ändert sich sein
    Symbol: In der Mitte wird er zu einem Vierwegepfeil, der Ihnen
    erlaubt, die Position der Markierung durch Klicken und ziehen zu
    ändern. Im Umfeld der Umrandung können Sie die Kanten (mit einem
    Zweiwegepfeil) oder Ecken (mit einem diagonalen Zweiwegepfeil) auf
    ähnliche Weise verschieben. Änderungen an Polygonen können in
    ähnlicher Form durchgeführt werden mit der Ausnahme, dass einzelne
    Kanten von Polygonen nicht verschoben oder angepasst werden können.
- Verknüpfte Objekte (d. h. Markierungen in Text und Bild und ihre
    Verknüpfung) können über “Verknüpfung für ausgewählte Markierung(en)
    aufheben” im Kontextmenü oder durch Klicken
    von ![](attachments/40220669/40436857.png "unlink_obj.png") in der
    Werkzeugleiste gelöscht werden.

## Attachments

- [unlink_obj.png](attachments/7439418/7766177.png) (image/png)  
