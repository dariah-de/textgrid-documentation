# Suche-Sicht

last modified on Aug  18, 2015

Die Suche-Sicht verfügt über eine Option für den Ort der Suche und die
Auswahl zwischen einer Einfachen oder Erweiterten Suche.

Die Option für den Ort der Suche legt fest, wo die Suche durchgeführt
werden soll. Die beiden Möglichkeiten sind:

- “meine Projekte”: diese Suche wird ausschließlich in jenen Projekten
    durchgeführt, für die Sie eine Rolle zugewiesen bekommen haben, die
    Ihnen den Zugriff auf die Inhalte des Projekts erlaubt. Sie können
    auf diese Projekte nur zugreifen, nachdem Sie sich authentifiziert
    und im TextGridLab angemeldet haben.
- “öffentliche Daten im TextGridRep”: diese Suche wird im öffentlich
    zugänglichen Teil des TextGridRep durchgeführt, der alle
    veröffentlichten Objekte zuzüglich externer Datenressourcen
    beinhaltet. Das TextGridRep kann ohne vorherige Authentifizierung
    durchsucht werden.

Um eine Suche durchzuführen, klicken Sie die Schaltfläche “Suche
starten” in der Suche-Sicht oder klicken Sie das Lupen-Symbol in der
Titelleiste der Sicht. Wenn Sie “Hinweise zur Suche” klicken, erhalten
Sie eine kurze Einführung in der Verwendung der Suche-Sicht. Sowohl in
der Einfachen als auch in der Erweiterten Suche können Platzhalter nach
dem ersten Buchstaben (auch innerhalb eines) eines Suchbegriffs
verwendet werden. Das Asterisk-Zeichen (\*) steht dabei für eine
beliebige Zeichenkette und das Fragezeichen (?) steht für genau ein
Zeichen.

- [Einfache Suche](Einfache-Suche_40220294.md)
- [Erweiterte Suche](Erweiterte-Suche_40220296.md)
