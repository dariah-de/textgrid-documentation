# Interaction of Digilib with Other Components

last modified  on Mai 04, 2016

Digilib kann verwendet werden, um Bilder zu bearbeiten und zu speichern,
bevor sie
im [Text-Bild-Link-Editor](Text-Bild-Link-Editor_40220625.md) mit
XML-Dokumenten verknüpft werden.
