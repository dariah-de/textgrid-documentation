# Lokale Dateien importieren

last modified on Jul 28, 2015

Führen Sie diese Schritte durch, um Dateien von ihrem lokalen
Betriebssystem oder einem Speichermedium in das TextGridLab zu
importieren:

1. Wählen Sie im Aufklappmenü am unteren Rand der Sicht ein Zielprojekt
    aus, in das die Dateien importiert werden sollen.
2. Ziehen Sie Dateien oder Ordner aus der Dateiverwaltung Ihres
    Betriebssystems und legen Sie diese in die Liste des
    Import-Werkzeugs. Alternativ können Sie oben in der Import-Sicht
    “Hinzufügen” klicken, um Dateien in die Liste darunter einzufügen.
    Klicken Sie die Schaltfläche “Entfernen”, um eine Datei aus der
    Liste zu entfernen.
3. Das Import-Werkzeug schlägt einen Inhaltstyp, einen Objekttitel und
    eine Umschreiben-Methode vor. Sie können diese Angaben durch Klicken
    des entsprechenden Eintrags ändern oder unter Verwendung des
    [*Metadaten-Editors*](https://doc.textgrid.de/search.html?q=Metadata+Editor)
    durch Auswahl des entsprechenden Eintrags in der Liste zusätzliche
    Metadaten angeben.
4. Klicken Sie “Import!”, um den Vorgang abzuschließen. Nachdem der
    Import-Vorgang abgeschlossen ist, wird eine Liste mit allen Dateien,
    die importiert wurden, gemeinsam mit einer Bestätigung oder Warnung
    bzw. Fehlermeldung, wenn der Import-Vorgang nicht erfolgreich war,
    angezeigt.
5. Sie können optional die Importspezifikationen, d. h. die Liste der
    ursprünglichen Dateinamen aller importierten Dateien und der damit
    verbundenen TextGrid-URIs sowie dem Umschreiben-Modus) speichern, um
    den späterem Re-Export desselben Sets von Objekten zu vereinfachen.
    Die Importspezifikationen können als TextGrid-Objekt oder als lokale
    Datei gespeichert werden. Klicken Sie dafür die Schaltflächen unten
    in der Sicht.
