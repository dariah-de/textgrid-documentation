# Element-spezifische Metadaten

last modified on Jul 28, 2015

In der Metadaten-Sicht von Elementen (und Aggregationen) gibt es die
folgenden spezifischen Eingabefelder:

1. Title(s) (Objekt-)Titel
2. Identifier(s) Identifikatoren
3. Rights Holder(s) Rechteinhaber
4. Notes Notizen
5. Part of Edition(s) Teil von Edition(en)

Im Falle von Elementen können die Namen von Rechteinhabern (wie
beispielsweise Autoren, Mitwirkende, Editoren) angefügt werden. Im Feld
"Rights Holder(s)" können diese mit einem Namen und einem URI
identifiziert werden. Weitere Rechteinhaber können durch Klicken der
entsprechenden Schaltflächen hinzugefügt oder entfernt werden. Setzen
Sie das Häkchen im Ankreuzfeld, wenn es sich dabei um eine juristische
Person handelt.

Das Eingabefeld mit dem Namen "Part of Edition(s)" zeigt die Relation
des Elements zu einem Edition-Objekt. Das Feld kann nicht direkt vom
Benutzer ausgefüllt werden. Der Standardeintrag ist "no related
Editionen found" (keine zugehörige Editionen gefunden) solange keine
Verknüpfung zu einem Edition-Objekt erstellt wurde. Der Eintrag ändert
sich, sobald eine Relation über den
[Aggregationen-Editor](https://doc.textgrid.de/search.html?q=Using+the+Aggregations+Editor)
festgelegt wurde
