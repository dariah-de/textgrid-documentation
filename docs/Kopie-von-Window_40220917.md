# Kopie von Window

Created on Jul 24, 2015

**![](attachments/40220917/40436968.png "086-oeffne-ImNeuenEditor.png") New
Editor**: creates a second Score View which opens in a second tab behind
the current editor field.

**![](attachments/40220917/40436967.png "087-zeige_NavigationGruppe.png")
Navigation**: allows you to activate and switch between editors.
Activated editors can be minimized and maximized. You can choose the
previous perspective, editor, or view.

![](attachments/40220917/40436966.png "101-zeige-Grundeinstellungen3.png") **Preferences**:
Here you can also change preferences for MEISE under "Meise
Preferences", e.g. to require the Lab to use the MEISE perspective when
opening a MEI document.

## Attachments

- [101-zeige-Grundeinstellungen3.png](attachments/40220917/40436966.png)
(image/png)  
- [087-zeige_NavigationGruppe.png](attachments/40220917/40436967.png)
(image/png)  
- [086-oeffne-ImNeuenEditor.png](attachments/40220917/40436968.png)
(image/png)  
