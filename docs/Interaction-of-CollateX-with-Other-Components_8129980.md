# Interaction of CollateX with Other Components

last modified on Mai 04, 2016

Der CollateX-Kollationierer interagiert mit
dem [Navigator](40220331.md) und
der [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md). Objekte
können von dort gezogen und im Kollationierungsset abgelegt werden. Das
XML-Ergebnis kann mit [oXygen](40220583.md) oder
dem [XML-Editor](XML-Editor_40220521.md). kopiert und bearbeitet
werden.
