# Colophon

last modified on Feb 28, 2018

Das Verbund-Forschungsprojekt TextGrid wird finanziert durch das
deutsche Bundesministerium für Bildung und Forschung, BMBF.

Das Handbuch wurde erstellt von Mayce Al Azawi, Yahya Al Hajj, Arno
Bosse, Robert Casties, Julian Dabbert, Martin Haase, Celia Krause,
Michael Leuk, Oliver Schmid, Kathleen Smith, Sibylle Söring, Frank
Queens, Sabina Ulsamer, Philipp Vanscheidt und Thorsten Vitt. Die
deutsche Übersetzung erfolgte durch Oliver Schmid mit Unterstützung von
Philipp Vanscheidt.

Wenn Sie Fragen haben, können Sie uns kontaktieren unter:

- Technischer Support: [textgrid-tech@gwdg.de](mailto:textgrid-tech@gwdg.de)
- Nutzer-Support und -Feedback: [textgrid-support@gwdg.de](mailto:textgrid-support@gwdg.de)
- IRC
    Channel: [http://webchat.freenode.net/?channels=textgrid](http://webchat.freenode.net/?channels=textgrid)

Fehlermeldungen können erstellt und beobachtet werden über die
Oberfläche [https://projects.gwdg.de/projects/tg](https://projects.gwdg.de/projects/tg).

Die aktuelle Versionen des Entwickler- und des Benutzer-Handbuchs sind
auch als Wiki verfügbar unter

``` syntaxhighlighter-pre
https://doc.textgrid.de/search.html?q=Main+Page
```
