# Interaktion der Unicode-Zeichen-Tabelle-Sicht mit anderen Komponenten

last modified on Aug 20, 2015

Die Unicode-Zeichen-Tabelle-Sicht wird automatisch geöffnet, wenn Sie
den [XML-Editor](XML-Editor_40220521.md) geöffnet haben. Auf diese
Weise können Zeichen in einen XML-Text eingefügt werden, ohne eine
weitere Sicht zu öffnen.
