# Kopie von Outline View of the MEI Score Editor

Created on Jul 24, 2015

The Outline View in MEISE has three different functions. First, it
facilitates navigation in large MEI documents by providing an overview
of the document’s tree-structure. Second, as in the [Properties View ](Properties-View-of-the-MEI-Score-Editor_7439882.md) it is
essential for editing a document because you can build your hierarchical
MEI tree using drag and drop in this area, and the graphical results are
shown in the Musical Score View. Finally, it offers a quick switch
between the sources in the MEI document (if they exist). More detailed
information is provided in the section on [Using the MEI Score
Editor](Using-the-MEI-Score-Editor_7439898.md) . Every time you select
an element (Measure, Note, Rest, Beam, Tie, Slur) in the Outline View
via left-clicking, the corresponding element in the [Score View](Musical-Score-View_7439823.md) will be highlighted in blue so
that the user always knows the current location within the document.

![](attachments/40220931/40437025.png)

|                    |
|--------------------|
| MEISE Outline View |

- [Kopie von Context Menu of the MEISE Outline View](Kopie-von-Context-Menu-of-the-MEISE-Outline-View_40220933.md)
- [Kopie von Title Bar of the MEISE Outline View](Kopie-von-Title-Bar-of-the-MEISE-Outline-View_40220935.md)

## Attachments

- [mei-outlineview.png](attachments/40220931/40437025.png) (image/png)  
- [outline.PNG](attachments/40220931/40437026.png) (image/png)  
- [meise-f4_outline view.png](attachments/40220931/40437027.png) (image/png)  
