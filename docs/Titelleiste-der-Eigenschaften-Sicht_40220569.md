# Titelleiste der Eigenschaften-Sicht

last modified on Jul 31, 2015

Die Titelleiste der Eigenschaften-Sicht bietet Ihnen verschiedene
Symbole, um eine Reihe von Operationen durchzuführen:

- Klicken Sie ![](attachments/40220569/40436811.png "pin.png") diese
    Eigenschaften-Sicht an die aktuelle Auswahl anzuheften.
- Klicken Sie ![](attachments/40220569/40436810.png "tree_mode.png")
    um Kategorien wie “Attribute” in der Sicht anzuzeigen.
- Klicken Sie ![](attachments/40220569/40436809.png "filter.png") um
    Fortgeschrittene Eigenschaften anzuzeigen.
- Klicken Sie ![](attachments/40220569/40436808.png "restore.png") um
    den Standardwert wiederherzustellen.
- Klicken Sie ![](attachments/40220569/40436807.png "delete.png") um
    die gewählte Eigenschaft zu entfernen.
- Klicken Sie das kleine weiße Dreieck, um das Sichtmenü zu öffnen.
    Mit dem Sichtmenü können Sie
  - eine neue Eigenschaften-Sicht öffnen
  - eine Eigenschaft an eine Auswahl anheften
  - Kategorien und Erweiterte Eigenschaften anzeigen
  - eine ausgewählte Eigenschaft entfernen
  - durch Auswahl von “Spalteneinstellungen...” die Breite der
        beiden Spalten “Eigenschaft” und “Wert” festlegen
- Klicken Sie die beiden weiteren Symbole zum Minimieren und
    Maximieren der Sicht.

## Attachments

- [delete.png](attachments/40220569/40436807.png) (image/png)  
- [restore.png](attachments/40220569/40436808.png) (image/png)  
- [filter.png](attachments/40220569/40436809.png) (image/png)  
- [tree_mode.png](attachments/40220569/40436810.png) (image/png)  
- [pin.png](attachments/40220569/40436811.png) (image/png)  
