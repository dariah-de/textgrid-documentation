# Inbetriebnahme

last modified on Aug 18, 2015

Bevor Sie mit der Arbeit mit TextGrid beginnen können, müssen Sie die
Software herunterladen, sich für eine TextGrid Nutzerkennung
registrieren und sich einloggen.

- [Das Leitbild von TextGrid](Das-Leitbild-von-TextGrid_40220200.md)
- [Warum Sie TextGrid nutzen sollten](Warum-Sie-TextGrid-nutzen-sollten_40220202.md)
- [Download (de)](40220205.md)
- [Marketplace (de)](40220207.md)
- [Software installieren und entfernen](Software-installieren-und-entfernen_40220213.md)
- [Updates](Updates_40220215.md)
- [Nutzerkennung und Registrierung](Nutzerkennung-und-Registrierung_40220217.md)
- [Startbildschirm und Login](Startbildschirm-und-Login_40220219.md)
- [Sprachauswahl](Sprachauswahl_40220222.md)
- [Fehlerbehebung](Fehlerbehebung_40220224.md)
