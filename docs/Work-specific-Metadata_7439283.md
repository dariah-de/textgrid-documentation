# Work-specific Metadata

last modified on Jun 10, 2020

Werke sind Objekte, die in erster Linie über ihre Metadaten definiert
sind. Im Gegensatz
zu [Editionen](https://doc.textgrid.de/search.html?q=Editions) und [Kollektionen](https://doc.textgrid.de/search.html?q=Collections),
beinhalten Sie keine anderen Objekte. Das Metadatenformular für Werke
enthält die folgenden Elemente:

1. Title(s) (Objekt-)Titel
2. Identifier(s) Identifikatoren
3. Agent(s) Agent(en)
4. Abstract Zusammenfassung
5. Date of Creation Erstellungsdatum
6. Spatial(s) Ortsangabe(n)
7. Temporal(s) Zeitangabe(n)
8. Subject(s) Thema/en
9. Genre(s) Genre(s)
10. Type(s) Typen
11. Notes Notizen

Das Element "Agent" für Werke ist für die Veröffentlichung ein
Pflichtfeld und wiederholbar. Es besteht aus einem Rollenattribut, für
das im Aufklappmenü ein Dublin Core (DC) Relator Term ausgewählt werden
kann. Im Attribut “id” kann ein identifizierender URI wie PND oder FOAF
hinzugefügt werden. Der Name des Agenten kann für dieses Element als
Wert angegeben werden. Das Element "Abstract" ist optional und
wiederholbar. Es kann für jede übergreifende Beschreibung des Werks
verwendet werden.

Einem "Agent" kann eine Rolle zugewiesen werden. Diese muss aus der
geschlossenen Liste nach
[MARC21](https://www.loc.gov/marc/relators/relaterm.md) ausgewählt
werden.

Das Element "Date of Creation" ist ein Pflichtfeld. Es muss in einer
Form eingegeben werden, die TextGrid unterstützt. Räumliche, zeitliche
und fachliche Schlüsselwörter können hinzugefügt werden. Ihre
Identifikatoren sollten mit einem kontrollierten Vokabular verknüpft
sein. Jede der drei Kategorien “Spatial”, “Temporal” und “Subject”
umfasst ein Feld “Value”. Mit diesem kann der dargestellte Wert
beeinflusst werden.

Das Element "Genre" ist ein Pflichtfeld und wiederholbar. Es wird für
die grundlegende Klassifikation verwendet. Für eine genauere
Klassifikation über "Genre" hinaus kann das Element "Type" verwendet
werden. Es ist optional und wiederholbar.
