# Aggregations Editor

last modified on Mai 03, 2016

Mit dem Aggregationen-Editor haben Sie die Möglichkeit, Objekte
auszuwählen und durch Ziehen und Ablegen zusammenzufassen, anstatt nach
einzelnen Objekten zu suchen, die über mehrere Projekte verteilt sind.
Objekte, die für ein bestimmtes Thema oder Forschungsinteresse relevant
sind, können aggregiert werden. Sie werden im Navigator als Aggregation
angezeigt, die bearbeitet, erweitert und aufgelöst werden kann, ohne
die [Objekte](https://doc.textgrid.de/search.html?q=TextGrid+Objects) darin
zu löschen.

- [Open the Aggregations Editor](Open-the-Aggregations-Editor_7439447.md)
- [Aggregations Editor View](Aggregations-Editor-View_7439449.md)
- [Using the Aggregations Editor](Using-the-Aggregations-Editor_7439455.md)
- [Interaction of the Aggregations Editor with Other Components](Interaction-of-the-Aggregations-Editor-with-Other-Components_7439457.md)
