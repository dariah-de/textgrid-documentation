# Kopie von Edit

last modified on Jul 24, 2015

![](attachments/40220915/40436965.png "037-zuruecknehme-Aenderung.png") Undo
and ![](attachments/40220915/40436964.png "038-wiederhole-Aenderung.png")
Redo**: To undo or redo a change made to a document, click “Edit \>
Undo” or “Edit \> Redo“.

** ![](attachments/40220915/40436963.png "042-loesche-Auswahl.png")
Delete**: Use “Delete” to delete all selected elements from the
document.

**![](attachments/40220915/40436962.png "043-auswaehle-BereichGesamt3.png") Select
All**: This item works like Crtl+A and all elements in the Outline View
will be selected or highlighted. This function is enabled only when no
changes have been made.

## Attachments

- [043-auswaehle-BereichGesamt3.png](attachments/40220915/40436962.png)
(image/png)  
- [042-loesche-Auswahl.png](attachments/40220915/40436963.png)
(image/png)  
- [038-wiederhole-Aenderung.png](attachments/40220915/40436964.png)
(image/png)  
- [037-zuruecknehme-Aenderung.png](attachments/40220915/40436965.png)
(image/png)  
