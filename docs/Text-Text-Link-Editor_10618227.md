# Text Text Link Editor

last modified on Feb 26, 2019

Der Text-Text-Link-Editor steht im Marketplace nicht mehr zum Download
zur Verfügung.

Der Text-Text-Link-Editor dient der Verknüpfung von Fragmenten
verschiedener XML-Dokumente und speichert die Ergebnisse als
TEI-konforme Objekte. Der Editor validiert zudem bestehende
Verknüpfungen zwischen XML-Dokumenten.

- [Install the Text Text Link Editor](Install-the-Text-Text-Link-Editor_10618235.md)
- [Open the Text Text Link Editor](Open-the-Text-Text-Link-Editor_10618241.md)
- [The Text Text Link Editor Perspective](The-Text-Text-Link-Editor-Perspective_10618246.md)
- [The XML Document Field](The-XML-Document-Field_10618300.md)
- [The Text Text Link Field](The-Text-Text-Link-Field_10618308.md)
- [The Annotation Field](The-Annotation-Field_10618312.md)
- [Using the Text Text Link Editor](Using-the-Text-Text-Link-Editor_10618314.md)
