# TextGrid-Objekte

last modified on Aug 19, 2015

TextGrid verwaltet Objekte und die Arten von Relationen zwischen ihnen.
Diese Relationen drücken sich in Aggregationen, Editionen und
Kollektionen aus, die selbst ebenfalls TextGrid-Objekte sind. Um
Relationen zwischen Objekten zu erstellen, muss jedes Objekt
identifizierbar sein. Um eine weitere Identifikation der Objekte und
eine projektübergreifende [Suche](Suche_40220282.md) zu ermöglichen,
müssen die Objekte durch
[Metadaten](https://wiki.de.dariah.eu/display/tgarchiv1/Metadaten)
beschrieben werden. Für einen Überblick über [alle Metadaten-Kategorien](https://doc.textgrid.de/search.html?q=Metadata+Categories),
siehe die entsprechende Seite.

- [Objekte](Objekte_40220426.md)
- [Elemente](Elemente_40220428.md)
- [Aggregationen](Aggregationen_40220430.md)
- [Werke](Werke_40220432.md)
- [Editionen](Editionen_40220434.md)
- [Kollektionen](Kollektionen_40220436.md)
- [Referenzen zwischen Objekten](Referenzen-zwischen-Objekten_40220438.md)
