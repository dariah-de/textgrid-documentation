# Metadata Categories

last modified on Jun 01, 2016

## Generated Metadata

| **Kategorie**       | **Beschreibung**                                                                                                                           | **Beispiel**                       | **Pflichtfeld (Ja/Nein)** |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|---------------------------|
| **created**         | Erstellungsdatum der Ressource                                                                                                             | 2012-11-09T09:52:27.622+01:00      |                           |
| **lastModified**    | Datum der letzten Änderung des Objektes                                                                                                    | 2012-11-09T09:52:27.622+01:00      |                           |
| **issued**          | Datum der formellen Auslieferung (z.B. durch Publikation)                                                                                  | 2012-11-09T09:52:27.622+01:00      |                           |
| **textgridUri**     | Eindeutiger Referenz zu einem Objektes innerhalb des TextGrid-KontextesTODO: externe Referenz                                              | textgrid:1br5r.0                   |                           |
| **revision**        | Nummer zur Referenz auf eine bestimmte Revision                                                                                            | 0                                  |                           |
| **pid**             | Persistent Identifier im Kontext der Langzeitarchivierung, diese PID kann verschiedene Ausprägungen haben                                  | hdl:11858/00-1734-0000-0005-1424-B |                           |
| **tent**            | Die Größe der Ressource in Bytes                                                                                                           |                                    |                           |
| **fixity**          | Information zur Dokumentation, ob ein Objekt unautorisiert geändert wurde oder ob diese Änderungen nicht dokumentiert wurden               |                                    |                           |
| **dataContributor** | Information zur Zuordnung einer Person, die die entsprechenden Daten in TextGrid eingegeben hat                                            | pempe@textgrid.de                  |                           |
| **project**         | Identifizierung des Projektes aus dem die Ressource stammt. Zur eindeutigen Identifizierung des Objektes wird zusätzlich eine ID angegeben | Digitale Bibliothek                |                           |
| **warning**         | Fehlerreport der durch die Middleware bereitgestellt wird, um ein inkorrektes TextGrid-Objekt gespeichert wurde                            |                                    |                           |
| **permissions**     | Darstellung der Zugriffsrechte                                                                                                             | Read                               |                           |
| **availability**    | Information darüber, ob eine Ressource frei verfügbar ist oder durch das Rechtemanagement behandelt werden muss                            | public                             |                           |

## Bibliographic Citation Type

| **Kategorie**          | **Beschreibung**                                                                                                | **Beispiel** | **Pflichtfeld (Ja/Nein)** |
|------------------------|-----------------------------------------------------------------------------------------------------------------|--------------|---------------------------|
| **author**             | Autor des Originaltextes                                                                                        | Franz Kafka  |                           |
| **editor**             | Eine Person oder Organisation, die Texte für die Publikation bearbeitet, dieser Text muss nicht der eigene sein |              |                           |
| **editionTitle**       | Name des Quelltextes (zum Beispiel Buch- oder Journaltitel)                                                     |              |                           |
| **placeOfPublication** | Ort, an der der Quelltext veröffentlicht wurde                                                                  |              |                           |
| **publisher**          | Einer Person oder Organisation, die die Quelle verfügbar gemacht hat                                            |              |                           |
| **dateOfPublication**  | TG date type                                                                                                    |              |                           |
| **editionNo**          | Nummer der Edition (muss nicht zwingend numerisch sein)                                                         |              |                           |
| **series**             | Titel einer Serie, in der die Quelle veröffentlicht wurde                                                       |              |                           |
| **volume**             | Benennung des Volumes                                                                                           |              |                           |
| **issue**              | Benennung des Journals in dem der Text veröffentlicht wurde                                                     |              |                           |
| **spage**              | Benennt den Start des Textes in einem Volume                                                                    |              |                           |
| **epage**              | Benennt das Ende des Textes in einem Volume                                                                     |              |                           |
| **bibIdentifier**      |                                                                                                                 |              |                           |

## Object Citation Type

| **Kategorie**         | **Beschreibung**                                                                                                                                                              | **Beispiel**                         | **Pflichtfeld (Ja/Nein)** |
|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|---------------------------|
| **objectTitle**       | Bezeichnung/Name der Quelle                                                                                                                                                   | Name einer Zeichnung, Skulptur, usw. |                           |
| **objectContributor** | Einer Person oder Organisation, die verantwortlich für die verantwortlich für den intellektuellen Inhalt eines Work Objektes sind                                             |                                      |                           |
| **objectDate**        | Datum eines Ereignisses im Lebenszyklus einer RessourceFür eine detailierte Beschreibung müssen die Elemente „dateOfCreation“ und „timeOfCreation“ im WorkType genutzt werden |                                      |                           |
| **objectIdentifier**  |                                                                                                                                                                               |                                      |                           |

## Work Type

| **Kategorie**      | **Beschreibung**                                       | **Beispiel**                                                                                                                                                          | **Pflichtfeld (Ja/Nein)** |
|--------------------|--------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------|
| **agent**          |                                                        |                                                                                                                                                                       |                           |
| **abstract**       |                                                        |                                                                                                                                                                       |                           |
| **dateOfCreation** |                                                        |                                                                                                                                                                       |                           |
| **spatial**        | „dreidimensionalle“ Charakteristik eines Work-Objektes | <http://purl.org/dc/terms/spatial>                                                                                                                                      |                           |
| **temporal**       | Zeitliche Charakteristik eines Work-Objektes           | <http://purl.org/dc/terms/temporal>                                                                                                                                     |                           |
| **subject**        | Der Gegenstand eines Work-Objektes                     | <http://purl.org/dc/terms/subject>                                                                                                                                      |                           |
| **genre**          | Das Genre eines Work-Objektes                          | <http://purl.org/dc/terms/typeroman>, drama, prosa, verse, reference work, non-fiction, non-text, other, siehe auch <http://textgrid.info/namespaces/metadata/genre/2015> |                           |

## Item Type

| **Kategorie**    | **Beschreibung**                                                      | **Beispiel**                          | **Pflichtfeld (Ja/Nein)** |
|------------------|-----------------------------------------------------------------------|---------------------------------------|---------------------------|
| **RightsHolder** | Person oder Organisation, die die Copyrights an einem Objekt besitzen | <http://purl.org/dc/terms/rightsHolder> |                           |

## Edition Type

| **Kategorie**      | **Beschreibung** | **Beispiel** | **Pflichtfeld (Ja/Nein)** |
|--------------------|------------------|--------------|---------------------------|
| **isEditionOf**    |                  |              |                           |
| **agent**          |                  |              |                           |
| **source**         |                  |              |                           |
| **formOfNotation** |                  |              |                           |
| **language**       |                  |              |                           |
| **licencense**     |                  |              |                           |

## Collection Type

| **Kategorie**             | **Beschreibung**                                 | **Beispiel**                                                              |
|---------------------------|--------------------------------------------------|---------------------------------------------------------------------------|
| **collector**             | Bezeichnung für den Ersteller einer Collektion   | <http://id.loc.gov/vocabulary/relators/col.html>                            |
| **abstract**              | Zusammenfassung der Collection                   | <http://purl.org/dc/terms/abstract>                                         |
| **collectionDescription** | URL zu einer Beschreibung der Collection         | <http://purl.org/dc/terms/descriptionProjektbeschreibung> auf einer Website |
| **spatial**               | Dreidimensionale Charakteristiken der Collection | <http://purl.org/dc/terms/spatial>                                         |
| **temporal**              | Zeitliche Charakteristiken der Collection        | <http://purl.org/dc/terms/temporal>                                         |
| **subject**               | Das Thema der Collection                         | <http://purl.org/dc/terms/subject>                                          |

## Source Type

Beziehung zwischen dem TextGrid-Objekt und der Beschreibung der Quelle
des Objektes.

## Custom Type

Bei der Eingabe der Metadaten ist der Nutzer nicht auf diese vorhandenen
festgelegt. Das vorgegebene Metadatenschema ist frei erweiterbar durch
den Metadatentyp *custom*.
