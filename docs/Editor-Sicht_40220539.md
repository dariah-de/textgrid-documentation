# Editor-Sicht

last modified on Jul 31, 2015

Dieser Abschnitt beschreibt, wie Sie mit der Editor-Sicht in der Mitte
arbeiten können, wenn ein Dokument geöffnet ist. Am unteren Ende der
Editor-Sicht können Sie zwischen den drei verschiedenen
Bearbeitungs-Ansichten “Entwurf”, “Quelle” und “WYSIWYM” sowie einer
Vorschau-Ansicht als Anzeigemodus wählen.

- Die Entwurf-Ansicht zeigt die hierarchische Struktur des Dokuments.
- Die Quelle-Ansicht zeigt das Dokument in der Auszeichnungssprache
    XML. Dies erlaubt Ihnen, auf seine Struktur zuzugreifen und sie zu
    verändern.
- Die WYSIWYM (=“What You See Is What You Mean”)-Ansicht zeigt Ihnen
    eine beispielhafte Darstellung und formatiert den Inhalt für das Web.
- Die Vorschau-Ansicht präsentiert ein HTML-Dokument nach der
    Verarbeitung des XML-Objekts durch eine adäquate XSLT-Datei.

Sie können durch Klicken der Reiter am unteren Ende des Editors wischen
diesen Bearbeitungs-Ansichten wechseln. Das Asterisk-Zeichen (\*) in der
Titelleiste der Editorsicht zeigt an, dass die Datei noch nicht
gespeichert wurde. In jeder Ansicht ist eine Liste von Tastaturkürzeln
durch Eingabe von Strg+Umschalttaste+L verfügbar.

- [Entwurf-Ansicht](Entwurf-Ansicht_40220541.md)
- [Quelle-Ansicht](Quelle-Ansicht_40220547.md)
- [Vorschau-Ansicht](Vorschau-Ansicht_40220559.md)
- [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md)
