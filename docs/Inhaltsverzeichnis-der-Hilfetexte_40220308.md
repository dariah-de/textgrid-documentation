# Inhaltsverzeichnis der Hilfetexte

last modified on Aug 18, 2015

Nach der Auswahl von “Inhaltsverzeichnis der Hilfetexte” wird die
allgemeine Hilfefunktion in einem neuen Fenster Ihres
Standard-Webbrowsers angezeigt. Das neue Fenster enthält eine Kopfzeile,
in der Sie ein Schlüsselwort für die Suche eingeben können. Wenn Sie
„Suchbereich“ anklicken, können Sie die Suche auf einen bestimmten
Bereich einschränken. Im Standardmodus werden alle Themen durchsucht.

Das Hauptfenster ist in zwei Teile unterteilt. Der linke Bereich ähnelt
der Navigator-Sicht. Er zeigt eine Baumstruktur aller
Inhaltsverzeichnisse der Hilfetexte. Der rechte Bereich zeigt die
Inhalte der Einträge der Hilfe an. In der Werkzeugleiste des linken
Bereichs können Sie verschiedene Funktionen auswählen:

- Drucken ![](attachments/40220308/40436681.png "print.png") Sie das
    ausgewählte Thema mit oder ohne Unterthemen.
- Klicken Sie das
    Symbol ![](attachments/40220308/40436668.gif "105-zeige-SchluesselwortSuche.gif") ,
    um in einem Thema mit oder ohne Unterthemen nach einem Schlüsselwort
    zu suchen. Die Entscheidung, in einem Thema mit oder ohne
    Unterthemen zu suchen, kann nach Klicken des nach unten gerichteten
    Dreiecks für das Aufklappmenü getroffen werden.
- Außerdem können Sie alle Knoten in der Listen durch Klicken des
    Symbols ![](attachments/40220308/40436679.png "collapse-all-nodes.png")
    zuklappen sowie das Fenster minimieren und maximieren.
- Durch Klicken des
    Symbols ![](attachments/40220308/40436678.png "synchron.png") können
    Sie den Navigationsbaum mit dem aktuellen Thema synchron halten (das
    heißt in der Baumstruktur werden diejenigen Begriffe hervorgehoben,
    die durch die Verknüpfungen des ausgewählten Eintrags auf der
    rechten Seite aktiviert wurden).

Die Liste darunter zeigt die Inhalte in einer Baumstruktur an. Durch
Klicken der Symbole “+” und “-” können die Inhalte aus- oder zugeklappt
werden. Durch Klicken eines Elements im Baum wird dessen Inhalt im
rechten Bereich des Fensters angezeigt.

In der Fußzeile des linken Bereichs des Navigators können Sie wählen
zwischen:

- einem
    Inhaltsverzeichnis ![](attachments/40220308/40436677.png "content.png")
- einer Sicht, um den
    Index ![](attachments/40220308/40436672.png "index.png") zu
    durchsuchen
- einer Auflistung der
    Suchergebnisse ![](attachments/40220308/40436680.png "search-lamp-results.png")
- einer Sicht mit den
    Lesezeichen ![](attachments/40220308/40436675.png "bookmarks.png") ,
    die durch Klicken des
    Symbols ![](attachments/40220308/40436674.png "remove-bookmark.png")
    (ausgewählte Lesezeichen) und
    ![](attachments/40220308/40436673.png "remove-all-bookmarks.png")(alle
    Lesezeichen) in der Werkzeugleiste der Sicht gelöscht werden können.

Der rechte Bereich des Fensters “Inhaltsverzeichnis der Hilfetexte”
zeigt den ausgewählten Eintrag der Hilfe an. In der Werkzeugleiste des
rechten Bereichs können Sie verschiedene Funktionen auswählen:

- Klicken Sie die Schaltflächen mit den Pfeilen, um vor und zurück zu
    navigieren, und das
    Symbol ![](attachments/40220308/40436671.png "home.png") , um zum
    Ausgangspunkt zurückzukehren.
- Klicken
    Sie ![](attachments/40220308/40436670.png "table-of-contents.png") ,
    um das angezeigte Thema im Inhaltsverzeichnis im linken Bereich des
    Fensters anzuzeigen, wenn der Baum zugeklappt ist.
- Sie können für ein Dokument ein Lesezeichen
    setzen ![](attachments/40220308/40436669.png "bookmark-document.png")
    oder die Seite
    drucken ![](attachments/40220308/40436681.png "print.png").

## Attachments

- [105-zeige-SchluesselwortSuche.gif](attachments/40220308/40436668.gif)
(image/gif)  
- [bookmark-document.png](attachments/40220308/40436669.png) (image/png)  
- [table-of-contents.png](attachments/40220308/40436670.png) (image/png)  
- [home.png](attachments/40220308/40436671.png) (image/png)  
- [index.png](attachments/40220308/40436672.png) (image/png)  
- [remove-all-bookmarks.png](attachments/40220308/40436673.png)
(image/png)  
- [remove-bookmark.png](attachments/40220308/40436674.png) (image/png)  
- [bookmarks.png](attachments/40220308/40436675.png) (image/png)  
- [search-lamp.png](attachments/40220308/40436676.png) (image/png)  
- [content.png](attachments/40220308/40436677.png) (image/png)  
- [synchron.png](attachments/40220308/40436678.png) (image/png)  
- [collapse-all-nodes.png](attachments/40220308/40436679.png)
(image/png)  
- [search-lamp-results.png](attachments/40220308/40436680.png)
(image/png)  
- [print.png](attachments/40220308/40436681.png) (image/png)  
