# Wortform lemmatisieren

last modified on Aug 20, 2015

1. Öffnen Sie den Lemmatisierer durch Auswahl von “Werkzeuge \> Sicht
    anzeigen \> Wortform lemmatisieren” in der Menüleiste oder durch
    Klicken von  in der Werkzeugleiste.
2. Geben Sie eine Wortform ein.
3. Wählen Sie eine Konfiguration.
4. Klicken Sie “Lemmatisierung starten”. Das Ergebnis wird im
    Ergebnisfeld darunter angezeigt.

Alternativ können Sie „Lemmatisierer öffnen“ im Kontextmenü im
Text-Editor oder im [XML-Editor](XML-Editor_40220521.md) (sowohl in
der [Quelle-Sicht](Quelle-Ansicht_40220547.md) als auch in der
[WYSIWYM-Sicht](WYSIWYM-Ansicht_40220553.md)) auswählen.

1. Markieren Sie das Wort und rechtsklicken Sie es. Das Kontextmenü
    öffnet sich.
2. Wählen Sie "Lemmatisierer".
3. Wählen Sie "Wortform lemmatisieren" oder "Analyse starten". Das
    Ergebnis wird im Ergebnisfeld darunter angezeigt.

## Attachments

- [082-zeige-WortformLemmatisiererAnsicht.gif](attachments/40220728/40436897.gif)
(image/gif)  
