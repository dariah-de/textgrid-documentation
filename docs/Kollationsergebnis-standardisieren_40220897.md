# Kollationsergebnis standardisieren

last modified on Aug 20, 2015

1. Nachdem Sie [verschiedene Texte
    kollationiert](Texte-kollationieren_40220895.md) haben, können
    spezielle Lesarten als äquivalent festgelegt werden.
2. Klicken
    Sie ![](attachments/40220897/40436954.gif "156-Datei-Icon-Ausnahmeliste.gif")
    oder verwenden Sie die Verknüpfung am unteren Ende der
    Kollationierungssetliste, um die Äquivalenzsetliste zu öffnen.
3. Wählen Sie ein Projekt und einen Titel sowie weitere Metadaten für
    das Set in dem Assistenten aus, der sich jetzt öffnet.
4. Ziehen Sie Objekte aus der [Alignment-Tabelle](40220889.md) und
    legen Sie diese in der Äquivalenzsetliste ab. Tokens, die als
    äquivalent behandelt werden sollen, müssen durch Platzieren des
    Mauszeigers in der entsprechenden Zeile abgelegt werden. Um
    ausgewählte Zeilen zu löschen, klicken
    Sie ![](attachments/40220897/40436953.gif) in der Werkzeugleiste.
5. In der zweiten Spalte der Äquivalenzsetliste können Sie festlegen,
    ob diese Äquivalenz nur lokal oder global für die gesamte
    Kollationierung verwendet werden soll. Klicken Sie "Global" oder
    "Lokal", um die Einstellungen zu ändern.
6. Durch Klicken der Schaltfläche "..." können bestehende
    Äquivalenzsets verwendet werden.
7. Durch Klicken der Schaltfläche "X" können bestehende Beziehungen mit
  Äquivalenzsets gelöscht werden.
8. Speichern Sie das Äquivalenzset durch Drücken von \[Strg+S\] und
    starten Sie die Kollationierung wenn gewünscht neu.

## Attachments

- [042-loesche-Auswahl.gif](attachments/40220897/40436953.gif)
(image/gif)  
- [156-Datei-Icon-Ausnahmeliste.gif](attachments/40220897/40436954.gif)
(image/gif)  
