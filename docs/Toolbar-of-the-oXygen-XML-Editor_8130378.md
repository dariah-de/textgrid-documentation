# Toolbar of the oXygen XML Editor

last modified on Mai 04, 2016

Wenn der oXygen-XML-Editor geöffnet ist, werden Elemente aus der
Werkzeugleiste dieses Editors in der Werkzeugleiste des TextGridLab
hinzugefügt. Diese können beispielsweise zur Validierung und
Transformation verwendet werden. Für weitere Informationen, siehe

*[http://www.oxygenxml.com/doc/ug-editor/](http://www.oxygenxml.com/doc/ug-editor/)*
