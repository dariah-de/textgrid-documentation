# Marketplace

last modified on Feb 26, 2019

Das TextGridLab steht als Basispaket, das die wichtigsten Komponenten
enthält, zum Download zur Verfügung. Der kostenlose Download
zusätzlicher Open Source-Werkzeuge und –Dienste ist mit dem
Eclipse-Interface "Marketplace" im TextGridLab über die Menüleiste am
oberen Bildschirmrand möglich. Wählen Sie das Symbol  auf dem
Startbildschirm oder den Menüpunkt "Eclipse Marketplace ..." unter
"Hilfe" in der Menüleiste. Prinzipiell gibt es keine Einschränkungen für
die Werkzeugliste. Der Marketplace bietet einen Katalog zusätzlicher
Werkzeuge an, die im TextGridLab installiert werden können und entweder
vom TextGrid-Team oder von externen Entwicklern zur Verfügung gestellt
werden.

[Noteneditor MEISE](https://wiki.de.dariah.eu/display/TGINT/MEISE+Noteneditor)

[CollateX](40220881.md)

[XML-Editor oXygen](40220583.md)

[Publikationswerkzeug SADE](Publikationswerkzeug-SADE_40220503.md)

[Digilib](40220959.md)

Folgende Werkzeuge stehen im Marketplace nicht mehr zum Download zur
Verfügung:

[Linguistische Werkzeuge](Linguistik-Werkzeuge_40220700.md) (d.h.
Lemmatisierer, LEXUS, Cosmas II, ANNEX)

[Text-Text-Link-Editor](Text-Text-Link-Editor_40220608.md)

Der Marketplace befindet sich noch in der Erprobung, so dass nicht
sichergestellt ist, dass alle Funktionen wie gewünscht funktionieren.

Es gibt außerdem einen Mechanismus, um [Software zu installieren](Software-installieren-und-entfernen_40220213.md), die
nicht über den Marketplace angeboten wird.

## Attachments

- [164-Marketplace64x64.png](attachments/8130167/8192283.png)
(image/png)
