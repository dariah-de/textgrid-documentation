# Dokument lemmatisieren

last modified on Aug 04, 2015

1. Öffnen Sie den "BatchLemmatisierer" durch Auswahl von "Werkzeuge \>
    Sicht anzeigen \> Datei lemmatisieren " in der Menüleiste.
2. Spezifizieren Sie die Eingabedatei und die Konfiguration.
3. Klicken Sie "Lemmatisierung starten".
