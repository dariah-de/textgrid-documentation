# Interaction of the Musical Note Editor with Other Components

last modified on Mai 04, 2016

MEI documents that were created or modified with the MEI Score Editor
can also be opened with the [XML Editor](XML-Editor_7439318.md) to
display and/or modify their source code.
