# Der oXygen-XML-Editor

last modified on Jul 31, 2015

Der oXygen-XML-Editor ist ausführlich beschrieben unter

[*http://www.oxygenxml.com/doc/ug-editor/*](http://www.oxygenxml.com/doc/ug-editor/)

Wenn oXygen in TextGrid geöffnet ist, wirkt sich dies auf die Menüleiste
und die Werkzeugleiste des TextGridLab aus.

- [Menüleiste des oXygen-XML-Editors](40220593.md)
- [Werkzeugleiste des oXygen-XML-Editors](Werkzeugleiste-des-oXygen-XML-Editors_40220595.md)
- [Kontextmenü des oXygen-XML-Editors](40220597.md)
