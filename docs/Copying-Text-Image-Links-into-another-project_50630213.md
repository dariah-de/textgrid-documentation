# Copying Text Image Links into another project

last modified on Nov 29, 2016

If an image is linked with a text, a new file with these information is
created. If you copy these objects seperately, the links still refer to
the objects in the original project and problems concerning the rights
management might occur. That is why it is recommended to mark all three
objects in conjunction. Keep CTRL or cmd pressed and select the objects
with the left mouse button. Afterwards you can copy the objects by using
drag and drop and ponting to the title of the new project or by using
the context menu. All URI will be rewritten und the links will be set
between the new copies of the objects.
