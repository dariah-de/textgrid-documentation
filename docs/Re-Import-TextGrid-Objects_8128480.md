# Re-Import TextGrid Objects

last modified on Mai 03, 2016

Sie können Objekte, die Sie zuvor exportiert haben, re-importieren.
Während des Export-Vorgangs werden die eigentliche Datei und eine
weitere Datei mit deren Metadaten auf einem externen Speichermedium
gespeichert. Durch Auswahl des Zielprojekts im Aufklappmenü der
Import-Sicht kann das Objekt entweder als völlig neues Objekt mit einem
neuen URI oder als
neue [Revision](https://doc.textgrid.de/search.html?q=Revisions+and+Locking+Mechanism) des
zuvor exportierten Objekts re-importiert werden.

## Eine Datei als völlig neues Objekt re-importieren

[Exportieren](https://doc.textgrid.de/search.html?q=Using+the+Export+Tool) Sie
zunächst das oder die Objekt(e) aus TextGrid. Nach dem Export werden sie
als lokale Dateien in Ihrem Betriebssystem gespeichert.

1\. Öffnen Sie das Import-Werkzeug über “Datei \> Lokale Dateien
importieren ...” in der Menüleiste.

2\. Wählen Sie das Zielprojekt, in das die zuvor exportierten
TextGrid-Dateien re-importiert werden sollen. Klicken Sie dann die
Schaltfläche “Hinzufügen” in der Import-Sicht oder “Datei \> Lokale
Datei öffnen ...” in der Menüleiste, um die Dateien in Ihrem
Betriebssystem auszuwählen, die re-importiert werden sollen. Sie können
entweder einfach nur die eigentlichen Dateien auswählen oder gemeinsam
mit den damit zusammengehörigen Metadatendateien und “Öffnen” klicken.
Sie werden nun in der Import-Sicht angezeigt. Das Hinzufügen von Dateien
durch Ziehen und Ablegen in die Import-Sicht ist ebenfalls möglich.

Jede Datei, die zur Import-Sicht hinzugefügt wurde, wird mit weitere
Angaben (lokaler Dateiname, TextGrid-URI, Titel und Format) angezeigt
und ist noch nicht re-importiert. Der neue URI ist ebenfalls noch nicht
erstellt. Wenn Sie eine Datei in der Import-Sicht durch Linksklicken
auswählen, werden Ihre Metadaten geöffnet. Sie können nun zusätzliche
Metadaten angeben oder ihren Titel ändern.

3\. Klicken Sie “Import!”, um den Vorgang abzuschließen.

## Eine Datei als neue Revision des ursprünglichen Objekts re-importieren

[Exportieren](https://doc.textgrid.de/search.html?q=Using+the+Export+Tool) Sie
zunächst das oder die Objekt(e) aus Ihrem Projektordner. Nach dem Export
werden sie als lokale Dateien in Ihrem Betriebssystem gespeichert.

1\. Öffnen Sie das Import-Werkzeug über “Datei \> Lokale Dateien
importieren ...” in der Menüleiste.

2\. Fügen Sie die zuvor exportierte(n) Datei(en) in der Import-Sicht
hinzu. Sie könne dies durch Klicken der Schaltfläche "Hinzufügen" oder
durch Ziehen und Ablegen tun.

3\. Wählen Sie im Aufklappmenü unterhalb der Liste den ursprünglichen
Projektordner, aus dem die Datei(en) exportiert wurde(n). Stellen Sie
sicher, dass der Projektordner mit der Ergänzung " – neue Revisionen"
ausgewählt ist. Sobald Sie diesen Ordner auswählen, werden die
ursprünglichen TextGrid-URIs erkannt.

4\. Klicken Sie "Import!", um die Datei(en) als neue Revision(en) im
ursprünglichen Projekt zu speichern.
