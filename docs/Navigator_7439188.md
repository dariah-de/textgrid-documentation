# Navigator

last modified on Feb 28, 2018

Der Navigator ist ein Projektbrowser, der Ihnen Zugriff zu allen
Materialen gewährt, die mit dem Projektordner, in dem Sie gerade
arbeiten, in Verbindung stehen.

![](attachments/40220331/40436689.png)

|                |
|----------------|
| Navigator View |

- [Open the Navigator](Open-the-Navigator_7439190.md)
- [Navigator View](Navigator-View_7439192.md)
- [Using the Navigator](Using-the-Navigator_7439202.md)
- [Interaction of the Navigator with Other Components](Interaction-of-the-Navigator-with-Other-Components_7439208.md)

## Attachments

- [pum-navigatorlang.png](attachments/7439188/7766072.png) (image/png)  
- [pum-navigator-view.png](attachments/7439188/9240666.png) (image/png)  
- [pum-navigator-view.png](attachments/7439188/9240667.png) (image/png)  
- [pum-navigator-view.png](attachments/7439188/7766073.png) (image/png)  
