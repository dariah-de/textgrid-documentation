# Kopie von Properties View of the MEI Score Editor

Created on Jul 24, 2015

This view shows the modifiable properties of the elements from the
[Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) in two
columns (i.e. attributes of selected elements in the Outline View and
their appropriate values).

The information displayed in the Properties View will change depending
on the preselected (highlighted) element in the Outline View. The
properties “ID” and "N" (number) are basic and static but their values
depend on the preselected element type. If you make changes in the Value
column, the modifications will be displayed immediately in the [Score View](Musical-Score-View_7439823.md). To validate all modifications in
the Value columns, please click anywhere in the [Score View](Musical-Score-View_7439823.md).

![](attachments/40220937/40437039.png)

|                       |
|-----------------------|
| MEISE Properties View |

- [Kopie von Features of the Properties View](Kopie-von-Features-of-the-Properties-View_40220939.md)
- [Kopie von Title Bar of the MEISE Properties View](Kopie-von-Title-Bar-of-the-MEISE-Properties-View_40220941.md)
- [Kopie von Context Menu of the MEISE Properties View](Kopie-von-Context-Menu-of-the-MEISE-Properties-View_40220943.md)

## Attachments

- [mei-propertiesview.png](attachments/40220937/40437039.png)
(image/png)  
- [meise-f8_properties view.png](attachments/40220937/40437040.png) (image/png)
