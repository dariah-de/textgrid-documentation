# Text Image Link Editor

last modified on Mai 03, 2016

Der Text-Bild-Link-Editor kann verwendet werden, um innerhalb des
TextGridLab Textsegmente mit Bildausschnitten zu verknüpfen. Eine
typische Anwendung ist, ein Faksimile und Transkriptionen zu verknüpfen,
aber diese Texte können auch während des Verknüpfungsprozesses erstellt
werden, was die Verwendung weiterer Werkzeuge wie Bildannotationen
erlaubt.

XML-Texte und Bilder können in den entsprechenden Sichten geöffnet
werden. Die entsprechenden Komponenten werden dann paarweise markiert
und der Verknüpfungsvorgang wird bestätigt. Die Ergebnisse können als
ein neues Objekt, das mit dem
Symbol ![](attachments/40220625/40436818.png "069-zeige-BildLinkEditorPerspektive.png") versehene
Text-Bild-Link-Objekt, gespeichert werden, das die
Verknüpfungsinformationen (die Text- und Bildkoordinaten, Pfad der
verwendeten XML- und Bild-Objekte) enthält. Sobald ein
Text-Bild-Link-Objekt gespeichert ist, können Sie durch einen
Doppelklick XML-Texte, Bilder und Verknüpfungen neu laden und mit der
Bearbeitung fortfahren.

- [Open the Text Image Link Editor](Open-the-Text-Image-Link-Editor_7439376.md)
- [Text Image Link Editor Perspective](Text-Image-Link-Editor-Perspective_7439378.md)
- [Image View](Image-View_7439384.md)
- [XML Editor View](XML-Editor-View_7439400.md)
- [Thumb View](Thumb-View_7439402.md)
- [Toolkit](Toolkit_7439404.md)
- [Using the Text Image Link Editor](Using-the-Text-Image-Link-Editor_7439406.md)
- [Interaction of the Text Image Link Editor with Other Components](Interaction-of-the-Text-Image-Link-Editor-with-Other-Components_7439420.md)

## Attachments

- [069-zeige-BildLinkEditorPerspektive.png](attachments/7439374/8192253.png)
(image/png)  
