# Aggregationen-Editor-Sicht

last modified on Aug 19, 2015

Der Aggregationen-Editor ist Bestandteil einer Perspektive mit dem
[Navigator](40220331.md) und dem
[Metadaten-Editor](Metadaten-Editor_40220458.md). Wenn eine
Aggregation bearbeitet wird, können die Werkzeugleiste und das
Kontextmenü des Aggregationen-Editors verwendet werden.

![](attachments/40220444/40436721.png)

|                          |
|--------------------------|
| **Aggregationen-Editor** |

- [Werkzeugleiste des Aggregationen-Editors](Werkzeugleiste-des-Aggregationen-Editors_40220446.md)
- [Kontextmenü des Aggregationen-Editors](40220448.md)

## Attachments

- [pum-aggregationeditor.png](attachments/40220444/40436721.png)
(image/png)  
- [pum-aggregationcomposer.png](attachments/40220444/40436722.png)
(image/png)  
