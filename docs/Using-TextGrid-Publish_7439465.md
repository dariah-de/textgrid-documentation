# Using TextGrid Publish

last modified on Jan 21, 2021

Bevor Sie ein Objekt veröffentlichen, ist es wichtig zu wissen, dass nur
Editionen und Kollektionen veröffentlicht werden können. Um
veröffentlicht zu werden, müssen Editionen ein
TextGrid-“[Werk](https://wiki.de.dariah.eu/display/TextGrid/Werke)”
enthalten. Wenn eine Edition veröffentlicht werden soll, werden
Metadatenfelder, die vorher optional für eine Edition waren,
Pflichtfelder: Diese Felder sind “Edition of” und “License”.

Wenn ein Projekt mit mindestens einem zu veröffentlichenden Objekt im
TextGridLab erstellt wurde, kann eine Edition dieses Objekts mit Hilfe
des [Aggregationen-Editors](https://wiki.de.dariah.eu/display/TextGrid/Aggregationen-Editor) und
des [Metadaten-Editors](https://wiki.de.dariah.eu/display/TextGrid/Metadaten-Editor) veröffentlicht
werden:

1. Erstellen und speichern Sie ein Werk.
2. Erstellen Sie eine Edition.
3. Kopieren Sie das zu veröffentlichende Objekt und das Werk mit den
    Metadaten in der Edition.
4. Vervollständigen Sie die Metadaten der Edition: “Edition of” und
    “License” werden für die Publikation benötigt.
5. Speichern Sie die Edition.
6. Veröffentlichen Sie die Edition.

Diese Edition ist nun veröffentlicht. Dieser Vorgang ist irreversibel.
