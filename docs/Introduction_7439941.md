# Introduction

In der Einleitung werden die Vorzüge des TextGrid Labs vorgestellt.
Außerdem werden die besonderen Begrifflichkeiten der auf Eclipse
basierenden Oberfläche eingeführt. Dazu gehören Perspektive und Ansicht
sowie die Such- und die Hilfefunktion.

- [Getting Started](Getting-Started_7439062.md)
- [User Interface](User-Interface_7439164.md)
- [Search](Search.md)
- [Help](Help_7439487.md)
