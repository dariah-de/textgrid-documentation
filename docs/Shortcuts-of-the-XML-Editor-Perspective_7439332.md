# Shortcuts of the XML Editor Perspective

last modified on Mai 04, 2016

|                                    |                                                                                                                                                     |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| \[Alt+/\]                          | [Wortabschluss](40220529.md)                                                                                                                      |
| \[Alt+ Umschalttaste +Runter\]     | Letzte Auswahl wiederherstellen                                                                                                                     |
| \[Alt+ Umschalttaste +Links\]      | Auswahl erweitern auf vorheriges Element                                                                                                            |
| \[Alt+ Umschalttaste +Hoch\]       | Auswahl erweitern auf einschließendes Element                                                                                                       |
| \[Alt+ Umschalttaste +Rechts\]     | Auswahl erweitern auf nächstes Element                                                                                                              |
| \[Alt+ Umschalttaste +W\]          | Anzeigen in [Navigator-](40220331.md), [Gliederung-](Gliederung-Sicht_40220561.md) und [Eigenschaften-Sicht](Eigenschaften-Sicht_40220567.md) |
| \[Crtl+1\]                         | [Schnellkorrektur](Quelle-Ansicht_40220547.md)                                                                                                    |
| \[Strg+A\]                         | Alles auswählen                                                                                                                                     |
| \[Strg +C\]                        | Kopieren                                                                                                                                            |
| \[Strg +F\]                        | Suchen und Ersetzen                                                                                                                                 |
| \[Strg +I\]                        | [Aktive Elemente formatieren](Quelle-Ansicht_40220547.md)                                                                                         |
| \[Strg +Umschalttaste+/\]          | [Block-Kommentar hinzufügen](Quelle-Ansicht_40220547.md)                                                                                          |
| \[Strg + Umschalttaste +\\\]       | [Block-Kommentar entfernen](Quelle-Ansicht_40220547.md)                                                                                           |
| \[Strg + Umschalttaste +C\]        | [Kommentar umschalten](Quelle-Ansicht_40220547.md)                                                                                                |
| \[Strg + Umschalttaste +F\]        | [Formatieren](Quelle-Ansicht_40220547.md)                                                                                                         |
| \[Strg + Umschalttaste +Einfügen\] | [Intelligenter Einfügemodus](Quelle-Ansicht_40220547.md)                                                                                          |
| \[Strg +V\]                        | Einfügen                                                                                                                                            |
| \[Strg +X\]                        | Ausschneiden                                                                                                                                        |
| \[Strg +Y\]                        | Wiederholen                                                                                                                                         |
| \[Strg +Z\]                        | Rückgängig                                                                                                                                          |
| \[Löschen\]                        | Löschen                                                                                                                                             |

|        |                                 |
|--------|---------------------------------|
| \[F2\] | QuickInfo-Beschreibung anzeigen |
