# Components of Digilib

last modified on Mai 04, 2016

Digilib besteht aus dem Skalierungsdienst und dem Digilib-Klienten.

*Der Digilib-Skalierungsdienst*

Der Digilib-Skalierungsdienst ist ein Webdienst, der auf einem
TextGrid-Server läuft und Befehle vom Digilib-Klienten im TextGridLab
empfängt und mit dem TextGrid Repository kommuniziert. Der
Digilib-Klient teilt dem Dienst mit, welche Bilder angezeigt und welche
Bildoperationen darauf ausgeführt werden sollen, und der
Digilib-Skalierungsdienst gibt das angeforderte Bild zurück.

*Der Digilib-TextGrid-Klient*

Der [Digilib-TextGrid-Klient](40220963.md) ist ein Pluginmodul für das
TextGridLab, das Sie Bilder anzeigen, bearbeiten und annotieren lässt.
Der Klient kommuniziert mit dem Digilib-Skalierungsdienst.
