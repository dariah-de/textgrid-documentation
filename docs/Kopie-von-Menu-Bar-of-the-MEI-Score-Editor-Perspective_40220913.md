# Kopie von Menu Bar of the MEI Score Editor Perspective

Created on Jul 24, 2015

When you open the Note Editor and make changes in the current document,
certain items in the menu bar will be activated.

- [Kopie von Edit](Kopie-von-Edit_40220915.md)
- [Kopie von Window](Kopie-von-Window_40220917.md)
