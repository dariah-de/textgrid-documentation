#Kopie von Toolbar of the MEI Score Editor Perspective

Created on Jul 24, 2015

The MEI Score Editor has certain specific functions in its toolbar. This
toolbar displays more items for editing the MEI note sheet on the left
side of the general TextGrid toolbar.

- "Undo and Redo":
    Choose **![](attachments/40220919/40436981.png "037-zuruecknehme-Aenderung.png")**
    or **![](attachments/40220919/40436980.png "038-wiederhole-Aenderung.png")**
    to undo and redo changes made in the document.
- "Zoom Factor
    box": **![](attachments/40220919/40436979.png "meise-old_toolbar_prozent.png")** 
    shows the zoom factor (in percentage) of the notes. You can change
    the dimension from 25% to 300%. Please note: The extent of the Score
    View does not automatically fit the level of the zooming factor. To
    change this setting, there are three options: “Page” adapts the size
    of the note sheet to the size of the Score View. “Height” extends
    the musical notes to the height of the Score View. “Width” sets the
    width of the music sheet to the width of the field. The zoom in and
    zoom out icons      **![](attachments/7439879/8192034.png "meise-zoominzoomout.png")**
    serve to increase or reduce the size of staves, notes, and all
    musical symbols shown in the Score View. Please note: The extent of
    the Score View does not automatically change with the level of the
    zooming factor.
- "Insert Measures":
    **![](attachments/40220919/40436973.png "insertMeasures.PNG")**
    allows the user to insert a number of prepared measures into the MEI
    document. Up to 100 measures can be inserted. The user can also
    define if a new section for the new measures should be created or if
    new staves and layers within the new measures should be created.
-   "Insert
    Staffdefs": **![](attachments/40220919/40436972.png "insertStaffdefs.PNG")**
    allows the user to insert specified StaffDefs into the MEI document.
    In a table, all criteria for the StaffDefs can be defined
    previously, such as, for example, the shape of the clef, the key
    signature and the time signature.
- "Score Image Export": Use the photo camera
    symbol ![](attachments/40220919/40436971.png "scoreImageExport.PNG")
    to create an image of the visible section or the whole score. Export
    your score image as a screenshot (.png .jpeg or .gif) to a folder on
    your operating system.
- "MEI-prune
    model": ![](attachments/40220919/40436970.png "meiPrune.PNG") allows
    you to save the current variant configuration of the MEI into an
    unambiguous MEI file without variants to the hard disk. Please
    select the parent folder in the list of the dialog box “Save As”.  
- "Manage
    Sources": ![](attachments/40220919/40436969.png "manageSrc.PNG")
    creates and edits sources in the current MEI document.

## Attachments

- [manageSrc.PNG](attachments/40220919/40436969.png) (image/png)  
- [meiPrune.PNG](attachments/40220919/40436970.png) (image/png)  
- [scoreImageExport.PNG](attachments/40220919/40436971.png) (image/png)
- [insertStaffdefs.PNG](attachments/40220919/40436972.png) (image/png)  
- [insertMeasures.PNG](attachments/40220919/40436973.png) (image/png)  
- [meise-toolbar_prunemodel.png](attachments/40220919/40436974.png)
(image/png)  
- [meise-toolbar_refreshscreen.png](attachments/40220919/40436975.png)
(image/png)  
- [meise-toolbar_managesources.png](attachments/40220919/40436976.png)
(image/png)  
- [meise-toolbar_scoreimageexport.png](attachments/40220919/40436977.png)
(image/png)  
- [meise-toolbar_prozent.png](attachments/40220919/40436978.png)
(image/png)  
- [meise-old_toolbar_prozent.png](attachments/40220919/40436979.png)
(image/png)  
- [038-wiederhole-Aenderung.png](attachments/40220919/40436980.png)
(image/png)  
- [037-zuruecknehme-Aenderung.png](attachments/40220919/40436981.png)
(image/png)  
