# Features of the Image View

last modified on Mai 03, 2016

Zur Bearbeitung von Bildern stellt Ihnen die Bild-Sicht verschiedene
Funktionalitäten zur Verfügung: Markierungen, Andocklinien,
Schriftmodus, Rotationsfunktion und den Ebenen-Editor. Auf die meisten
kann über das Kontextmenü durch Rechtsklicken zugegriffen werden.

Markierungen

Eine grundlegende Funktion der Bild-Sicht ist das Erstellen und
Bearbeiten verschiedener Markierungen in einem Bild. Sie können zwei
Arten von Markierung (Rechtecke und Polygone) erstellen, um Ausschnitte
eines Bildes zu markieren. Bei einer Markierung, die noch nicht mit
einem Textabschnitt verknüpft ist, wird die Umrandung mit einer
gestrichelten Linie dargestellt. Eine durchgezogene Linie kennzeichnet
Markierungen im Bild, die bereits mit Textabschnitten in der
XML-Editor-Sicht verknüpft sind. Eine existierende Markierung kann durch
Doppelklicken gewählt oder aktiviert werden. Die Umrandung wird dann
ihre Farbe ändern. Sie können mehr als eine Markierung durch Drücken der
Strg-Taste beim Doppelklicken der erstellten Markierungen auswählen.

Andocklinien-Einstellungen

Sie können Andocklinien als visuelles Hilfsmittel einsetzen, nachdem Sie
den Andocklinien-Modus
im [Werkzeugkasten](Werkzeugkasten_40220657.md) gewählt haben. Nachdem
eine neue Andocklinie gezeichnet wurde, kann sie mit \[Umschalttaste+H\]
oder \[Umschalttaste+V\] oder über das Kontextmenü horizontal oder
vertikal ausgerichtet werden. Drücken Sie \[Strg+V\], um eine
Andocklinie zu duplizieren. Das Erscheinungsbild der Andocklinien kann
unter "Andocklinien-Einstellungen" angepasst werden:

1. Rechtsklicken Sie in die Bild-Sicht.
2. Wählen Sie “Andocklinien”.
3. Wählen Sie “Einstellungen”.
4. Sie können zwischen verschiedenen Linienstilen wählen: gepunktet,
    durchgezogen, gestrichelt, Strich-Punkt, Strich-Punkt-Punkt (Dot,
    Solid, Dash, DashDot, DashDotDot).
5. Es gibt fünf verschiedene Linienbreiten für Andocklinien. Je größer
    die Zahl, desto dicker ist die Linie.
6. Der Zeilenabstand zwischen duplizierten Andocklinien kann durch
    wählen eines neuen Wertes zwischen “10” und “150” gewählt werden.

Die Farben der aktiven und inaktiven Andocklinien können auch durch
Klicken der beiden Farbigen Quadrate im Assistenten
“Andocklinien-Einstellungen” gewählt werden. Nach dem Klicken der
Quadrate öffnet sich eine Palette. Mit den Pfeilen können Sie die Farben
der aktiven und inaktiven Andocklinien ändern.

![](attachments/40220641/40436833.png)

|                                |
|--------------------------------|
| **Andocklinien-Einstellungen** |

Schriftmodus

Im "Schriftmodus", können Sie Informationen über die Textrichtung zu den
gewählten Ausschnitten in einem Bild hinzufügen. Drücken Sie
\[Umschalttaste+S\], um den Schriftmodus einer gewählten Markierung ein-
oder auszublenden. Der Schriftmodus für die aktive(n) Markierung(en)
oder für das aktive Dokument kann durch Rechtsklicken in der Bild-Sicht
geändert werden. Die folgenden Optionen können gewählt werden:

- keine: dies ist der Standardwert
- lr: von links nach rechts
- lr-ou: von links nach rechts und von oben nach unten
- rl: von rechts nach links
- rl-ou: von rechts nach links und von oben nach unten
- ou: von oben nach unten
- ou-rl: von oben nach unten und von rechts nach links

Wenn Sie einen Schriftmodus für das gesamte Dokument festlegen wollen,
wählen Sie “Schriftmodus \> Schriftmodus für aktives Dokument wählen”,
bevor Sie die Markierungen erstellen. Dies wird den Schriftmodus für
alle Markierungen ändern, die noch keinen Schriftmodus haben. Alle
danach erstellten Markierungen werden diesen Wert annehmen. Wenn Sie den
Schriftmodus nur für eine gewählte Markierung ändern wollen, wählen Sie
“Schriftmodus \> Schriftmodus für aktive Markierung(en)” wählen.

Rotationswinkel einstellen

Es gibt zwei Möglichkeiten, eine Rechteck-Markierung um ihre Mittelachse
zu rotieren: über einen Assistenten oder durch eine Rotation um eine
virtuelle Achse durch Bewegen der Maus. Der Assistent “Rotation” kann
nur geöffnet werden, wenn eine Markierung ausgewählt ist durch

- Rechtsklicken in der Bild-Sicht und Wählen von “Rotation” und
    “Rotation für aktive Markierung(en) aktivieren”
- Klicken des Feldes mit der Anzeige des Rotationswinkels in der
    Statusleiste der Bild-Sicht. Der Standardwert ist 0^(°). Über das
    Kontextmenü oder \[Umschalttaste+P\] können Sie den Rotationswinkel
    in der [Statusleiste der
    Bild-Sicht](Statusleiste-der-Bild-Sicht_40220647.md) ein- oder
    ausblenden.

![](attachments/40220641/40436832.png)

|                                |
|--------------------------------|
| **Rotationswinkel einstellen** |

Der Winkel kann über einen Rollbalken eingestellt werden. Sie können
auch gewählte Rechteck-Markierungen mit der Maus nach Klicken von
“Rotation \> Rotationsmodus (De)aktivieren " im Kontextmenü oder durch
Drücken von \[Umschalttaste+R\] rotieren.

Der Ebenen-Editor

Es ist möglich, logische Gruppen von Verknüpfungen (beispielsweise Verse
oder Kommentare) unter Verwendung des Ebenen-Editors zu erstellen. Durch
Klicken des Felds mit der Anzeige der Ebenen in
der [Statusleiste](https://doc.textgrid.de/search.html?q=Status+Bar+of+the+Image+View) am
rechten Rand der Bild-Sicht können Sie den Ebenen-Editor öffnen.
Alternativ können Sie “Textebenen \> Ebenen editieren” im Kontextmenü
der Bild-Sicht wählen.

![](attachments/40220641/40436831.png)

|                   |
|-------------------|
| **Ebenen-Editor** |

Im oberen Bereich des Assistenten werden alle existierenden Ebenen
aufgelistet. Im unteren Bereich des Assistenten können neue Ebenen
erstellt werden. Um eine neue Ebene zu erstellen, klicken Sie
“Neu/Übernehmen”. Die Ebene erhält automatisch eine Nummer. Klicken Sie
in das “Name”-Textfeld, um einen Namen für die Ebene einzutragen. Durch
Klicken von “Farbe” wird eine Palette geöffnet, aus der eine Farbe
ausgewählt werden kann. Standardmäßig ist diese Ebene sichtbar. Klicken
Sie das Feld „Aktiv“, um die Ebene zu aktivieren. Nur eine Ebene kann zu
einem bestimmten Zeitpunkt aktiv sein. Um eine Ebene zu entfernen,
klicken Sie die Schaltfläche “Entfernen”.

Sie können Ebenen über die [Titelleiste der
Bild-Sicht](Titelleiste-der-Bild-Sicht_40220645.md) ein- oder
ausblenden. Drücken Sie \[Umschalttaste+L\], um alle Ebenen
einzublenden.

![](attachments/40220641/40436830.png)

|                                  |
|----------------------------------|
| **Arbeiten mit mehreren Ebenen** |

## Attachments

- - [tble-dockinglinesettings-wizard.png](attachments/7439386/9240684.png)
(image/png)  
- [tble-setrotationangle-wizard.png](attachments/7439386/9240685.png)
(image/png)  
- [tble-layereditor-wizard.png](attachments/7439386/9240686.png)
(image/png)
- [tble-twolayers.png](attachments/7439386/9240687.png) (image/png)  
- [tble-dockinglinesettings-wizard.png](attachments/7439386/7766158.png)
(image/png)  
- [tble-setrotationangle-wizard.png](attachments/7439386/7766159.png)
(image/png)  
- [tble-layereditor-wizard.png](attachments/7439386/7766160.png)
(image/png)  
- [tble-twolayers.png](attachments/7439386/7766161.png) (image/png)  
