# Philologische Werkzeuge

last modified on Aug 04, 2015

Dieses Kapitel beschreibt Werkzeuge für Forschung im Bereich Textkritik,
insbesondere für die Kollationierung von Textzeugen und die Darstellung
von Glossen.

- [CollateX (de)](40220881.md)
- [Kopie von MSCollateX](Kopie-von-MSCollateX_40220901.md)
