# Kopie von MSMEISE

Created by Unbekannter Benutzer (annabelköppel) on Jul 24, 2015

|                        |                                                                                                                                                                                                                |
|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                  | MEISE Noteneditor                                                                                                                                                                                              |
| Beschreibung           | Mit dem Noten-Editor MEISE können Notentexte in MEI graphisch kodiert, bearbeitet und auf einem einfachen Niveau auch dargestellt werden. So wird u.a. die Visualisierung von Varianten erheblich erleichtert. |
| Logo                   | ![](attachments/40220955/40437066.png)                                                                                                                                                                         |
| Lizenz                 | [https://www.gnu.org/licenses/gpl-3.0-standalone.html](https://www.gnu.org/licenses/gpl-3.0-standalone.md)                                                                                                   |
| Sourcecode             | [http://sourceforge.net/projects/meise/files/](http://sourceforge.net/projects/meise/files/)                                                                                                                   |
| Beispieldateien        | [http://music-encoding.org/documentation/samples](http://music-encoding.org/documentation/samples)                                                                                                             |
| Installationsanleitung | [Install the MEI Score Editor](Install-the-MEI-Score-Editor_9012776.md)                                                                                                                                      |
| Dokumentation          | [MEI Score Editor (Musical Note Editor)](7439804.md)                                                                                                                                                         |
| Tutorials              | [http://music-encoding.org/support/MEI1st](http://music-encoding.org/support/MEI1st)                                                                                                                           |
| Entwickler             | Musikwissenschaftliches Seminar Detmold/Paderborn, Julian Dabbert, Nikolaos Beer                                                                                                                               |
| Homepage/Kontakt       | [https://sourceforge.net/projects/meise/](https://sourceforge.net/projects/meise/)[, nikolaos.beer@upb.de](mailto:nikolaos.beer@upb.de)                                                                        |

## Attachments

- [digilib-logo-big.png](attachments/40220955/40437065.png) (image/png)  
- [meise_64.png](attachments/40220955/40437066.png) (image/png)  
