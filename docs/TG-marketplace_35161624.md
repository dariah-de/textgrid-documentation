# TG-marketplace

last modified on Jul 21, 2017

The marketplace Interface *msInterface* is the connector between the
TextGridLab and the repository that contains additional plugins
([Installing Additional Tools](Installing-Additional-Tools_9929273.md)). This has been
rewritten from scratch, because the old one
([http://sourceforge.net/p/marketplace-cat/home/Home/](http://sourceforge.net/p/marketplace-cat/home/Home/))
is not maintained any more and also buggy.

The new implementation is pretty lightweight, consisting of three files:
a configuration, a script (Python) and an htaccess server configuration
file which handles the rewriting of links. Information about the
external plugins is maintained in this confluence wiki
([https://dev2.dariah.eu/wiki/label/TextGrid/marketplace](https://dev2.dariah.eu/wiki/label/TextGrid/marketplace))
and parts of these pages are parsed and offered to the Lab. Source code
(GPL v2) is at
[https://projects.gwdg.de/projects/textgridlab-marketplace/repository](https://projects.gwdg.de/projects/textgridlab-marketplace/repository).
To add this marketplace to the Lab, go to its config file
(textgridlab.ini) and add the line

    -Dorg.eclipse.epp.internal.mpc.core.service.DefaultCatalogService.url=http://textgridlab.org/marketplace2/

If started, the new marketplace will show as an additional one in the
lower window of the Marketplace:

![](attachments/35161624/39747801.png)

## Installation

The script itself is written in Python3 and will run as a cgi script on
an Apache server. Apart from modules of the standard library, the only
external module required is lxml. We also need to install the Apache
module for Python. On a Debian-based Linux system, these can be
installed with

    sudo apt-get install libapache2-mod-python python3 python3-lxml

Clone the masterbranch of the software to anywhere you like on the
server:

    git clone git@git.projects.gwdg.de:textgridlab-marketplace.git

Set up the directory structure in the DocumentRoot and copy files to it.

    sudo mkdir -p /var/www/marketplace/cgi
    sudo cp ms.conf msInterface.cgi /var/www/marketplace/cgi
    sudo cp htaccess /var/www/marketplace/.htaccess

It's also neat to have a little icon (enhances the recognition value).
It must be in png and of 32x32 pixel size. This goes in the root
directory. The one here is just an example.

    cd /var/www/marketplace
    sudo wget http://ocropus.rz-berlin.mpg.de/~kthoden/m2/files/tg32.png

Apply to the directory the appropriate rights so that the webserver can
read and write data. The cgi directory will also contain cache and
logfiles.

    sudo chown -Rv www-data:www-data marketplace

Modify three files.

1. `cgi/ms.conf `contains the configuration of the script and is well
    commented. Customize it to your needs.
2. `.htaccess `contains the rewrite rules and have to be customized to
    the URLs where the service is running.
3. The configuration of the server has to be extended by two additional
    directives:

     <Directory /var/www/marketplace/>
             Options Indexes FollowSymLinks MultiViews
             AllowOverride All
             Order allow,deny
             allow from all
     </Directory>
     <Directory "/var/www/marketplace/cgi/">
        SetHandler cgi-script
        AllowOverride All
        Options +ExecCGI
    </Directory>

Two additional apache modules have to be activated and the server then
restarted:

    cd /etc/apache2/mods-enabled
    sudo ln -s ../mods-available/cgi.load .
    sudo ln -s ../mods-available/rewrite.load .
    sudo /etc/init.d/apache2 restart

Check the functionality by browsing to

[http://textgridlab.org/marketplace/cgi/msInterface.cgi?action=main](http://textgridlab.org/marketplace/cgi/msInterface.cgi?action=main)

or

[http://textgridlab.org/marketplace/cgi/msInterface.cgi?action=list&type=favorites](http://textgridlab.org/marketplace/cgi/msInterface.cgi?action=list&type=favorites)

If this works, you might want to try out if the redirects work:

[http://textgridlab.org/marketplace/api/p](http://textgridlab.org/marketplace/api/p)

[http://textgridlab.org/marketplace/](http://textgridlab.org/marketplace/)
should take you to the Confluence wiki.

Errors are written to Apache's error log.

## Adding new Plugins

The webservice does not have to be touched to add new plugins. This is
all handled in the Wiki.

1. Add a new page in the same style of one of the pages you find at
    [https://dev2.dariah.eu/wiki/label/TextGrid/marketplace](https://dev2.dariah.eu/wiki/label/TextGrid/marketplace).
    Fill in as many fields in the table as possible! We want to be as
    informative as we can and help the users using your great tool!
2. Write down the id of the wiki page you are editing (step 1). You
    will see the id if you edit your plugin description page in
    Confluence. Look at the URL field in the browser:
    `https://dev2.dariah.eu/wiki/pages/editpage.action?pageId=35161624`
    pageId is the relevant one, the id of the page would be `35161624`.
3. The page [TGMPL](TGMPL_36342854.md) keeps a list of all the
    plugins and is the source for the plugins listed in the TG
    marketplace. Add a new line for your new plugin. Increment the `id`
    by one, featured is not yet implemented, insert the correct pageId
    (step 2). Make sure that installableUnit matches the namespace
    declaration of you plugin (eg.
    com.example.textgrid.mynewplugin.feature). The available category
    ids are listed if you browse to
    [https://textgridlab.org/marketplace/cgi/msInterface.cgi?action=main](https://textgridlab.org/marketplace/cgi/msInterface.cgi?action=main).
    Make sure that you provide the correct updateUrl which points to
    your eclipse plugin update site. You can generate an update site via
    Eclipse:
    [https://wiki.eclipse.org/FAQ_How_do_I\_create_an_update_site\_(site.xml)%3F](https://wiki.eclipse.org/FAQ_How_do_I_create_an_update_site_(site.xml)%3F).
    After building your plugin, copy the files from the update site
    folder into a public accessible web server. It is also possible to
    deploy the update site via Github pages.
4. Refresh the cache of the Marketplace by directing your browser to
    [https://textgridlab.org/marketplace/cgi/msInterface.cgi?action=cache_reload](https://textgridlab.org/marketplace/cgi/msInterface.cgi?action=cache_reload)

## Attachments

- [Bildschirmfoto 2015-04-30 um 10.49.56.png](attachments/35161624/39747801.png) (image/png)  
