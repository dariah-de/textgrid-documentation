# Publikationswerkzeug SADE

last modified on Jul 28, 2015

Das Publikationswerkzeug SADE ist ein TextGridLab-Modul zur
Veröffentlichung von Daten aus dem TextGridLab auf individuelle
SADE-Installationen ([http://www.bbaw.de/telota/projekte/digitale-Editionen/sade](http://www.bbaw.de/telota/projekte/digitale-editionen/sade)).

- [Publikationswerkzeug SADE installieren](Publikationswerkzeug-SADE-installieren_40220505.md)
- [SADE-Server (de)](40220507.md)
- [Publikationswerkzeug SADE öffnen](40220509.md)
- [Publikationswerkzeug SADE verwenden](Publikationswerkzeug-SADE-verwenden_40220511.md)
- [Interaktion des Publikationswerkzeugs SADE mit anderen Komponenten](Interaktion-des-Publikationswerkzeugs-SADE-mit-anderen-Komponenten_40220513.md)
- [SADE-TextGrid-Referenzinstallation](SADE-TextGrid-Referenzinstallation_40220515.md)
