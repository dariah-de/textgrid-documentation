# Interaction of the Unicode Character Table View with Other Components

last modified on Mai 03, 2016

Die Unicode-Zeichen-Tabelle-Sicht wird automatisch geöffnet, wenn Sie
den [XML-Editor](XML-Editor_40220521.md) geöffnet haben. Auf diese
Weise können Zeichen in einen XML-Text eingefügt werden, ohne eine
weitere Sicht zu öffnen.
