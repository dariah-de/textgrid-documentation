# Functional tests with Sikuli

last modified on Jun 01, 2012

[Sikuli](http://sikuli.org/) is a visual technology to automate and test
graphical user interfaces (GUI) using images.

This tool is used to test the behavior of certain Textgrid Lab tools. To
simulate multiple users, each test scenario is run on 21 iMacs with OS X
10.6.

Each client is receives different user credentials from a webservice.

The tests can be found
in [subversion](https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/tests/funktional/).

- Create
  1. start the lab
  2. login
  3. select the XML-Editor
  4. create new Project (File -\> New Project ...)
  5. create new object in this project (rightclick New Object...)
  6. save newly created object
  7. delete object
  8. delete project
  9. close lab
- Edit Metadata
    1. **Create** steps 1. - 6.
    2. open Metadata Editor
    3. add new title
    4. add three identifier (ISBN, ISSN, URL)
    5. add two rights holder
    6. add notes
    7. save
    8. remove text of second title (do not remove field)
    9. save and accept the error message.
    10. fill second title field again
    11. save
    12. delete object and project
    13. close lab
- Import
    1. **Create** steps 1. - 4.
    2. open import view (File -\> Import local files ...)
    3. select project
    4. add testfiles
    5. import
    6. double click on the imported .tgl - Objekt
    7. wait until everything is open.
    8. close TBLE view
    9. delete all objects
    10. delete project
    11. close lab
- Import of a large XML document (9.5 MB)  
    1. **Import** steps 1. - 3.
    2. add XML file
    3. import
    4. double click on imported XML document
    5. wait for a message
    6. click on open in text editor
    7. delete XML document
    8. delete project
- Import, export and reimport  
    1. **Import** steps 1. - 5.
    2. mark all objects and open export view (right click -\> Export
        from TextGrid ...)
    3. Browse ... -\> create “export” directory
    4. export
    5. delete all objects
    6. import exported objects
    7. **Import** steps 6. - 11.
- Import, export and reimport as an other user  
    1. “**Import, export and reimport**” steps 1. - 5.
    2. Login as an other user
    3. “**Import, export and reimport**” steps 6. - 7.
- Copy and paste  
    1. **Create** steps 1. - 4.
    2. use TG-Search to search for "Goethe"
    3. copy an object from the result
    4. paste it in the TestProject
    5. copy an object from the repository
    6. paste it in the TestProject
    7. **Create** steps 7. - 9.
