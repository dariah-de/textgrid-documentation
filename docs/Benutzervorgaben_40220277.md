# Benutzervorgaben

last modified on Aug 18, 2015

Die Eclipse-basierte Nutzeroberfläche des TextGridLab verwendet für
seine allgemeine Darstellung die Systemeinstellungen und bietet den
Assistenten *Benutzervorgaben*, um die Darstellung jener Elemente der
Oberfläche anzupassen, die Eclipse- oder TextGridLab-spezifisch sind.

## Lab-übergreifende Schriftgröße anpassen

Die Lab-übergreifende Schriftgröße wird beispielsweise im
[Navigator](40220331.md) und in den [Menüs](40220246.md) verwendet
und kann nur über die Systemsteuerung Ihres Betriebssystems angepasst
werden:

- Unter Linux hängt die Einstellung von der Benutzeroberfläche Ihrer
    Linux-Distribution ab, beispielsweise müssen Sie dafür unter Gnome 3
    den Reiter *Fonts* der Tweak Tool oder des Unity Tweak Tools unter
    Ubuntu/Unity verwenden.
- Für Mac OS [gibt es ein Werkzeug eines Drittanbieters zur Anpassung der Systemschrift](http://apple.stackexchange.com/questions/52602/how-can-i-increase-my-font-sizes-without-decreasing-my-resolution).
- Unter Windows können Sie über die Systemsteuerung die [globale Schriftgröße vergrößern](http://windows.microsoft.com/en-us/windows7/make-the-text-on-your-screen-larger-or-smaller?v=t)
    oder [die Farben und Schriften ändern, die für bestimmte Teile der Nutzeroberfläche verwendet werden](#1TC=windows-7). Die meisten
    Bedienelemente des TextGridLab verwenden die Schrifteinstellung, die
    für die Inhalte von Dialogfenstern des Betriebssystems eingestellt
    sind.

## Schrift in Editoren anpassen und andere spezifische Einstellungen der Nutzeroberfläche

Zur Feinabstimmung von Elementen der Nutzeroberfläche, die Eclipse- oder
TextGridLab-spezifisch sind öffnen Sie den Assistenten Benutzervorgaben
über den Unterpunkt „Benutzervorgaben“ im Menü „Fenster“ (oder
"TextGridLab" unter Mac OS) in der Menüleiste. Der Assistent ist
detailliert beschrieben unter

[http://Hilfe.eclipse.org/indigo/index.jsp](http://help.eclipse.org/indigo/index.jsp)

In diesem Assistenten können Sie Benutzervorgaben ändern, die wichtig
für TextGrid sind. Wenn Sie beispielsweise die Schriftgröße in der
[Quelle-Ansicht](Quelle-Ansicht_40220547.md) des
[XML-Editors](XML-Editor_40220521.md) ändern wollen, wählen Sie
"Allgemein" im Assistenten. Wählen Sie dann "Darstellung" und danach
"Farben und Schriftarten" in der Baumstruktur auf der linken Seite.
Wählen Sie "Basis" und "Text Schriftart" aus der Liste in der Mitte des
Assistenten aus. Klicken Sie "Bearbeiten", um die Schriftgröße zu ändern
und "OK" oder "Anwenden", um die Benutzervorgaben zu speichern.

![](attachments/40220277/40436659.png)

## Attachments

- [gs-preferences-wizard.png](attachments/40220277/40436659.png)
(image/png)  
- [preferences.png](attachments/40220277/40436660.png) (image/png)  
