# Integrating Browser Based Tools

last modified on Jul 28, 2015

Some projects choose to implement most of their user interface using a
web-based approach and just implement a thin layer in the TextGridLab.
To do so, you can make use of the SWT *Browser Control*. The browser
control lets you display arbitrary websites, observe their load status
and changed URLs and it allows you to call JavaScript code on the web
site. There is a limited set of simple data types that can be exchanged
with the JavaScript functions you call.

However, you should be aware that you will have to deal with different
browsers on the various operation systems and that this *does not*
correspond to the user's preferred browser. On Windows, the control will
be based on Internet Explorer, on Linux on the WebKit implementation
that ships with the OS's GTK library and on MacOS on the webkit
implementation that comes with the operating system.

If you follow this approach to edit TextGrid objects, you will typically
want to

- implement an EditorPart in Eclipse that features a Browser Control
    as its UI
- implement the editor input handling by adapting to TextGridObject
    and passing the object's URL to the webapp displayed in the browser
- call RBACSession.getSessionID() to implement user login and pass the
    session id to the webapp.

An example project is the [RDF input mask developed for the Blumenbach project](https://projects.gwdg.de/projects/blumenbachiana-textgridlab-plugin).
