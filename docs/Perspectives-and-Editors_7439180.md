# Perspectives and Editors

last modified on Feb 28, 2018

Eine spezifische Zusammenstellung von Komponenten der Nutzeroberfläche
ist eine sogenannte Perspektive. Im Allgemeinen besteht eine Perspektive
immer aus mehreren [Sichten](Sichten_40220269.md). Eine Perspektive
kann durch Rechtsklicken des geöffneten Reiters in der
Perspektivenleiste angepasst (gespeichert, geschlossen und in ihren
ursprünglichen Zustand zurückversetzt) werden. Das
Symbol ![](attachments/40220267/40436658.png "036-schliesse-TextGridLab.png") in
der Werkzeugleiste kann verwendet werden, um eine Perspektive zu
schließen.

Editoren wie der [XML Editor](XML-Editor_40220521.md) sind
Perspektiven bis auf wenige Unterschiede sehr ähnlich:

- Solange sie nicht verändert wurden, öffnen sich alle Editoren im
    mittleren Bereich, üblicherweise in der Mitte des Bildschirms, und
    werden als übereinander angeordnete Reiter angezeigt.
- Editoren müssen Teil einer Perspektive sein, ansonsten werden sie
    wieder geschlossen.
- Während es üblicherweise nur eine Sicht gibt, deren Inhalt vom
    ausgewählten Objekt abhängt, können Sie einen eigenen Editor für
    jedes Objekt öffnen, das Sie bearbeiten wollen.
- Der Editor bleibt geöffnet, bis Sie explizit ihn oder das gesamte
    Laboratory schließen.

Wenn Editoren ungespeicherte Inhalte beinhalten, ist ihr Reiter mit
einem Asterisk (\*) gekennzeichnet.

## Attachments

- [036-schliesse-TextGridLab.png](attachments/7439180/8192277.png)
(image/png)  
