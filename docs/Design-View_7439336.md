# Design View

last modified on Mai 04, 2016

Die Entwurf-Ansicht bietet in der linken Spalte einen Überblick über die
Baumstruktur des Dokuments. Die Inhalt-Spalte auf der rechten Seite
zeigt das Inhaltsmodell von Elementen, die Werte der Attribute und den
Textinhalt an.

Knoten können durch Klicken der Symbole + und – vor ihnen geöffnet und
geschlossen werden. Sie können die Werte von Attributen, den Inhalt von
Verarbeitungsanweisungen und den Textinhalt von Elementen direkt durch
Eingabe in das entsprechende Textfeld in der Spalte ‘Inhalt’ bearbeiten.
Sie können die Position von XML-Objekten durch Auswahl des Elements
mittels Ziehen und Ablegen an die neue graphisch hervorgehobene Position
verändern

![](attachments/40220541/40436782.png)

|                     |
|---------------------|
| **Entwurf-Ansicht** |

- [Features of the Design  View](Features-of-the-Design-View_8129674.md)
- [Context Menu of the Design View](Context-Menu-of-the-Design-View_8129677.md)

## Attachments

- [xml-designview.png](attachments/7439336/9240680.png) (image/png)  
- [xml-designview.png](attachments/7439336/7766130.png) (image/png)  
