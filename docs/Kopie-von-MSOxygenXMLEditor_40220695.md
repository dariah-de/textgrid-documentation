# Kopie von MSOxygenXMLEditor

Created on Jul 23, 2015

|                                    |                                                                                                                                                                                                     |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | Oxygen XML Editor                                                                                                                                                                                   |
| Beschreibung                       | Der Oxygen XML Editor ist eine vollständige XML-Entwicklungsumgebung, die alle Werkzeuge mitbringt, um mit einer großen Menge von XML-Standards und -Technologien arbeiten zu können.               |
| Logo                               | ![](attachments/40220695/40436890.png)                                                                                                                                                              |
| Lizenz                             | [http://www.oxygenxml.com/eula.html](http://www.oxygenxml.com/eula.md)                                                                                                                            |
| Systemanforderungen                | Note: If you get timeouts while installing this software (e.g., "an error occurred while collecting items to be installed"), it might help to temporarily deactivate your local antivirus software. |
| Sourcecode                         |                                                                                                                                                                                                     |
| Projekte, die das Plugin verwenden |                                                                                                                                                                                                     |
| Beispieldateien                    |                                                                                                                                                                                                     |
| Installationsanleitung             | [Install oXygen](Install-oXygen_8130370.md)                                                                                                                                                       |
| Dokumentation                      | [oXygen](oXygen_7440092.md)                                                                                                                                                                       |
| Screenshots                        | ![](attachments/40220695/40436891.png)                                                                                                                                                              |
| Tutorials                          |                                                                                                                                                                                                     |
| Entwickler                         | Syncro Soft                                                                                                                                                                                         |
| Mitentwickler                      |                                                                                                                                                                                                     |
| Homepage/Kontakt                   | [http://www.oxygenxml.com/xml_editor.html](http://www.oxygenxml.com/xml_editor.md)                                                                                                                |
| Bugtracker                         |                                                                                                                                                                                                     |

## Attachments

- [collatex_logo_small.png](attachments/40220695/40436885.png)
(image/png)  
- [meise_64.png](attachments/40220695/40436886.png) (image/png)  
- [digilib-logo-big.png](attachments/40220695/40436887.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/40220695/40436888.png)
(image/png)  
- [screenshotSADE.png](attachments/40220695/40436889.png) (image/png)  
- [IconOxygen70.png](attachments/40220695/40436890.png) (image/png)  
- [screenshot-oxygenxml_text_editor.png](attachments/40220695/40436891.png)
(image/png)  
