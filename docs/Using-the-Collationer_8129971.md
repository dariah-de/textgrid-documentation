# Using the Collationer

last modified on Mai 04, 2016

CollateX kann für die Kollationierung bestehender Text-Objekte ebenso
verwendet werden wie für die Standardisierung der Lesarten der Texte.

- [Collating Texts](Collating-Texts_8129973.md)
- [Standardize the Collation Result](Standardize-the-Collation-Result_8129975.md)
