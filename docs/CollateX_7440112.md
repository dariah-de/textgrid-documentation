# CollateX

last modified by Christoph Kudella on Mai 04, 2016

CollateX ist eine Java-Software zur Kollationierung von Textquellen,
beispielsweise um einen kritischen Apparat zu erstellen. Es wurde
gemeinsam von mehreren Partnerinstitutionen und Einzelpersonen unter dem
Schirm der europäischen Initiative "Interedition" entwickelt. Für
weitere Informationen, siehe

[http://collatex.sourceforge.net/](http://collatex.sourceforge.net/)

- [Install CollateX](Install-CollateX_8129948.md)
- [Open CollateX](Open-CollateX_8129959.md)
- [The CollateX Perspective](The-CollateX-Perspective_8129962.md)
- [Collation Set and Equivalence Set Component](Collation-Set-and-Equivalence-Set-Component_8129965.md)
- [Collation Result Component](Collation-Result-Component_8129967.md)
- [Using the Collationer](Using-the-Collationer_8129971.md)
- [Interaction of CollateX with Other Components](Interaction-of-CollateX-with-Other-Components_8129980.md)
