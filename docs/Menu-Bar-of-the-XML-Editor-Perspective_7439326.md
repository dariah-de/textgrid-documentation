# Menu Bar of the XML Editor Perspective

last modified on Mai 04, 2016

Wenn Sie den XML-Editor öffnen, werden bestimmte Elemente in der
Menüleiste aktiviert. Einige dieser Funktionen finden Sie unter
“Bearbeiten” und einige unter “XML”.

**Bearbeiten**

Wenn der XML-Editor geöffnet wird, können abhängig von der Ansicht, die
in der [Editor-Sicht](Editor-Sicht_40220539.md) geöffnet ist, Optionen
unter dem Punkt “Bearbeiten” in der Menüleiste genutzt werden. Die
meisten Elemente werden angezeigt, wenn die XML-Datei in
der [Quelle-Ansicht](Quelle-Ansicht_40220547.md) geöffnet wird. Diese
Elemente werden hier aufgelistet:

- Rückgängig ![](attachments/40220529/40436767.png "037-zuruecknehme-Aenderung.png") und
    Wiederholen ![](attachments/40220529/40436766.png "038-wiederhole-Aenderung.png"):
    Um im Dokument gemachte Änderungen rückgängig zu machen oder zu
    wiederholen, wählen Sie “Bearbeiten \> Rückgängig” oder
    “Bearbeiten \> Wiederholen” in der Menüleiste.
- Ausschneiden ![](attachments/40220529/40436765.png "039-ausschneide-Auswahl.png"),
    Kopieren ![](attachments/40220529/40436764.png "040-kopiere-Auswahl.png") und
    Einfügen ![](attachments/40220529/40436763.png "041-einfuege-Auswahl.png"):
    Wählen Sie „Bearbeiten \> Ausschneiden”, „Bearbeiten \> Kopieren”
    und „Bearbeiten \> Einfügen” in der Menüleiste, um Teile des
    Quelltextes zu arrangieren.
- Löschen ![](attachments/40220529/40436762.png "042-loesche-Auswahl.png") und
    Alles
    auswählen ![](attachments/40220529/40436761.png "043-auswaehle-BereichGesamt3.png"):
    Um Teile des Texts zu löschen, wählen Sie „Bearbeiten \> Löschen”.
    Wählen Sie „Bearbeiten \> Alles auswählen ”, um den gesamten
    Quelltext eines XML-Dokument zu markieren.
- Suchen und
    Ersetzen ![](attachments/40220529/40436760.png "047-finde-Auswahl.png"):
    Um den Assistent “Suchen und Ersetzen” zu öffnen, wählen Sie
    „Bearbeiten \> Suchen und Ersetzen ” in der Menüleiste. Sie können
    den zu suchenden Text angeben und bei Bedarf den ersetzenden Text.
    Sie können Folgendes festlegen:
    - die Suchrichtung (“Vorwärts” oder “Rückwärts” von der aktuellen
        Cursorposition)
    - den Bereich (verwenden Sie “Alles” oder “Ausgewählte Zeilen”, um
        innerhalb eines ausgewählten Bereichs zu suchen)
    - ob Ihre Suche Groß- und Kleinschreibung beachten soll
    - ob die Suchzeichenkette als einzelnes Wort gefunden werden soll
        “Ganzes Wort”
    - ob Sie die “Suche am Dokumentanfang fortsetzen” wollen, wenn das
        Ende der Datei erreicht ist
    - ob der Editor während der Eingabe der Zeichenkette zum ersten
        vollständigen Auftreten des Suchtextes springen soll, indem Sie
        “Schrittweise” wählen
    - ob “Reguläre Ausdrücke” gesucht werden sollen Geben Sie
        Strg+Leertaste im Eingabefeld ein, um sich von einer
        Inhaltshilfe alle möglichen (regulären) Ausdrücke anzeigen zu
        lassen
- Auswahl erweitern auf: Um Passagen in der Quelle-Ansicht zu
    markieren, ermöglicht Ihnen die Option “Auswahl erweitern auf” das
    einschließende, das nächste oder das vorherige Element auszuwählen.
    Zusätzlich können Sie die letzte Auswahl wiederherstellen.
- Inhaltshilfe: Für weitere Informationen,
    siehe [Quelle-Ansicht](Quelle-Ansicht_40220547.md).
- QuickInfo-Beschreibung anzeigen: Zeigt die QuickInfo-Beschreibung
    an, sofern verfügbar. Sie zeigt den Wert an der aktuellen Stelle des
    Mauszeigers an.
- Wortabschluss: Wählen Sie diesen Punkt, um vom Programm Vorschläge
    für die Vervollständigung eines Wortes zu erhalten.
- Schnellkorrektur: Für weitere Informationen,
    siehe [Quelle-Ansicht](Quelle-Ansicht_40220547.md).
- Intelligenter Einfügemodus: Für weitere Informationen,
    siehe [Quelle-Ansicht](Quelle-Ansicht_40220547.md).

**XML**

Die folgenden Funktionen stehen unter dem Punkte “XML” in der Menüleiste
zur Verfügung:

- Elemente
    einfügen ![](attachments/40220529/40436759.png "052-einfuege-Elemente.png"):
    Zeigt eine Liste aller möglicher Elemente.
- Aktuelles Element umbenennen in...
- Quelle ![](attachments/40220529/40436758.png "054-bearbeite-Quelle.png"):
    Dient der Steuerung der Kommentare und der Formatierung des
    Quelltextes. Außerdem können Sie das Dokument
    bereinigen ![](attachments/40220529/40436757.png "058-bereinige-Dokument2.png").
- [Validierungsfehler
    anzeigen](Funktionen-des-XML-Editors_40220527.md) ![](attachments/40220529/40436754.gif "062-zeige-Validierungsfehler.gif")
- [Schema
    assoziieren](Funktionen-des-XML-Editors_40220527.md) ![](attachments/40220529/40436755.gif "063-verbinde-Schema.gif")
- [CSS assoziieren](Funktionen-des-XML-Editors_40220527.md)
- [Adapter
    assoziieren](Funktionen-des-XML-Editors_40220527.md) ![](attachments/40220529/40436753.gif "064-verbinde-Adaptor2.gif")
- URI-Fragment
    kopieren ![](attachments/40220529/40436756.png "066-kopiere-URlFragment2.png"):
    Dupliziert ein URI-Fragment für lokale Adressierung.
- [XML-Editor Debuggen](Funktionen-des-XML-Editors_40220527.md)

## Attachments

- [037-zuruecknehme-Aenderung.png](attachments/7439326/8192218.png)
(image/png)  
- [038-wiederhole-Aenderung.png](attachments/7439326/8192219.png)
(image/png)  
- [039-ausschneide-Auswahl.png](attachments/7439326/8192220.png)
(image/png)  
- [040-kopiere-Auswahl.png](attachments/7439326/8192221.png) (image/png)  
- [041-einfuege-Auswahl.png](attachments/7439326/8192222.png)
(image/png)  
- [042-loesche-Auswahl.png](attachments/7439326/8192223.png) (image/png)  
- [043-auswaehle-BereichGesamt3.png](attachments/7439326/8192224.png)
(image/png)  
- [047-finde-Auswahl.png](attachments/7439326/8192225.png) (image/png)  
- [052-einfuege-Elemente.png](attachments/7439326/8192226.png)
(image/png)  
- [054-bearbeite-Quelle.png](attachments/7439326/8192227.png)
(image/png)  
- [058-bereinige-Dokument2.png](attachments/7439326/8192228.png)
(image/png)  
- [066-kopiere-URlFragment2.png](attachments/7439326/8192229.png)
(image/png)  
- [063-verbinde-Schema.gif](attachments/7439326/8192288.gif) (image/gif)  
- [062-zeige-Validierungsfehler.gif](attachments/7439326/8192289.gif)
(image/gif)  
- [064-verbinde-Adaptor2.gif](attachments/7439326/8192290.gif)
(image/gif)
