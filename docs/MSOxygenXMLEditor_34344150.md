# MSOxygenXMLEditor

last modified on Jan 07, 2019

|                                    |                                                                                                                                                                                                     |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Titel                              | Oxygen XML Editor                                                                                                                                                                                   |
| Beschreibung                       | Der Oxygen XML Editor ist eine vollständige XML-Entwicklungsumgebung, die alle Werkzeuge mitbringt, um mit einer großen Menge von XML-Standards und -Technologien arbeiten zu können.               |
| Logo                               | ![](attachments/34344150/34472161.png)                                                                                                                                                              |
| Lizenz                             | [http://www.oxygenxml.com/eula.html](http://www.oxygenxml.com/eula.md)                                                                                                                            |
| Systemanforderungen                | Note: If you get timeouts while installing this software (e.g., "an error occurred while collecting items to be installed"), it might help to temporarily deactivate your local antivirus software. |
| Sourcecode                         |                                                                                                                                                                                                     |
| Projekte, die das Plugin verwenden |                                                                                                                                                                                                     |
| Beispieldateien                    |                                                                                                                                                                                                     |
| Installationsanleitung             | [Install oXygen](Install-oXygen_8130370.md)                                                                                                                                                       |
| Dokumentation                      | [oXygen](oXygen_7440092.md)                                                                                                                                                                       |
| Screenshots                        | ![](attachments/34344150/34472160.png)                                                                                                                                                              |
| Tutorials                          |                                                                                                                                                                                                     |
| Entwickler                         | Syncro Soft                                                                                                                                                                                         |
| Mitentwickler                      |                                                                                                                                                                                                     |
| Homepage/Kontakt                   | [http://www.oxygenxml.com/xml_editor.html](http://www.oxygenxml.com/xml_editor.md)                                                                                                                |
| Bugtracker                         |                                                                                                                                                                                                     |

## Attachments

- [screenshot-oxygenxml_text_editor.png](attachments/34344150/34472160.png)
(image/png)  
- [IconOxygen70.png](attachments/34344150/34472161.png) (image/png)  
- [screenshotSADE.png](attachments/34344150/34472162.png) (image/png)  
- [sade_logo153-Web-Preview64x64.png](attachments/34344150/34472163.png)
(image/png)  
- [digilib-logo-big.png](attachments/34344150/34472164.png) (image/png)  
- [meise_64.png](attachments/34344150/34472165.png) (image/png)  
- [collatex_logo_small.png](attachments/34344150/34472166.png)
(image/png)  
