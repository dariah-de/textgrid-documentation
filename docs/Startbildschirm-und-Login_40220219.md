# Startbildschirm und Login

last modified on Aug 18, 2015

Nach der Installation des TextGrid Laboratory können Sie die Software
starten. Der Startbildschirm bietet zwei Möglichkeiten:

- In das TextGridLab einloggen,
- Die [*Suche*](https://doc.textgrid.de/search.html?q=Search)
    und die [*Wörterbuch Suche*](https://doc.textgrid.de/search.html?q=Dictionary+Search)
    verwenden, die ohne Login verfügbar sind.

![](attachments/40220219/40436618.png)

|                     |
|---------------------|
| **Startbildschirm** |

Sobald Sie auf "Login" klicken, öffnet sich ein neues Fenster. Sie
können sich mit Ihren TextGrid-Nutzerinformationen (der Nutzername und
das Passwort, das Sie per E-Mail erhalten haben) einloggen, oder Sie
können sich über DFN-AAI (= Authentifikations- und
Autorisierungs-Infrastruktur des Deutschen Forschungsnetzes (DFN))
einloggen. Wenn Sie das Passwort für Ihre TextGrid Nutzerkennung
vergessen haben, wählen Sie bitte „Hilfe \> Authentifizierung“ und
klicken Sie die Schaltfläche "Passwort vergessen?''.

![](attachments/40220219/40436617.png)

|                  |
|------------------|
| **Login Screen** |

Nach Ihrem ersten Login werden Sie aufgefordert, Ihre Benutzerdaten zu
vervollständigen und die "TextGrid-Nutzungsbedingungen" zu akzeptieren.
Wenn Sie die Option “Suchbar” aktivieren, können andere
TextGrid-NutzerInnen Sie finden, wenn Sie nach Ihrem Namen, Ihrer
Institution oder Ihrer E-Mail-Adresse suchen. Die Eingabefelder mit
einem Asterisk (\*) sind Pflichtfelder. Sie können Ihre Benutzerdaten
jederzeit über "Hilfe \> Authentifikation'' in der Menüleiste des Labs
ändern.

![](attachments/40220219/40436616.png)

|                               |
|-------------------------------|
|  **Assistent Nutzermerkmale** |

Nachdem Sie sich erfolgreich eingeloggt haben, erscheint der
Startbildschirm mit Ihrem Benutzernamen in der Mitte. Sie können jetzt
alle Werkzeuge nutzen, die auf dem Startbildschirm angezeigt werden:

|                                                                     |                                                                                                        |
|---------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| ![](attachments/40220219/40436624.png "usermanagement.png")         | [*Projekt- und Benutzer-Verwaltung*](https://dev2.dariah.eu/wiki/pages/viewpage.action?pageId=7439216) |
| ![](attachments/40220219/40436623.png "search.png")                 | [*Suche*](https://doc.textgrid.de/search.html?q=Search)                                         |
| ![](attachments/40220219/40436622.png "text-image-link-editor.png") | [*Text-Bild-Link-Editor*](https://doc.textgrid.de/search.html?q=Text+Image+Link+Editor)         |
| ![](attachments/40220219/40436621.png "xml-editor.png")             | [*XML-Editor*](https://doc.textgrid.de/search.html?q=XML+Editor)                                |
| ![](attachments/40220219/40436620.png "dictionaries.png")           | [*Wörterbuch-Suche*](https://doc.textgrid.de/search.html?q=Dictionary+Search)                   |
| ![](attachments/40220219/40436619.png "aggregations.png")           | [*Aggregationen-Editor*](https://doc.textgrid.de/search.html?q=Aggregations+Editor)             |

Diese Werkzeuge werden in den verschiedenen Kapiteln des Nutzerhandbuchs
und der Online-Hilfe erklärt.

## Attachments

- [DEUTSCHgs-userinformation-wizard.png](attachments/40220219/40436616.png)
(image/png)  
- [gs-login-window.png](attachments/40220219/40436617.png) (image/png)  
- [gs-welcome-screen.png](attachments/40220219/40436618.png) (image/png)  
- [aggregations.png](attachments/40220219/40436619.png) (image/png)  
- [dictionaries.png](attachments/40220219/40436620.png) (image/png)  
- [xml-editor.png](attachments/40220219/40436621.png) (image/png)  
- [text-image-link-editor.png](attachments/40220219/40436622.png)
(image/png)  
- [search.png](attachments/40220219/40436623.png) (image/png)  
- [usermanagement.png](attachments/40220219/40436624.png) (image/png)  
- [gs-userinformation-wizard.png](attachments/40220219/40436625.png)
(image/png)  
