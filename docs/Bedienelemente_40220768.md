# Bedienelemente

last modified on Aug 04, 2015

Das erste Bedienelement links von der Videoanzeige hat üblicherweise
vier Schaltflächen: "Text", "Raster", "Untertitel" und "Zeitleiste".
Wenn das Objekt einen Schwingungsverlauf enthält, sind zwei weitere
Optionen verfügbar: "Schwingungsverlauf" und "Kombiniert". Abhängig vom
aktivierten Datenanzeigemodus ändert sich der Rest des Arbeitsbereichs
entsprechend der angezeigten Daten im geeigneten Modus.

**Text-Modus**

Diese Sicht zeigt eine Spalte für jede der gewählten Schichten. In jeder
Spalte werden die Annotationen einer Schicht zeilenweise angezeigt. Sie
können auf einen Annotation klicken, um den entsprechenden Teil der
Mediendatei abzuspielen. Wenn die Aufnahme abgespielt wird, werden die
relevanten Teile der Annotationen rot hervorgehoben.

![](attachments/40220768/40436913.png)

**Raster-Modus**

Diese Sicht zeigt alle Annotationen von den gewählten Schichten mit
ihren Start- und Endzeiten im Medieninformation-Feld an. Die
Annotationen werden in chronologischer Reihenfolge angezeigt. Sie können
auf eine Annotation klicken, um den entsprechenden Teil des
Medienobjekts abzuspielen. Während das Video weiterläuft, werden die
relevanten Teile hervorgehoben. Zusätzlich können Sie links vom Raster
den Typ einer Schicht aus einem Aufklappmenü wählen.

![](attachments/40220768/40436912.png)

|                  |
|------------------|
| **Raster-Modus** |

**Untertitel-Modus**

Diese Sicht zeigt die Annotationen beim Abspielen des Videos an, als
wären sie Untertitel.

![](attachments/40220768/40436911.png)

|                     |
|---------------------|
| **Untertitel-Modi** |

**Zeitleiste-Modus**

Diese Sicht ähnelt dem Zeitleistenbetrachter in ELAN. Sie zeigt die
Schichten und deren Annotationen an und jede Annotation entspricht einem
bestimmten Zeitintervall. Oberhalb der Schichten wird eine Zeitskala
angezeigt. Während der Wiedergabe bewegt sich eine rote vertikale Linie,
das Fadenkreuz, durch die Annotationen und zeigt den momentanen
Zeitpunkt an.  
Darüber hinaus wird, wenn Sie ein Zeitintervall auswählen, dieses mit
einem blauen Rand markiert. Die entsprechende Start- und Endzeit sowie
die Dauer werden im Medieninformation-Feld angezeigt.

Wenn Sie einen bestimmten Abschnitt ausgewählt haben, sind drei weitere
Optionen verfügbar (links von der Zeitleiste):

- Auswahl abspielen: In der Videoanzeige wird nur der ausgewählte
    Abschnitt angezeigt
- Auswahl löschen: Der blaue Rand verschwindet gemeinsam mit den
    Informationen über Start- und Endzeit etc., die im
    Medieninformation-Feld rechts des Videos enthalten sind
-  Auswahl verknüpfen: Wenn Sie diese Option wählen, wird der Hinweis
    eingeblendet, dass die Adresse des ausgewählten Abschnitts in die
    Zwischenablage kopiert wurde

Des Weiteren gibt es links von der Zeitleiste weitere Optionen, die nur
im Zeitleiste-, Schwingungsverlauf- und Kombinierten Modus zur Verfügung
stehen:

\>\| und \|\< (Zeitleiste-Sicht zum Ende/Anfang bewegen)

\>\> und \<\< (Zeitleiste-Sicht vorwärts/rückwärts bewegen)

\> und \< (Zeitleiste-Sicht langsam vorwärts/rückwärts bewegen)

\+ und - (In Zeitleiste-Sicht hinein-/aus Zeitleiste-Sicht herauszoomen)

Die Zeitleiste kann auch in zwei verschiedenen Modi abgespielt werden:

1. Bild für Bild abspielen, in dem das Fadenkreuz (rote vertikale
    Linie) sich an den rechten Rand des Bildschirms bewegen wird und
    sich jedes Mal ändert, oder
2. Kontinuierlich abspielen, in dem das Fadenkreuz in der Mitte des
    Bildschirms verweilen wird und sich die Annotationen in den
    Schichten bewegen.

Unterhalb dieser beiden Betrachtungsmodi gibt es die Möglichkeit, den
Zeichensatz der Schichten zu ändern. Rechts vom Medieninformation-Feld
gibt es ein weiteres Fenster, das es Ihnen erlaubt, bestimmte Schichten
isoliert zu betrachten. Während das Video weiterläuft, werden die
relevanten Teile des Textes der ausgewählten Schicht rot hervorgehoben.

![](attachments/40220768/40436910.png)

|                      |
|----------------------|
| **Zeitleiste-Modus** |

**Schwingungsverlauf und Kombiniert**

Wenn ein Schwingungsverlauf im Dokument enthalten ist, kann dieser im
Schwingungsverlauf-Modus betrachtet werden. Sie können zudem den
Schwingungsverlauf- und den Zeitleiste-Modus mit dem Kombinierten Modus
in einer Ansicht kombinieren.

## Attachments

- [lit-annex-timelinemode.png](attachments/40220768/40436910.png)
(image/png)  
- [lit-annex-subtitlemode.png](attachments/40220768/40436911.png)
(image/png)  
- [lit-annex-gridmode.png](attachments/40220768/40436912.png)
(image/png)  
- [lit-annex-textmode.png](attachments/40220768/40436913.png)
(image/png)  
- [textgrid-manual-screenshot-annex05.jpg](attachments/40220768/40436914.jpg)
(image/jpeg)  
- [textgrid-manual-screenshot-annex04.jpg](attachments/40220768/40436915.jpg)
(image/jpeg)  
- [textgrid-manual-screenshot-annex03.jpg](attachments/40220768/40436916.jpg)
(image/jpeg)  
- [textgrid-manual-screenshot-annex02.jpg](attachments/40220768/40436917.jpg)
(image/jpeg)  
