# Navigator-Sicht

last modified on Aug 19, 2015

Die Navigator-Sicht wird für die Verwaltung von TextGrid-Projekten und
-Objekten verwendet. Alle Projekte, auf die der Nutzer Zugriff hat,
werden mit einem Ordnersymbol in der Baumansicht, die durch Klicken der
Knotenpunkte aus- oder zugeklappt werden kann, angezeigt. Es wird zudem
ein Symbol  angezeigt, das den Inhalt des TextGrid Repository im
Navigator repräsentiert. Die Inhalte werden nach den Namen der Autoren
sortiert aufgelistet. Der Navigator verwendet verschiedene Symbole für
verschiedene Objekttypen, zum Beispiel:

|                                                                                      |                       |
|:-------------------------------------------------------------------------------------|:---------------------:|
| ![](attachments/40220337/40436703.png "xml.png")                                     |     XML-Dokument      |
| **![](attachments/40220337/40436696.gif "113-Dateityp-MEI.gif")**                    |     MEI-Dokument      |
| ![](attachments/40220337/40436692.gif)                                               |    Klartext-Objekt    |
| ![](attachments/40220337/40436702.png "image.png")                                   |      Bild-Objekt      |
| **![](attachments/40220337/40436695.png "069-zeige-BildLinkEditorPerspektive.png")** | Text-Bild-Link-Objekt |
| ![](attachments/40220337/40436701.png "108-ist-Edition.png")                         |        Edition        |
| ![](attachments/40220337/40436700.png "130-ist_Werk.png")                            |         Werk          |
| ![](attachments/40220337/40436699.png "109-ist-Collection.png")                      |      Kollektion       |
| ![](attachments/40220337/40436698.png "107-ist-Aggregation.png")                     |      Aggregation      |

Für weitere Informationen zu “Edition”, “Werk”, “Kollektion” und
“Aggregation”, siehe [Objekte](Objekte_40220426.md).

- [Menüleiste der Navigator-Sicht](40220340.md)
- [Werkzeugleiste der Navigator-Sicht](Werkzeugleiste-der-Navigator-Sicht_40220343.md)
- [Kontextmenü der Navigator-Sicht](40220346.md)

## Attachments

- [file_obj.gif](attachments/40220337/40436692.gif) (image/gif)  
- [nav_stop.gif](attachments/40220337/40436693.gif) (image/gif)  
- [tree_explorer.gif](attachments/40220337/40436694.gif) (image/gif)  
- [069-zeige-BildLinkEditorPerspektive.png](attachments/40220337/40436695.png)
(image/png)  
- [113-Dateityp-MEI.gif](attachments/40220337/40436696.gif) (image/gif)  
- [163-TextGrid-Repository.gif](attachments/40220337/40436697.gif)
(image/gif)  
- [107-ist-Aggregation.png](attachments/40220337/40436698.png)
(image/png)  
- [109-ist-Collection.png](attachments/40220337/40436699.png)
(image/png)  
- [130-ist_Werk.png](attachments/40220337/40436700.png) (image/png)  
- [108-ist-Edition.png](attachments/40220337/40436701.png) (image/png)  
- [image.png](attachments/40220337/40436702.png) (image/png)  
- [xml.png](attachments/40220337/40436703.png) (image/png)  
