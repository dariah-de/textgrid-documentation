# Quelle-Ansicht

last modified on Jul 31, 2015

In der Quelle-Ansicht können Sie den Quelltext eines Dokuments direkt
manuell bearbeiten sowie Elemente und Attribute einfügen oder entfernen.
Zeilen können durch Klicken der Symbole + und – am linken Rand geöffnet
und geschlossen werden. Eine Schema-abhängige Inhaltshilfe unterstützt
Sie durch eingeblendete Hinweise und eine Liste von Vorschlägen beim
Hinzufügen neuer Elemente.

![](attachments/40220547/40436785.png)

|                |
|----------------|
| Quelle-Ansicht |

- [Funktionen der Quelle-Ansicht](Funktionen-der-Quelle-Ansicht_40220549.md)
- [Kontextmenü der Quelle-Ansicht](40220551.md)

## Attachments

- [xml-sourceview.png](attachments/40220547/40436785.png) (image/png)  
- [xml-hoverhelp-detail.png](attachments/40220547/40436786.png)
(image/png)  
- [xml-validationerror-detail.png](attachments/40220547/40436787.png)
(image/png)  
