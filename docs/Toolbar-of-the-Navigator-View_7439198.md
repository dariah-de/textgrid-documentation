# Toolbar of the Navigator View

last modified on Feb 28, 2018

Die Werkzeugleiste des Navigators bietet Ihnen verschiedene Funktionen.
Wenn ein anderer Nutzer die Inhalte des Repository verändert, müssen Sie
möglicherweise die Navigator-Sicht manuell aktualisieren. Klicken Sie
dafür ![](attachments/40220343/40436708.png "refresh.png") oben in der
Navigator-Sicht. Durch Klicken
von ![](attachments/40220343/40436706.png "collapse-all-nodes.png") können
Sie alle Knotenpunkte zuklappen, und durch Klicken des Dreiecks neben
dem
Symbol **![](attachments/40220343/40436704.gif "tree_explorer.gif")** können
Sie wahlweise die Projekte und Objekte nach Änderungsdatum, Titel, Typ
oder ihrer ursprünglichen Reihenfolge sortieren und wählen, ob auf- oder
absteigend sortiert werden soll. Durch Klicken
von ![](attachments/40220343/40436705.png "filter.png") öffnet sich ein
Assistent „Navigator Filter“, mit dem Sie definieren können, welche
Projekte oder Objekte im Navigator angezeigt werden. Sie können nach
Projektrollen oder Objekttypen filtern.

Im Sicht-Menü, das durch Klicken des weißen Dreiecks aufgeklappt wird,
können Sie die Sicht aktualisieren und anpassen. Die Sicht kann durch
Auswahl eines Filters oder Auswahl der angezeigten Inhalte angepasst
werden. Sie können es außerdem mit einem geöffneten Editor verknüpfen.

## Attachments

- [refresh.png](attachments/7439198/7766081.png) (image/png)  
- [tree_mode.png](attachments/7439198/7766082.png) (image/png)  
- [collapse-all-nodes.png](attachments/7439198/7766083.png) (image/png)  
- [filter.png](attachments/7439198/8192240.png) (image/png)  
- [tree_explorer.gif](attachments/7439198/8192302.gif) (image/gif)  
