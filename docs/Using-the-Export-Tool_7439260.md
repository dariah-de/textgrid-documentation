# Using the Export Tool

last modified on Mai 03, 2016

Führen Sie diese Schritte durch, um TextGrid-Objekte auf ihr lokales
Betriebssystem oder ein Speichermedium zu exportieren:

1. Mit Ziehen und Ablegen können Sie Objekte aus
    dem [Navigator](40220331.md) in die Export-Sicht hinzufügen. Wenn
    Sie eine Aggregation hinzufügen, werden auch alle aggregierten
    Objekte zur Liste hinzugefügt und entsprechende Optionen zur
    Anpassung der Verknüpfungen werden gewählt. Klicken Sie die
    Schaltfläche “Entfernen”, um ein Objekt aus der Liste zu entfernen.
    Unter der Liste von Objekten muss das Zielverzeichnis ausgewählt
    werden. Klicken Sie “Browsen …”, um das Zielverzeichnis anzugeben.
2. Klicken Sie “Export!”, um den Vorgang abzuschließen. Nachdem der
    Export-Vorgang abgeschlossen ist, wird eine Liste mit allen Dateien,
    die exportiert wurden, gemeinsam mit einer Bestätigung oder Warnung
    bzw. Fehlermeldung, wenn der Export-Vorgang nicht erfolgreich war,
    angezeigt. Während des Export-Vorgangs werden von dem Werkzeug zwei
    Dateien für jedes Objekt, das in das Zielverzeichnis auf dem lokalen
    Betriebssystem exportiert wurde, erstellt: eine für die eigentlichen
    Daten, in deren Namen der URI des Objekts integriert ist, und eine
    zusätzliche Datei, die die Metadaten des Objekts im XML-Format
    enthält. Für komplexe Objekte wie Editionen werden Verzeichnisse mit
    deren Inhalten angelegt.
3. Sie können optional die Exportspezifikationen speichern (d. h. die
    Liste aller exportierten Objekte gemeinsam mit dem Umschreiben-Modus
   und den verwendeten Dateinamen).
4. Nachdem eine Datei exportiert worden ist, verbleibt das Objekt im
    TextGrid Repository und damit weiterhin in dem Projekt, aus dem es
    exportiert worden ist.
