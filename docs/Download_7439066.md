# Download

last modified on Feb 26, 2019

Die aktuelle Version des TextGridLab (TextGridLab Base) bietet alle
grundlegenden Komponenten. Das Lab wird für verschiedene Betriebssysteme
als kostenloser Download auf der TextGrid-Website zur Verfügung
gestellt:

[*http://www.textgridlab.org/download/2.0/*](http://www.textgridlab.org/download/2.0/)

Plug-ins und eine Auswahl externer Werkzeuge, die für die Nutzung im
TextGridLab angepasst wurden, können über den Marketplace vom
Startbildschirm oder einem beliebigen anderen Punkt des Labs aus
kostenlos heruntergeladen werden.

**Systemvoraussetzungen**

- **Java**

Seit Version 3.3 packt das TextGridLab Java mit ein, so dass keine
Probleme mehr mit falsch installierten oder fehlenden Java-Paketen
auftreten. Die TextGridLab-Versionen bis 3.2 benötigten
eine **Java-Laufzeitumgebung** (Java Runtime Environment, JRE) der
Version 6 oder höher. Download:

[*http://java.com/download*](http://java.com/download)

- **Linux**

Unter Linux benötigen Sie außerdem Version 1 (1.2 oder höher)
des [*WebKit-GTK+-bindings*](http://webkitgtk.org/) – Version 3.0
ist *nicht* geeignet. Sie werden erkennen, dass die Bibliothek fehlt,
wenn der [Startbildschirm](Startbildschirm-und-Login_40220219.md) leer
bleibt und das Öffnen des Login-Fensters eine Fehlermeldung verursacht.
Unter Ubuntu oder anderen Debian-basierten Distributionen heißt das
Paket libwebkitgtk-1.0-0. Um es zu installieren, können Sie den
entsprechenden Link auf der [Download-Seite](Download_7439066.md),
verwenden, im Software-Center oder Synaptic nach dem Paket suchen, oder
den folgenden Befehl in einem Terminal-Fenster eingeben:

``` syntaxhighlighter-pre
sudo apt-get install libwebkitgtk-1.0-0
```

Als Referenz dient die SWT FAQ
([*http://www.eclipse.org/swt/faq.php#browserlinux*](https://dev2.dariah.eu/#browserlinux))
– TextGridLab 2.0 basiert auf Eclipse 3.7.
