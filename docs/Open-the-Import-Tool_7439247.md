# Open the Import Tool

last modified on Mai 03, 2016

Eine Perspektive muss geöffnet sein, um dieses Werkzeug zu nutzen.
Wählen Sie “Datei \> Lokale Dateien importieren ...” in der Menüleiste.

![](attachments/40220397/40436718.png)

|        |
|--------|
| Import |

## Attachments

- [imp-import.png](attachments/7439247/9240671.png) (image/png)  
- [imp-import.png](attachments/7439247/7766092.png) (image/png)  
