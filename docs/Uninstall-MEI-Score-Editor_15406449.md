# Uninstall MEI Score Editor

last modified on Mai 04, 2016

You can remove the MEI Score Editor from your TextGridLab installation
by way of the *installation detail* dialog:

1. Open the *About TextGridLab* dialog from the *Help* menu (or
    the *TextGridLab* menu on Mac OS)
2. Click the *Installation Details* button to open the corresponding
    dialog
3. On the *Installed Software* page, select MEISE Noteeditor and click
    the *Uninstall* button.
4. The uninstall wizard will list everything to be uninstalled.
    Click *Finish*.
5. After uninstallation, you should restart the TextGridLab as
    recommended.

Note that you cannot remove tools that are TextGridLab base components
or that other tools depend on.
