# Subject-specific Tools and Services

last modified on Feb 28, 2018

In diesem Abschnitt werden die Werkzeuge für spezielle wissenschaftliche
Anforderungen erklärt.

- [Linguistic Tools](Linguistic-Tools_7439958.md)
- [Philological Tools](Philological-Tools_7440108.md)
- [Musicological Tools](Musicological-Tools_7439987.md)
- [Tools for Image Studies](Tools-for-Image-Studies_7440020.md)
- [Miscellaneous tools](Miscellaneous-tools_58493344.md)
