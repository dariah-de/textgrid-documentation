# Suche

last modified on Aug 18, 2015

Das Suche-Werkzeug erlaubt Ihnen, gleichzeitig die Inhalte
(beispielsweise TEI-kodierte Dokumente) und die Objekt-Metadaten im
TextGrid Repository zu durchsuchen.

- [Suche-Perspektive öffnen](40220287.md)
- [Suche-Perspektive](Suche-Perspektive_40220289.md)
- [Suche-Sicht](Suche-Sicht_40220291.md)
- [Suchergebnisse-Sicht](Suchergebnisse-Sicht_40220299.md)
