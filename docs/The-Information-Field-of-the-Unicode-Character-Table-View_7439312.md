# The Information Field of the Unicode Character Table View

last modified on Mai 03, 2016

Das Informationsfeld am unteren Ende der Sicht zeigt den
Unicode-Codepoint sowie den Beschreibungstext des momentan gewählten
Zeichens an und stellt das gewählte Zeichen zur genaueren Betrachtung
vergrößert dar. Die Schaltfläche “Einfügen” auf der rechten Site erlaubt
Ihnen, das gewählte Zeichen in ein geöffnetes XML-Dokument einzufügen.
