# Help

last modified on Feb 28, 2018

Das TextGridLab beinhaltet ein integriertes Online-Hilfe-System, das als
Ganzes geöffnet werden und nach Schlüsselwörtern durchsucht werden kann.
Das TextGrid Laboratory bietet Ihnen außerdem eine Dynamische Hilfe.

- [Static Help](Static-Help_7439489.md)
- [Dynamic Help](Dynamic-Help_7439499.md)
