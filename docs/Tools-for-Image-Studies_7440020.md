# Tools for Image Studies

last modified on Feb 28, 2018

Dieser Abschnitt beinhaltet Werkzeuge und Dienste für die Betrachtung
und – in einem geringeren Umfang – Bearbeitung digitaler Bilder. Weniger
nützlich für die Korrektur von Fehlern oder Defekten in digitalisierten
Scans oder digitalen Fotos ist ihr Ziel eher die Möglichkeit, Bilder
kollaborativ zu bearbeiten, sie mit Metadaten und zusätzlichen
Informationen zu annotieren oder den Arbeitsbereich komfortabel mit
Vergrößerungsfunktionen für Details und zum Rotieren und Spiegeln des
Bildes einzurichten. Zusammenfassend ist diese Zusammenstellung den
Instrumenten der Bilddatenverarbeitung gewidmet. Derzeit bietet TextGrid
Digilib an, ein integriertes Werkzeug, das hauptsächlich für die
Bildbetrachtung gedacht ist.

- [Digilib](Digilib_7440022.md)
- [MSDigilib](MSDigilib_34341688.md)
