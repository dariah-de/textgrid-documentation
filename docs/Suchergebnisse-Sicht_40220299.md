# Suchergebnisse-Sicht

last modified on Aug 18, 2015

Diese Sicht listet die Suchergebnisse auf, nachdem eine Suchanfrage
übermittelt wurde. Sie zeigt zunächst die Titel der Dokumente und die
Übereinstimmungen in ihrem Kontext an. Der Titel wird mit zusätzlichen
Informationen über den Dokumenttyp, das Projekt, die Revision und den
Namen des Bearbeiters angegeben.

Die Treffer in der Liste können durch Linksklicken ausgewählt werden.
Durch Doppelklicken können Sie die Dokumente in ihrem Standardeditor
öffnen. Ein Rechtsklick gewährt Ihnen Zugriff zu weiteren Funktionen,
die für das Dokument vorgesehen sind, beispielsweise es mit einem
anderen Editor zu öffnen, seine Metadaten anzuzeigen oder seinen URI in
die Zwischenablage zu kopieren. Das Kontextmenü ist größtenteils
identisch mit jenem im [Navigator](40220346.md).
