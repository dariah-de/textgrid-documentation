# Digilib-Editor-Sicht

last modified on Aug 20, 2015

Die Digilib-Editor-Sicht zeigt das Bild in der Größe und Auflösung Ihres
Bildschirms an. Wenn der Editor geöffnet ist, werden außerdem
zusätzliche Digilib-Schaltflächen in der Werkzeugleiste angezeigt.

Mit den Schaltflächen in der Werkzeugleiste können Sie in das Bild
hineinzoomen, um es bei der höchsten vom Server angebotenen Auflösung zu
untersuchen, es rotieren oder spiegeln oder Helligkeit bzw. Kontrast des
Bildes ändern.

Sobald der Digilib-Editor geöffnet ist, wird in der Werkzeugleiste eine
Reihe von Symbolen und Schaltflächen angezeigt.

- Hoch-/Herunter-Skalieren: Das Bild kann hoch- oder herunterskaliert
    werden, d. h. die Gesamtgröße kann mit  verkleinert oder mit 
    vergrößert werden.
- Zoom Bereich: Nach Klicken der Schaltfläche  können Sie in einen
    Bereich des Bildes zoomen, indem Sie zuerst in die obere linke Ecke
    und dann in die untere rechte Ecke des Bereichs klicken. Nach dem
    ersten Klick erscheint ein rotes Rechteck und den gerade gewählten
    Bereich anzeigen. Nach dem zweiten Klick wird das Bild durch eine
    gezoomte Ansicht des ausgewählten Bereichs ersetzt.
- Zoom Vollbild: Die
    Schaltfläche **![](attachments/40220967/40437076.png)** zoomt heraus
    und zeigt das gesamte Bild.
- Rotieren: Die
    Schaltfläche **![](attachments/40220967/40437075.png)** öffnet einen
    Schieberegler, um das Bild zu rotieren.
- Vertikal spiegeln: Die
    Schaltfläche ![](attachments/40220967/40437074.png) spiegelt das
    Bild vertikal (von oben nach unten)
- Horizontal spiegeln: Die
    Schaltfläche ![](attachments/40220967/40437073.png) spiegelt das
    Bild horizontal (von links nach rechts).
- Kontrast: Die Schaltfläche ![](attachments/40220967/40437072.png)
    öffnet einen Schieberegler, um den Kontrast des Bildes zu ändern.
- Helligkeit: Die Schaltfläche ![](attachments/40220967/40437071.png)
    öffnet einen Schieberegler, um die Helligkeit des Bildes zu ändern.

Diese Schaltflächen bieten die wichtigsten Funktionalitäten, auf die
auch über die fortgeschrittenen Bearbeitungsmöglichkeiten in der
[Eigenschaften-Sicht](Digilib-Eigenschaften-Sicht_40220969.md)
zugegriffen werden kann.

Der aktuelle Status des Bildes im Editor kann als ein
Digilib-Editor-Sicht-Objekt im TextGrid Repository gespeichert und
jederzeit wieder geöffnet werden.

## Attachments

- [brightness.png](attachments/40220967/40437071.png) (image/png)  
- [contrast.png](attachments/40220967/40437072.png) (image/png)  
- [mirror-horizontal.png](attachments/40220967/40437073.png) (image/png)  
- [mirror-vertical.png](attachments/40220967/40437074.png) (image/png)  
- [rotate.png](attachments/40220967/40437075.png) (image/png)  
- [zoom-full.png](attachments/40220967/40437076.png) (image/png)  
- [zoom-area.png](attachments/40220967/40437077.png) (image/png)  
- [size-smaller.png](attachments/40220967/40437078.png) (image/png)  
- [size-bigger.png](attachments/40220967/40437079.png) (image/png)  
- [digilib-editor-view-only.png](attachments/40220967/40437080.png)
(image/png)  
- [digilib-editor-view.png](attachments/40220967/40437081.png)
(image/png)  
