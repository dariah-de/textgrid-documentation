# TextGridLab Development Environment

last modified on Mär 28, 2022

For the TextGridLab, we use Eclipse for development and
[Tycho](http://www.eclipse.org/tycho) for the build. This page sets you
running with setting up your TextGridLab development environment with
Eclipse, while [TextGridLab Modular Build](TextGridLab-Modular-Build_8128007.md) explains how to set up
your tool to be built with Tycho.

## Getting Started

You should be able to run TextGridLab locally – especially, you'll need
[webkit-gtk.](https://wiki.de.dariah.eu/display/TextGrid/Troubleshooting#Troubleshooting-BrowserbasierteKomponentenfunktionierennicht(Linux))
You should also have a JDK available, not only a JRE.

### Eclipse and Friends

Download the up-to-date (I last tried Oxygen 2) **Eclipse IDE for
Eclipse Committers** from the [Eclipse Packages download page.](https://www.eclipse.org/downloads/eclipse-packages/) It contains
all you need to get started. (If you prefer to re-use an existing
Eclipse, you should have at least the PDE (Plugin Development
Environment) and m2e (Maven toolbox) installed before you go on).

Unpack and start Eclipse and create a new workspace.

### Add TextGridLab to your Target Platform

To develop a part of an Eclipse-based app like the TextGridLab, Eclipse
PDE has the concept of a [Target Platform](http://help.eclipse.org/indigo/index.jsp?topic=/org.eclipse.pde.doc.user/concepts/target.htm).
The Target Platform contains everything your code requires to run but
you don't actually modify: E.g., all the TextGridLab plug-ins that
really are Eclipse parts or originate from some third party. We will
setup a target platform that contains the current Nightly version of the
TextGridLab first:

1. In Eclipse, open the preferences page at **Window → Preferences →
    Plug-in Development → Target Platform**
2. **Add** a new target platform. Select *Start with an empty target
    definition*. Give it a sensible name, e.g., *TextGridLab TP*.
3. At the *Locations* tab, **add** a new location of type **Software
    Site**.
4. Enter one of the TextGridLab update sites [Update Site Management](Update-Site-Management_9929010.md) into the *Work with* field – e.g., <http://www.textgridlab.org/updates/nightly> – and
    hit your enter key.
5. Select the **TextGridLab SDK** category.
    - The *SDK* features include the source code you might want for
        debugging. The *TextGridLab Base* stuff includes everything that
        is in the TextGridLab application you can download from
        [textgrid.de](http://www.textgrid.de/download).
    - You may, at your wish, add additional stuff to your target
        platform, if you intend to develop against it.
6. Click **Finish**. You are back at the *New Target Definition*
    preference page.
7. On the **Content** page, select *Manage using: (•) Features*.
8. On the **Arguments** page, add the following to the **VM arguments**
    field:

    ``` syntaxhighlighter-pre
    -Djavax.ws.rs.ext.RuntimeDelegate=org.apache.cxf.jaxrs.impl.RuntimeDelegateImpl
    ```

9. Click **Finish**. You are back at the *Target Platform* preference
    page. You may always go back to the previous dialog by selecting the
    target platform and clicking *Edit*.
10. Click the checkmark in front of your freshly defined target platform
    and click apply in order to make it the active one.

Note that Eclipse takes quite long for some steps in working with the
target platform since it involves parsing large update sites and
dowloading lots of plugins to your computer.

You might want to also add the Eclipse SDK and the Marketplace Sources
for the target release to your platform – while the respective binary
plugins are part of the TextGridLab SDK, the sources are not. Thus
adding the Eclipse SDK to your target platform helps you in viewing help
or stepping through sources of eclipse stuff. To do so, basically
perform steps 4–7 as above, but in steps 4/5 instead:

1. use
    [http://download.eclipse.org/releases/luna](http://download.eclipse.org/releases/luna)
    as the update site
2. uncheck the *Group by Category* checkbox
3. type 'Eclipse SDK' in the filter field, check the corresponding
    entry, clear the filter
4. type 'Marketplace' in the filter field, check the sources entry,
    clear the filter
5. OK → will take some time

### Create a Run Configuration

In order to run or debug the TextGridLab (with all your tools):

1. Run → **Run Configurations**
2. In the list on the left side of the run configurations dialog,
    select **Eclipse Application** and click the *New* button at the top
    of the list. A new run configuration will be created. Give it a
    useful *Name*.
3. On the *Main* tab of the new run configuration, in the *Program to
    run* group, choose **(•) Run a product** and select the product
    **info.textgrid.lab.core.application.base_product** from the
    dropdown list. (If it doesn't show up, you haven't setup your target
    platform properly – see above.)
4. You currently need to **manually add command line arguments** to the
    *Arguments* tab of your run configuration. You should add the stuff
    from a current *textgridlab.ini* (found in the root folder of a
    downloaded TextGridLab). You need to at least add the following to
    the *VM Arguments* section or TG-search won't work:

    ``` syntaxhighlighter-pre
    -Djavax.ws.rs.ext.RuntimeDelegate=org.apache.cxf.jaxrs.impl.RuntimeDelegateImpl
    ```

5. You currently need to configure the AXIOM bundles to autostart
    (until TG-1822 has been resolved). To do so,
    1. go to the **Plugins** tab
    2. Select *Launch with: Plug-ins selected below*
    3. Check all target platform plug-ins plus the relevant stuff from
        your workspace
    4. Look for the two AXIOM plugins in the list (use the filter text
        field), org.apache.ws.commons.axiom.axiom-api and
        org.apache.ws.commons.axiom.axiom-impl, and select *Auto-Start:
        true* for both of them
6. OPTIONAL Marketplace:  
    1. If you want to also work with the Marketplace you need to
        activate "Support software installation in the launched
        application" in the configuration tab
    2. then you also need the more options from textgridlab.ini in
        vmargs

        ``` syntaxhighlighter-pre
        -Dorg.eclipse.update.reconcile=false
        -Dorg.eclipse.epp.internal.mpc.core.service.DefaultCatalogService.url=https://textgridlab.org/
        -Declipse.p2.unsignedPolicy=allow
        ```

7. Modify the other options at will and hit **Run**. The TextGridLab
    with your stuff inside will start. You can re-use the launch
    configuration later.

You can also create this run configuration for debugging from the debug
menu items.

### Work on TextGridLab itself

The following steps checkout the complete TextGridLab. They have been
tested on a linux box that has git and maven 3 working at the command
line and assume you have a SSH key correctly configured both on your
system and on gitlab.gwdg.de. Checking out the complete lab has the
advantage that there is a shell script `./build-locally` that will build
a version of the lab using the components on your disk, so this is great
for refactoring activities or building TextGridLab executables before
you push to develop. Alternatively, see the next section for a guide to
check out individual components.

1. To clone the lab,
    type `    git clone --recursive -b develop https://gitlab.gwdg.de/dariah-de/textgridlab/textgrid-laboratory.git`
    at a terminal window in a suitable folder in your file system. This
    will create a folder textgrid-laboratory with the lab's source code
    in all subdirectories.
2. cd textgrid-laboratory
3. The following command will bring all your submodules to the current
    state of the develop branch:

    ``` syntaxhighlighter-pre
    git submodule foreach "
      git checkout develop
      git pull
    "
    ```

4. Run `./setup-push-urls.sh`. It will setup suitable push URLs for all
    the submodules, so you can actually push your changes back to our
    repository.

Now you could run `./build-locally.sh` to actually build the whole
TextGridLab from the sources on your disk, if you do that, you'll find
readily packaged as well as unpacked versions for various operating
systems under `base/base-repository/target`.

#### Importing checked-out components into your Eclipse workspace

Now we'll use one of the components (I'll take `core`) into the Eclipse
workspace we setup before in order to hack on it.

Back to your Eclipse …

1. Open the *Git* perspective from the perspective switcher in the top
    right corner.
2. In the *Git Repositories* view, select *Add an existing local git
    repository*.
3. Under *Directory*, browse for your `textgrid-laboratory` directory
    from the last section. Check *Look for nested repositories* and
    click *Search*. Check all results and click Finish. You will now see
    the main project and all submodules in the *Git Repositories* view.
4. Right-click the `core` entry and select *Import Projects* from the
    context menu. Eclipse will show a dialog that lists a bunch of
    folders, each of which is marked with Import as: Maven.
5. Leave everything as is and click *Finish*.
6. The first time you'll do this, Eclipse will ask you to *Setup Maven
    plugin connectors*. Accept all suggestions and click *Finish*.
    Eclipse will install some components, just accept everything you are
    asked for.
7. After restarting Eclipse, open the *Plugin Development* perspective
    from the perspective switcher. You will see your projects in the
    project explorer. You might want to switch the project explorer's
    *Project Presentation* to *Hierarchical* from its overflow menu (the
    icon with the triangle pointing downwards).
8. Now hack on, the run configuration you configured above should use
    the code from your workspace.

### Alternative: Checkout existing individual TextGridLab projects

The following steps show how to checkout existing TextGridLab projects
(using the `core` component as an example), e.g., to fix some bugs in
TextGridLab proper. Note that you only need checkout those components
you wish to modify, it's perfectly possible to develop your own tools
without checking out the whole TextGridLab.

The steps assume you have installed m2e and Subclipse and configured
Subclipse so it works.

1. Setup your development environment (including target platform and
    run configuration) as outlined above.
2. Open the *Browse Git Repositories* perspective.
3. Choose the project you wish to clone from the subprojects of
    [https://gitlab.gwdg.de/dariah-de/textgridlab](https://gitlab.gwdg.de/dariah-de/textgridlab)
4. Clone the chosen repository from the SSH clone URL. Checkout the
    branch *develop*, this is where the main line of development
    happens.
5. From the component directory's context menu, choose *Checkout as
    Maven project*
6. Finish the wizard and wait ...
7. Fix the issues below ...

Note that Eclipse will have automatically added the plugins you just
checked out to the run configuration you defined above, so you'll work
with the plugins from your workspace and eclipse will take the other
plugins from the target platform.

Instead of cloning the repository inside eclipse you may also clone the
repo to your local disk using an external git client and add the local
repository to the repositories view.

#### Currently expected errors

- No support for maven-javadoc-plugin
  - can be safely ignored.
- JUnit is missing
  - either add org.junit to the target platform, e.g., from Eclipse
        Orbit (TODO detailed instructions)
  - or close those projects requiring org.junit, i.e. the \*.tests
        projects

### German installation manual

- The following attachment contains a german installation manual.

[![](rest/documentConversion/latest/conversion/thumbnail/64970084/1)](/download/attachments/8127335/2018-08%20TextGridLab%20Development%20Environment_Neon.pdf?version=1&modificationDate=1536246875558&api=v2)

- This manual was successfully tested under Windows, MacOS and Linux
    (Ubuntu and Debian). If you use Eclipse Photon, please consider the
    following information
    from [stackoverflow](https://stackoverflow.com/a/51780614).

## Frequently Asked Bugfixes

If there are still compiler errors in the workspace:

1. (1.0 only) Check the project
    info.textgrid.lab.core.tgcrud.client.jaxws in the Package Explorer -
    is there content in the folder 'generated-src'?
    - If not: Switch Build mode to build manually
        (Menu/Project/Uncheck Build Automatically), hit F5 (Refresh),
        and maybe trigger a new build. This should work automatically,
        but it appearently doesn't sometimes. (This step calls wsimport
        to automatically generate JAXB sources.)
    - If the wsimport is not being called (visible in the eclipse
        console), restart eclipse platform and try again
2. Make sure you have set the target platform and edited it to match
    your individual build platform (See step 'Set the Target Platform'
    above)

If there is a crash at runtime:

1. Error 'UnsatisfiedLinkError: Cannot load 32-bit SWT libraries on
    64-bit JVM'
    - Check Eclipse Preferences/Java/Installed JREs/Execution
        environment/ JavaSE-1.6: Make sure there is an SDK 6 installed
    - Modify the .target file to match architecture x86_64, afterwards
        rebuild workspae

## Attachments

- [2018-08 TextGridLab Development Environment_Neon.pdf](attachments/8127335/64970084.pdf)
(application/pdf)
