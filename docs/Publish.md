# Publish

last modified on Mai 03, 2016

Das TextGrid Publikationswerkzeug erlaubt Ihnen, die Dateien, die sie im
TextGridLab bearbeitet haben, dauerhaft im Repository zu
veröffentlichen.
Nur [Editionen](Editionen_40220434.md) und [Kollektionen](Kollektionen_40220436.md) können
veröffentlicht werden.
Nur [Projekt-Manager](Projekt--und-Benutzerverwaltung_40220359.md) haben
das Recht, sie zu veröffentlichen.

Bei der Veröffentlichung werden die Metadaten automatisch validiert. Die
Objekte sind nach der Veröffentlichung frei zugänglich und suchbar
auf [www.textgridrep.de](http://www.textgridrep.de/). Alle
veröffentlichten Objekte erhalten einen persistenten Identifikatoren
(persistent Identifier) und alle Daten werden dupliziert in einen
statischen Speicherbereich mit Backups und Langzeitarchivierungsdienst
migriert.

- [Open the Publish View](Open-the-Publish-View_7439461.md)
- [Publish View](Publish-View_7439463.md)
- [Using TextGrid Publish](Using-TextGrid-Publish_7439465.md)
- [Interaction of TextGrid Publish with Other Components](Interaction-of-TextGrid-Publish-with-Other-Components_7439467.md)
