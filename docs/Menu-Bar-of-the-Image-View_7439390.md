# Menu Bar of the Image View

last modified on Mai 03, 2016

Wenn die Bild-Sicht aktiviert ist, können die Operationen “Rückgängig” ![](attachments/40220643/40436835.png "037-zuruecknehme-Aenderung.png") und
“Wiederholen” ![](attachments/40220643/40436834.png "038-wiederhole-Aenderung.png") unter
dem Menüpunkt “Bearbeiten” in der Menüleiste verwendet werden. Wenn
der [XML-Editor](XML-Editor_40220521.md) geöffnet und fokussiert ist,
werden alle anderen Funktionen unter diesem Menüpunkt ebenfalls zur
Verfügung gestellt.

## Attachments

- [037-zuruecknehme-Aenderung.png](attachments/7439390/8192257.png)
(image/png)  
- [038-wiederhole-Aenderung.png](attachments/7439390/8192258.png)
(image/png)  
