# Titelleiste der Bild-Sicht

last modified on Aug 20, 2015

Das Asterisk-Zeichen \* in der Titelleiste der Bild-Sicht zeigt an, dass
Änderungen noch nicht gespeichert sind. Es gibt mehrere Schaltflächen in
der Titelleiste der Bild-Sicht:

- Klicken
    Sie ![](attachments/40220645/40436839.png "showAllLayer_16.png") ,
    um alle Ebenen einzublenden(wenn es
    [Ebenen](Funktionen-der-Bild-Sicht_40220641.md) gibt, die derzeit
    ausgeblendet sind).
- Klicken
    Sie ![](attachments/40220645/40436838.png "showLayer_16.png")  ,
    um Ebenen einzublenden.
- Klicken
    Sie ![](attachments/40220645/40436837.png "hideLayer_16.png")  , um
    Ebenen auszublenden.
- Klicken Sie 
    ![](attachments/40220645/40436836.png "graphics_toolkits.png") , um
    den [Werkzeugkastens](Werkzeugkasten_40220657.md) zu öffnen oder
    zu schließen.

## Attachments

- [graphics_toolkits.png](attachments/40220645/40436836.png) (image/png)  
- [hideLayer_16.png](attachments/40220645/40436837.png) (image/png)  
- [showLayer_16.png](attachments/40220645/40436838.png) (image/png)  
- [showAllLayer_16.png](attachments/40220645/40436839.png) (image/png)  
