# Interaction of the Aggregations Editor with Other Components

last modified on Mai 03, 2016

Der Aggregationen-Editor basiert auf der Konzeption
der [Objekte](TextGrid-Objekte.md) und interagiert mit dem
Schema, das
dem [Metadaten-Editor](Metadaten-Editor_40220458.md) unterliegt. Um
die Verknüpfung von Objekte und Aggregationen zu ermöglichen, wird
der [Navigator](40220331.md) standardmäßig mit dem
Aggregationen-Editor geöffnet.
