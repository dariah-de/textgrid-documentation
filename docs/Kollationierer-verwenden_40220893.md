# Kollationierer verwenden

last modified on Aug 04, 2015

CollateX kann für die Kollationierung bestehender Text-Objekte ebenso
verwendet werden wie für die Standardisierung der Lesarten der Texte.

- [Kollationsergebnis standardisieren](Kollationsergebnis-standardisieren_40220897.md)
- [Texte kollationieren](Texte-kollationieren_40220895.md)
