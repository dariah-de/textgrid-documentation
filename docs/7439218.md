# Open the Project & User Management

Es gibt mehrere Möglichkeiten, die Projekt- und Benutzerverwaltung zu öffnen:

- Klicken Sie “Projekt- und Benutzerverwaltung” auf dem [Startbildschirm](Startbildschirm-und-Login_40220219.md).
- Wählen Sie “Werkzeuge \> Projekt- und Nutzerverwaltung ” in der Menüleiste
- Klicken Sie ![](attachments/40220371/40436713.png "074-zeige-ProjektNutzerVerwaltung.png") in der Werkzeugleiste

## Attachments

- [074-zeige-ProjektNutzerVerwaltung.png](attachments/7439218/7766087.png)
(image/png)  
