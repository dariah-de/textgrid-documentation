# Unicode Character Table

last modified on Mai 03, 2016

Die Unicode-Zeichen-Tabelle ermöglicht es Ihnen, Zeichen aus den
Unicode-Zeichensätzen zu suchen, zu kopieren und in den aktiven Editor
oder die Zwischenablage einzufügen. Sie können Ihre eigenen von Ihnen
angepassten Sätze von Unicode-Zeichen für die Nutzung in der
Unicode-Zeichen-Tabelle erstellen.

- [Open the Unicode Character Table](Open-the-Unicode-Character-Table_7439301.md)
- [Unicode Character Table View](Unicode-Character-Table-View_7439303.md)
- [Using the Unicode Character Table](Using-the-Unicode-Character-Table_7439314.md)
- [Interaction of the Unicode Character Table View with Other Components](Interaction-of-the-Unicode-Character-Table-View-with-Other-Components_7439316.md)
