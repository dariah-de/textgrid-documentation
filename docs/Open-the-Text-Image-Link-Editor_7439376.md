# Open the Text Image Link Editor

last modified on Mai 17, 2016

Der Text-Bild-Link-Editor kann gestartet werden durch

- das Klicken seines Icons auf dem Startbildschirm oder
- das Auswählen aus der Menüleiste von TextGridLab oder
- das Klicken auf das
    ![](attachments/7439376/7766149.gif "069-zeige-BildLinkEditorPerspektive.gif")
    Symbol in der Werkzeugleiste von TextGridLab

## Attachments

- [069-zeige-BildLinkEditorPerspektive.gif](attachments/7439376/7766149.gif)
(image/gif)  
