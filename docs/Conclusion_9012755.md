# Conclusion

last modified on Feb 28, 2018

Dieses Handbuch bietet grundlegende Informationen und Unterstützung für
die Arbeit mit TextGrid. Da TextGrid entwickelt wurde, um eine
fortlaufende Erweiterung und Weiterentwicklung zu unterstützen, werden
Handbücher und Informationen über zusätzliche Werkzeuge und Dienste
hinzugefügt, wenn sie entwickelt und in das TextGridLab und Rep
integriert sind. Bitte kontaktieren Sie uns
unter [info@textgrid.de](mailto:info@textgrid.de) wenn Sie Kommentare
bzw. Vorschläge machen oder Rückmeldungen über dieses Handbuch oder
TextGrid im Allgemeinen geben möchten.

TextGrid ist ein Open-Source-Projekt, das entwickelt wurde, um sich an
die Bedürfnisse textwissenschaftlich orientierter Geisteswissenschaftler
anzupassen. Da die Prioritäten von TextGrid sowohl jetzt als auch
zukünftig die Unterstützung kollaborativer digitaler Forschungsmethoden
und die Förderung zuverlässiger und sicherer Datenpflegepraktiken ist,
ist unsere Nutzer-Community essenziell für die Festlegung der Zukunft
von TextGrid. Wir möchten Sie daher ermuntern, durch Ihre Teilnahme
etwas zu TextGrid beizutragen. Unsere primären Nutzergruppen setzen sich
aus den folgenden drei Kategorien:

- Wenn Sie Mitglied eines Forschungsprojekts sind, wir sind an einer
    Zusammenarbeit mit wissenschaftlichen Projekten interessiert, um
    weitere Anwendungen zu entwickeln, die Ihren Anforderungen
    entsprechen. Bitte kontaktieren Sie uns, wenn Sie daran interessiert
    sind, die Möglichkeiten für Ihr Projekt zu eruieren.
- Wenn Sie ein Entwickler sind und eine Anwendung implementiert haben,
    von der Sie annehmen, dass Sie für der Verwendung in TextGrid
    geeignet ist, lassen Sie uns dies wissen. TextGrid unterstützt die
    Integration externer Werkzeuge und Dienste, und wir würden uns
    darüber freuen, mit Ihnen über die technischen Anforderungen und
    Möglichkeiten zu sprechen.
- Wenn Sie ein Inhaltsanbieter wie ein Archiv oder ein
    Forschungsinstitut sind und daran interessiert, Ihre Daten in
    TextGrid zugänglich zu machen, kontaktieren Sie uns bitte um zu
    prüfen, in welcher Form wir zusammenarbeiten können, um Ihre Daten
    in TextGrid verfügbar zu machen.

Willkommen in TextGrid!
