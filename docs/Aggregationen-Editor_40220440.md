# Aggregationen-Editor

last modified on Jul 28, 2015

Mit dem Aggregationen-Editor haben Sie die Möglichkeit, Objekte
auszuwählen und durch Ziehen und Ablegen zusammenzufassen, anstatt nach
einzelnen Objekten zu suchen, die über mehrere Projekte verteilt sind.
Objekte, die für ein bestimmtes Thema oder Forschungsinteresse relevant
sind, können aggregiert werden. Sie werden im Navigator als Aggregation
angezeigt, die bearbeitet, erweitert und aufgelöst werden kann, ohne die
[Objekte](https://doc.textgrid.de/search.html?q=TextGrid+Objects)
darin zu löschen.

- [Aggregationen-Editor öffnen](40220442.md)
- [Aggregationen-Editor-Sicht](Aggregationen-Editor-Sicht_40220444.md)
- [Aggregationen-Editor verwenden](Aggregationen-Editor-verwenden_40220450.md)
- [Interaktion des Aggregationen-Editors mit anderen Komponenten](Interaktion-des-Aggregationen-Editors-mit-anderen-Komponenten_40220453.md)
