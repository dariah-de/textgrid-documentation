# Search Perspective

last modified on Feb 28, 2018

Die Suche-Perspektive besteht aus zwei Sichten: der Suche-Sicht und der
Suchergebnisse-Sicht.

![](attachments/40220289/40437417.png)

|                       |
|-----------------------|
| **Suche-Perspektive** |

## Attachments

- [sea-search.png](attachments/7439476/9240665.png) (image/png)  
- [sea-search.png](attachments/7439476/7766192.png) (image/png)  
