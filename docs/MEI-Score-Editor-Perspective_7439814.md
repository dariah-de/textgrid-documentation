# MEI Score Editor Perspective

last modified on Mai 04, 2016

The MEI Score Editor Perspective consists of four views:

1. The [Musical Score View](Musical-Score-View_7439823.md) on the
    left.
2. A [Palette](Palette_7439859.md) which appears simultaneously on
    the right side of the Score View when a document is opened. The
    Palette can be closed and opened separately via the white triangle
    at the top.
3. The [Outline View](Outline-View-of-the-MEI-Score-Editor_7439872.md) in the
    upper right area.
4. The [Properties View](Properties-View-of-the-MEI-Score-Editor_7439882.md) on the
    lower right. The [Navigator View](Navigator-View_7439192.md) from
    which the Note Editor has been started after an MEI Object has been
    opened appears as a hidden tab behind the Properties View.

The Outline and Properties Views are only opened when an MEI document is
created or opened or if the [XML Editor](XML-Editor_7439318.md) is
open in the background. Both views make it possible to edit an MEI
Object.

- [Menu Bar of the MEI Score Editor Perspective](Menu-Bar-of-the-MEI-Score-Editor-Perspective_7439817.md)
- [Toolbar of the MEI Score Editor Perspective](Toolbar-of-the-MEI-Score-Editor-Perspective_7439819.md)
- [Shortcuts in the MEI Score Editor Perspective](Shortcuts-in-the-MEI-Score-Editor-Perspective_7439821.md)

## Attachments

- [meise-f1_perspectives.png](attachments/7439814/8192005.png)
(image/png)  
