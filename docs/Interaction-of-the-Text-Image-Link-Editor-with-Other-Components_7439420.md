# Interaction of the Text Image Link Editor with Other Components

last modified on Mai  03, 2016

Der Text-Bild-Link-Editor interagiert vor allem mit dem XML-Editor, aber
er kann auch mit dem Navigator und dem Metadaten-Editor interagieren.

Wie oben beschrieben können Texte, Bilder und Text-Bild-Link-Objekte
im [Navigator](40220331.md) geöffnet werden.

Der [Metadaten-Editor](Metadaten-Editor_40220458.md) wird verwendet,
um Metadaten für TextGrid-Objekte zu bearbeiten und zu speichern. Sowohl
Bilder als auch Text-Bild-Link-Objekte müssen mit Metadaten assoziiert
werden, um suchbar zu sein.

Der [XML-Editor](XML-Editor_40220521.md) ist standardmäßig Bestandteil
der Text-Bild-Link-Editor-Perspektive.
