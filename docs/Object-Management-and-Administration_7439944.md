# Object Management and Administration

In diesem Abschnitt werden die Instrumente zur Verwaltung von Projekten,
Objekten und Nutzern erklärt. In diesem Zusammenhang wird auch
erläutert, wie der Import- und Export-Vorgang sowie die Aggregation und
Publikation von Objekten abläuft und wie ihre Metadaten verwaltet
werden.

- [Navigator](Navigator_7439188.md)
- [Project & User Management](7439216.md)
- [Import](Import_7439245.md)
- [Export](Export_7439253.md)
- [Revisions and Locking Mechanism](Revisions-and-Locking-Mechanism_7439262.md)
- [TextGrid Objects](TextGrid-Objects.md)
- [Aggregations Editor](Aggregations-Editor_7439445.md)
- [Metadata Editor](Metadata-Editor_7439270.md)
- [Metadata Template Editor](Metadata-Template-Editor_7439291.md)
- [Publish](Publish.md)
- [Publish Tool SADE](Publish-Tool-SADE_8129840.md)
- [MSSADEPublishTool](MSSADEPublishTool_34344152.md)
- [RDF Object Editor](RDF-Object-Editor_40212773.md)
