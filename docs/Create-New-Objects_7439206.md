# Create New Objects

last modified on Mai 03, 2016

Wählen Sie “Datei \> Neues Objekt” in der Menüleiste oder “Neues Objekt”
im Kontextmenü des Navigators, um in einem ausgewählten Projekt ein
TextGrid-Objekt zu erstellen.

![](attachments/40220353/40437118.png)

|                      |
|----------------------|
| **Objekt erstellen** |

Um ein Objekt zu erstellen, müssen ein Projektordner und ein Objekttyp
gewählt werden. Wenn ein Projekt im Navigator vorausgewählt war, wird
dieses Projekt im Assistenten “Neues TextGrid-Objekt” vorgeschlagen.
Nach Klicken der Schaltfläche “Weiter” wird die
[*Metadaten-Editor*](https://doc.textgrid.de/search.html?q=Metadata+Editor)-Schnittstelle
geöffnet. Vervollständigen Sie die Metadaten und klicken Sie “Weiter”.
Abhängig vom Dokumenttyp unterscheiden sich die folgenden Schritte. Wenn
es sich um ein XML-Dokument handelt, können Sie ein Schema wählen.
Nachdem alle Pflichtfelder ausgefüllt wurden, können Sie zum Abschließen
des Vorgangs “Fertigstellen” klicken.

## Attachments

- [pum-createobject-wizard.png](attachments/7439206/9240669.png)
(image/png)  
- [pum-createobject-wizard.png](attachments/7439206/7766086.png)
(image/png)  
