# Interaktion von TextGrid-Publish mit anderen Komponenten

last modified on Aug 19, 2015

Die Publish-Sicht kann direkt aus dem [Navigator](40220331.md)
geöffnet werden. “Publish” überprüft die Informationen der [Projekt- und Benutzerverwaltung](Projekt--und-Benutzerverwaltung_40220359.md) und
den [Objekttyp](TextGrid-Objekte.md) sowie die
[Metadaten](Metadaten-Editor_40220458.md), bevor ein TextGrid-Objekt
im Repository veröffentlicht wird.
