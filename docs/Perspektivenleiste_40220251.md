# Perspektivenleiste

last modified on Aug 18, 2015

Die Perspektivenleiste ist am oberen Bildschirmrand auf der rechten
Seite, unterhalb der Menüleiste, sofern die Anordnung der
Werkzeugleisten nicht verändert wurde. Die Perspektivenleiste gibt Ihnen
Zugriff auf die Perspektiven, die gerade geöffnet sind. Sie bietet Ihnen
außerdem die Möglichkeit, eine neue Perspektive durch Klicken des
Symbols ![](attachments/40220251/40436656.png "new_persp.png") zu
öffnen. Durch Rechtsklicken eines der geöffneten Reiter in der
Perspektivenleiste ist es möglich, die Perspektive anzupassen.

## Attachments

- [new_persp.png](attachments/40220251/40436656.png) (image/png)  
