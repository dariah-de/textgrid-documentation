# Statische Hilfe

last modified on Aug 18, 2015

Die allgemeine nicht-spezifische Hilfe kann durch Auswahl von “Hilfe” in
der Menüleiste geöffnet werden. Sie können außerdem “Inhaltsverzeichnis
der
Hilfetexte” ![](attachments/40220305/40436667.gif "103-zeige-Hilfe.gif") ,
“Spickzettel” ![](attachments/40220305/40436666.gif "104-zeige-Merkzettel2.gif") ,
“Dynamische Hilfe” und
“Schlüsselwort-Suche” ![](attachments/40220305/40436664.gif "sinfocenter_obj.gif")
auswählen.

- [Inhaltsverzeichnis der Hilfetexte](Inhaltsverzeichnis-der-Hilfetexte_40220308.md)
- [Spickzettel](Spickzettel_40220312.md)
- [Kontext-Sensitive Hilfe](Kontext-Sensitive-Hilfe_40220314.md)
- [Schlüsselwort-Suche](40220318.md)

## Attachments

- [sinfocenter_obj.gif](attachments/40220305/40436664.gif) (image/gif)  
- [105-zeige-SchluesselwortSuche.gif](attachments/40220305/40436665.gif)
(image/gif)  
- [104-zeige-Merkzettel2.gif](attachments/40220305/40436666.gif)
(image/gif)  
- [103-zeige-Hilfe.gif](attachments/40220305/40436667.gif) (image/gif)  
