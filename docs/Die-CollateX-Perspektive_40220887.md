# Die CollateX-Perspektive

last modified on Aug 20, 2015

Die CollateX-Perspektive besteht aus zwei Komponenten. Die erste listet
die [Kollations- und Äquivalenzsets](https://wiki.de.dariah.eu/display/tgarchiv1/Kollationierung)
auf und die zweite zeigt die [Ergebnisse der Kollationierung](Kollationsergebnis-Komponente_40220891.md) an.

![](attachments/40220887/40436940.png)

|                          |
|--------------------------|
| **CollateX-Perspektive** |

Wenn CollateX geöffnet ist, werden vier Symbole in der Werkzeugleiste
angezeigt: 

1. Klicken
    Sie ![](attachments/40220887/40436946.gif "155-Datei-Icon-Collation-Set.gif")
    zum Erstellen neuer Kollationierungssets.
2. Klicken
    Sie ![](attachments/40220887/40436945.gif "156-Datei-Icon-Ausnahmeliste.gif")
    zum Erstellen neuer Äquivalenzsets.
3. Klicken
    Sie ![](attachments/40220887/40436944.gif "157-starte-Kollationierung.gif") ,
    um Texte zu kollationieren.
4. Klicken Sie ![](attachments/40220887/40436941.gif) , um die aktive
    Auswahl im Äquivalenzset zu löschen.

## Attachments

- [lit-collatex.png](attachments/40220887/40436940.png) (image/png)  
- [042-loesche-Auswahl.gif](attachments/40220887/40436941.gif)
(image/gif)  
- [textgrid-manual-screenshot-collateX01.png](attachments/40220887/40436942.png)
(image/png)  
- [textgrid-manual-collateX-screenshot01.png](attachments/40220887/40436943.png)
(image/png)  
- [157-starte-Kollationierung.gif](attachments/40220887/40436944.gif)
(image/gif)  
- [156-Datei-Icon-Ausnahmeliste.gif](attachments/40220887/40436945.gif)
(image/gif)  
- [155-Datei-Icon-Collation-Set.gif](attachments/40220887/40436946.gif)
(image/gif)  
