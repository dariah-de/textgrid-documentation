# Features of the XML Editor

last modified on Mai 04, 2016

Der XML-Editor bietet im Allgemeinen bestimmte Funktionen, um die Arbeit
mit XML-Dokumenten zu vereinfachen.

**Validierungsfehler anzeigen**

Wählen Sie “Validierungsfehler
anzeigen” ![](attachments/40220527/40436752.png "062-zeige-Validierungsfehler.png") unter
dem Punkt “XML” in der Menüleiste öffnet eine neue Sicht, in der
Fehlermeldungen mit Informationen über nicht valides XML und Offset
angezeigt werden.

**Schema assoziieren**

Wenn ein XML-Dokument geöffnet ist, können Sie ein Schema mit ihm
assoziieren. Nachdem Sie “Schema
assoziieren” ![](attachments/40220527/40436751.png "063-verbinde-Schema.png") unter
dem Punkt “XML” in der Menüleiste gewählt haben, öffnet sich der
Assistent “Neues Schema auswählen”.

![](attachments/40220527/40436748.png) 

|                        |
|------------------------|
| **Schema assoziieren** |

Wenn das Dokument bereits mit einem Schema assoziiert ist, werden einige
zusammenfassende Informationen über der Liste der Schemata angezeigt.
Sie können eines der Schemata, die mit Informationen wie dem Titel des
Schemas, dem entsprechenden Projekt, von wem es bereitgestellt wurde
sowie dem Erstellungsdatum aufgelistet werden, auswählen. Klicken Sie
ein Schema und klicken Sie “OK”, um es mit dem XML-Dokument zu
assoziieren.

Wenn kein Schema assoziiert ist, wird das Optionsfeld “keine explizite
Schema-Assoziierung” links unterhalb der Liste ausgewählt. Die
Assoziierung eines Schemas mit einem Dokument wird nur dann persistent,
wenn das Dokument oder seine Metadaten gespeichert werden.

**Cascading Style Stylesheet (CSS) assoziieren**

Sie können einem XML-Dokument ein Cascading Style Sheet (CSS)
hinzufügen, wenn es geöffnet ist und Sie den Unterpunkt “CSS-Stylesheet
assoziieren” unter “XML” in der Menüleiste auswählen. Das CSS wird für
die Anzeige des XML-Dokuments in
der [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md) verwendet. Sie
können ein CSS durch Klicken in der Liste auswählen. Standardmäßig wird
ein vom Editor zur Verfügung gestelltes TEI-Stylesheet verwendet. Um
diese Option zu reaktivieren, klicken Sie auf das Optionsfeld unter der
Liste von CSS-Stylesheets.

**Adapter assoziieren**

Ein Adapter kann assoziiert werden, um das ausgewählte Dokument in das
TextGrid-Baseline Encoding zu übersetzen. Wählen Sie den Unterpunkt
“Adapter assoziieren...”  unter “XML” in der Menüleiste, wenn ein
Dokument geöffnet ist, oder wählen Sie im Navigator diesen Punkt im
Kontextmenü des vorausgewählten Objekts. Dann öffnet sich ein neuer
Assistent.

In der oberen Liste des Assistenten werden die ausgewählten Objekte mit
Informationen über "Titel", "Projekt", "Daten bereitgestellt von" und
"Erstellungsdatum" angezeigt. Sie können das Auswahlfeld unter der Liste
aktivieren, um die Zuweisung durch Klicken der Schaltfläche “OK”
persistent zu machen.

In der unteren Liste können Sie den Adapter durch Klicken auswählen.
Hier finden Sie außerdem zusätzliche Informationen über die Adapter. Um
Ihre Auswahl rückgängig zu machen, klicken Sie auf das Optionsfeld “Mit
keinen Adapter assoziieren” unter der Liste.

**XML-Editor Debuggen**

Die Debug-Funktion findet und reduziert die Zahl der Probleme. Das
XML-Editor-Debuggen arbeitet mit
der [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md). Durch Auswahl von
“XML-Editor Debuggen” unter “XML” in der Menüleiste können Sie die
WYSIWYM-Ansicht neu laden. Durch Auswahl von “XML-Dokument debuggen”
können Sie die WYSIWYM-Debug-Ausgabe speichern. Sie können die
WYSIWYM-Debuggen-Sicht auch durch Auswahl von “Layout Debuggen” öffnen.

## Attachments

- [xml-schema-wizard.png](attachments/7439324/9240679.png) (image/png)  
- [062-zeige-Validierungsfehler.png](attachments/7439324/8192214.png)
(image/png)  
- [063-verbinde-Schema.png](attachments/7439324/8192215.png) (image/png)  
- [064-verbinde-Adaptor.png](attachments/7439324/8192216.png)
(image/png)  
- [064-verbinde-Adaptor2.png](attachments/7439324/8192217.png)
(image/png)  
- [xml-schema-wizard.png](attachments/7439324/7766119.png) (image/png)  
