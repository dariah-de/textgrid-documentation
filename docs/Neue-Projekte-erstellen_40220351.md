# Neue Projekte erstellen

last modified on Jul 28, 2015

Um ein neues Projekt zu erstellen, wählen Sie “Datei \> Neu \> Neues
Projekt...” in der Menüleiste oder im Kontextmenü des Navigators, wenn
ein Projekt ausgewählt ist. Alternativ können Sie den Pfeil neben dem
Symbol ![](attachments/40220351/40436710.png "plus-icon.png") in der
Werkzeugleiste klicken. Die "Projekt-Manager"-Rolle, "Berechtigung zum
Löschen" und "Editor"-Rolle werden Ihnen dabei durch die [*Projekt- und Benutzerverwaltung*](https://dev2.dariah.eu/wiki/pages/viewpage.action?pageId=7439216)
zugewiesen.

![](attachments/40220351/40436709.png)

|                       |
|-----------------------|
| **Projekt erstellen** |

Geben Sie einen Namen und eine Beschreibung des Projekts ein. Die
[*Projekt- und Benutzerverwaltung*](https://dev2.dariah.eu/wiki/pages/viewpage.action?pageId=7439216)
wird standardmäßig geöffnet, nachdem Sie den Prozess durch Klicken der
Schaltfläche “Erstellen” abgeschlossen haben. Wenn Sie die Perspektive
nicht angezeigt bekommen möchten, entfernen Sie das Häkchen im
Kontrollkästchen.

## Attachments

- [pum-createproject-wizard.png](attachments/40220351/40436709.png)
(image/png)  
- [plus-icon.png](attachments/40220351/40436710.png) (image/png)  
