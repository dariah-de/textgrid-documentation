# Open XML Documents

last modified on Mai 04, 2016

Um XML-Dokumente aus dem Repository im XML-Editor zu öffnen können Sie
entweder das Dokument im [Navigator](40220331.md) doppelklicken oder
das Dokument im Navigator rechtsklicken und “Öffnen” wählen. Eine andere
Möglichkeit ist, das XML-Dokument im Navigator auszuwählen und "Datei \>
Objekt öffnen " in der Menüleiste auszuwählen. Um XML-Dokumente von
Ihrem lokalen Betriebssystem im XML-Editor zu öffnen, klicken Sie
“Datei \> Lokale Datei öffnen” und wählen Sie das gewünschte Dokument im
Assistenten “Datei öffnen” Ihres lokalen Betriebssystems aus.

Sobald ein Dokument oder eine Datei geöffnet ist,

- zeigt der [Metadaten-Editor](Metadaten-Editor_40220458.md) die zum
    Dokument zugehörigen Metadaten an,
- in der [Editor-Sicht](Editor-Sicht_40220539.md) wird der Inhalt
    des Dokuments als XML-Quelltext angezeigt,
- in der
    separaten [Gliederung-Sicht](Gliederung-Sicht_40220561.md) auf der
    rechten Seite wird das Dokument in einer hierarchischen Baumstruktur
    dargestellt
- wenn Sie auf ein Element in der XML-Editor-Sicht oder der
    Gliederung-Sicht klicken, werden seine Attribute und Werte in einer
    Tabelle in
    der [Eigenschaften-Sicht](Eigenschaften-Sicht_40220567.md) angezeigt.

Am unteren Rand der Editor-Sicht in der Mitte können Sie zwischen drei
verschiedenen Ansichten zur Bearbeitung des Dokuments wählen: der
Entwurf-Ansicht, der Quelle-Ansicht und der WYSIWYM-Ansicht.
Die [Entwurf-Ansicht](Entwurf-Ansicht_40220541.md) zeigt nur die
hierarchische Struktur des Dokuments.
Die [Quelle-Ansicht](Quelle-Ansicht_40220547.md) zeigt das Dokument
als XML-Quelltext an. Sie erlaubt Ihnen, auf seine Struktur zuzugreifen
und sie zu verändern.
Die [WYSIWYM-Ansicht](WYSIWYM-Ansicht_40220553.md) (= “What You See Is
What You Mean”-Ansicht) zeigt die endgültige Darstellung und
Formatierung des Inhalts für das Web an. Sie können durch Klicken der
Reiter am unteren Ende der Editor-Sicht zwischen den
Bearbeitungs-Ansichten wechseln.
