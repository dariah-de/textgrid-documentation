# Toolbar of the Aggregations Editor

last modified on Mai 03, 2016

Um Aggregationen zu verwalten, stellt der Aggregationen-Editor
verschiedene Funktionen in seiner Werkzeugleiste zur Verfügung:

- Klicken Sie ![](attachments/40220446/40436726.png "plus-icon.png"),
    um eine neue Aggregation, Edition oder Kollektion zu erstellen, die
    einer existierenden Aggregation, Edition oder Kollektion
    untergeordnet ist
- Klicken Sie **![](attachments/40220446/40436725.png "delete.png")**,
    um ein gewähltes Objekt aus der Liste darunter zu entfernen
- Klicken
    Sie ![](attachments/40220446/40436724.png "expand-all-nodes.png") oder ![](attachments/40220446/40436723.png "collapse-all-nodes.png"),
    um alle Äste aus- oder zuzuklappen

## Attachments

- [plus-icon.png](attachments/7439451/7766184.png) (image/png)  
- [delete.png](attachments/7439451/7766185.png) (image/png)  
- [expand-all-nodes.png](attachments/7439451/7766186.png) (image/png)  
- [collapse-all-nodes.png](attachments/7439451/7766187.png) (image/png)  
