# User Interface

last modified on Feb 28, 2018

In diesem Kapitel finden Sie Informationen über die Struktur der
TextGrid Laboratory-Oberfläche. Die Eclipse-basierte graphische
Nutzeroberfläche besteht aus allgemeinen Menüleisten und
Werkzeug-spezifischen Perspektiven. Für weitere Informationen,
siehe [*http://www.eclipse.org/documentation/*](http://www.eclipse.org/documentation/).

![](attachments/40220240/40436627.png)

|                                                     |
|-----------------------------------------------------|
| **Beispielhafte Nutzeroberfläche mit leerer Suche** |

- [Bars](Bars_7439166.md)
- [Perspectives and Editors](Perspectives-and-Editors_7439180.md)
- [Views](Views_7439182.md)
- [Selection and Context Menu](Selection-and-Context-Menu_7439184.md)
- [Shortcuts](Shortcuts_7439186.md)
- [Preferences](Preferences_9012401.md)

## Attachments

- [gs-overview.png](attachments/7439164/9240661.png) (image/png)  
- [gs-overview.png](attachments/7439164/7766057.png) (image/png)  
