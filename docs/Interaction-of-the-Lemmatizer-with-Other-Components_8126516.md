# Interaction of the Lemmatizer with Other Components

last modified on Mai 04, 2016

Auf den Lemmatisierer kann direkt aus
dem [XML-Editor](XML-Editor_40220521.md) und dem [oXygen XML-Editor](Der-oXygen-XML-Editor_40220591.md) über das Kontextmenü
zugegriffen werden.
