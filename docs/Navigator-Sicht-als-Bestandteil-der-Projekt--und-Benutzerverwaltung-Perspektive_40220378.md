# Navigator-Sicht als Bestandteil der Projekt- und Benutzerverwaltung-Perspektive

last modified on Aug 19, 2015

Projekte werden in TextGrid verwendet, um den Zugriff auf Objekte zu
verwalten. All TextGrid-Objekte gehören zu einem Projekt. Die Objekte,
die zu einem Projekt gehören, werden im Navigator aufgelistet. Um ein
existierendes Projekt zu verwalten, wählen Sie es durch Linksklicken im
Navigator aus. Die Nutzer, die auf dieses Projekt zugreifen dürfen,
werden in der Benutzerverwaltung-Sicht angezeigt. Um Objekte und
Projekte zu bearbeiten, können Sie den [Navigator](40220331.md)
verwenden.
