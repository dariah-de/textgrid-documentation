# Create Links between Text and Image

last modified on Mai 03, 2016

Führen Sie diese Schritte durch, um Verknüpfungen zu erstellen:

1. Wählen Sie die Markierung, die Sie im Bild erstellt haben, durch
    Doppelklicken aus.
2. Wählen Sie Text in der XML-Editor-Sicht durch Klicken und Ziehen,
    durch Doppelklicken (wenn die Verknüpfung aus nur einem Wort
    besteht), Dreifachklicken (wenn die Verknüpfung aus einer ganzen
    Zeile besteht) oder durch Klicken des XML-Tags, um das gesamte
    XML-Element auszuwählen. Text und Bildausschnitte müssen nicht
    sofort verknüpft werden. Es ist auch möglich, eine größere Zahl von
    Markierungen zu erstellen und diese in einem späteren Schritt zu
    verknüpfen.
3. Erstellen Sie eine Verknüpfung durch Klicken
    von ![](attachments/40220667/40436856.png "link.png") in der
    Werkzeugleiste. Nachdem eine Verknüpfung zwischen einer Markierung
    im Bild und einem Textabschnitt hergestellt wurde, wird die
    Rechteck- oder Polygon-Markierung, die verknüpft wurde, in der
    Bild-Sicht mit einer durchgezogenen Umrandung dargestellt.
4. Speichern Sie das Ergebnis als neues Text-Bild-Link-Objekt, das die
    Verknüpfungsinformationen (die Text- und Bildkoordinaten, Pfad der
    verwendeten XML- und Bild-Objekte) enthält und das
    Symbol **![](attachments/40220667/40436853.png "069-zeige-BildLinkEditorPerspektive.png")** erhält.
    Klicken
    Sie ![](attachments/40220667/40436855.png "save.png") neben ![](attachments/40220667/40436856.png "link.png") ,
    um dieses neue Text-Bild-Link-Objekt zu speichern. Sobald ein Objekt
    gespeichert ist, können Sie durch einen Doppelklick XML-Texte,
    Bilder und Verknüpfungen neu laden und mit der Bearbeitung
    fortfahren.

![](attachments/40220667/40436852.png)

|                             |
|-----------------------------|
| **Text und Bild verknüpft** |

## Attachments

- [link.png](attachments/7439416/7766173.png) (image/png)  
- [save.png](attachments/7439416/7766174.png) (image/png)  
- [tble-prelink.png](attachments/7439416/7766175.png) (image/png)  
- [tble-allsaved.png](attachments/7439416/9240688.png) (image/png)  
- [069-zeige-BildLinkEditorPerspektive.png](attachments/7439416/8192276.png)
(image/png)  
- [tble-allsaved.png](attachments/7439416/7766176.png) (image/png)  
