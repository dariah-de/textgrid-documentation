# Interaction of the Metadata Template Editor View with Other Components

last modified on Mai 03, 2016

Der Metadaten-Vorlagen-Editor erlaubt Ihnen, die Metadaten für die
Bedürfnisse eines Projekts anzupassen. Die Schnittstelle
des [Metadaten-Editors](Metadaten-Editor_40220458.md) ändert sich
entsprechend der Anpassungen, die im Metadaten-Vorlagen-Editor gemacht
wurden.
