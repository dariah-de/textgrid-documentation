# Entwurf-Ansicht

last modified on Jul 31, 2015

Die Entwurf-Ansicht bietet in der linken Spalte einen Überblick über die
Baumstruktur des Dokuments. Die Inhalt-Spalte auf der rechten Seite
zeigt das Inhaltsmodell von Elementen, die Werte der Attribute und den
Textinhalt an.

Knoten können durch Klicken der Symbole + und – vor ihnen geöffnet und
geschlossen werden. Sie können die Werte von Attributen, den Inhalt von
Verarbeitungsanweisungen und den Textinhalt von Elementen direkt durch
Eingabe in das entsprechende Textfeld in der Spalte ‘Inhalt’ bearbeiten.
Sie können die Position von XML-Objekten durch Auswahl des Elements
mittels Ziehen und Ablegen an die neue graphisch hervorgehobene Position
verändern

![](attachments/40220541/40436782.png)

|                     |
|---------------------|
| **Entwurf-Ansicht** |

- [Funktionen der Entwurf-Ansicht](Funktionen-der-Entwurf-Ansicht_40220543.md)
- [Kontextmenü der Entwurf-Ansicht](40220545.md)

## Attachments

- [xml-designview.png](attachments/40220541/40436782.png) (image/png)  
