# Lemmatizer

last modified on Mai 04, 2016

Der Lemmatisierer erlaubt Ihnen, sich das zu einer bestimmten deutschen
Wortform gehörende Lemma ausgeben zu lassen sowie die unflektierte
Wortform zu analysieren. Der Lemmatisierer basiert auf der deutschen
Computermorphologie SMOR

[http://www.ims.uni-stuttgart.de/projekte/gramotron/PAPERS/LREC04/smor.pdf](http://www.ims.uni-stuttgart.de/projekte/gramotron/PAPERS/LREC04/smor.pdf)

und auf der morphologischen Lexikonkomponente MORPHISTO:

[http://www1.ids-mannheim.de/lexik/TextGrid/morphisto/](http://www1.ids-mannheim.de/lexik/TextGrid/morphisto/)

- [Install the Lemmatizer](Install-the-Lemmatizer_8130745.md)
- [Open the Lemmatizer](Open-the-Lemmatizer_8126499.md)
- [The Lemmatizer Tool](The-Lemmatizer-Tool_8126501.md)
- [Using the Lemmatizer](Using-the-Lemmatizer_8126504.md)
- [Interaction of the Lemmatizer with Other Components](Interaction-of-the-Lemmatizer-with-Other-Components_8126516.md)
