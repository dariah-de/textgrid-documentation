# Configuration of the plug-in

last modified on Mai 03, 2016

Um das Plugin nutzen zu können, muss in den
[Benutzervorgaben](Preferences_9012401.md) ein Endpunkt in Form einer
URL angegeben werden, die zu einem gestarteten SADE-Server führt. Im
Fall einer lokalen Installation auf dem eigenen PC lautet diese URL:
[http://localhost:8080/exist/apps/textgrid-connect/publish/textgrid/](http://localhost:8080/exist/apps/textgrid-connect/publish/textgrid/)

Als „Authorized User" gibt man „admin" an und das Passwortfeld kann in
der Standardkonfiguration leer bleiben.

![](attachments/41423843/41681306.jpg)

## Attachments

- [tg-sade-pub1.jpg](attachments/41423843/41681306.jpg) (image/jpeg)  
