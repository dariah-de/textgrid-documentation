# Kopie von Context Menu of the MEISE Outline View

last modified on Jul 24, 2015

The MEISE Outline View provides the following commands for editing the
document:

- “Undo” ![](attachments/40220933/40437029.png)and
    “Redo” ![](attachments/40220933/40437028.png) to reverse and restore
    changes in the document.
- “Delete” ![](attachments/40220933/40437034.png "042-loesche-Auswahl.png")
    deletes the selection from the document.
- The “Insert Container/Variant/Event/Additions" operations are only
    available when valid in the selected area (e.g. events can only be
    inserted into a layer - see the MEI schema) and offer quick access
    to frequently used elements.  
    ![](attachments/40220933/40437033.png "insertContextMenu.png")
- The Measure elements can be relabeled by right-clicking them and
    selecting "Relabel Measures"
    **![](attachments/40220933/40437032.png "action-relabelmeasures.png")**.
- The
    “Clone” **![](attachments/40220933/40437031.png "action-clone.png")**
    command duplicates the selected element with default initial values.
- To save your changes, click
    “Save” ![](attachments/40220933/40437030.png "save.png") on the
    bottom of the context menu. This button is only active if there have
    been changes since the last save.

## Attachments

- [redo_edit.png](attachments/40220933/40437028.png) (image/png)  
- [undo_edit.png](attachments/40220933/40437029.png) (image/png)  
- [save.png](attachments/40220933/40437030.png) (image/png)  
- [action-clone.png](attachments/40220933/40437031.png) (image/png)  
- [action-relabelmeasures.png](attachments/40220933/40437032.png)
(image/png)  
- [insertContextMenu.png](attachments/40220933/40437033.png) (image/png)  
- [042-loesche-Auswahl.png](attachments/40220933/40437034.png)
(image/png)  
- [meise-toolbar_doredo.png](attachments/40220933/40437035.png)
(image/png)  
