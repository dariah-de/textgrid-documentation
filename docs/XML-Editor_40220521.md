# XML-Editor

last modified on Aug 19, 2015

Der XML Editor ist ein interaktives Werkzeug zur Bearbeitung von
bestehenden und zur Erstellung von neuen XML-Dokumenten sowie der
Annotation derselben. XML ist eine Auszeichnungssprache, wobei in den
Auszeichnungselementen die Bedeutung des Inhaltes festgelegt wird und
optional auch dessen Darstellung. Mit XML können Daten in einem Text
organisiert und klassifiziert werden. Somit wird der
Informationsaustausch zwischen Programmen erleichtert, und durch die
Maschinenlesbarkeit sind sowohl Text als auch Struktur gut durchsuchbar.
Mehr Informationen zu XML gibt es auf

[http://www.w3.org/XML/](http://www.w3.org/XML/)

Innerhalb des TextGridLab stehen zwei XML Editoren zur Auswahl: der von
Eclipse mitgelieferte und ein speziell für TextGrid angepaßter. Dieser
basiert auf dem "[Eclipse Web Tools Platform Project](http://www.eclipse.org/webtools)" und dem Editor
"[Vex](http://wiki.eclipse.org/Vex)" (John Krasnay et al).

Das Handbuch erläutert die [TextGrid XML Editor Perspektive](XML-Editor-Perspektive_40220525.md) und seine
Bestandteile im einzelnen und enthält einige Beispiele zur [Benutzung des XML Editors](XML-Editor-verwenden_40220573.md).

- [XML-Editor öffnen](40220523.md)
- [XML-Editor-Perspektive](XML-Editor-Perspektive_40220525.md)
- [Editor-Sicht](Editor-Sicht_40220539.md)
- [Gliederung-Sicht](Gliederung-Sicht_40220561.md)
- [Eigenschaften-Sicht](Eigenschaften-Sicht_40220567.md)
- [XML-Editor verwenden](XML-Editor-verwenden_40220573.md)
- [Interaktion des XML-Editors mit anderen Komponenten](Interaktion-des-XML-Editors-mit-anderen-Komponenten_40220581.md)
