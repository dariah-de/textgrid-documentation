# Dictionary Search

last modified on Mai 03, 2016

Das Wörterbuchsuche-Werkzeug erlaubt Ihnen, das Wörterbuchnetz

[*http://www.woerterbuchnetz.de*](http://www.woerterbuchnetz.de/)

am Kompetenzzentrum für elektronische Erschließungs- und
Publikationsverfahren in den Geisteswissenschaften der Universität Trier
zu durchsuchen. Die folgenden Wörterbücher können durchsucht werden:

- Allgemeine Wörterbücher (Sprachstadien-übergreifend): Deutsches
    Wörterbuch von Jacob und Wilhelm Grimm; Grammatisch-Kritisches
    Wörterbuch der Hochdeutschen Mundart von Johann Christoph Adelung
- Autoren-Wörterbücher: Goethe-Wörterbuch
- Dialekt-Wörterbücher: Elsässisches Wörterbuch von Ernst Martin und
    Hans Lienhart; Lothringisches Wörterbuch von Ferdinand Follmann;
    Pfälzisches Wörterbuch von Ernst Christmann et al.; Rheinisches
    Wörterbuch; Rheinisches Wörterbuch Nachträge
- Mittelhochdeutsche Wörterbücher: Mittelhochdeutsches Handwörterbuch
    von Matthias Lexer; Mittelhochdeutsches Wörterbuch von Georg
    Friedrich Benecke, Wilhelm Müller und Friedrich Zarncke; Nachträge
    zum Mittelhochdeutschen Wörterbuch von Matthias Lexer
- Luxemburger Wörterbücher: Großes Luxemburger Wörterbuch, Wörterbuch
    der Luxemburger Mundart; Wörterbuch der Luxemburger Umgangssprache
- [Open the Dictionary Search](Open-the-Dictionary-Search_7439431.md)
- [Dictionary Search Perspective](Dictionary-Search-Perspective_7439433.md)
- [Dictionary Search View](Dictionary-Search-View_7439437.md)
- [Dictionary Search Results View](Dictionary-Search-Results-View_7439439.md)
- [Dictionary Grid View](Dictionary-Grid-View_7439441.md)
- [Using Dictionary Search](Using-Dictionary-Search_9012016.md)
- [Interaction of the Dictionary Search with Other Components](Interaction-of-the-Dictionary-Search-with-Other-Components_7439443.md)
