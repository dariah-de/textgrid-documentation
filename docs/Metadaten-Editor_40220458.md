# Metadaten-Editor

last modified on Aug 19, 2015

Der Metadaten-Editor wird verwendet, um die grundlegenden
Metadaten-Elemente von TextGrid-Objekten zu erstellen und zu verwalten.
Diese Metadaten werden in TextGrid für projektübergreifende Suchen
genutzt. Das Metadaten-Eingabeformular kann mit dem
[Metadaten-Vorlagen-Editor](Metadaten-Vorlagen-Editor_40220482.md) an
die individuellen Bedürfnisse angepasst werden.

- [Metadaten-Editor öffnen](40220460.md)
- [Metadaten-Editor-Sicht](Metadaten-Editor-Sicht_40220463.md)
- [Metadaten-Editor verwenden](Metadaten-Editor-verwenden_40220477.md)
- [Interaktion des Metadaten-Editors mit anderen Komponenten](Interaktion-des-Metadaten-Editors-mit-anderen-Komponenten_40220480.md)
