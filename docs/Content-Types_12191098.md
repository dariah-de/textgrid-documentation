# Content Types

last modified on Nov 12, 2012

Here is a table of content types available from the TextGridLab with all
our own plugins installed.

|                                                                                   |                                         |                                   |                    |                                                          |                                             |
|-----------------------------------------------------------------------------------|-----------------------------------------|-----------------------------------|--------------------|----------------------------------------------------------|---------------------------------------------|
|                                                                                   | MIME Type                               | Name                              | Filename Extension | Eclipse Content Type                                     | Contributing Plugin                         |
| ![linkeditorlinkedfile](attachments/12191098/12419235.png "linkeditorlinkedfile") | text/linkeditorlinkedfile               | Text Image Link Object            | tgl                | info.textgrid.lab.linkeditor.linkedfile                  | info.textgrid.lab.linkeditor.rcp_linkeditor |
| ![jpeg](attachments/12191098/12419234.png "jpeg")                                 | image/jpeg                              | JPEG image                        | jpg                | null                                                     | info.textgrid.lab.linkeditor.rcp_linkeditor |
| ![tiff](attachments/12191098/12419241.png "tiff")                                 | image/tiff                              | TIFF image                        | tiff               | null                                                     | info.textgrid.lab.linkeditor.rcp_linkeditor |
| ![png](attachments/12191098/12419238.png "png")                                   | image/png                               | PNG image                         | png                | null                                                     | info.textgrid.lab.linkeditor.rcp_linkeditor |
| ![gif](attachments/12191098/12419230.png "gif")                                   | image/gif                               | GIF image                         | gif                | null                                                     | info.textgrid.lab.linkeditor.rcp_linkeditor |
| ![xml-dtd](attachments/12191098/12419248.png "xml-dtd")                           | application/xml-dtd                     | DTD                               | dtd                | org.eclipse.wst.dtd.core.dtdsource                       | info.textgrid.lab.xmleditor.mpeditor        |
| ![xml](attachments/12191098/12419247.png "xml")                                   | text/xml                                | XML Document                      | xml                | org.eclipse.core.runtime.xml                             | info.textgrid.lab.xmleditor.mpeditor        |
| ![collation-set](attachments/12191098/12419226.png "collation-set")               | text/collation-set+xml                  | Collation Set                     | coll               | info.textgrid.lab.collatex.collation-set                 | info.textgrid.lab.collatex                  |
| ![collation-equivs](attachments/12191098/12419225.png "collation-equivs")         | text/collation-equivs+xml               | Collation Equivalences            | equiv              | info.textgrid.lab.collatex.equivalences                  | info.textgrid.lab.collatex                  |
| ![vnd-gcml](attachments/12191098/12419277.png)                                    | text/vnd.gcml+tei+xml                   | Gloss Commentary Document         | gcml               | info.textgrid.lab.glosses.gcml                           | info.textgrid.lab.glosses                   |
| ![glosscontrol](attachments/12191098/12419231.png "glosscontrol")                 | text/tg.glosscontrol+tg.aggregation+xml | Gloss Aggregation                 | gla                | info.textgrid.lab.core.aggregations.ui.aggregationeditor | info.textgrid.lab.glosses                   |
| ![mei](attachments/12191098/12419236.png "mei")                                   | text/mei+xml                            | MEI Notesheet                     | mei                | info.textgrid.lab.noteeditor.mei                         | info.textgrid.lab.noteeditor_restruc        |
| ![aggregation](attachments/12191098/12419224.png "aggregation")                   | text/tg.aggregation+xml                 | Aggregation                       | aggregation        | info.textgrid.lab.core.aggregations.ui.aggregationeditor | info.textgrid.lab.core.aggregations.ui      |
| ![edition](attachments/12191098/12419229.png "edition")                           | text/tg.edition+tg.aggregation+xml      | Edition                           | edition            | info.textgrid.lab.core.aggregations.ui.aggregationeditor | info.textgrid.lab.core.aggregations.ui      |
| ![collection](attachments/12191098/12419227.png "collection")                     | text/tg.collection+tg.aggregation+xml   | Collection                        | collection         | info.textgrid.lab.core.aggregations.ui.aggregationeditor | info.textgrid.lab.core.aggregations.ui      |
| ![imex](attachments/12191098/12419233.png "imex")                                 | text/tg.imex+xml                        | Import / Export Mapping           | imex               | info.textgrid.lab.core.importexport.importspec           | info.textgrid.lab.core.importexport         |
| ![zip](attachments/12191098/12419251.png "zip")                                   | application/zip                         | ZIP Archive                       | zip                | null                                                     | info.textgrid.lab.core.importexport         |
| ![projectfile](attachments/12191098/12419239.png "projectfile")                   | text/tg.projectfile+xml                 | Project Configuration             | .tgp               | info.textgrid.lab.core.model.projectfile                 | info.textgrid.lab.core.model                |
| ![unknown](attachments/12191098/12419243.png "unknown")                           | unknown/unknown                         | Unknown                           | tgo                | null                                                     | info.textgrid.lab.core.model                |
| ![x-unknown](attachments/12191098/12419250.png "x-unknown")                       | image/x-unknown                         | Image                             | null               | null                                                     | info.textgrid.lab.core.model                |
| ![work](attachments/12191098/12419245.png "work")                                 | text/tg.work+xml                        | Work                              | work               | null                                                     | info.textgrid.lab.core.model                |
| ![ttle](attachments/12191098/12419242.png "ttle")                                 | text/ttle                               | Text-Text-Link Object             | tlo                | info.textgrid.lab.ttle.texttextlink                      | info.textgrid.lab.ttle                      |
| ![xml-xslt](attachments/12191098/12419279.png)                                    | text/xml+xslt                           | XSLT Stylesheet                   | xsl                | org.eclipse.wst.xml.core.xslsource                       | info.textgrid.lab.xsltsupport               |
| ![xsd](attachments/12191098/12419249.png "xsd")                                   | text/xsd+xml                            | XML Schema                        | xsd                | org.eclipse.wst.xsd.core.xsdsource                       | info.textgrid.lab.core.application          |
| ![css](attachments/12191098/12419228.png "css")                                   | text/css                                | Cascading Stylesheet (CSS)        | css                | org.eclipse.core.runtime.text                            | info.textgrid.lab.core.application          |
| ![plain](attachments/12191098/12419237.png "plain")                               | text/plain                              | Plain Text                        | txt                | org.eclipse.core.runtime.text                            | info.textgrid.lab.core.application          |
| ![workflow](attachments/12191098/12419246.png "workflow")                         | text/tg.workflow+xml                    | TextGrid Workflow Document        | tgwf               | org.eclipse.core.runtime.xml                             | info.textgrid.lab.workflow                  |
| ![gwdl-workflow](attachments/12191098/12419281.png)                               | text/gwdl.workflow+xml                  | TextGrid Workflow Document (GWDL) | gwdl               | org.eclipse.core.runtime.xml                             | info.textgrid.lab.workflow                  |
| ![servicedescription](attachments/12191098/12419240.png "servicedescription")     | text/tg.servicedescription+xml          | TextGrid Service Description      | service            | org.eclipse.core.runtime.xml                             | info.textgrid.lab.workflow                  |

The table above has been generated automatically from the TextGridLab
(nightly version): Hit Ctrl+3, type *Export Content Types*. In the save
dialog, select *Confluence Wiki* as output format. You're advised to
save the stuff to an empty directory since the tool will write out the
icons as png files along the wiki file, and it will overwrite without
asking.

## Attachments

- [aggregation.png](attachments/12191098/12419224.png) (image/png)  
- [collation-equivs.png](attachments/12191098/12419225.png) (image/png)  
- [collation-set.png](attachments/12191098/12419226.png) (image/png)  
- [collection.png](attachments/12191098/12419227.png) (image/png)  
- [css.png](attachments/12191098/12419228.png) (image/png)  
- [edition.png](attachments/12191098/12419229.png) (image/png)  
- [gif.png](attachments/12191098/12419230.png) (image/png)  
- [glosscontrol.png](attachments/12191098/12419231.png) (image/png)  
- [imex.png](attachments/12191098/12419233.png) (image/png)  
- [jpeg.png](attachments/12191098/12419234.png) (image/png)  
- [linkeditorlinkedfile.png](attachments/12191098/12419235.png)
(image/png)  
- [mei.png](attachments/12191098/12419236.png) (image/png)  
- [plain.png](attachments/12191098/12419237.png) (image/png)  
- [png.png](attachments/12191098/12419238.png) (image/png)  
- [projectfile.png](attachments/12191098/12419239.png) (image/png)  
- [servicedescription.png](attachments/12191098/12419240.png)
(image/png)  
- [tiff.png](attachments/12191098/12419241.png) (image/png)  
- [ttle.png](attachments/12191098/12419242.png) (image/png)  
- [unknown.png](attachments/12191098/12419243.png) (image/png)  
- [work.png](attachments/12191098/12419245.png) (image/png)  
- [workflow.png](attachments/12191098/12419246.png) (image/png)  
- [xml.png](attachments/12191098/12419247.png) (image/png)  
- [xml-dtd.png](attachments/12191098/12419248.png) (image/png)  
- [xsd.png](attachments/12191098/12419249.png) (image/png)  
- [x-unknown.png](attachments/12191098/12419250.png) (image/png)  
- [zip.png](attachments/12191098/12419251.png) (image/png)  
- [vnd-gcml.png](attachments/12191098/12419277.png) (image/png)  
- [xml-xslt.png](attachments/12191098/12419279.png) (image/png)  
- [gwdl-workflow.png](attachments/12191098/12419281.png) (image/png)  
