# Installing Additional Tools

last modified on Sep 09, 2014

As a modular system, the TextGridLab allows for installation of
additional software or updates of existing modules.

## Installing additional software using the Marketplace

The [Marketplace](Marketplace_8130167.md) offers a catalogue of
additional tools that can be installed into the TextGridLab, tools from
the TextGrid team as well as tools from external developers. 

The marketplace is still an experimental feature, not all features will
work as desired.

## Updating existing tools

By default, the TextGridLab will check for updates of all installed
components in the background after startup. If there are updates
available, it will notify you using a notification window (that will go
away after a few seconds) and an icon in the status bar. Click on the
icon or notification to open an update wizard that will install the new
versions of the respective tools on request.

You can manually force an update using Help → Check for Updates. You can
turn off automatic update checks using *Window → Preferences →
Install/Update → Automatic Updates* (find the Preferences window in the
TextGridLab menu on MacOS). 

If you wish to only update a specific tool, open the *Installation
Details* dialog as described in the following section, select the tool
on the *Installed Software* page and click *Update*.

## Removing tools

You can remove tools you no longer need from your TextGridLab
installation by way of the *installation detail* dialog:

1. Open the *About TextGridLab* dialog from the *Help* menu (or
    the *TextGridLab* menu on Mac OS)
2. Click the *Installation Details* button to open the corresponding
    dialog
3. On the *Installed Software* page, select the tool(s) you wish to
    uninstall and click the *Uninstall* button.
4. The uninstall wizard will list everything to be uninstalled. Click
    *Finish*.
5. After uninstallation, you should restart the TextGridLab as
    recommended.

Note that you cannot remove tools that are TextGridLab base components
or that other tools depend on.

## Advanced Tasks

Marketplace and update management are easy-to-use frontends for the
plugin management system that you can also use directly. If you plan to
install tools or software versions that are not available from the
Marketplace, follow the instructions in this section.

### Background: Update Sites

TextGridLab components are available from *Update Sites*. There are
three main composite update site for TextGridLab components:

- [http://www.textgridlab.org/updates/**stable**](http://www.textgridlab.org/updates/stable) for the stable, 2.x release train,
- [http://www.textgridlab.org/updates/**beta**](http://www.textgridlab.org/updates/beta) for
    beta-quality tools, and
- [http://www.textgridlab.org/updates/**nightly**](http://www.textgridlab.org/updates/nightly) for
    bleeding-edge and untested versions that come directly out of our
    [automatic build system](http://dev.digital-humanities.de/ci/view/lab-tycho).

### Installing software directly from update sites

To browse an update site and directly install software from it (that may
not yet be part of the marketplace), 

1. Open the wizard at *Help → Install New Software*
2. Select the update site you want to browse from the *Work with* drop
    down, or enter its URL (see the list above) into the drop down's
    input field and press your Enter key
3. Check the tool(s) you wish to install in the tree below
4. Work through the rest of the wizard using the *Next* and *Finish*
    buttons.

You will need to restart the TextGridLab after installing software.

You are not neccessarily limited to TextGridLab tools – you can
theoretically install any Eclipse plug-in into the TextGridLab using
this method. However, tools that are neither at our update site nor in
our marketplace have not been tested to work with TextGridLab, and thus
may not work.

### Managing update sites

You can use the preference page at *Window → Preferences →
Install/Update → Available Software Sites* to manage the sites that
should be available from the dropdown box and that should be checked for
updates.

If you would like to (e.g.) only work with the nightly versions of a
specific tool and leave all other tools at their stable versions, you
might want to enable only the nightlies update site for that tool. To do
so,

1. find out the nightly update site for the tool. Either you look at
    the [index page of the composite nightly update site](http://www.textgridlab.org/updates/nightly), or have a look at
    the corresponding build page on the integration build server.
2. install the tool from that site as described above.
3. Go to the *Available Software Sites* page and check that the
    composite nightly update
    site, [http://www.textgridlab.org/updates/**nightly**](http://www.textgridlab.org/updates/nightly),
    is not enabled.

If you wish, you can also download the nightly build of the complete
TextGridLab from our [integration build server](http://dev.digital-humanities.de/ci/job/lab-base).

### Fixing broken nightlies

Since nightlies and their update process are not tested, it can happen
that after an update the TextGridLab does not start anymore. There are
various ways to recover from that:

- You can always download a new working lab and move the `workspace`
    directory from the old one to the new one before the first start.
    This will retain your settings, but you'll have to reinstall all
    additional plugins in the new lab.
- When someone has fixed the nightly such that the current nightly
    site contains a working version, you can update your installed lab
    from the command line by running the following command in the
    TextGridLab application directory:

    ``` syntaxhighlighter-pre
    ./textgridlab -nosplash -application org.eclipse.equinox.p2.director \ 
                  -uninstallIU info.textgrid.lab.base.product \
                  -installIU info.textgrid.lab.base.product \
                  -repository http://www.textgridlab.org/updates/nightly
    ```

    Windows users will need to run `eclipsec.exe` instead
    of `./textgridlab`.
