#!/usr/bin/env python

import re
import os
from datetime import datetime

# URLs have the format prefix/version/filename
prefix = "http://www.textgridlab.org/download"

# table head
head = """
<table class="table table-hover">
    <tbody>
"""


# Pattern for each table row
row = """
    <tr class="download {os} {arch}">
      <td><a href="{prefix}/{version}/{filename}">{filename}</a></td>
      <td>{size}</td>
      <td>{date}</td>
    </tr>
    """

# end of table
tail = """
    </tbody>
</table>
"""

def human_size(bytesize):
    """
    Formats a file size in bytes to a human readable format.
    """
    for x in ['bytes','KB','MB','GB','TB']:
        if bytesize < 1024.0:
            return "%3.1f %s" % (bytesize, x)
        bytesize /= 1024.0

# match, e.g., TextGridLab-2.0.2-linux.gtk.x86_64.zip
#                         version  os  ws  arch   ext
fnpattern = re.compile("^TextGridLab-(?P<version>[^-]*)-"
        "(?P<os>[^.]*)\.(?P<ws>[^.]*)\.(?P<arch>[^.]*)\."
        "(?P<ext>.*)$")

print(head)
for filename in sorted([x for x in os.listdir(".") if fnpattern.match(x)]):
    stat = os.stat(filename)
    details = fnpattern.match(filename).groupdict()
    print(row.format(
        os = details["os"],
        arch = details["arch"],
        prefix = prefix,
        version = details["version"],
        filename = filename,
        size = human_size(stat.st_size),
        date = datetime.fromtimestamp(stat.st_ctime).strftime("%Y-%m-%d %H:%M:%S")
        ))
print(tail)
