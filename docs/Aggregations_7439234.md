# Aggregations

last modified on Mai 03, 2016

Eine Aggregation ist ein TextGrid-Objekt, das aus seiner sortierten
Liste von Referenzen auf andere TextGrid-Objekte besteht, die wiederum
selbst Aggregationen sein können.

Aggregationen werden verwendet, um TextGrid-Objekte zu organisieren. Sie
werden ähnlich wie Dateiordner verwendet, sind aber flexibler: Auf ein
Objekt kann von mehreren Aggregationen referenziert werden, und Sie
können Objekte in Ihrer Aggregation sammeln, die zu anderen Nutzern
gehören und auf die Sie nur lesend zugreifen können. Wenn Sie eine
Aggregation löschen, verbleiben die aggregierten Objekte im Normalfall –
es sei denn, Sie geben explizit den Befehl, sie zu löschen. Siehe dazu
die Ankreuzfelder im Assistenten “Löschen”.

Es können verschiedene Arten von Objekten in einer Aggregation sein, die
sich in ihrer Semantik und ihren Metadaten unterscheiden, aber alle
haben die gleiche Art von Inhalt: Einfache Aggregationen, die nur
Element-[Metadaten](Metadaten-Editor_40220458.md) haben und verwendet
werden können, um beispielsweise ein Kapitel in einem Buch zu
repräsentieren, das auf verschiedene Dateien aufgeteilt
ist. [Editionen](Editionen_40220434.md) oder [Kollektionen](Kollektionen_40220436.md) sind
ebenfalls Aggregationen, die durch ein charakteristisches Set von
Metadaten definiert ist. Verwenden Sie
den [Aggregationen-Editor](Aggregationen-Editor_40220440.md), um
Objekte in einem Set von Aggregationen anzuordnen.
