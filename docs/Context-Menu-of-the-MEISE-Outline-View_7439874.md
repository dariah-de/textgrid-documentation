# Context Menu of the MEISE Outline View

last modified on Mai 04, 2016

The MEISE Outline View provides the following commands for editing the
document:

- “Undo” ![](attachments/7439874/9240622.png)and
    “Redo” ![](attachments/7439874/9240623.png) to reverse and restore
    changes in the document.
- “Delete” ![](attachments/7439874/8192033.png "042-loesche-Auswahl.png") deletes
    the selection from the document.
- The “Insert Container/Variant/Event/Additions" operations are only
    available when valid in the selected area (e.g. events can only be
    inserted into a layer - see the MEI schema) and offer quick access
    to frequently used elements.  
    ![](attachments/7439874/8192194.png "insertContextMenu.png")
- The Measure elements can be relabeled by right-clicking them and
    selecting "Relabel
    Measures"** ![](attachments/7439874/8192195.png "action-relabelmeasures.png")**.
- The
    “Clone” **![](attachments/7439874/8192196.png "action-clone.png")** command
    duplicates the selected element with default initial values.
- To save your changes, click
    “Save” ![](attachments/7439874/8192307.png "save.png") on the bottom
    of the context menu. This button is only active if there have been
    changes since the last save.

## Attachments

- [meise-toolbar_doredo.png](attachments/7439874/8192032.png)
(image/png)  
- [042-loesche-Auswahl.png](attachments/7439874/8192033.png) (image/png)  
- [insertContextMenu.png](attachments/7439874/8192194.png) (image/png)  
- [action-relabelmeasures.png](attachments/7439874/8192195.png)
(image/png)
- [action-clone.png](attachments/7439874/8192196.png) (image/png)  
- [save.png](attachments/7439874/8192307.png) (image/png)  
- [undo_edit.png](attachments/7439874/9240622.png) (image/png)  
- [redo_edit.png](attachments/7439874/9240623.png) (image/png)  
