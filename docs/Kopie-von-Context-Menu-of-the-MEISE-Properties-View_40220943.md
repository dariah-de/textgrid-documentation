# Kopie von Context Menu of the MEISE Properties View

last modified on Jul 24, 2015

The context menu in the Properties View allows you to copy and to
restore the value of a given property. If you click in the text field of
the value column, the context menu will be borrowed from your operating
system.
