# Kopie von Title Bar of the MEISE Properties View

Created on Jul 24, 2015

The Title Bar of the MEISE Properties View offers you several icons to
perform a range of operations:

- Click ![](attachments/40220941/40437043.png "Bildschirmfoto 2012-01-30 um 11.56.17.png")
    to pin this property view to the current selection
- Click **![](attachments/40220941/40437044.png "Bildschirmfoto 2012-01-30 um 11.56.41.png") **
    to show all properties of the preselected element in the view
    (including the properties in the “Basic” category)
- Click** ![](attachments/40220941/40437046.png "Bildschirmfoto 2012-01-30 um 11.56.52.png")**
    to show advanced properties
- Click **![](attachments/40220941/40437045.png "Bildschirmfoto 2012-01-30 um 11.57.07.png")**
    to restore the default value of a selected property
- Click the white triangle to open the view menu. The view menu allows
    you to open a new Properties View
    ![](attachments/40220941/40437041.png "085-oeffne-InNeuenFenster.png"), 
    pin a property to a selection and  show categories and advanced
    properties to define the width of the two columns “Property” and
    “Value” after clicking “Configure Columns…”
- Click the remaining icons  to minimize and maximize the view

There is also a context menu in the title bar that is identical to all
view title bar context menus in Eclipse.

## Attachments

- [085-oeffne-InNeuenFenster.png](attachments/40220941/40437041.png)
(image/png)  
- [086-oeffne-ImNeuenEditor.gif](attachments/40220941/40437042.gif)
(image/gif)  
- [Bildschirmfoto 2012-01-30 um 11.56.17.png](attachments/40220941/40437043.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.56.41.png](attachments/40220941/40437044.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.57.07.png](attachments/40220941/40437045.png) (image/png)  
- [Bildschirmfoto 2012-01-30 um 11.56.52.png](attachments/40220941/40437046.png) (image/png)  
- [tree_mode.png](attachments/40220941/40437047.png) (image/png)  
