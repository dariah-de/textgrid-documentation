# SADE TextGrid reference installation

last modified on Mai 03, 2016

Unter der Adresse [http://sade.textgrid.de/](http://sade.textgrid.de/)
steht ein Testserver zur Verfügung, um ohne Download und Installation
der Software die Benutzung und Eignung für eigene Projekte zu testen.
Für die hier abgelegten Daten wird keinerlei Gewährleistung übernommen,
diese können jederzeit zurückgestzt werden, somit eignet sich dieser
Server nur zum testen. Unter der Adresse  
[http://sade.textgrid.de/exist/apps/SADE/textgrid/sign-up.html](http://sade.textgrid.de/exist/apps/SADE/textgrid/sign-up.md)
können eigene Testaccounts an der Sade-Installation registriert werden,
eine Email mit den benötigten Informationen wird dann versendet.
