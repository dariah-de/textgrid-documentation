# Navigator öffnen

last modified on Jul 28, 2015

Der Navigator ist standardmäßig Bestandteil der Projekt- und
Benutzerverwaltung-Perspektive, des Workflow-Werkzeugs, des XML und des
Text-Bild-Link-Editors sowie der Aggregationen-Editor-Perspektive. Der
Navigator kann in allen Perspektiven (wieder) geöffnet werden, was
bedeutet, dass eine Perspektive geöffnet sein muss, die den Navigator
beinhalten kann. Um den Navigator zu öffnen:

- wählen Sie “Werkzeuge \> Sicht anzeigen \> Navigator” in der
    Menüleiste
- klicken Sie  in der Werkzeugleiste.

Das Verwenden der Navigator-Sicht erfordert eine Authentifizierung.
Mindestens eine Perspektive muss zudem geöffnet sein, um den Navigator
zu öffnen.

## Attachments

- [079-zeige-NavigatorAnsicht3.png](attachments/40220333/40436691.png)
(image/png)  
