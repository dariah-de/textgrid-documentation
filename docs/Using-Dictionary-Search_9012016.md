# Using Dictionary Search

last modified on Mai 03, 2016

Um ein Wort nachzuschlagen, füllen Sie die Eingabemaske der
Wörterbuchsuche wie folgt aus:

1. Geben Sie ein Schlüsselwort im ersten Eingabefeld
    der [Wörterbuchsuche-Sicht](40220708.md) ein. Die Suche
    berücksichtigt keine Groß- und Kleinschreibung. Alternativ können
    Sie ein Wort
    im [XML-Editor](XML-Editor_40220521.md) Doppelklicken, wenn die
    Wörterbuchsuche geöffnet ist.
2. Sie können die Zahl der angezeigten Ergebnisse beschränken.
    Standardmäßig werden die ersten zehn Ergebnisse angezeigt.
3. Wählen sie zwischen exakter und unscharfer (“fuzzy”) Suche. In der
    unscharfen Suche werden auch ähnliche Zeichenketten gefunden.
4. Setzen Sie Häkchen in den Ankreuzfeldern der Wörterbücher, die
    durchsucht werden sollen.
5. Um die Suche zu starten, klicken Sie die Schaltfläche “Suche
    starten”.
6. Sie können Platzhalter verwenden. Das Asterisk-Zeichen (\*) steht
    dabei für eine beliebige Zeichenkette (auch null Zeichen) und das
    Fragezeichen (?) steht für genau ein Zeichen. Sie können diese
    Platzhalter beliebig kombinieren.
7. Die Ergebnisse werden in
    der[ Wörterbuchsuchergebnisse-Sicht](40220710.md) angezeigt und
    die ausgewählten Ergebnisse in
    der [Wörterbuch-Grid-Sicht](40220712.md)
