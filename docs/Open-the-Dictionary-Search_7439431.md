# Open the Dictionary Search

last modified on Mai 03, 2016

Um die Wörterbuchsuche-Perspektive zu öffnen

- wählen Sie das Wörterbuchsuche-Werkzeug auf dem Startbildschirm oder
- wählen Sie “Werkzeuge \> Wörterbuchsuche” in der Menüleiste oder
- klicken Sie ![](attachments/40220704/40436893.png) in der
    Werkzeugleiste.

Die Wörterbuchsuchergebnisse-Sicht, die Bestandteil der Wörterbuchsuche
ist, kann auch separat geöffnet werden durch

- Wählen von “Werkzeuge \> Wörterbuchsuche” in der Menüleiste oder
- Klicken von **![](attachments/40220704/40436892.png)** in der
    Werkzeugleiste.

## Attachments

- [073-zeige-WoerterbuchPerspektive.png](attachments/7439431/7766178.png)
(image/png)  
- [084-zeige-WoerterbuchErgebnisse.png](attachments/7439431/7766179.png)
(image/png)  
