# Image View

last modified on Mai 03, 2016

Die Bild-Sicht ist der Arbeitsbereich des TextGridLab zur Bearbeitung
von Bildern. Einige seiner Funktionen können zudem zur Verknüpfung von
Bild- und Textelementen verwendet werden.

- [Features of the Image View](Features-of-the-Image-View_7439386.md)
- [Menu Bar of the Image View](Menu-Bar-of-the-Image-View_7439390.md)
- [Title Bar of the Image View](Title-Bar-of-the-Image-View_7439392.md)
- [Status Bar of the Image View](Status-Bar-of-the-Image-View_7439394.md)
- [Shortcuts of the Image View](Shortcuts-of-the-Image-View_7439396.md)
- [Context Menu of the Image View](Context-Menu-of-the-Image-View_7439398.md)
