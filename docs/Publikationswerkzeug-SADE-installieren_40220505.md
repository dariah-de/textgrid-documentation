# Publikationswerkzeug SADE installieren

last modified on Aug 19, 2015

Um das SADE-Modul zu nutzen, muß es zunächst über den
[Marketplace](40220207.md) installiert werden. Sie benötigen außerdem
einen [SADE-Server](40220507.md), auf dem die Texte dann angezeigt
werden.
