# Architecture Introduction

last modified on Mai 30, 2012

## Introduction

As one of the Community Grid projects of the first D-Grid call, TextGrid
established a Virtual Research Environment for the Humanities in
Germany. This VRE is part of a larger infra- structure enabling the
collective utilisation and exchange of data, tools and methods – with a
strong focus on text-oriented research. The central component of this
infrastructure is a grid- based repository ensuring the sustainable
availability of and access to research data, the so- called TextGrid
Repository. This repository has proven itself as a stable and reliable
storage device. While currently providing Bitstream Preservation on a
very basic level, TextGrid aims for a repository solution that supports
the curation and long-term preservation of research data. With the
proposal for the second project phase (2009-2012), TextGrid committed
itself to implement the long-term storage architecture to be developed
by the WissGrid project, an- other D-Grid project bringing together the
five academic communities of the first D-Grid call to develop generic
solutions and blueprints for academic grid communities and users. This
report gives an overview of the concepts and technical solutions
developed by WissGrid – as far as TextGrid is concerned – and describes
the technical context and measures for the im- plementation of these
solutions with the TextGrid infrastructure. For a better understanding
of the chosen implementation strategy we'll start with an overview of
the basic concepts in TextGrid.  
Since the interaction with WissGrid and the implementation of the
WissGrid solutions is a highly dynamical process, this report is
considered to be a living document, to be updated (more or less)
regularly.
