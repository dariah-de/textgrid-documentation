# Editor Field

last modified on Mai 04, 2016

Dieser Abschnitt beschreibt, wie Sie mit der Editor-Sicht in der Mitte
arbeiten können, wenn ein Dokument geöffnet ist. Am unteren Ende der
Editor-Sicht können Sie zwischen den drei verschiedenen
Bearbeitungs-Ansichten “Entwurf”, “Quelle” und “WYSIWYM” sowie einer
Vorschau-Ansicht als Anzeigemodus wählen.

- Die Entwurf-Ansicht zeigt die hierarchische Struktur des Dokuments.
- Die Quelle-Ansicht zeigt das Dokument in der Auszeichnungssprache
    XML. Dies erlaubt Ihnen, auf seine Struktur zuzugreifen und sie zu
    verändern.
- Die WYSIWYM (=“What You See Is What You Mean”)-Ansicht zeigt Ihnen
    eine beispielhafte Darstellung und formatiert den Inhalt für das
    Web.
- Die Vorschau-Ansicht präsentiert ein HTML-Dokument nach der
    Verarbeitung des XML-Objekts durch eine adäquate XSLT-Datei.

Sie können durch Klicken der Reiter am unteren Ende des Editors wischen
diesen Bearbeitungs-Ansichten wechseln. Das Asterisk-Zeichen (\*) in der
Titelleiste der Editorsicht zeigt an, dass die Datei noch nicht
gespeichert wurde. In jeder Ansicht ist eine Liste von Tastaturkürzeln
durch Eingabe von Strg+Umschalttaste+L verfügbar.

- [Design View](Design-View_7439336.md)
- [Source View](Source-View_7439338.md)
- [WYSIWYM View](WYSIWYM-View_7439340.md)
- [Preview](Preview_8129668.md)
