# [1.3.0](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.14...v1.3.0) (2024-09-12)


### Bug Fixes

* increase doc version ([f201cd5](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/f201cd538ad3b6b236d7e049907ce7219224a5df))


### Features

* add cts logo again! yeah! ([184e68c](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/184e68ccbb11d39a90dd32dfe02bedf88a3333b8))

## [1.2.14](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.13...v1.2.14) (2024-04-11)


### Bug Fixes

* replace dev2 links ([bea0ef6](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/bea0ef626583b90955d15965a7a92a351547bc47))

## [1.2.13](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.12...v1.2.13) (2024-03-25)


### Bug Fixes

* add folder (again...) :-D ([7d430ed](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/7d430ed72d237f10be64b2c5e1e46970950ee9d8))
* fix favicon location ([d016586](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/d016586e7d1ac31391ac36a6569bdf1f6f338522))

## [1.2.12](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.11...v1.2.12) (2024-03-25)


### Bug Fixes

* add logos and update footer ([efefb04](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/efefb04af74b25c9b677e864af8549fa02024da1))
* make transition faster ([710c464](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/710c464a38f0c028181ead4b00d975042b1613a8))
* move overrides folder ([164cd81](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/164cd81002e9aa54dcaee5f59e271cca5d622056))
* move overrides folder ([0592f2f](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/0592f2f6393a7350b57e28e224e8453660703337))
* remove Architecture from sitebar ([09dc64b](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/09dc64b691dbbb16eafef11344458571894de388))
* use relative pathes to config folders ([3f88418](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/3f88418e3e032700ec5fd8044ad85ad8458a1414))

## [1.2.11](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.10...v1.2.11) (2024-03-06)


### Bug Fixes

* add some more redirects ([61e5ae8](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/61e5ae8a4a2a17b8d2e1983b0c5c11b1c5bcdbcc))

## [1.2.10](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.9...v1.2.10) (2023-12-14)


### Bug Fixes

* add to troubleshooting, increase version ([f808368](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/f8083682c172a471062a4a3662e3f188aa208ec4))
* fix link to tgrep doc ([d37f61c](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/d37f61c1b61d992fc83e990b65f473d9de84d3f2))
* rename troubleshooting_old ([6106f78](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/6106f78774a764fe76fb133d16c4a2b559d20f50))

## [1.2.9](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.8...v1.2.9) (2023-11-09)


### Bug Fixes

* edit troubleshooting, add ts to main menu ([19cc19b](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/19cc19b317554169544b578853b0270c38204814))

## [1.2.8](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.7...v1.2.8) (2023-11-07)


### Bug Fixes

* add mkdocs-redirects plugin ([7c2f5b7](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/7c2f5b72a48069eed39d4111ec47849a22b69911))
* add new pages ([5018152](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/5018152da28836c369b71743e0fa558a0730321a))
* delete old user manual poge ([0190fc3](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/0190fc335fc83f0b908aa6a7331064d16545c988))
* fix config error ([5282e1b](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/5282e1b8c8ec540b2bd6677257427ec42d0a9bad))
* rename left main menu ([1f2f996](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/1f2f9967286b5801ce2668ed318bd8f6e83cf2ce))
* rename Nutzerhandbuch-2.0_40219599.md ([252f0b0](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/252f0b04657e93c1a942b79eb6c5fa83a9e90f37))
* upDATE :-) ([9e41c9b](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/9e41c9b2009a81ccc130307e19c89ceebeb5599b))

## [1.2.7](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.6...v1.2.7) (2023-08-01)


### Bug Fixes

* automatic version changes removed from pages, too ([ac603a6](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/ac603a667d1ebb34e51eb48c406540b1a5a30e60))

## [1.2.6](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.5...v1.2.6) (2023-08-01)


### Bug Fixes

* version string update ([7bf9a28](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/7bf9a2805c6886015a3f957b87f9b8e806e01128))

## [1.2.5](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.4...v1.2.5) (2023-08-01)


### Bug Fixes

* add cat to dockerfile for date check ([50dd0eb](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/50dd0eb25ea2d7ee334f01f04a5a8f7638ce60a3))
* add date check output ([19674af](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/19674af2691bf7687f5ed8772e25757bac7aff85))
* arg! ([9db10f2](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/9db10f2f31df41bb42fef70816b02fbab3d26a96))
* arg! date testing ([9879e7b](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/9879e7bae7e927f5d1c08e7653311d0a464b73f3))
* more date testing...? ([8c77b4a](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/8c77b4a0a6dbeeb92603cc9233986fdefadaef81))
* remove automatic date creation ([9676001](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/9676001727081b3598c03778405da70288fd45d6))
* testing sed ([824fa16](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/824fa164942ed159a50c6cb6635e58d1d18ae403))

## [1.2.4](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.3...v1.2.4) (2023-07-31)


### Bug Fixes

* fix broken links on main page ([669d1d8](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/669d1d8fa708ad52ec9ccec79fe7072009547452))
* more broken link fixes ([19a4c16](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/19a4c163b136f8ff814ecf4d6df4942b42ef4d60))
* reorder some list items ([7950e39](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/7950e39e74a9efe6f64b856a24397b76554e8328))

## [1.2.3](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.2...v1.2.3) (2023-06-22)


### Bug Fixes

* put date as version in dockerfile ([e794019](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/e7940197b17024cbd533a7f802fd9e437353cbc6))

## [1.2.2](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.1...v1.2.2) (2023-06-22)


### Bug Fixes

* add cheatsheet also as png ([8c4486f](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/8c4486f6d7a94f0fab5cea7ca3afe1346eefcc1a))

## [1.2.1](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.2.0...v1.2.1) (2023-06-22)


### Bug Fixes

* add new URLs for TextGrid metadata related sites ([e9ef633](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/e9ef6333bf7ba69d24fb90b69c2ee200f001bed2))

# [1.2.0](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.1.0...v1.2.0) (2023-06-08)


### Features

* new version look ([76167fb](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/76167fb533e0f110eeec213e112bc050e8279007))

# [1.1.0](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.0.1...v1.1.0) (2023-06-06)


### Features

* add new container refs ([75882b4](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/75882b48eb13a0ed88919ab2cec283d0e41e5070))

## [1.0.1](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/compare/v1.0.0...v1.0.1) (2023-06-06)


### Bug Fixes

* css update ([556e44e](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/556e44efc2241d6824842329cc5dd4bc2c517274))

# 1.0.0 (2023-06-05)


### Bug Fixes

* remove some ids from filenames ([5c78e2e](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/5c78e2ea483f05e87301fe7ef0514fad2f2104a2))


### Features

* add improved ci and deployment workflow ([84315a2](https://gitlab.gwdg.de/dariah-de/textgrid-documentation/commit/84315a2e8ab1df8565a2d83791c56d70c7a3eeec))

# CHANGELOG

## April 2023

- first commit and deployment to GitLab pages
