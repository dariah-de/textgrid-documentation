###
# build docs
###
FROM python:3.10 as doc-builder

COPY ./docs /docs
COPY ./mkdocs.yml /
WORKDIR /

RUN apt-get update && apt-get -y install python3-virtualenv
RUN virtualenv venv
RUN pwd . venv/bin/activate
RUN pip install mkdocs
RUN pip install mkdocs-redirects
RUN mkdocs build

###
# assemble container
###

FROM docker.io/nginxinc/nginx-unprivileged:1.24.0-alpine3.17-slim

COPY --from=doc-builder /site /usr/share/nginx/html
