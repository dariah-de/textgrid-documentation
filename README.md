# TextGrid Documentation

All the public documentation from the TextGrid public wiki <https://wiki.de.dariah.eu/display/TextGrid>.

For mkdocs all MD files shall be in the docs/ folder, attachments shall be put in /docs/attachments.

## Edit and Release Workflow

1. Create new **issue** and **branch**, and **merge request** for a documentation update
2. Edit the **markdown** files in the newly created branch and commit as you like
    - The HTML content will be automatically created and deployed as GitLab Pages <https://dariah-de.pages.gwdg.de/textgrid-documentation/>
3. Check merge request and merge to main
4. If **release** due to semantic release (*fix*, *feat*):
      - A new **tag** will be created (according to semantic release rules)
      - A docker image is being build and put into the GitLab container Registry <https://gitlab.gwdg.de/dariah-de/textgrid-documentation/container_registry>
5. To deploy the content to <https://doc.textgrid.de> please use the SUB K8S workflows.

## License

This documentation is licensed under CC-BY-4.0 <https://creativecommons.org/licenses/by/4.0/>.

## Migration from TextGrid Public Wiki

### Things Done for (more or less) Proper Migration

- [x] (i) export HTML from Confluence space or (ii) get HTML via *wget* or *curl*, **I did (i) for this space**!
- [x] convert all HTML files using THIS SCRIPT:

``` bash
  #!/bin/bash

  SRC=TextGrid

  for I in $SRC/*; do
    xmllint --html --xpath "//div[@id='page']" $I | pandoc -f html -t gfm-raw_html > $I.md;
  done
```

- [x] process generated MD files by hand (see generated `.html.md` files and processed `.md` files)
- [x] convert tables using `xmllint --html --xpath "//div[@id='content']" BLUBB.html | pandoc -f html -t markdown > BLUBB.md;`, tables are being created, but it **is not working at all :-(**
- [x] extract table HTML for each HTML table and use: <https://tableconvert.com/html-to-markdown>
- [x] process references in tables by hand!
- [x] Set up MD2HTML local generation using mkdocs
- [x] Set up GitLab CI for mkdocs generation
- [x] Use GitLab Pages for first deployment
- [x] Set up nginx docker container to deliver documentation
- [x] Set up automatic CI/CD
- [x] Lizenz!
- [x] Set up doc.textgrid.de as a subdomain for this TextGrid Public Wiki content
- [x] Set up K8S deployment
